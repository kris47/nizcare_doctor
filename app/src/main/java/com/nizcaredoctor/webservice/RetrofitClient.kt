package com.nizcaredoctor.webservice

import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object RetrofitClient {

    private var retrofitClient: API

    init {
        val retrofit = initRetrofitClient()
        retrofitClient = retrofit.create(API::class.java)
    }

    fun getApi(): API = retrofitClient

    private fun initRetrofitClient(): Retrofit {
        val client = OkHttpClient.Builder()
                .connectTimeout(30, TimeUnit.SECONDS)
                .addInterceptor(getInterceptor())
                .readTimeout(30, TimeUnit.SECONDS)
                .build()

        val gson = GsonBuilder()
                .setLenient()
                .create()

        return Retrofit.Builder()
                .baseUrl(WebConstants.LIVE_PATH)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build()
    }

    fun getInterceptor(): HttpLoggingInterceptor {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        return interceptor
    }
}