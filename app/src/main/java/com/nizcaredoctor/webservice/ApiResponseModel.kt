package com.nizcaredoctor.webservice

class ApiResponseModel<T> {
    val statusCode: Int? = null
    val msg: String? = null
    val success: Int? = null
    val data: T? = null
}