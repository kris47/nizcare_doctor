package com.nizcaredoctor.webservice.model

data class Profile(
        val firstname: String?,
        val lastname: String?,
        val wallet_balance: String?,
        val is_blocked: String?,
        val is_online: Int?,
        val is_subscribed: Int?,
        val phone_no: String?,
        val is_verified: Int?,
        val signup_step: Int?,
        val email: String?,
        val image: String?,
        val country_id: Int?,
        val phone_code: String?,
        val user_role_id: Int?
)
