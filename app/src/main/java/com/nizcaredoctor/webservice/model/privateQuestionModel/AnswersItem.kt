package com.nizcaredoctor.webservice.model.privateQuestionModel

data class AnswersItem(
        val attachment: Any? ,
        val user_id: Int? ,
        val created_at: String?,
        val id: Int? ,
        val text: String? ,
        val question_id: Int?
)
