package com.nizcaredoctor.webservice.model

data class CityModel(
        val cities: MutableList<GetItemModel>
)
