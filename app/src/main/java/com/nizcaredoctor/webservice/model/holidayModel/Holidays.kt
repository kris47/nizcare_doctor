package com.nizcaredoctor.webservice.model.holidayModel

data class Holidays(
    val per_page: Int?,
    val total: Int?,
    val data: List<HolidayDataItem>,
    val current_page: Int? = null
)