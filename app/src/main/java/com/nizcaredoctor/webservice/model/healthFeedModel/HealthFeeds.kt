package com.nizcaredoctor.webservice.model.healthFeedModel

data class HealthFeeds(
        val per_page: Int?,
        val total: Int,
        val data: MutableList<HealthFeedDataItem>,
        val current_page: Int?
)
