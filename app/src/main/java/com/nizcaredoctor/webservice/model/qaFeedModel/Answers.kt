package com.nizcaredoctor.webservice.model.qaFeedModel

import com.nizcaredoctor.webservice.model.User

data class Answers(
        var upvote_count: Int?,
        val attachment: String?,
        var my_vote_count: Int?,
        var bookmark_count: Int?,
        val user_id: Int?,
        val created_at: String?,
        val id: Int?,
        val text: String?,
        val question_id: Int?,
        val user: User?
)
