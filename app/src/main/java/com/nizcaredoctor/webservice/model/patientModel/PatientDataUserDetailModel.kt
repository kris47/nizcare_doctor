package com.nizcaredoctor.webservice.model.patientModel

import android.os.Parcel
import android.os.Parcelable

data class PatientDataUserDetailModel(val id: Int,
                                      val user_id: Int,
                                      val dob: String?) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readInt(),
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeInt(user_id)
        parcel.writeString(dob)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<PatientDataUserDetailModel> {
        override fun createFromParcel(parcel: Parcel): PatientDataUserDetailModel {
            return PatientDataUserDetailModel(parcel)
        }

        override fun newArray(size: Int): Array<PatientDataUserDetailModel?> {
            return arrayOfNulls(size)
        }
    }
}