package com.nizcaredoctor.webservice.model.bankModel

data class BankAccountModel(val bankAccounts: List<BankAccountItemModel>)