package com.nizcaredoctor.webservice.model

data class LanguageModelObject(val id : Int,
                               val name : String)
