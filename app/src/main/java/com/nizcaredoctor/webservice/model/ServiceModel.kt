package com.nizcaredoctor.webservice.model

import com.google.gson.annotations.SerializedName
import com.nizcaredoctor.webservice.model.GetItemModel

data class ServiceModel(
        @field:SerializedName("serviceCategories")
        val serviceCategories: MutableList<GetItemModel>
)