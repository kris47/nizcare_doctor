package com.nizcaredoctor.webservice.model

data class IdViewModel(
        val docId: Int,
        val docName: String,
        val docFile: String
)
