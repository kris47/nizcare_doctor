package com.nizcaredoctor.webservice.model.patientModel

import android.os.Parcel
import android.os.Parcelable

data class PatientDataUserModel(val id :Int,
                                val firstname : String?,
                                val lastname : String?,
                                val image : String?,
                                val gender : Int?,
                                val phone_no : String?,
                                val user_detail: PatientDataUserDetailModel?) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString(),
            parcel.readParcelable(PatientDataUserDetailModel::class.java.classLoader)) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(firstname)
        parcel.writeString(lastname)
        parcel.writeString(image)
        parcel.writeValue(gender)
        parcel.writeString(phone_no)
        parcel.writeParcelable(user_detail, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<PatientDataUserModel> {
        override fun createFromParcel(parcel: Parcel): PatientDataUserModel {
            return PatientDataUserModel(parcel)
        }

        override fun newArray(size: Int): Array<PatientDataUserModel?> {
            return arrayOfNulls(size)
        }
    }
}