package com.nizcaredoctor.webservice.model.branchModel

data class Branches(
        val per_page: Int?,
        val total: Int?,
        val data: List<ClinicDataItem>,
        val current_page: Int? = null
)
