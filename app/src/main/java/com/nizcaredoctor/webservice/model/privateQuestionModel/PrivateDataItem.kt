package com.nizcaredoctor.webservice.model.privateQuestionModel

data class PrivateDataItem(
	val question: Question? ,
	val userId: Int? ,
	val answers: List<AnswersItem>? ,
	val created_at: String? ,
	val id: Int? ,
	val question_id: Int?
)
