package com.nizcaredoctor.webservice.model

import java.io.File

data class AttachmentModel(
        val name: String,
        val fileType: String,
        val file: File?
)
