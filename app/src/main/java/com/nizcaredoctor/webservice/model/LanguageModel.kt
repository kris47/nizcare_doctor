package com.nizcaredoctor.webservice.model

import com.google.gson.annotations.SerializedName

data class LanguageModel(
        @field:SerializedName("app_languages")
        val app_languages: MutableList<AppLanguagesItem>
)
