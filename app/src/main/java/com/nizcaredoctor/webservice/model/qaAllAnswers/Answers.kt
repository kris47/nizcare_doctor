package com.nizcaredoctor.webservice.model.qaAllAnswers

data class Answers(
        val per_page: Int?,
        val total: Int?,
        val data: List<AnswerItem>?,
        val current_page: Int?
)
