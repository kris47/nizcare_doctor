package com.nizcaredoctor.webservice.model

import com.google.gson.annotations.SerializedName

data class GetItemModel(
        @field:SerializedName("name")
        val name: String,

        @field:SerializedName("id")
        val id: Int
)