package com.nizcaredoctor.webservice.model.qaAllAnswers

data class AllQAAnswersModel(
        val answers: Answers?
)
