package com.nizcaredoctor.webservice.model

data class StatesModel(
        val states: MutableList<GetItemModel>
)
