package com.nizcaredoctor.webservice.model.healthFeedModel

import com.nizcaredoctor.webservice.model.User

data class HealthFeedDataItem(
        val description: String?,
        val created_at: String?,
        var bookmark_count: Int?,
        var my_vote_count: Int?,
        val title: String?,
        var upvote_count: Int?,
        val service_provider_id: Int?,
        val attachment: Any?,
        val user_id: Int?,
        val is_draft: Int?,
        val id: Int?,
        val views_count: Int?,
        val user: User?
)
