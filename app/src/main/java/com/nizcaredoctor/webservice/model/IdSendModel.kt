package com.nizcaredoctor.webservice.model

data class IdSendModel(
        val doc_type_id: Int,
        val doc_file: String
)
