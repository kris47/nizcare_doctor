package com.nizcaredoctor.webservice.model

import com.google.gson.annotations.SerializedName
import com.nizcaredoctor.webservice.model.GetItemModel

data class DoctorTitleModel(
        @field:SerializedName("user_titles")
        val user_titles: MutableList<GetItemModel>
)
