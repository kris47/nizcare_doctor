package com.nizcaredoctor.webservice.model

data class EducationModel(
        val name_number: String,
        val org_name: String,
        val pass_year: String,
        val image: String,
        val edu_regis: String
)
