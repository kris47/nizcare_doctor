package com.nizcaredoctor.webservice.model

data class User(
        val firstname: String?,
        val gender: Int?,
        val user_detail: UserDetail?,
        val id: Int?,
        val lastname: String?,
        val image: String?,
        var user_role: GetItemModel,
        var user_title: GetItemModel
)
