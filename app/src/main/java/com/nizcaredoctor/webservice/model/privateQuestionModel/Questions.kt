package com.nizcaredoctor.webservice.model.privateQuestionModel

data class Questions(
		val per_page: Int,
		val total: Int,
		val data: MutableList<PrivateDataItem>,
		val current_page: Int
)
