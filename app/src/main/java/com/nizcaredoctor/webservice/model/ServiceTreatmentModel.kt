package com.nizcaredoctor.webservice.model

data class ServiceTreatmentModel(
        val treatSymSurgeries: MutableList<GetItemModel>
)
