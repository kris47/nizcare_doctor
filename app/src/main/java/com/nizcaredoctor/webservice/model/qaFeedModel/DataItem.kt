package com.nizcaredoctor.webservice.model.qaFeedModel

import com.nizcaredoctor.webservice.model.ServiceCategory
import com.nizcaredoctor.webservice.model.User

data class DataItem(
        val service_category_id: Int?,
        val attachment: Any?,
        val user_id: Int?,
        val answers_count: Int?,
        val service_category: ServiceCategory?,
        val answers: Answers?,
        val description: String?,
        val created_at: String?,
        val id: Int?,
        val title: String?,
        val user: User?,
        val unseen: Int?
)
