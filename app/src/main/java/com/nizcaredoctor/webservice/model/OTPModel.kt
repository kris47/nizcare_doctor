package com.nizcaredoctor.webservice.model

data class OTPModel(
	val profile: Profile? ,
	val token: String?
)
