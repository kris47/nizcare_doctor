package com.nizcaredoctor.webservice.model.branchModel

import android.os.Parcel
import android.os.Parcelable

data class ClinicDataItem(
        val consultation_fees: String?,
        val address: String?,
        val name: String?,
        val id: Int?,
        val status: Int?) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int)

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(consultation_fees)
        parcel.writeString(address)
        parcel.writeString(name)
        parcel.writeValue(id)
        parcel.writeValue(status)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ClinicDataItem> {
        override fun createFromParcel(parcel: Parcel): ClinicDataItem {
            return ClinicDataItem(parcel)
        }

        override fun newArray(size: Int): Array<ClinicDataItem?> {
            return arrayOfNulls(size)
        }
    }
}
