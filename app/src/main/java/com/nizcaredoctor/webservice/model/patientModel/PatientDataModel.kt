package com.nizcaredoctor.webservice.model.patientModel

data class PatientDataModel(val current_page: Int?,
                            val data: List<PatientDataItemModel>,
                            val total: Int?)