package com.nizcaredoctor.webservice.model.subsciptionModel

data class SubsciptionItemModel(
        val id: Int,
        val online_consultation: Int,
        val instant_consultation: Int,
        val regular_consultation: Int,
        val add_upto_no_branches: Int,
        val buy_sell_package:Int,
        val validity: String,
        val plan_type: Int,
        val for_user_role: Int,
        val amount: String,
        val country_id: Int,
        val name: String,
        val description: String

)