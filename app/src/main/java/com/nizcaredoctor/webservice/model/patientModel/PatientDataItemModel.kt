package com.nizcaredoctor.webservice.model.patientModel

import android.os.Parcel
import android.os.Parcelable

data class PatientDataItemModel(val id: Int,
                                val user_id: Int,
                                val branch_id: Int,
                                val created_at: String?,
                                val user: PatientDataUserModel?) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readString(),
            parcel.readParcelable(PatientDataUserModel::class.java.classLoader)) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeInt(user_id)
        parcel.writeInt(branch_id)
        parcel.writeString(created_at)
        parcel.writeParcelable(user, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<PatientDataItemModel> {
        override fun createFromParcel(parcel: Parcel): PatientDataItemModel {
            return PatientDataItemModel(parcel)
        }

        override fun newArray(size: Int): Array<PatientDataItemModel?> {
            return arrayOfNulls(size)
        }
    }
}


