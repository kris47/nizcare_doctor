package com.nizcaredoctor.webservice.model.healthFeedModel

data class HealthFeedModel(
	val healthFeeds: HealthFeeds?
)
