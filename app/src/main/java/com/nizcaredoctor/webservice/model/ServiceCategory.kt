package com.nizcaredoctor.webservice.model

data class ServiceCategory(
        val name: String?,
        val id: Int? )
