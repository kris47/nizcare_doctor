package com.nizcaredoctor.webservice.model.qaFeedModel

data class Questions(
        val perPage: Int,
        val total: Int,
        val data: MutableList<DataItem>,
        val current_page: Int
)
