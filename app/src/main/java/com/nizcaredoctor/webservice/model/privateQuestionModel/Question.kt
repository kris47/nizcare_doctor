package com.nizcaredoctor.webservice.model.privateQuestionModel

import com.nizcaredoctor.webservice.model.ServiceCategory
import com.nizcaredoctor.webservice.model.User

data class Question(
        val serviceCategoryId: Int? = null,
        val attachment: Any? = null,
        val userId: Int? = null,
        val serviceCategory: ServiceCategory? = null,
        val description: String? = null,
        val id: Int? = null,
        val title: String? = null,
        val user: User? = null
)
