package com.nizcaredoctor.webservice.model.holidayModel

data class HolidayDataItem(val id: Int?,
                           val branch_id: Int?,
                           val date_from: String?,
                           val date_to: String?,
                           val branch: BranchItemModel?
)

data class BranchItemModel(val id: Int?,
                           val name: String?)