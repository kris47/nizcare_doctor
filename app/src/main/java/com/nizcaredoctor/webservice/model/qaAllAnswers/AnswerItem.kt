package com.nizcaredoctor.webservice.model.qaAllAnswers

import com.nizcaredoctor.webservice.model.User

data class AnswerItem(
        var upvote_count: Int?,
        val attachment: Any?,
        val user_id: Int?,
        val created_at: String?,
        val id: Int?,
        val text: String?,
        var my_vote_count: Int?,
        var bookmark_count: Int?,
        val question_id: Int?,
        val user: User?
)
