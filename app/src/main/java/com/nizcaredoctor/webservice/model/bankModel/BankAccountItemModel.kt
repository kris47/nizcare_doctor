package com.nizcaredoctor.webservice.model.bankModel

import java.io.Serializable

data class BankAccountItemModel(val id: Int,
                                val user_id: Int,
                                val account_holder_name: String?,
                                val bank_name: String?,
                                val account_number: String?,
                                val ifsc_code: String?,
                                val micr_code: String?,
                                val address: String?,
                                val type: Int?,
                                val is_default: Int?,
                                val updated_at: String?) : Serializable