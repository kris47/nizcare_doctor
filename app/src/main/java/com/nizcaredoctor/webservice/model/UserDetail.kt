package com.nizcaredoctor.webservice.model

data class UserDetail(
	val dob: String? ,
	val id: Int?
)
