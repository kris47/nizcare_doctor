package com.nizcaredoctor.webservice.model.privateQuestionModel

data class PrivateChatResponse(
	val questions: Questions? = null
)
