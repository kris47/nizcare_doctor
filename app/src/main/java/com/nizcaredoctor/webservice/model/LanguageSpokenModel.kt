package com.nizcaredoctor.webservice.model

data class LanguageSpokenModel(
        val languages: List<LanguageModelObject>?)

