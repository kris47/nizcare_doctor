package com.nizcaredoctor.webservice;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.provider.Settings;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.nizcaredoctor.R;


public class DialogPopup {

    public DialogPopup() {

    }

    public Dialog alertPopup(final Activity activity, String title, String message, final String customMessage) {
        Dialog dialog = null;
        try {
//
            dialog = new Dialog(activity, android.R.style.Theme_Translucent_NoTitleBar);
            dialog.setContentView(R.layout.dialog_popup);
            WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
            lp.dimAmount = 0.6f;
            dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);

            TextView text = dialog.findViewById(R.id.text);
            Button ok = dialog.findViewById(R.id.ok);
            Button cancel = dialog.findViewById(R.id.cancel);
            text.setText(message);

            if ("Location".equalsIgnoreCase(customMessage)) {
                cancel.setVisibility(View.VISIBLE);
            } else {
                cancel.setVisibility(View.GONE);
            }

            final Dialog finalDialog = dialog;
            cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finalDialog.dismiss();
                }
            });


            ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (customMessage.equalsIgnoreCase("Location")) {
                        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        activity.startActivity(intent);
                    } else if ("Logout".equalsIgnoreCase(customMessage)) {
/*
                        //erase data and save fcm Token after erasing data
                        Prefs prefs=Prefs.with(activity);
                        String deviceToken=prefs.getString(Constants.DEVICE_TOKEN,"");
                        prefs.removeAll();
                        prefs.save(Constants.DEVICE_TOKEN,deviceToken);
                        //end

                        Intent intent = new Intent(activity, AccountActivity.class);
                        activity.startActivity(intent);
                        activity.finish();
                        activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_in_left);*/
                    } else if ("PaymentError".equalsIgnoreCase(customMessage)) {
                        activity.finish();
                    }
                    finalDialog.dismiss();
                }
            });
            dialog.show();


        } catch (Exception e) {
            e.printStackTrace();
        }

        return dialog;
    }
}