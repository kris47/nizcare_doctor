package com.nizcaredoctor.webservice

import okhttp3.MediaType
import okhttp3.RequestBody
import java.io.File

object RetrofitUtils {
    fun stringToRequestBody(string: String): RequestBody = RequestBody.create(MediaType.parse("text/plain"), string)

    fun csvToRequestBody(imageFile: File): RequestBody = RequestBody.create(MediaType.parse("application/csv"), imageFile)

    fun imageToRequestBody(imageFile: File): RequestBody = RequestBody.create(MediaType.parse("image/*"), imageFile)

    fun imageToRequestBodyKey(parameterName: String, fileName: File): String = parameterName + "\"; filename=\"" + fileName.name
}