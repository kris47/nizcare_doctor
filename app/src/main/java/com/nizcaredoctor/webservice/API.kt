package com.nizcaredoctor.webservice

import com.nizcaredoctor.views.account.settings.clinics.addClinic.BranchDataModel
import com.nizcaredoctor.views.account.settings.clinics.addClinic.timings.timeModel.AllWorkingHoursModel
import com.nizcaredoctor.webservice.model.*
import com.nizcaredoctor.webservice.model.bankModel.BankAccountModel
import com.nizcaredoctor.webservice.model.branchModel.MyClinicModel
import com.nizcaredoctor.webservice.model.healthFeedModel.HealthFeedModel
import com.nizcaredoctor.webservice.model.holidayModel.HolidayModel
import com.nizcaredoctor.webservice.model.patientModel.PatientModel
import com.nizcaredoctor.webservice.model.privateQuestionModel.PrivateChatResponse
import com.nizcaredoctor.webservice.model.qaAllAnswers.AllQAAnswersModel
import com.nizcaredoctor.webservice.model.qaFeedModel.QAFeedModel
import com.nizcaredoctor.webservice.model.subsciptionModel.SubsciptionModel
import retrofit2.Call
import retrofit2.http.*

interface API {

    @FormUrlEncoded
    @POST("register")
    fun signUp(@FieldMap map: HashMap<String, String>): Call<ApiResponseModel<Any>>

    @GET("data/appLanguages")
    fun getAppLanguages(): Call<ApiResponseModel<LanguageModel>>

    @FormUrlEncoded
    @POST("verifyOtp")
    fun verifyOTP(@FieldMap map: HashMap<String, String>): Call<ApiResponseModel<OTPModel>>

    @GET("doctorSP/userTitles")
    fun getDoctorSPTitles(@Header("Authorization") accessToken: String): Call<ApiResponseModel<DoctorTitleModel>>

    @FormUrlEncoded
    @POST("serviceCategories")
    fun getDoctorSPServiceCategory(@Header("Authorization") accessToken: String,
                                   @Field("user_role_id") userRoleID: Int?): Call<ApiResponseModel<ServiceModel>>

    @GET("data/languages")
    fun getLanguages(): Call<ApiResponseModel<LanguageSpokenModel>>

    @FormUrlEncoded
    @POST("data/countrywiseStates")
    fun getStates(@Field("country_id") countyCode: String): Call<ApiResponseModel<StatesModel>>

    @FormUrlEncoded
    @POST("data/statewiseCities")
    fun getCities(@Field("state_id") stateCode: String?): Call<ApiResponseModel<CityModel>>

    @FormUrlEncoded
    @POST("doctorSP/documentType")
    fun getDocumentsType(@Header("Authorization") accessToken: String,
                         @Field("country_Id") countyCode: String): Call<ApiResponseModel<IdModel>>

    @FormUrlEncoded
    @POST("doctorSP/setupAccount")
    fun setUpAccount(@Header("Authorization") accessToken: String,
                     @FieldMap map: HashMap<String, String>): Call<ApiResponseModel<OTPModel>>

    @FormUrlEncoded
    @POST("login")
    fun login(@FieldMap map: HashMap<String, String>): Call<ApiResponseModel<OTPModel>>

    @FormUrlEncoded
    @POST("forgotPassword")
    fun forgotPassword(@FieldMap map: HashMap<String, String>): Call<ApiResponseModel<OTPModel>>

    @FormUrlEncoded
    @POST("forgotVerifyOtp")
    fun verifyPasswordOTP(@FieldMap map: HashMap<String, String>): Call<ApiResponseModel<OTPModel>>

    @FormUrlEncoded
    @POST("changePassword")
    fun changePassword(@Field("token") token: String,
                       @Field("password") password: String): Call<ApiResponseModel<Any>>

    @FormUrlEncoded
    @POST("questions")
    fun getQAFeed(@Header("Authorization") accessToken: String,
                  @Field("user_role_id") userRoleID: Int,
                  @Field("type") questionType: String,
                  @Field("page") pageNo: Int): Call<ApiResponseModel<QAFeedModel>>

    @FormUrlEncoded
    @POST("doctorSP/questions")
    fun getPrivateQuestions(@Header("Authorization") accessToken: String,
                            @Field("type") privatE_TYPE: String,
                            @Field("page") page: Int): Call<ApiResponseModel<PrivateChatResponse>>

    @FormUrlEncoded
    @POST("doctorSP/answerPrivateQuestion")
    fun postPrivateAnswer(@Header("Authorization") accessToken: String,
                          @Field("text") text: String,
                          @Field("question_id") question_id: Int?,
                          @Field("attachment") attachments: String): Call<ApiResponseModel<Any>>

    @FormUrlEncoded
    @POST("answers")
    fun getAllQAAnswers(@Header("Authorization") accessToken: String,
                        @Field("question_id") id: Int,
                        @Field("page") page: Int)
            : Call<ApiResponseModel<AllQAAnswersModel>>

    @FormUrlEncoded
    @POST("upvoteDownvote")
    fun upVoteQA(@Header("Authorization") accessToken: String,
                 @Field("type") answeR_TYPE: String,
                 @Field("ref_id") id: Int?,
                 @Field("status") my_vote_count: Int?): Call<ApiResponseModel<Any>>

    @FormUrlEncoded
    @POST("bookmarkUnbookmark")
    fun bookmarkQAAPI(@Header("Authorization") accessToken: String,
                      @Field("type") answeR_TYPE: String,
                      @Field("ref_id") id: Int?,
                      @Field("status") status: Int?): Call<ApiResponseModel<Any>>

    @FormUrlEncoded
    @POST("healthFeeds")
    fun getAllHealthFeeds(@Header("Authorization") accessToken: String,
                          @Field("user_role_id") userRoleID: Int,
                          @Field("type") healthFeedType: Int,
                          @Field("page") page: Int): Call<ApiResponseModel<HealthFeedModel>>

    @FormUrlEncoded
    @POST("doctorSP/createHealthFeed")
    fun createHealthFeed(@Header("Authorization") accessToken: String,
                         @FieldMap map: HashMap<String, Any>): Call<ApiResponseModel<Any>>

    @POST("doctorSP/subscriptionPlans")
    fun getSubsciptionPlan(@Header("Authorization") accessToken: String): Call<ApiResponseModel<SubsciptionModel>>

    @FormUrlEncoded
    @POST("doctorSP/subscribe")
    fun subscibreForPlan(@Header("Authorization") accessToken: String,
                         @FieldMap map: HashMap<String, Any>): Call<ApiResponseModel<Any>>

    @FormUrlEncoded
    @POST("treatSymSurgeries")
    fun getServicesForClinic(@Header("Authorization") accessToken: String,
                             @Field("search") search: String,
                             @Field("type") treatment: String): Call<ApiResponseModel<ServiceTreatmentModel>>

    @FormUrlEncoded
    @POST("doctorSP/myBranches")
    fun getMyClinics(@Header("Authorization") accessToken: String,
                     @Field("page") page: Int): Call<ApiResponseModel<MyClinicModel>>

    @FormUrlEncoded
    @POST("doctorSP/addEditBranch")
    fun addClinicAPI(@Header("Authorization") accessToken: String,
                     @FieldMap map: HashMap<String, String>): Call<ApiResponseModel<Any>>

    @FormUrlEncoded
    @POST("doctorSP/branch")
    fun getBranchDetail(@Header("Authorization") accessToken: String,
                        @Field("branch_id") id: Int?): Call<ApiResponseModel<BranchDataModel>>

    @FormUrlEncoded
    @POST("doctorSP/addEditBranchTreatments")
    fun addServicesForClinic(@Header("Authorization") accessToken: String,
                             @Field("treatments") ids: String?,
                             @Field("branch_id") branchId: Int?): Call<ApiResponseModel<Any>>

    @FormUrlEncoded
    @POST("doctorSP/addEditWorkingHours")
    fun addClinicTimings(@Header("Authorization") accessToken: String,
                         @FieldMap map: HashMap<String, String>): Call<ApiResponseModel<Any>>

    @GET("doctorSP/allWorkingHours")
    fun getAllBranchTimings(): Call<ApiResponseModel<AllWorkingHoursModel>>
//

    @FormUrlEncoded
    @POST("doctorSP/removeBranch")
    fun removeBranch(@Header("Authorization") accessToken: String,
                     @FieldMap map: HashMap<String, String>): Call<ApiResponseModel<Any>>

    @FormUrlEncoded
    @POST("doctorSP/addEditHolidays")
    fun addEditHolidays(@Header("Authorization") accessToken: String,
                        @FieldMap map: HashMap<String, String>): Call<ApiResponseModel<Any>>

    @FormUrlEncoded
    @POST("doctorSP/branchHolidays")
    fun getHolidays(@Header("Authorization") accessToken: String,
                    @FieldMap map: HashMap<String, String>): Call<ApiResponseModel<HolidayModel>>

    @FormUrlEncoded
    @POST("doctorSP/removeHoliday")
    fun removeHoliday(@Header("Authorization") accessToken: String,
                      @Field("holiday_id") holiday_id: String): Call<ApiResponseModel<Any>>

    @FormUrlEncoded
    @POST("doctorSP/addEditBankAccount")
    fun addEditBankAccount(@Header("Authorization") accessToken: String,
                           @FieldMap map: HashMap<String, String>): Call<ApiResponseModel<Any>>

    @GET("doctorSP/bankAccounts")
    fun getBankAccounts(@Header("Authorization") accessToken: String): Call<ApiResponseModel<BankAccountModel>>

    @FormUrlEncoded
    @POST("doctorSP/removeBankAccount")
    fun removeBankAccount(@Header("Authorization") accessToken: String,
                          @Field("user_bank_detail_id") user_bank_detail_id: String): Call<ApiResponseModel<Any>>

// -------- PATIENTS_MODULE------------------

    @FormUrlEncoded
    @POST("doctorSP/myCustomers")
    fun getALlPatients(@Header("Authorization") accessToken: String,
                       @FieldMap map: HashMap<String, String>):
            Call<ApiResponseModel<PatientModel>>

    @FormUrlEncoded
    @POST("doctorSP/addEditSignature")
    fun addEditSignature(@Header("Authorization") accessToken: String,
                         @Field("signature") branch_id: String?): Call<ApiResponseModel<Any>>

    @FormUrlEncoded
    @POST("doctorSP/addCustomer")
    fun addCustomer(@Header("Authorization") accessToken: String,
                    @FieldMap map: HashMap<String, String>): Call<ApiResponseModel<Any>>

    @FormUrlEncoded
    @POST("doctorSP/removeCustomer")
    fun removeCustomer(@Header("Authorization") accessToken: String,
                       @Field("branch_id") branch_id: String?, @Field("user_id") user_id: String?):
            Call<ApiResponseModel<Any>>
}