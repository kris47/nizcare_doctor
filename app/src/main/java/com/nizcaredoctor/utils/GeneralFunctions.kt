package com.nizcaredoctor.utils

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.Color
import android.net.ConnectivityManager
import android.net.Uri
import android.os.Build
import android.provider.MediaStore
import android.provider.Settings
import android.support.design.widget.Snackbar
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.text.SpannableString
import android.text.format.DateUtils
import android.text.style.ForegroundColorSpan
import android.text.style.RelativeSizeSpan
import android.util.DisplayMetrics
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.google.gson.Gson
import com.nizcaredoctor.R
import kotlinx.android.synthetic.main.layout_toolbar.*
import java.io.File
import java.io.IOException
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import java.util.regex.Pattern

fun replaceFragment(fragmentManager: FragmentManager?, fragment: Fragment, tag: String) {
    fragmentManager?.beginTransaction()?.setCustomAnimations(R.anim.slide_in_right,
            R.anim.slide_out_left,
            R.anim.slide_in_left,
            R.anim.slide_out_right)?.replace(R.id.container, fragment, tag)?.commit()
}

fun addFragment(fm: FragmentManager, fragment: Fragment, container: Int, replace: Boolean, addToBackStack: Boolean, addAnimation: Boolean) {
    val fragmentTransaction = fm.beginTransaction()
    if (addAnimation)
        fragmentTransaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
    if (replace)
        fragmentTransaction.replace(container, fragment, fragment.javaClass.name)
    else
        fragmentTransaction.add(container, fragment, fragment.javaClass.name)
    if (addToBackStack)
        fragmentTransaction.addToBackStack(fragment.javaClass.name)
    fragmentTransaction.commit()
}

fun Activity.activityFinishAnimation() {
    overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
}

fun Activity.setToolbarTitle(title: String) {
    tvTitle.text = title
}

fun setProgressText(scored: Int?, total: Int?): SpannableString {
    val styledString = SpannableString("$scored\n/$total")
    // make the text twice as large
    styledString.setSpan(ForegroundColorSpan(Color.BLACK), 0, scored.toString().length, 0)
    styledString.setSpan(RelativeSizeSpan(2f), 0, scored.toString().length, 0)
    // make text bold
    styledString.setSpan(ForegroundColorSpan(Color.GRAY), scored.toString().length + 1, styledString.length, 0)
    return styledString
}

fun getProgressPercentage(total: Int, value: Int): Int {
    if (total != 0) {
        val percentage = value / total
        return percentage * 100
    }
    return 0
}

fun getDate(originalDate: String): String {
    val format1 = SimpleDateFormat("yyyy-MM-dd hh:mm:ss")
    val date = format1.parse(originalDate)
    return DateUtils.getRelativeTimeSpanString(date.time).toString()
}


fun getFormattedDate(parseFormat: String, convertFormat: String, orderDate: String?,
                     timezoneParse: TimeZone, timeZoneConverted: TimeZone): String {
    if (orderDate != null) {

        val form = SimpleDateFormat(parseFormat, Locale.getDefault())
        form.timeZone = timezoneParse
        val date: Date
        try {
            date = form.parse(orderDate)
            val form1 = SimpleDateFormat(convertFormat, Locale.getDefault())
            form.timeZone = timeZoneConverted
            return form1.format(date)
        } catch (e: Exception) {
            e.printStackTrace()
            return orderDate
        }
    }
    return ""
}

//fun replaceFragmentWithNoBack(fragmentManager: FragmentManager?, fragment: Fragment, tag: String){
//    fragmentManager?.beginTransaction()?.setCustomAnimations(R.anim.design_bottom_sheet_slide_in, R.anim.design_bottom_sheet_slide_out,
//            R.anim.design_bottom_sheet_slide_in, R.anim.design_bottom_sheet_slide_out)?.add(R.id.container, fragment, tag)?.addToBackStack(null)?.commit()
//}
fun isValidMobile(phone: String): Boolean {
    return android.util.Patterns.PHONE.matcher(phone).matches()
}

fun isValidEmail(target: String): Boolean {
    return if (target.isEmpty()) {
        false
    } else {
        android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches()
    }
}

fun View.hideKeyboard() {
    val inputMethodManager = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    inputMethodManager.hideSoftInputFromWindow(windowToken, 0)
}

fun getUniqueId(): String = ((Calendar.getInstance().timeInMillis) +
        Random().nextInt(1000) + Random().nextInt(1000) +
        Random().nextInt(1000)).toString()

//fun showToastyInfo(context: Context?, message:String){
//    if (context != null) {
//        Toasty.info(context,"Text to speech engine is initializing. Please wait...", Toast.LENGTH_SHORT).show()
//    }
//}

fun isOnline(context: Context?): Boolean {
    if (context != null) {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val netInfo = cm.activeNetworkInfo
        return netInfo != null && netInfo.isConnected
    } else
        return false
}

fun pxFromDp(context: Context?, dp: Float): Float {

    if (context != null) {
        return dp * context.resources.displayMetrics.density
    }
    return 0f
}


fun View.Keyboard() {
    val inputMethodManager = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    inputMethodManager.hideSoftInputFromWindow(windowToken, 0)
}
/*

fun View.displayApiError(errorBody: ResponseBody?, statusCode: Int, context: Activity?) {
    if (context != null)
        if (statusCode == 401) {
            Toast.makeText(context, context.getString(R.string.device_lo_in_from_other_device), Toast.LENGTH_SHORT).show()
            // context?.let { showError(it, "Sorry your account had been logged in from other device.") }
            val deviceToken = PrefsManager.get().getString(Constants.DEVICE_TOKEN, "")
            PrefsManager.get().removeAll()
            PrefsManager.get().save(Constants.DEVICE_TOKEN, deviceToken)
            context.finishAffinity()
            context.startActivity(Intent(context, SplashActivity::class.java))
        } else {
            if (errorBody != null) {
                try {
                    val jsonObject = JSONObject(errorBody.string())
                    val msg = jsonObject.getString("message")
                    this.showErrorSnack(msg, R.color.sms_success_red)
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }
        }
}
*/



fun shareImage(anImage: Bitmap, context: Context) {
    try {
        val intent = Intent(Intent.ACTION_SEND)
        val bitmapPath = MediaStore.Images.Media.insertImage(context.contentResolver, anImage, "Event invite for", "hiuhifhgifdhgid")
        val bitmapUri = Uri.parse(bitmapPath)
        intent.putExtra(Intent.EXTRA_STREAM, bitmapUri)
        intent.type = "image/*"
        context.startActivity(Intent.createChooser(intent, "Share image via..."))

    } catch (e: Exception) {
        //e.toString();
    }

}

fun validateDate(strDate: String): Boolean {
    if (strDate.trim().equals("")) {
        return true
    } else {
        /*
	     * Set preferred date format,
	     * For example MM-dd-yyyy, MM.dd.yyyy,dd.MM.yyyy etc.*/
        val sdfrmt = SimpleDateFormat("dd/MM/yyyy")
        sdfrmt.isLenient = false
        /* Create Date object
	     * parse the string into date
             */
        try {
            val javaDate = sdfrmt.parse(strDate)
            println(strDate + " is valid date format")
            val calendar = Calendar.getInstance()
            calendar.set(Calendar.HOUR_OF_DAY, 0)
            calendar.set(Calendar.MILLISECOND, 0)
            calendar.set(Calendar.SECOND, 0)
            calendar.set(Calendar.MINUTE, 0)

            if (javaDate.after(calendar.time)) {
                return false
            }
        } catch (e: ParseException) {
            println(strDate + " is Invalid Date format")
            return false
        }
        /* Date format is invalid */
        /* Return true if date format is valid */

        return true
    }
}

fun getAccessToken(): String = "Bearer " + PrefsManager.get().getString(Constants.Access_Token, "")

fun getAccessTokenWir(): String = PrefsManager.get().getString(Constants.Access_Token, "")


fun View.showSnack(msgId: String, color: Int) {
    try {
        val snackbar = Snackbar.make(this, msgId, Snackbar.LENGTH_LONG)
        snackbar.view.setBackgroundColor(color)
        snackbar.setAction(R.string.ok) { snackbar.dismiss() }
        snackbar.setActionTextColor(Color.parseColor("#FFFFFF"))
        snackbar.show()
    } catch (e: Exception) {
        e.printStackTrace()
    }
}

fun View.showErrorSnack(string: String) {
    try {
        val snackbar = Snackbar.make(this, string, Snackbar.LENGTH_LONG)
        snackbar.view.setBackgroundColor(Color.parseColor("#FFF74158"))
        snackbar.setAction(R.string.ok) { snackbar.dismiss() }
        snackbar.setActionTextColor(Color.parseColor("#FFFFFF"))
        snackbar.show()
    } catch (e: Exception) {
        e.printStackTrace()
    }
}

fun View.showErrorSnack(resId: Int) {

    try {
        val snackbar = Snackbar.make(this, resId, Snackbar.LENGTH_LONG)
        snackbar.view.setBackgroundColor(Color.parseColor("#FFF74158"))
        snackbar.setAction(R.string.ok) { snackbar.dismiss() }
        snackbar.setActionTextColor(Color.parseColor("#FFFFFF"))
        snackbar.show()
    } catch (e: Exception) {
        e.printStackTrace()
    }
}

/*fun showToastSuccess(context: Context?, message: String?) {
    context?.let { Toast.makeText(it, message.toString(), Toast.LENGTH_SHORT).show() }

}*/

fun View.showInternetError() {
    this.showErrorSnack(R.string.network_error)
}

//fun showInternetError(context: Context?) {
//    context?.let { Toast.makeText(it, context.getString(R.string.network_error), Toast.LENGTH_SHORT).show() }
//}

fun View.showServerError() {
    this.showErrorSnack(R.string.server_error)
}

fun showError(context: Context?, message: String) {
    context?.let { Toast.makeText(it, message, Toast.LENGTH_SHORT).show() }
}

fun getStaticMapImageURl(long: Float?, lat: Float?, context: Context?) = "https://maps.googleapis.com/maps/api/staticmap" +
        "?center=" + lat + "," + long +
        "&zoom=22.0&size=360x136&markers=color:red|" +
        lat + "," + long +
        "&key=" + context?.getString(R.string.google_map_api_keys)

fun Context.showMessageOKCancel(message: String) {
    android.support.v7.app.AlertDialog.Builder(this)
            .setMessage(message)
            .setPositiveButton("OK") { _, _ ->
                val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                val uri = Uri.fromParts("package", this.packageName, null)
                intent.data = uri
                (this as AppCompatActivity).startActivityForResult(intent, 100)
            }
            .setCancelable(false)
            .create()
            .show()
}

fun isValidName(text: String): Boolean {
    val regx = "^[\\p{L} .'-]+$"
    return Pattern.compile(regx).matcher(text).matches()
}

fun hasPermission(context: Context?, permissions: Array<String>): Boolean {
    if (context != null) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            permissions
                    .asSequence()
                    .filter { ActivityCompat.checkSelfPermission(context, it) != PackageManager.PERMISSION_GRANTED }
                    .forEach { return false }
        }
        return true
    }
    return false
}


fun openSettingDialog(context: Activity?) {
    if (context != null) {
        val builder: AlertDialog.Builder = AlertDialog.Builder(context)
        builder.setMessage(R.string.deniedpermission)
                .setCancelable(false)
                .setPositiveButton(R.string.gotosettings) { _, _ ->
                    val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                    val uri = Uri.fromParts("package", context.packageName, null)
                    intent.data = uri
                    context.startActivityForResult(intent, 0)
                }
                .setNegativeButton("Cancel") { dialog, _ ->
                    dialog.dismiss()
                }
        val alert = builder.create()
        alert.show()
    }
}

fun Activity.shareText() {
    val sendIntent = Intent()
    sendIntent.action = Intent.ACTION_SEND
    sendIntent.putExtra(Intent.EXTRA_TEXT, "This is the testing of nizcare app.")
    sendIntent.type = "text/plain"
    startActivity(sendIntent)
}

fun createNoMedia(path: String) {
    val file: File = if (path.endsWith("/")) {
        File(path + ".nomedia")
    } else {
        File(path + "/.nomedia")
    }

    if (!file.exists()) {
        try {
            file.createNewFile()
        } catch (e: IOException) {
            e.printStackTrace()
        }

    }
}
//
//fun showMessageOKCancel(message: String, context: Context,
//                        okListener: DialogInterface.OnClickListener) {
//    AlertDialog.Builder(context, R.style.MyDialogTheme)
//            .setMessage(message)
//            .setPositiveButton("OK", okListener)
//            .setCancelable(false)
//            .create()
//            .show()
//}

fun randomColor(): Int =
        Color.argb(255, Random().nextInt(256), Random().nextInt(256), Random().nextInt(256))


//fun Context.showPopUp(context: Context, token_expired: String) {
//    showMessageOKCancel(token_expired, context, DialogInterface.OnClickListener { _, _ ->
//        (context as AppCompatActivity).finishAffinity()
//        context.startActivity(Intent(context, MyActivity::class.java))
//    })
//}


/**
 *
 */
var gson = Gson()

fun <T> Context.getOBJFromJSON(json: String, objClass: Class<T>): T? {
    var obj: T? = null
    try {
        obj = gson.fromJson(json, objClass)
    } catch (exception: Exception) {
        exception.printStackTrace()
    }

    return obj
}

/**
 *
 * @param obj
 * @param <T>
 * @return
</T> */
fun <T> Context.getJSONFromOBJ(obj: T): String? {
    try {
        val json = gson.toJson(obj)
        return json
    } catch (exception: Exception) {
        exception.printStackTrace()
    }

    return null

}

fun Context.openLocationOnMaps(long: Float?, lat: Float?) {
    if (lat != null && long != null) {
        val geoUri = "http://maps.google.com/maps?q=loc:$lat,$long"
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(geoUri))
        intent.`package` = "com.google.android.apps.maps"
        if (intent.resolveActivity(packageManager) != null) {
            startActivity(intent)
        }
    }
}

fun Context.getFormatFromDate(date: Date, format: String): String {

    val locale = Locale.US
    val sdf = SimpleDateFormat(format, locale)
    return try {
        sdf.format(date)
    } catch (e: Exception) {
        e.printStackTrace()
        ""
    }
}

fun getDateFromString(time: String): Date {
    return SimpleDateFormat("HH:mm").parse(time)
}

fun getTimeWithAMPM(hourOfDay: Int, minute: Int): String {
    val min = if (minute.toString().length == 1) {
        "0$minute"
    } else {
        minute
    }

    return when {
        hourOfDay > 12 -> {
            (hourOfDay - 12).toString() + ":" + min + " pm"
        }
        hourOfDay == 12 -> {
            hourOfDay.toString() + ":" + min + " pm"
        }
        hourOfDay == 0 -> {
            12.toString() + ":" + min + " am"
        }

        else -> {
            hourOfDay.toString() + ":" + min + " am"
        }
    }
}

fun Context.getDaysDifference(date1: String?, date2: String?): String {
    val sdf = SimpleDateFormat("yyyy-MM-dd")
    val dateFrom = sdf.parse(date1)
    val dateTo = sdf.parse(date2)
    val difference = dateTo.time - dateFrom.time
    val convert = TimeUnit.DAYS.convert(difference, TimeUnit.MILLISECONDS)
    return convert.toString()
}

fun getDpTOpx(dp: Float): Int {
    val metrics = Resources.getSystem().displayMetrics
    val px = dp * (metrics.densityDpi / 160f)
    return Math.round(px)
}


fun getDeviceWidth(activity: Activity?): Int {
    val displayMetrics = DisplayMetrics()
    activity?.windowManager?.defaultDisplay?.getMetrics(displayMetrics)
    return displayMetrics.widthPixels
}

fun getDeviceHeight(activity: Activity?): Int {
    val displayMetrics = DisplayMetrics()
    activity?.windowManager?.defaultDisplay?.getMetrics(displayMetrics)
    return displayMetrics.heightPixels
}

/*fun openAlertDialog(title: String, message: String, positiveText: String,
                    negativeText: String, postiveClickListener: DialogInterface.OnClickListener,
                    negativeClickListener: DialogInterface.OnClickListener, context: Context?) {
    android.app.AlertDialog.Builder(context, R.style.)
            .setTitle(title)
            .setMessage(message)
            .setPositiveButton(positiveText, postiveClickListener)
            .setNegativeButton(negativeText, negativeClickListener)
            .create()
            .show()
}*/


/*
fun getPhoneContacts(activity: Activity): MutableList<PickContactModel> {
    val list = mutableListOf<PickContactModel>()
    val cr = activity.contentResolver
    val cur = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null)!!

    if (cur.count > 0) {
        while (cur.moveToNext()) {
            val id = cur.getString(
                    cur.getColumnIndex(ContactsContract.Contacts._ID))

            */
/*InputStream is =openPhoto(Long.parseLong(id),activity);
            BufferedInputStream bufferedInputStream = new BufferedInputStream(is);
            Bitmap bm=BitmapFactory.decodeStream(bufferedInputStream);*//*

            val name = cur.getString(cur.getColumnIndex(
                    ContactsContract.Contacts.DISPLAY_NAME))
            if (cur.getInt(cur.getColumnIndex(
                            ContactsContract.Contacts.HAS_PHONE_NUMBER)) > 0) {
                val pCur = cr.query(
                        ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                        ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                        arrayOf(id), null)!!
                while (pCur.moveToNext()) {
                    val phoneNo = pCur.getString(pCur.getColumnIndex(
                            ContactsContract.CommonDataKinds.Phone.NUMBER))

                    val emailIndex = pCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA)
                    val email = pCur.getString(emailIndex)

                    Log.d(TAG, "addContacts: ID: $id Name: $name Phoneno: $phoneNo")
                    val contactModel = PickContactModel(email, name, phoneNo, false)
                    if (!list.contains(contactModel))
                        list.add(contactModel)
                }
                pCur.close()
            }
        }
    }
    cur.close()
    return list
}*/
