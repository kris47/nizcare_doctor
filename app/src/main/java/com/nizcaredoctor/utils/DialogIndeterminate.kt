package com.nizcaredoctor.utils

import android.app.Dialog
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import com.nizcaredoctor.R


class DialogIndeterminate(context: Context?) {
    private var mDialog: Dialog
    private var dialogView: View

    init {
        val li = LayoutInflater.from(context)
        dialogView = li.inflate(R.layout.progress_wheel, null, false)
        mDialog = Dialog(context, R.style.NewDialog)
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        mDialog.setContentView(dialogView)
        mDialog.setCancelable(false)
    }

    fun show() {
        try {
            mDialog.show()

        } catch (e: Exception) {

        }

    }

    fun dismiss() {
        try {
            mDialog.dismiss()
        } catch (e: Exception) {
        }

    }
}