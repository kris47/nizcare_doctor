package com.nizcaredoctor.utils

import android.app.Application

class MyApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        PrefsManager.initialize(this)
    }
}