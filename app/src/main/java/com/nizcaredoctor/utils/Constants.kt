package com.nizcaredoctor.utils

class Constants {

    companion object {

        val Access_Token = "accessToken"
        val APPOINTMENT = 0
        val CONSULTATION = 1
        val VISIT = 2
        val PRESCRIPTION = 3
        val LAB_RECORDS = 4
        val RECORDS = 5
        val PUBLIC_CHAT = "pubicChat"
        val PRIVATE_CHAT = "privateChat"
        val LOCALE = "locale"
        val DOCTOR = 4
        val MOBILE = 0
        val APPLANGUAGEID = "APPLANGUAGEID"
        val EMAIL = "email"
        val USER_DETAIL = "userDetail"
        val States = "states"
        val Cities = "cities"
        val ListType = "listType"
        val SELECTED_POSITION = "selectedPosition"
        val SELECTED_STATE_ID = "selectedStateID"
        val EDUCATION = "0"
        val REGISTRATION = "1"
        val USER_FULL_DETAIL = "userSignUp"
        val SIGNUP = "signUp"
        val VERIFICATIONTYPE = "verificationType"
        val FORGOTPASSWORD = "forgotPassword"
        val PRIVATE_TYPE = "2"
        val ANSWER_TYPE = "1"
        val DOCTOR_SERVICE_ID = "1"
        val ALL_HEALTH_FEEDS = 2
        val DRAFT = 1
        val NOT_DRAFT = 0
        val HEALTH_FEED_TYPE = "2"
        val PDF = "pdf"
        val IMAGE = "image"
        val ALL_SERVICE = "1"
        val SELECTED_SERVICE = "2"
        val ADD = "add"
        val EDIT = "edit"
        val CLINIC_DATA = "clinicData"
        val HOLIDAY_DATA = "holiday_data"
        val TYPE = "type"
        val BRANCH_LIST = "branch_List"
        val BANK_DATA = "bank_data"
        val PATIENT_INFO = "patient_info"
        val BRANCH_ID = "branch_id"
        val SIMPLE_DATE_FORMAT = "yyyy-MM-dd"
    }
}