package com.nizcaredoctor.utils;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.View;

import com.nizcaredoctor.R;

public class MySpannable extends ClickableSpan {

    private boolean isUnderLined = false;
    private Context context;

    public MySpannable(Context context, boolean isUnderLined) {
        this.isUnderLined = isUnderLined;
        this.context = context;
    }

    @Override
    public void updateDrawState(TextPaint ds) {
        super.updateDrawState(ds);
        ds.setUnderlineText(isUnderLined);
        ds.setColor(ContextCompat.getColor(context, R.color.colorAccent));
    }

    @Override
    public void onClick(View widget) {

    }


}
