package com.nizcaredoctor.utils;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.nizcaredoctor.R;


public class NoDataView extends LinearLayout {

    TextView tvNoData, tvSubTitle;
    ImageView ivRefresh;
    LottieAnimationView ivIcon;

    public NoDataView(Context context) {
        this(context, null);
    }

    public NoDataView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public NoDataView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        init();
    }

    private void init() {
        inflate(getContext(), R.layout.item_no_data_found, this);
        //Get references to text views
        tvNoData = findViewById(R.id.tvNoData);
        ivRefresh = findViewById(R.id.ivRefresh);
        tvSubTitle = findViewById(R.id.tvSubTitle);
        ivIcon = findViewById(R.id.ivIcon);
    }

    public void setData(String data, String subTitle) {
        tvNoData.setText(data);
        tvSubTitle.setText(subTitle);
    }

    public void setImage(int image) {
        ivIcon.setAnimation(image);
    }
}