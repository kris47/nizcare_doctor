package com.nizcaredoctor.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.util.Log;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class ClickImage {
    private static final String TAG = ImagePicker.class.getSimpleName();
    private static final int REQUEST_CODE_ASK_PERMISSIONS = 123;
    private static final int REQUEST_CODE_CAMERA = 47;
    private static final int REQUEST_CODE_GALLERY_IMAGE = 48;
    private static final int REQUEST_CODE_PDF = 49;
    private boolean isDocumentEnabled = false;
    String title = "";

    private File imageFile;

    /**
     * Activity object that will be used while calling startActivityForResult(). Activity then will
     * receive the callbacks to its own onActivityResult() and is responsible of calling the
     * onActivityResult() of the ImagePicker for handling result and being notified.
     */
    private Activity context;

    /**
     * Fragment object that will be used while calling startActivityForResult(). Fragment then will
     * receive the callbacks to its own onActivityResult() and is responsible of calling the
     * onActivityResult() of the ImagePicker for handling result and being notified.
     */
    private Fragment fragment;
    private ClickImage.ImagePickerListener imagePickerListener;

    public ClickImage(@NonNull Fragment fragment) {
        this.fragment = fragment;
        this.context = fragment.getActivity();
        title = "Change Picture";
    }

    public void invokeCamera(boolean isCamera){
        if (isCamera) {
            openCamera();
        } else {
            openGallery();
        }
    }

    public void setImagePickerListener(@NonNull ClickImage.ImagePickerListener imagePickerListener) {
        this.imagePickerListener = imagePickerListener;
    }

    @Nullable
    public File getImageFile() throws NullPointerException {
        return imageFile;
    }

    public void setImageFile(@NonNull File imageFile) {
        this.imageFile = imageFile;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if ((requestCode == REQUEST_CODE_GALLERY_IMAGE) && (resultCode == Activity.RESULT_OK)) {
            String imagePath = getImagePathFromGallery(context, data.getData());
            if (imagePath != null) {
                Log.d(TAG, "Gallery image selected");
                imageFile = new File(imagePath);
                imagePickerListener.onImageSelectedFromPicker(imageFile);
            }
        } else if ((requestCode == REQUEST_CODE_CAMERA) && (resultCode == Activity.RESULT_OK)) {
            Log.d(TAG, "Image selected from camera");
            if (imageFile != null) {
                imagePickerListener.onImageSelectedFromPicker(imageFile);
                revokeUriPermission();
            }
        }
    }

    /**
     * Save the image to device external cache
     */
    private void openCamera() {
        checkListener();
        File imageDirectory = context.getExternalCacheDir();

        if (imageDirectory != null)
            startCameraIntent(imageDirectory.getAbsolutePath());
        else
            Log.d(TAG, "External cache directory is null");
    }

    /**
     * Save the image to a custom directory
     */
    private void openCamera(@NonNull final String imageDirectory) {
        checkListener();
        startCameraIntent(imageDirectory);
    }

    private void startCameraIntent(@NonNull final String imageDirectory) {
        try {
            imageFile = createImageFile(imageDirectory);
            if (fragment == null)
                context.startActivityForResult(getCameraIntent(), REQUEST_CODE_CAMERA);
            else
                fragment.startActivityForResult(getCameraIntent(), REQUEST_CODE_CAMERA);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Nullable
    public static File createImageFile(@NonNull final String directory) throws IOException {
        File imageFile = null;
        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            File storageDir = new File(directory);
            if (!storageDir.mkdirs()) {
                if (!storageDir.exists()) {
                    return null;
                }
            }
            String imageFileName = "IMG_" + System.currentTimeMillis() + "_";

            imageFile = File.createTempFile(imageFileName, ".jpg", storageDir);
        }
        return imageFile;
    }

    /**
     * Returns the camera intent using FileProvider to avoid the FileUriExposedException in Android N and above
     */
    private Intent getCameraIntent() {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        // Put the uri of the image file as intent extra
        Uri imageUri = FileProvider.getUriForFile(context,
                context.getPackageName() + ".provider",
                imageFile);

        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);

        // Get a list of all the camera apps
        List<ResolveInfo> resolvedIntentActivities =
                context.getPackageManager()
                        .queryIntentActivities(cameraIntent, PackageManager.MATCH_DEFAULT_ONLY);

        // Grant uri read/write permissions to the camera apps
        for (ResolveInfo resolvedIntentInfo : resolvedIntentActivities) {
            String packageName = resolvedIntentInfo.activityInfo.packageName;

            context.grantUriPermission(packageName, imageUri,
                    Intent.FLAG_GRANT_WRITE_URI_PERMISSION |
                            Intent.FLAG_GRANT_READ_URI_PERMISSION);
        }

        return cameraIntent;
    }

    private void openGallery() {
        checkListener();

        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        if (fragment == null)
            context.startActivityForResult(intent, REQUEST_CODE_GALLERY_IMAGE);
        else
            fragment.startActivityForResult(intent, REQUEST_CODE_GALLERY_IMAGE);
    }

    private String getImagePathFromGallery(@NonNull final Context context, @NonNull final Uri imageUri) {
        String imagePath = null;
        String[] filePathColumn = {MediaStore.Images.Media.DATA};
        Cursor cursor = context.getContentResolver().query(imageUri, filePathColumn, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            imagePath = cursor.getString(columnIndex);
            cursor.close();
        }
        return imagePath;
    }

    /**
     * Revoke access permission for the content URI to the specified package otherwise the permission won't be
     * revoked until the device restarts.
     */
    private void revokeUriPermission() {
        Log.i(TAG, "Uri permission revoked");
        context.revokeUriPermission(FileProvider.getUriForFile(context,
                context.getPackageName() + ".provider", imageFile),
                Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
    }

    private void checkListener() {
        if (imagePickerListener == null) {
            throw new RuntimeException("ImagePickerListener must be set before calling openCamera() or openGallery()");
        }
    }

    public interface ImagePickerListener {
        void onImageSelectedFromPicker(File imageFile);
    }
}
