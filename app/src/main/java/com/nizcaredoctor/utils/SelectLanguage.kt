package com.nizcaredoctor.utils

import android.content.Context
import android.content.DialogInterface
import android.support.design.widget.TextInputEditText
import android.support.v7.app.AlertDialog
import com.nizcaredoctor.R
import com.nizcaredoctor.webservice.model.LanguageModelObject

class SelectLanguage {

    private val selectedLanguagesList = mutableListOf<String>()
    private val languageSpokenId = mutableListOf<Int>()

    fun selectLanguage(context: Context?, languageSpokenList: MutableList<String>, selectedLanguages: BooleanArray,
                       languageModelObject: MutableList<LanguageModelObject>, editText: TextInputEditText): MutableList<Int> {
        val languages = Array(languageSpokenList.size, { i -> languageSpokenList[i] })
        val builder = AlertDialog.Builder(context!!)
        builder.setTitle("Select your preferred language")
                .setCancelable(false)
                .setMultiChoiceItems(languages, selectedLanguages,
                        DialogInterface.OnMultiChoiceClickListener { _, which, isChecked ->
                            selectedLanguages[which] != isChecked
                        })
                .setPositiveButton(R.string.ok, DialogInterface.OnClickListener { dialog, which ->
                    var text = ""
                    selectedLanguagesList.clear()
                    for (i in 0 until languageSpokenList.size) {
                        if (selectedLanguages[i]) {
                            selectedLanguagesList.add(languageSpokenList[i])
                            languageSpokenId.add(languageModelObject.get(i).id)
                            if (text.isNotEmpty()) {
                                text += ", "
                            }
                            text += languageSpokenList[i]
                        }
                    }
                    editText.setText(text)
                    dialog.dismiss()
                })
        builder.create().show()
        return languageSpokenId
    }
}