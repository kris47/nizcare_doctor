package com.nizcaredoctor.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ExifInterface;
import android.net.Uri;
import android.util.Log;

import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by RISHI PAUL SHARMA on 14/3/17.
 * CODE BREW LABS
 * rishipaul@code-brew@gmail.com
 */
public class AmazoneS3 {

    private Context context;

    private AmazonS3 s3;
    private TransferUtility transferUtility;
    private static final String S3_BUCKET = "nizcare";
    private static final String S3_SERVER = "https://s3.ap-south-1.amazonaws.com/" + S3_BUCKET + "/";
    private static String fileName = "", fileNameThumb;
    private static final int THUMBSIZE = 300;

    public AmazoneS3(Context context) {
        this.context = context;
        init();
    }

    private void init() {
        // callback method to call credentialsProvider method.
        credentialsProvider();
        // callback method to call the setTransferUtility method
        setTransferUtility();
    }

    public AmazonS3 getS3() {
        return s3;
    }


    /*AWS photos upload*/
    public void credentialsProvider() {
        // Initialize the Amazon Cognito credentials provider
        CognitoCachingCredentialsProvider credentialsProvider = new CognitoCachingCredentialsProvider(
                context,
                "ap-south-1:4590546f-261f-4e08-b508-7d56a87f1d68", // Identity pool ID
                Regions.AP_SOUTH_1// Region
        );

        setAmazonS3Client(credentialsProvider);
    }

    /**
     * Create a AmazonS3Client constructor and pass the credentialsProvider.
     *
     * @param credentialsProvider
     */
    public void setAmazonS3Client(CognitoCachingCredentialsProvider credentialsProvider) {

        // Create an S3 client
        s3 = new AmazonS3Client(credentialsProvider);

        // Set the region of your S3 bucket
        s3.setRegion(Region.getRegion(Regions.AP_SOUTH_1));
    }

    public void setTransferUtility() {
        transferUtility = new TransferUtility(s3, context);
    }

    public void setTransferUtility(TransferUtility transferUtility) {
        this.transferUtility = transferUtility;
    }

    /**
     * This method is used to upload the file to S3 by using TransferUtility class
     */
    public String setFileToUpload(File file) {
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy_hh-mm-ss-SSS");
            fileName = simpleDateFormat.format(new Date()) + ".jpg";
            TransferObserver transferObserver1 = transferUtility.upload(
                    S3_BUCKET,              //The bucket to upload to
                    fileName,
                    file//The key for the uploaded object
            );
            transferObserverListener(transferObserver1);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return S3_SERVER + fileName;
    }

    public String setPDFFileToUpload(File file, String displayName) {
        String time = "";
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy_hh-mm-ss-SSS");
            time = simpleDateFormat.format(new Date());
            TransferObserver transferObserver1 = transferUtility.upload(
                    S3_BUCKET,              //The bucket to upload to
                    time + ".pdf",
                    file//The key for the uploaded object
            );
            transferObserverListener(transferObserver1);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return S3_SERVER + time + ".pdf";
    }


    public int uploadFile(File file, String fileName, TransferListener transferListener) {
        TransferObserver transferObserver1 = null;
        try {
            transferObserver1 = transferUtility.upload(
                    S3_BUCKET,              //The bucket to upload to
                    fileName,
                    file//The key for the uploaded object
            );
            transferObserver1.setTransferListener(transferListener);
            return transferObserver1.getId();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public String getS3Server() {
        return S3_SERVER;
    }

    public String setVideoFileToUpload(File file) {
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy_hh-mm-ss-SSS");
            fileName = "vid_" + simpleDateFormat.format(new Date()) + ".mp4";
           /* fileNameThumb = simpleDateFormat.format(new Date()) + "_thumb.jpg";

            Bitmap thumbImage = ThumbnailUtils.extractThumbnail(BitmapFactory.decodeFile(Constants.CAMERA_IMAGE_FILE.getAbsolutePath()),
                    THUMBSIZE, THUMBSIZE);

            if (getCameraPhotoOrientation(Constants.CAMERA_IMAGE_FILE.getAbsolutePath()) > 0) {
                BitmapFactory.Options bmOptions = new BitmapFactory.Options();
                Bitmap bitmap = BitmapFactory.decodeFile(Constants.CAMERA_IMAGE_FILE.getAbsolutePath(), bmOptions);

                Matrix matrix = new Matrix();
                matrix.postRotate(getCameraPhotoOrientation(Constants.CAMERA_IMAGE_FILE.getAbsolutePath()));
                Bitmap bitmapNew = Bitmap.createBitmap(bitmap, 0, 0, bmOptions.outWidth, bmOptions.outHeight, matrix, false);

                Constants.CAMERA_IMAGE_FILE = persistImage(bitmapNew, fileName);
            }*/
//
            TransferObserver transferObserver1 = transferUtility.upload(
                    S3_BUCKET,              //The bucket to upload to
                    fileName,
                    file//The key for the uploaded object
            );
            transferObserverListener(transferObserver1);

           /* TransferObserver transferObserver = transferUtility.upload(
                    S3_BUCKET,          //The bucket to upload to
                    fileNameThumb,      //The key for the uploaded object
                    persistImage(thumbImage, fileNameThumb)                 //The file where the data to upload exists

            );
            transferObserverListener(transferObserver);*/

        } catch (Exception e) {
            e.printStackTrace();
        }
        return S3_SERVER + fileName;
    }

    /**
     * This is listener method of the TransferObserver
     * Within this listener method, we got status of uploading and downloading file,
     * to diaplay percentage of the part of file to be uploaded or downloaded to S3
     * It display error, when there is problem to upload and download file to S3.
     *
     * @param transferObserver
     */

    public void transferObserverListener(TransferObserver transferObserver) {

        transferObserver.setTransferListener(new TransferListener() {

            @Override
            public void onStateChanged(int id, TransferState state) {

            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                try {
                    int percentage = (int) (bytesCurrent / bytesTotal * 100);
                    Log.e("percentage", percentage + "");
                } catch (Exception e) {
                }

            }

            @Override
            public void onError(int id, Exception ex) {
                Log.e("error", "error");
            }

        });
    }


    public File persistImage(Bitmap bitmap, String name) {
        File filesDir = context.getFilesDir();
        File imageFile = new File(filesDir, name + ".jpg");

        OutputStream os;
        try {
            os = new FileOutputStream(imageFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);
            os.flush();
            os.close();
        } catch (Exception e) {
            Log.e(getClass().getSimpleName(), "Error writing bitmap", e);
        }
        return imageFile;
    }

    public File persistImages(File file) {

        BitmapFactory.Options bmOptions = new BitmapFactory.Options();

        bmOptions.inJustDecodeBounds = true;

        BitmapFactory.decodeFile(file.getAbsolutePath(), bmOptions);

        Bitmap bitmap;

        bmOptions.inSampleSize = calculateInSampleSize(bmOptions, 100, 100);

        bmOptions.inJustDecodeBounds = false;

        bitmap = BitmapFactory.decodeFile(file.getAbsolutePath(), bmOptions);

        OutputStream os;


        File f = new File(context.getCacheDir(), Calendar.getInstance().getTimeInMillis() + "");
        try {
            os = new FileOutputStream(f);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);
            os.flush();
            os.close();
        } catch (Exception e) {
            Log.e(getClass().getSimpleName(), "Error writing bitmap", e);
        }
        return f;
    }


    public static Bitmap decodeSampledBitmapFromResource(File file) {

        // First decode with inJustDecodeBounds=true to check dimensions
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();

        bmOptions.inJustDecodeBounds = true;

        BitmapFactory.decodeFile(file.getAbsolutePath(), bmOptions);

        bmOptions.inSampleSize = calculateInSampleSize(bmOptions, 100, 100);

        bmOptions.inJustDecodeBounds = false;

        return BitmapFactory.decodeFile(file.getAbsolutePath(), bmOptions);
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }


    private int getCameraPhotoOrientation(String imagePath) {
        int rotate = 0;
        try {
            context.getContentResolver().notifyChange(Uri.parse(imagePath), null);
            File imageFile = new File(imagePath);
            ExifInterface exif = new ExifInterface(imageFile.getAbsolutePath());
            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_UNDEFINED);
            switch (orientation) {
                case ExifInterface.ORIENTATION_NORMAL:
                    rotate = 0;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotate = 270;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotate = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotate = 90;
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rotate;
    }

    public interface MediaUploadStatus {
        int NOT_UPLOADING = 0;
        int UPLOADING = 1;
        int UPLOADED = 2;
        int CANCELLED = 3;
    }

}