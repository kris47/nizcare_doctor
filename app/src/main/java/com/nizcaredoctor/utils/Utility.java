package com.nizcaredoctor.utils;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.Toast;

import com.nizcaredoctor.BuildConfig;
import com.nizcaredoctor.R;
import com.yalantis.ucrop.UCrop;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class Utility {
    private static final String TAKEPHOTO = "Take photo";
    private static final String GALLERY = "Open Gallery";
    private static final String CANCEL = "Cancel";
    public static final String IMAGES_PREFIX = "IMG_";
    public static final String IMAGES_SUFFIX = ".jpg";
    private static final int PHOTO_REQUEST_CROP = 49;
    private static final String PATH = "/nizcaredoctor";
    private static final int PHOTO_REQUEST_CAMERA = 46;
    private static final int PHOTO_REQUEST_GALLERY = 48;
    private static String IMAGENAME = "imageName";
    private Fragment frag;
    private Activity act;
    private String campath;
    private PassValues passValues;

    public Utility(Activity activity) {
        this.act = activity;
        this.passValues = (PassValues) activity;
    }


    public Utility(Fragment fragment, Activity activity) {
        this.act = activity;
        passValues = (PassValues) fragment;
        frag = fragment;
    }


        public void selectImage() {
        final String[] items = {TAKEPHOTO, GALLERY, CANCEL};
        AlertDialog.Builder builder = new AlertDialog.Builder(act);
        builder.setTitle("Add Photo");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                switch (items[item]) {
                    case TAKEPHOTO:
                        camera();
                        break;
                    case GALLERY:
                        gallery();
                        break;
                    case CANCEL:
                        dialog.dismiss();
                        break;
                }
            }
        });
        builder.show();
    }

    public void camera() {
        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        File dir = new File(Environment.getExternalStorageDirectory(), PATH);
        campath = "IMG_" + new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date()) + ".jpg";
        IMAGENAME = campath;
        if (!dir.exists())
            dir.mkdirs();

        Uri photoURI = FileProvider.getUriForFile(act, BuildConfig.APPLICATION_ID + ".provider",
                new File(dir.getAbsolutePath(), campath));
        intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
        if (frag != null) {
            frag.startActivityForResult(intent, PHOTO_REQUEST_CAMERA);
        } else {
            act.startActivityForResult(intent, PHOTO_REQUEST_CAMERA);
        }
    }

    private void gallery() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        if (frag != null) {
            frag.startActivityForResult(intent, PHOTO_REQUEST_GALLERY);
        } else {
            act.startActivityForResult(intent, PHOTO_REQUEST_GALLERY);
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        File dir = new File(Environment.getExternalStorageDirectory(), PATH);
        if (!dir.exists())
            dir.mkdirs();
        switch (requestCode) {

            case PHOTO_REQUEST_GALLERY:

                if (data != null && resultCode == Activity.RESULT_OK) {
                    Log.d("authority", data.getData().getAuthority() + "");
                    campath = "IMG_" + new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date()) + ".jpg";
                    IMAGENAME = campath;
                    File destFile = new File(dir.getAbsolutePath(), campath);
                    if (data.getData().getAuthority().equals("com.google.android.apps.photos.contentprovider")) {
                        File image = null;
                        try {
                            image = new File(dir, IMAGES_PREFIX + Calendar.getInstance().getTimeInMillis()
                                    + IMAGES_SUFFIX);
                            FileOutputStream fOut = new FileOutputStream(image);
                            Bitmap googleBitmap = getBitmapFromUri(act, data.getData());
                            googleBitmap.compress(Bitmap.CompressFormat.JPEG, 100, fOut);
                            fOut.flush();
                            fOut.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        try {
                            copyFile(image, destFile);
                            Uri photoURI = Uri.parse("file://" + destFile.getAbsolutePath());
/*
                        Uri photoURI = FileProvider.getUriForFile(act, BuildConfig.APPLICATION_ID + ".provider", destFile);
*/
                            customCrop(photoURI);
                            //passValues.passImageURI(destFile,photoURI);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    } else {
                        File sourceFile = new File(getRealPathFromURI(data.getData()));
                        try {
                            copyFile(sourceFile, destFile);
                            Uri photoURI = Uri.parse("file://" + destFile.getAbsolutePath());
/*
                        Uri photoURI = FileProvider.getUriForFile(act, BuildConfig.APPLICATION_ID + ".provider", destFile);
*/
                            customCrop(photoURI);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                }
                break;
            case PHOTO_REQUEST_CAMERA:
                if (resultCode == Activity.RESULT_OK) {
                    if (IMAGENAME != null) {
                        File tempFile = new File(dir.getAbsolutePath(), IMAGENAME);
                        Uri uri = Uri.parse("file://" + tempFile.getAbsolutePath());
                        //  Uri photoURI = FileProvider.getUriForFile(act, BuildConfig.APPLICATION_ID + ".provider", tempFile);
                        //todo call utility function
                        customCrop(uri);
                        // passValues.passImageURI(tempFile,photoURI);
                    }
                }
                break;

            case PHOTO_REQUEST_CROP:
                if (resultCode == Activity.RESULT_OK) {
                    try {
                        if (data != null) {
                            File file = new File(dir.getAbsolutePath(), IMAGENAME);
                            Uri uri = Uri.parse("file://" + file.getAbsolutePath());
                            Log.d("URI", "onActivityResult: " + uri);
                            passValues.passImageURI(file, uri);
                        } else {
                            Log.d("", "error");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (resultCode == Activity.RESULT_CANCELED) {
                    File tempFile = new File(dir.getAbsolutePath(), IMAGENAME);
                    tempFile.delete();
                    Toast.makeText(act, "" + act.getString(R.string.cancel), Toast.LENGTH_SHORT).show();
//                    passValues.passImageURI(null);
                }
                break;
            case UCrop.REQUEST_CROP:
                if (resultCode == Activity.RESULT_OK) {
                    final Uri resultUri = UCrop.getOutput(data);
                    File file = new File(dir.getAbsolutePath(), IMAGENAME);
                    Log.d("URI", "onActivityResult: " + resultUri);
                    passValues.passImageURI(file, resultUri);
                }
                break;

            default:
                break;
        }
    }

    private void customCrop(Uri photoURI) {
        try {
            UCrop.Options options = new UCrop.Options();
            options.setCropFrameColor(ContextCompat.getColor(act, R.color.colorPrimary));
            options.setShowCropGrid(false);
            options.setToolbarColor(ContextCompat.getColor(act, R.color.colorPrimary));
            options.setHideBottomControls(true);
            if (frag != null) {
                UCrop.of(photoURI, photoURI)
                        .withAspectRatio(12, 12)
                        .withMaxResultSize(600, 600)
                        .withOptions(options)
                        .start(act, frag);
            } else {
                UCrop.of(photoURI, photoURI)
                        .withAspectRatio(12, 12)
                        .withMaxResultSize(600, 600)
                        .withOptions(options)
                        .start(act);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Bitmap getBitmapFromUri(Activity activity, Uri uri) throws IOException {
        ParcelFileDescriptor parcelFileDescriptor =
                activity.getContentResolver().openFileDescriptor(uri, "r");
        FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
        Bitmap image = BitmapFactory.decodeFileDescriptor(fileDescriptor);
        parcelFileDescriptor.close();
        return image;
    }

    private void copyFile(File sourceFile, File destFile) throws IOException {
        if (!sourceFile.exists()) {
            return;
        }
        FileChannel source = new FileInputStream(sourceFile).getChannel();
        FileChannel destination = new FileOutputStream(destFile).getChannel();
        if (source != null) {
            destination.transferFrom(source, 0, source.size());
        }
        if (source != null) {
            source.close();
        }
        destination.close();
    }

//    private void saveBitmap(Bitmap bitmap) {
//        File dir = new File(Constants.LOCAL_STORAGE_PATH_FOR_PHOTOS);
//        if (!dir.exists())
//            dir.mkdirs();
//        try {
//            File image = new File(dir, Constants.IMAGES_PREFIX + Calendar.getInstance().getTimeInMillis()
//                    + Constants.IMAGES_SUFFIX);
//            FileOutputStream fOut = new FileOutputStream(image);
//            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fOut);
//            fOut.flush();
//            fOut.close();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }

    private String getRealPathFromURI(Uri contentURI) {
        Cursor cursor = act.getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) {
            return contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            String s = cursor.getString(idx);
            cursor.close();
            return s;
        }
    }

    public interface PassValues {
        void passImageURI(File file, Uri photoURI);
    }
}