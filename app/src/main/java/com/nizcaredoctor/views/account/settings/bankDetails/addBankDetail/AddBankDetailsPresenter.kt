package com.nizcaredoctor.views.account.settings.bankDetails.addBankDetail

import com.nizcaredoctor.utils.getAccessToken
import com.nizcaredoctor.views.baseViews.BasePresenterImpl
import com.nizcaredoctor.webservice.ApiResponseModel
import com.nizcaredoctor.webservice.RetrofitClient
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AddBankDetailsPresenter : BasePresenterImpl<AddBankDetailsContract.View>(), AddBankDetailsContract.Presenter {
    override fun addBankAccount(map: HashMap<String, String>) {
        getView()?.showProgress()
        RetrofitClient.getApi().addEditBankAccount(getAccessToken(), map)
                .enqueue(object : Callback<ApiResponseModel<Any>> {
                    override fun onFailure(call: Call<ApiResponseModel<Any>>?, t: Throwable?) {
                        getView()?.dismissProgress()
                        getView()?.displayFailure(t?.message)
                    }

                    override fun onResponse(call: Call<ApiResponseModel<Any>>?, response: Response<ApiResponseModel<Any>>) {
                        getView()?.dismissProgress()
                        if (response.isSuccessful) {
                            getView()?.addBankAccountSuccess(response.body()?.msg)
                        } else
                            getView()?.displayError(JSONObject(response.errorBody()?.string()).getString("msg"), response.code())
                    }
                })
    }
}