package com.nizcaredoctor.views.account.settings

import android.os.Bundle
import android.util.Log
import com.nizcaredoctor.R
import com.nizcaredoctor.utils.activityFinishAnimation
import com.nizcaredoctor.utils.addFragment
import com.nizcaredoctor.views.baseViews.BaseActivity

class SettingsActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_container)
        init()
    }

    private fun init() {
        addFragment(supportFragmentManager, SettingsFragment(), R.id.container, false, true, false)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        Log.d(javaClass.simpleName, supportFragmentManager.backStackEntryCount.toString())
        if (supportFragmentManager.backStackEntryCount == 0) {
            finish()
            activityFinishAnimation()
        }
    }
}