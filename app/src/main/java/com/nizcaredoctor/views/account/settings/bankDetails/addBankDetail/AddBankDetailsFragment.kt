package com.nizcaredoctor.views.account.settings.bankDetails.addBankDetail

import android.content.Context
import android.os.Bundle
import android.support.design.widget.TextInputEditText
import android.support.design.widget.TextInputLayout
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Toast
import com.nizcaredoctor.R
import com.nizcaredoctor.utils.Constants
import com.nizcaredoctor.utils.isOnline
import com.nizcaredoctor.views.account.settings.bankDetails.BankAccountContract
import com.nizcaredoctor.views.baseViews.BaseViewFragImpl
import com.nizcaredoctor.webservice.WebConstants
import com.nizcaredoctor.webservice.model.bankModel.BankAccountItemModel
import kotlinx.android.synthetic.main.fragment_bank_details.*
import kotlinx.android.synthetic.main.layout_toolbar.*

class AddBankDetailsFragment : BaseViewFragImpl(), View.OnClickListener, AddBankDetailsContract.View {
    private var isError = true
    private val presenter = AddBankDetailsPresenter()
    private lateinit var listener: BankAccountContract.onEventClickListener
    private var type = "ADD"
    private var bank_id = ""
    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.tvAddAccount -> {
                if (isValid()) {
                    if (isOnline(context)) {
                        val map = HashMap<String, String>()
                        map.put(WebConstants.ACCOUNT_HOLDER_NAME, etAccountHolderName.text.toString().trim())
                        map.put(WebConstants.ACCOUNT_NUMBER, etAccountNumber.text.toString().trim())
                        map.put(WebConstants.IFSC_CODE, etIfsc.text.toString().trim())
                        map.put(WebConstants.MICR_CODE, etMicrCode.text.toString().trim())
                        map.put(WebConstants.IS_DEFAULT, "1")
                        map.put(WebConstants.BANK_NAME, "ICICI")
                        map.put(WebConstants.ADDRESS, "abc xyz")
                        map.put(WebConstants.TYPE, "1")
                        if (type.equals(Constants.EDIT)) {
                            map.put(WebConstants.USER_BANK_DETAIL_ID, bank_id)
                            presenter.addBankAccount(map)
                        } else presenter.addBankAccount(map)
                    }
                } else {

                }
            }
        }
    }

    fun bindListener(listener: BankAccountContract.onEventClickListener) {
        this.listener = listener
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_bank_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        setListener()
    }

    fun init() {
        tvTitle.text = getString(R.string.add_bank_details)
        setAccountTypeAdapter()
        val bundle = arguments
        if (bundle != null) {
            val model: BankAccountItemModel = bundle.getSerializable(Constants.BANK_DATA) as BankAccountItemModel
            type = bundle.getString(Constants.TYPE)
            setData(model)

        }
    }

    fun setData(model: BankAccountItemModel) {
        etMicrCode.setText(model.micr_code)
        etIfsc.setText(model.ifsc_code)
        etAccountNumber.setText(model.account_number)
        etAccountHolderName.setText(model.account_holder_name)
        etMicrCode.setText(model.micr_code)
        bank_id = model.id.toString()
    }

    fun setListener() {
        toolbar.setNavigationOnClickListener { activity?.supportFragmentManager?.popBackStack() }
        tvAddAccount.setOnClickListener(this)
        checkForEmptyData(etAccountHolderName, tilAccountHolder, R.string.error_empty_account_holder)
        checkForEmptyData(etAccountNumber, tilAccountNumber, R.string.error_empty_account_number)
        checkForEmptyData(etIfsc, tilIfsc, R.string.error_empty_ifsc)
        checkForEmptyData(etMicrCode, tilMicrCode, R.string.error_empty_micr_code)
    }

    private fun setAccountTypeAdapter() {
        val aa = ArrayAdapter.createFromResource(context, R.array.account_types, android.R.layout.simple_spinner_item)
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerAccountType.adapter = aa
    }

    private fun checkForEmptyData(editText: TextInputEditText, tilEditText: TextInputLayout, errorMessage: Int) {
        editText.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable) {
                if (editText.text.isEmpty()) {
                    tilEditText.error = getString(errorMessage)
                    isError = true
                } else {
                    tilEditText.isErrorEnabled = false
                    isError = false
                }
            }
        })
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        presenter.attachView(this)
    }

    override fun onDetach() {
        super.onDetach()
        presenter.detachView()
    }

    private fun isValid(): Boolean {
        if (etAccountHolderName.text.toString().trim().isEmpty()) {
            tilAccountHolder.isErrorEnabled = true
            tilAccountHolder.error = getString(R.string.error_empty_account_holder)
            etAccountHolderName.requestFocus()
            return false
        }
        if (etAccountNumber.text.toString().trim().isEmpty()) {
            tilAccountNumber.isErrorEnabled = true
            tilAccountNumber.error = getString(R.string.error_empty_account_number)
            etAccountNumber.requestFocus()
            return false
        }
        if (etIfsc.text.toString().trim().isEmpty()) {
            tilIfsc.isErrorEnabled = true
            tilIfsc.error = getString(R.string.error_empty_ifsc)
            etIfsc.requestFocus()
            return false
        }
        if (etMicrCode.text.toString().trim().isEmpty()) {
            tilMicrCode.isErrorEnabled = true
            tilMicrCode.error = getString(R.string.error_empty_micr_code)
            etMicrCode.requestFocus()
            return false
        }
        return true
    }

    override fun addBankAccountSuccess(msg: String?) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()
        if (::listener.isInitialized)
            listener.onNewAccountAdded()
        activity?.supportFragmentManager?.popBackStack()
    }
}