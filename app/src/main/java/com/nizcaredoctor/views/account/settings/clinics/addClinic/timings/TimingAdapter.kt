package com.nizcaredoctor.views.account.settings.clinics.addClinic.timings

import android.support.v4.app.FragmentActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import android.widget.Toast
import com.nizcaredoctor.R
import com.nizcaredoctor.views.account.settings.clinics.addClinic.timings.timeModel.TimeModel
import com.nizcaredoctor.views.account.settings.clinics.addClinic.timings.timeModel.TimeToFromModel
import kotlinx.android.synthetic.main.item_day.view.*

class TimingAdapter(val activity: FragmentActivity?, val timingList: MutableList<TimeModel>, val timingFragment: TimingFragment) :
        RecyclerView.Adapter<TimingAdapter.ViewHolder>() {
    private var onBind: Boolean = false

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_day, parent, false))
    }

    override fun getItemCount(): Int {
        return timingList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindDayView(timingList[position], position)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), CompoundButton.OnCheckedChangeListener {
        override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
            if (!onBind) {
                when (adapterPosition) {
                    0 -> {
                        timingList[adapterPosition].isEnabled = isChecked
                        timingList[adapterPosition + 1].isEnabled = !isChecked
                        timingList[adapterPosition + 2].isEnabled = !isChecked
                        timingList[adapterPosition + 3].isEnabled = !isChecked
                        timingList[adapterPosition + 4].isEnabled = !isChecked
                        timingList[adapterPosition + 5].isEnabled = !isChecked
                        notifyDataSetChanged()
                    }

                    1, 2, 3, 4, 5 -> {
                        if (timingList[0].isEnabled) {
                            timingList[0].isEnabled = false
                            notifyItemChanged(0)
                        }
                        timingList[adapterPosition].isEnabled = isChecked
                        notifyItemChanged(adapterPosition)

                    }

                    else -> {
                        timingList[adapterPosition].isEnabled = isChecked
                        notifyItemChanged(adapterPosition)
                    }
                }
            }
        }

        init {
            itemView.switchTime.setOnCheckedChangeListener(this)
            itemView.tvAddSession.setOnClickListener {
                if (timingList[adapterPosition].timings[timingList[adapterPosition].timings.size - 1].time_from == "Select" ||
                        timingList[adapterPosition].timings[timingList[adapterPosition].timings.size - 1].time_to == "Select") {
                    Toast.makeText(activity, "First add time in previous added slot", Toast.LENGTH_SHORT).show()
                } else {
                    timingList[adapterPosition].timings.add(TimeToFromModel())
                    notifyItemChanged(adapterPosition)
                }
            }
        }

        fun bindDayView(timeModel: TimeModel, position: Int) = with(itemView) {
            onBind = true
            tvDay.text = timeModel.name
            switchTime.isChecked = timeModel.isEnabled
            if (timeModel.isEnabled) {
                rvToFromTime.visibility = View.VISIBLE
                tvAddSession.visibility = View.VISIBLE
            } else {
                rvToFromTime.visibility = View.GONE
                tvAddSession.visibility = View.GONE
            }

            onBind = false
            itemView.rvToFromTime.adapter = TimeToFromAdapter(activity, timingList[adapterPosition].timings)

        }
    }
}