package com.nizcaredoctor.views.account.wallet

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.nizcaredoctor.R
import com.nizcaredoctor.views.baseViews.BaseViewFragImpl
import kotlinx.android.synthetic.main.fragment_patient_appointment.*

class AllTransactionFragment : BaseViewFragImpl() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_patient_appointment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setListener()
        init()
    }

    fun init() {
        recycleView.adapter = TransactionAdapter(context)
    }

    fun setListener() {

    }
}
