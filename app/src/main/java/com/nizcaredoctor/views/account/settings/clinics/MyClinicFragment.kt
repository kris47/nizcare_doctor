package com.nizcaredoctor.views.account.settings.clinics

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.gson.Gson
import com.nizcaredoctor.R
import com.nizcaredoctor.utils.Constants
import com.nizcaredoctor.utils.isOnline
import com.nizcaredoctor.utils.showInternetError
import com.nizcaredoctor.views.account.settings.clinics.addClinic.AddClinicActivity
import com.nizcaredoctor.views.account.settings.clinics.addClinic.BranchModel
import com.nizcaredoctor.views.baseViews.BaseViewFragImpl
import com.nizcaredoctor.webservice.model.branchModel.Branches
import com.nizcaredoctor.webservice.model.branchModel.ClinicDataItem
import kotlinx.android.synthetic.main.activity_my_clinic.*

class MyClinicFragment : BaseViewFragImpl(), MyClinicContract.View, View.OnClickListener, MyClinicContract.OnEventClickListener {

    private val presenter = MyClinicPresenter()
    private lateinit var layoutManager: LinearLayoutManager
    private val branchList = mutableListOf<ClinicDataItem>()
    private var page = 1
    private var total = 0
    private val ADD_CLINIC = 100

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.activity_my_clinic, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        setListeners()
        getAllClinics(page)
    }

    private fun init() {
        layoutManager = LinearLayoutManager(context)
        rvBranches.layoutManager = layoutManager
        rvBranches.adapter = BranchAdapter(context, branchList, this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.ivAddClinic -> {
                startActivityForResult(Intent(context, AddClinicActivity::class.java).putExtra("type", Constants.ADD), ADD_CLINIC)
            }
        }
    }

    private fun getAllClinics(page: Int) {
        if (isOnline(context)) {
            presenter.getMyClinics(page)
        } else {
            viewFlipper.displayedChild = 1
            noDataView.setImage(R.raw.no_internet)
            noDataView.setData(getString(R.string.no_interner_connection), getString(R.string.interent_substitles))
        }
    }

    override fun getBranchSuccess(branch: BranchModel?) {
        startActivityForResult(Intent(context, AddClinicActivity::class.java)
                .putExtra("type", Constants.EDIT)
                .putExtra("branchId", branch?.id)
                .putExtra(Constants.CLINIC_DATA, Gson().toJson(branch)), ADD_CLINIC)
    }

    private fun setListeners() {
        toolbar.setNavigationOnClickListener { activity?.supportFragmentManager?.popBackStack() }
        ivAddClinic.setOnClickListener(this)

        rvBranches.addOnScrollListener(object : RecyclerView.OnScrollListener() {
        })

        swipeRefresh.setOnRefreshListener {
            refreshPage()
        }
    }

    fun refreshPage() {
        page = 1
        getAllClinics(page)
    }

    override fun setAllBranches(branches: Branches?) {
        if (branches != null) {
            if (branches.data.isNotEmpty()) {
                viewFlipper.displayedChild = 2
                if (page == 1)
                    branchList.clear()
                branchList.addAll(branches.data)
                rvBranches.adapter.notifyDataSetChanged()
            } else {
                showNoBranchAvail()
            }
            page = branches.current_page ?: 1
            total = branches.total ?: 0
        }
    }

    fun showNoBranchAvail() {
        viewFlipper.displayedChild = 1
        noDataView.setImage(R.raw.no_data)
        noDataView.setData("No Branches available.", "Click on add icon to add branches")
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        presenter.attachView(this)
    }

    override fun onDetach() {
        super.onDetach()
        presenter.detachView()
    }

    override fun getBranchAllData(id: Int?) {
        if (isOnline(context)) {
            presenter.getBranchApi(id)
        } else {
            rootMyClinic.showInternetError()
        }
    }

    override fun removeBranchSuccess(id: Int?) {
        branchList.remove(branchList.filter { it.id == id }.single())
        rvBranches.adapter.notifyDataSetChanged()
        if (branchList.isEmpty())
            showNoBranchAvail()
    }

    override fun removeBranch(id: Int?) {
        presenter.removeBranch(id)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == ADD_CLINIC && resultCode == Activity.RESULT_OK) {
            refreshPage()
        }
    }
}