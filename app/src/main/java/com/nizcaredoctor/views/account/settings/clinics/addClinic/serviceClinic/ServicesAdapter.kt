package com.nizcaredoctor.views.account.settings.clinics.addClinic.serviceClinic

import android.app.Activity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.nizcaredoctor.R
import com.nizcaredoctor.utils.Constants
import com.nizcaredoctor.webservice.model.GetItemModel
import kotlinx.android.synthetic.main.item_services.view.*

class ServicesAdapter(val activity: Activity?, val allServicesList: MutableList<GetItemModel>,
                      val type: String, val fragment: ServiceFragment) : RecyclerView.Adapter<ServicesAdapter.ViewHolder>() {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (type == Constants.ALL_SERVICE) {
            holder.bindAllServicesView(allServicesList[position])
        } else {
            holder.bindSelectedService(allServicesList[position])
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(activity).inflate(R.layout.item_services, parent, false))
    }

    override fun getItemCount(): Int = allServicesList.size


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        init {
            itemView.ivAddRemove.setOnClickListener {
                if (type == Constants.ALL_SERVICE) {
                    fragment.addService(allServicesList[adapterPosition],adapterPosition)
                } else {
                    fragment.removeService(adapterPosition,allServicesList[adapterPosition])
                }
            }
        }

        fun bindAllServicesView(getItemModel: GetItemModel) = with(itemView) {
            tvServiceName.text = getItemModel.name
            ivAddRemove.setImageResource(R.drawable.fab)
        }

        fun bindSelectedService(getItemModel: GetItemModel) = with(itemView) {
            tvServiceName.text = getItemModel.name
            ivAddRemove.setImageResource(R.drawable.ic_minus)
        }
    }
}
