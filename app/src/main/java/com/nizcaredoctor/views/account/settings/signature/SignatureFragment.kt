package com.nizcaredoctor.views.account.settings.signature

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.nizcaredoctor.R
import com.nizcaredoctor.utils.AmazoneS3
import com.nizcaredoctor.utils.ClickImage
import com.nizcaredoctor.views.baseViews.BaseViewFragImpl
import kotlinx.android.synthetic.main.fragment_signature.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import java.io.File

class SignatureFragment : BaseViewFragImpl(), View.OnClickListener, ClickImage.ImagePickerListener,
        SignatureContract.View {

    private lateinit var amazonS3: AmazoneS3
    private var profilePicURL = ""
    private lateinit var clickImage: ClickImage
    private val presenter = SignaturePresenter()

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnCamera -> {
                clickImage.invokeCamera(true)
            }
            R.id.btnPhotoGallery -> {
                clickImage.invokeCamera(false)
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_signature, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        setListener()
    }

    fun init() {
        tvTitle.text = getString(R.string.signature)
        amazonS3 = AmazoneS3(context)
        clickImage = ClickImage(this)
        clickImage.setImagePickerListener(this)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        presenter.attachView(this)
    }

    override fun onDetach() {
        super.onDetach()
        presenter.detachView()
    }

    fun setListener() {
        toolbar.setNavigationOnClickListener { activity?.supportFragmentManager?.popBackStack() }
        btnPhotoGallery.setOnClickListener(this)
        btnCamera.setOnClickListener(this)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            clickImage.onActivityResult(requestCode, resultCode, data)
        }
    }

    override fun onImageSelectedFromPicker(imageFile: File?) {
        profilePicURL = amazonS3.setFileToUpload(imageFile)
        addEditSignature(profilePicURL)
        layStep1.visibility = View.GONE
        layStep2.visibility = View.VISIBLE
        Glide.with(context!!)
                .load(imageFile)
                .into(ivImgPreview)
    }

    override fun addEditSignature(signature: String?) {
        presenter.addEditSignature(signature)

    }
}