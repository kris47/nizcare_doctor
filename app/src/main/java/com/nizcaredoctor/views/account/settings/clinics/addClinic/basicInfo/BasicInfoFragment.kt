package com.nizcaredoctor.views.account.settings.clinics.addClinic.basicInfo

import android.Manifest
import android.app.Activity
import android.app.Activity.RESULT_OK
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.bumptech.glide.Glide
import com.google.android.gms.location.places.ui.PlacePicker
import com.google.android.gms.maps.model.LatLng
import com.google.gson.Gson
import com.nizcaredoctor.R
import com.nizcaredoctor.utils.*
import com.nizcaredoctor.views.account.settings.clinics.addClinic.BranchModel
import com.nizcaredoctor.views.accountSetup.stateCity.StateCityActivity
import com.nizcaredoctor.views.baseViews.BaseViewFragImpl
import com.nizcaredoctor.views.write.healthFeed.writeTip.AttachmentAdapter
import com.nizcaredoctor.webservice.WebConstants
import com.nizcaredoctor.webservice.model.AttachmentModel
import com.nizcaredoctor.webservice.model.GetItemModel
import com.nizcaredoctor.webservice.model.Profile
import kotlinx.android.synthetic.main.dialog_number_picker.view.*
import kotlinx.android.synthetic.main.fragment_basic_info.*
import pub.devrel.easypermissions.EasyPermissions
import pub.devrel.easypermissions.PermissionRequest
import java.io.File

class BasicInfoFragment : BaseViewFragImpl(), BasicInfoContract.View, EasyPermissions.PermissionCallbacks, ImagePicker.ImagePickerListener {

    private val permissions = arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    private val imageList = mutableListOf<AttachmentModel>()
    private val specialityList = mutableListOf<GetItemModel>()
    private val permissionRequestCode = 56
    private val selectedSpecialty = mutableListOf<Int>()
    private lateinit var amazoneS3: AmazoneS3
    private val PLACE_PICKER_REQUEST = 5
    private lateinit var imagePicker: ImagePicker
    private var selectedStateId = 0
    private var selectedCityId = 0
    private var latLong = LatLng(0.0, 0.0)
    private lateinit var branchModel: BranchModel
    private lateinit var selectedSpecialities: BooleanArray
    private val presenter = BasicInfoPresenter()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_basic_info, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        setListeners()
        if (isOnline(context)) {
            presenter.getServiceCategories()
        }
    }

    private fun init() {
        imagePicker = ImagePicker(this, false)
        imagePicker.setImagePickerListener(this)
        rvClinicImages.adapter = activity?.let { AttachmentAdapter(it, imageList) }

        if (activity?.intent?.getStringExtra("type") == Constants.EDIT) {
            branchModel = Gson().fromJson(activity?.intent?.getStringExtra(Constants.CLINIC_DATA), BranchModel::class.java)
            tvSaveClinic.text = getString(R.string.update_clinic)
            etClinic.setText(branchModel.name)
            etPhoneClinic.setText(branchModel.phone_no)
            etClinicEmail.setText(branchModel.email)
            etFee.setText(branchModel.consultation_fees)
            etTime.setText("")
            selectedStateId = branchModel.state_id
            selectedCityId = branchModel.city_id
            etState.setText(branchModel.state_id.toString())
            etCity.setText(branchModel.city_id.toString())
            etClinicAddress.setText(branchModel.address)
            switchWeb.isChecked = branchModel.show_on_web != 0
            this.latLong = LatLng(branchModel.latitude.toDouble(), branchModel.longitude.toDouble())
            ivMapImage.visibility = View.VISIBLE
            context?.let {
                Glide.with(tvSaveClinic.context)
                        .load(getStaticMapImageURl(latLong.longitude.toFloat(), latLong.latitude.toFloat(), context))
                        .into(ivMapImage)
            }
            for (i in 0 until branchModel.images.size) {
                imageList.add(AttachmentModel(branchModel.images[i].image.toString(), Constants.IMAGE, null))
            }
            rvClinicImages.adapter.notifyDataSetChanged()
        }
    }

    private fun setListeners() {
        presenter.attachView(this)
        etClinicAddress.setOnClickListener {
            val builder = PlacePicker.IntentBuilder()
            startActivityForResult(builder.build(activity), PLACE_PICKER_REQUEST)
        }
        ivClinicImage.setOnClickListener {
            openImage()
        }

        etState.setOnClickListener {
            startActivityForResult(Intent(activity, StateCityActivity::class.java)
                    .putExtra(Constants.SELECTED_POSITION, selectedStateId)
                    .putExtra(Constants.ListType, Constants.States), 1)
        }

        etCity.setOnClickListener {
            if (selectedStateId == 0) {
                view?.showErrorSnack("Please select state first.")
            } else {
                startActivityForResult(Intent(activity, StateCityActivity::class.java)
                        .putExtra(Constants.SELECTED_STATE_ID, selectedStateId)
                        .putExtra(Constants.SELECTED_POSITION, selectedCityId)
                        .putExtra(Constants.ListType, Constants.Cities), 2)
            }
        }

        etTime.setOnClickListener {
            val builder = AlertDialog.Builder(activity!!)
            val inflater = layoutInflater
            val dialogView = inflater.inflate(R.layout.dialog_number_picker, null)
            val values = arrayOf("15", "30", "45", "60", "75", "90")
            dialogView.picker.minValue = 0
            dialogView.picker.maxValue = values.size - 1
            dialogView.picker.displayedValues = values
            dialogView.picker.wrapSelectorWheel = true

            builder.setView(dialogView)
                    .setTitle(getString(R.string.select_time_in_minutes_consult))
                    .setPositiveButton(R.string.ok, { dialog, id ->
                        etTime.setText(values[dialogView.picker.value])
                        dialog.cancel()
                    })
                    .setNegativeButton(R.string.cancel, { dialog, id ->
                        dialog.cancel()
                    })

            builder.create().show()
        }

        tvSaveClinic.setOnClickListener {
            removeError()
            if (isValidated()) {
                if (isOnline(context)) {
                    val map = HashMap<String, String>()
                    map[WebConstants.NAME] = etClinic.text.trim().toString()
                    map[WebConstants.EMAIL] = etClinicEmail.text.trim().toString()
                    map[WebConstants.PHONENUMBER] = etPhoneClinic.text.trim().toString()
                    map[WebConstants.CONSULTATION_FEES] = etFee.text.trim().toString()
                    map[WebConstants.CONSULTATION_DURATION] = etTime.text.trim().toString()
                    map[WebConstants.LONGITUDE] = latLong.longitude.toString()
                    map[WebConstants.LATITUDE] = latLong.latitude.toString()
                    map[WebConstants.SERVICEPROVIDERID] = PrefsManager.get().getObject(Constants.USER_DETAIL, Profile::class.java)?.user_role_id.toString()
                    map[WebConstants.ADDRESS_HOME] = etClinicAddress.text.trim().toString()
                    map[WebConstants.STATE_ID] = selectedStateId.toString()
                    map[WebConstants.CITYID] = selectedCityId.toString()
                    map[WebConstants.SHOW_ON_WEB] = (if (switchWeb.isChecked) 1 else 0).toString()

                    if (activity?.intent?.getStringExtra("type") == Constants.EDIT) {
                        map[WebConstants.BRANCH_ID] = branchModel.id.toString()
                    }

                    if (imageList.isNotEmpty()) {
                        amazoneS3 = AmazoneS3(activity)
                        val urlPath = mutableListOf<String>()
                        for (i in 0 until imageList.size) {
                            if (imageList[i].file == null) {
                                urlPath.add(imageList[i].name)
                            } else {
                                urlPath.add(amazoneS3.setFileToUpload(imageList[i].file))
                            }
                        }
                        map[WebConstants.IMAGES] = Gson().toJson(urlPath)
                    }
                    map[WebConstants.SPECIALITIES] = Gson().toJson(selectedSpecialty)
                    presenter.addClinic(map)
                } else {
                    view?.showInternetError()
                }
            }
        }

        etSpeciality.setOnClickListener {
            if (specialityList.isNotEmpty()) {
                val services = Array(specialityList.size, { i -> specialityList[i].name })
                val builder = context?.let { it1 -> AlertDialog.Builder(it1) }
                builder?.setTitle("Select your specialities")
                        ?.setCancelable(false)
                        ?.setMultiChoiceItems(services, selectedSpecialities,
                                DialogInterface.OnMultiChoiceClickListener { _, which, isChecked ->
                                    selectedSpecialities[which] != isChecked
                                })
                        ?.setPositiveButton(R.string.ok, DialogInterface.OnClickListener { dialog, _ ->
                            var text = ""
                            selectedSpecialty.clear()
                            for (i in 0 until specialityList.size) {
                                if (selectedSpecialities[i]) {
                                    selectedSpecialty.add(specialityList[i].id)
                                    if (text.isNotEmpty()) {
                                        text += ", "
                                    }
                                    text += specialityList[i].name
                                }
                            }
                            etSpeciality.setText(text)
                            dialog.dismiss()
                        })
                builder?.create()?.show()
            } else {
                view?.showErrorSnack("Please wait loading specialities.")
            }
        }
    }

    override fun clinicSuccess(msg: String?) {
        if (activity?.intent?.getStringExtra("type") == Constants.EDIT) {
            Toast.makeText(context, msg, Toast.LENGTH_LONG).show()
        } else {
            Toast.makeText(context, "$msg Added clinic is under verification. After verification you can add services, clinic timings and staff members"
                    , Toast.LENGTH_LONG).show()
        }
        activity?.setResult(Activity.RESULT_OK)
        activity?.finish()
    }

    private fun isValidated(): Boolean {
        return when {
            imageList.isEmpty() -> {
                view?.showErrorSnack(getString(R.string.upload_clinic_image))
                false
            }
            etClinic.text.trim().toString().isEmpty() -> {
                tilClinic.isErrorEnabled = true
                tilClinic.error = getString(R.string.clinic_name_is_empty)
                tilClinic.requestFocus()
                false
            }
            etPhoneClinic.text.trim().toString().isEmpty() -> {
                tilPhoneClinic.isErrorEnabled = true
                tilPhoneClinic.error = getString(R.string.enter_mobile_no)
                tilPhoneClinic.requestFocus()
                false
            }

            etPhoneClinic.text.trim().toString().length < 10 -> {
                tilPhoneClinic.isErrorEnabled = true
                tilPhoneClinic.error = getString(R.string.enter_valid_mobile_no)
                tilPhoneClinic.requestFocus()
                false
            }

            !isValidMobile(etPhoneClinic.text.toString()) -> {
                tilPhoneClinic.isErrorEnabled = true
                tilPhoneClinic.error = getString(R.string.enter_valid_mobile_no)
                tilPhoneClinic.requestFocus()
                false
            }
            etClinicEmail.text.trim().toString().isEmpty() -> {
                tilClinicEmail.isErrorEnabled = true
                tilClinicEmail.error = getString(R.string.email_is_empty)
                tilClinicEmail.requestFocus()
                false
            }

            !isValidEmail(etClinicEmail.text.trim().toString()) -> {
                tilClinicEmail.isErrorEnabled = true
                tilClinicEmail.error = getString(R.string.enter_valid_email)
                tilClinicEmail.requestFocus()
                false
            }

            selectedSpecialty.isEmpty() -> {
                tilSpeciality.requestFocus()
                tilSpeciality.isErrorEnabled = true
                rootBasic.showErrorSnack("Select at least one speciality")
                tilSpeciality.error = "Select at least one speciality"
                false
            }

            etFee.text.trim().isEmpty() -> {
                tilFee.isErrorEnabled = true
                tilFee.error = "Enter consultation fee"
                tilFee.requestFocus()
                false
            }


            etTime.text.trim().isEmpty() -> {
                tilTime.isErrorEnabled = true
                tilTime.error = "Enter consultation time in minutes"
                tilTime.requestFocus()
                false
            }

            selectedStateId == 0 -> {
                etState.isFocusable = true
                tilState.isErrorEnabled = true
                tilState.error = "Select state"
                view?.showErrorSnack("Select state")
                tilState.requestFocus()
                etState.isFocusable = false
                false
            }


            selectedCityId == 0 -> {
                tilCity.requestFocus()
                tilCity.isErrorEnabled = true
                view?.showErrorSnack("Select city")
                tilCity.error = "Select city"
                false
            }
            etClinicAddress.text.trim().isEmpty() -> {
                tilAddress.isErrorEnabled = true
                tilAddress.error = "Enter clinic address"
                tilAddress.requestFocus()
                false
            }

            else -> true
        }
    }

    private fun removeError() {
        tilClinic.isErrorEnabled = false
        tilPhoneClinic.isErrorEnabled = false
        tilClinicEmail.isErrorEnabled = false
        tilSpeciality.isErrorEnabled = false
        tilFee.isErrorEnabled = false
        tilTime.isErrorEnabled = false
        tilAddress.isErrorEnabled = false

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            1 -> {
                if (resultCode == Activity.RESULT_OK) {
                    selectedCityId = 0
                    etCity.setText("")
                    etState.setText(data?.getStringExtra("stateName"))
                    selectedStateId = data?.getIntExtra("stateID", 1) ?: 0
                }
            }
            2 -> {
                if (resultCode == Activity.RESULT_OK) {
                    etCity.setText(data?.getStringExtra("cityName"))
                    selectedCityId = data?.getIntExtra("cityID", 1) ?: 0
                }
            }
            PLACE_PICKER_REQUEST -> {
                if (resultCode == RESULT_OK) {
                    if (ivMapImage.visibility == View.GONE) {
                        ivMapImage.visibility = View.VISIBLE
                    }
                    val place = PlacePicker.getPlace(context, data)
                    etClinicAddress.setText(place.address)
                    this.latLong = place.latLng
                    context?.let {
                        Glide.with(it)
                                .load(getStaticMapImageURl(place.latLng.longitude.toFloat(), place.latLng.latitude.toFloat(), context))
                                .into(ivMapImage)
                    }
                }
            }
            else -> imagePicker.onActivityResult(requestCode, resultCode, data)
        }
    }

    override fun setServiceCategory(serviceCategories: MutableList<GetItemModel>?) {
        if (serviceCategories != null) {
            specialityList.clear()
            specialityList.addAll(serviceCategories)
            selectedSpecialities = BooleanArray(serviceCategories.size, { false })
            if (activity?.intent?.getStringExtra("type") == Constants.EDIT) {
                var text = ""
                for (i in 0 until branchModel.service_categories.size) {
                    selectedSpecialty.add(branchModel.service_categories[i].service_category_id
                            ?: 0)
                }
                for (i in 0 until specialityList.size) {
                    for (j in 0 until branchModel.service_categories.size) {
                        if (branchModel.service_categories[j].service_category_id == specialityList[i].id) {
                            selectedSpecialities[i] = true
                            if (text.isNotEmpty()) {
                                text += ", "
                            }
                            text += specialityList[i].name
                        }
                    }
                }
                etSpeciality.setText(text)
            }
        }
    }

    private fun openImage() {
        if (context?.let { EasyPermissions.hasPermissions(it, *permissions) }!!) {
            imagePicker.showImagePicker()
        } else {
            EasyPermissions.requestPermissions(
                    PermissionRequest.Builder(this, permissionRequestCode, *permissions)
                            .setRationale(getString(R.string.rational_permission))
                            .setPositiveButtonText(R.string.ok)
                            .setNegativeButtonText(R.string.cancel)
                            .build())
        }
    }

    override fun onPermissionsDenied(requestCode: Int, perms: MutableList<String>) {
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            openSettingDialog(activity)
        }
    }

    override fun onPermissionsGranted(requestCode: Int, perms: MutableList<String>) {
        openImage()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }

    override fun onImageSelectedFromPicker(imageFile: File?) {
        if (imageFile != null) {
            imageList.add(AttachmentModel(imageFile.name.toString(), Constants.IMAGE, imageFile))
            rvClinicImages.adapter.notifyDataSetChanged()
            Log.d("file", imageFile.name.toString())
        }
    }

    override fun onPDFSelected(file: File?, displayName: String?) {

    }

    override fun onDestroyView() {
        super.onDestroyView()
        presenter.detachView()
    }
}