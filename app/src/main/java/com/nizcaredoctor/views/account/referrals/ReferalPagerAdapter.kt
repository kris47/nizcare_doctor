package com.nizcaredoctor.views.account.referrals

import android.content.Context
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import com.nizcaredoctor.R

class ReferalPagerAdapter(val context: Context, val fragmentList: MutableList<Fragment>, fragmentManager: FragmentManager) :
        FragmentStatePagerAdapter(fragmentManager) {

    override fun getItem(position: Int): Fragment {
        return fragmentList[position]
    }

    override fun getCount(): Int {
        return fragmentList.size
    }

    override fun getPageTitle(position: Int): CharSequence? {
        when (position) {
            0 -> return context.getString(R.string.refer_to_me)
            1 -> return context.getString(R.string.refer_by_me)
        }
        return ""
    }
}