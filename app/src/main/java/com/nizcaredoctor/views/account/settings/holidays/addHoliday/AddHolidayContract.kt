package com.nizcaredoctor.views.account.settings.holidays.addHoliday

import com.nizcaredoctor.views.baseViews.BasePresenter
import com.nizcaredoctor.views.baseViews.BaseView

interface AddHolidayContract {
    interface View : BaseView {
        fun holidayAddedSuccess(msg : String?)
    }

    interface Presenter : BasePresenter<View> {
        fun addEditHolidays(map : HashMap<String, String>)
    }
}