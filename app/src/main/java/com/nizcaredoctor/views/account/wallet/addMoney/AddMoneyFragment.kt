package com.nizcaredoctor.views.account.wallet.addMoney

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.nizcaredoctor.R
import com.nizcaredoctor.views.baseViews.BaseViewFragImpl
import kotlinx.android.synthetic.main.fragment_add_money.*
import kotlinx.android.synthetic.main.layout_toolbar.*

class AddMoneyFragment : BaseViewFragImpl(), View.OnClickListener,
        AddMoneyContract.onEventClickListener {

    private var amount = 0
    override fun onClick(v: View?) {

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_add_money, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        setListener()
    }

    fun init() {
        tvTitle.text = getString(R.string.add_money)
        recyclerMoney.adapter = AddMoneyAdapter(context, this)
        toolbar.setNavigationOnClickListener { activity?.supportFragmentManager?.popBackStack() }
    }

    fun setListener() {

    }

    override fun onMoneyAdded(value: String) {
        amount += value.toInt()
        tvAmount.text = amount.toString()
    }
}