package com.nizcaredoctor.views.account.wallet.addMoney

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.nizcaredoctor.R
import kotlinx.android.synthetic.main.item_add_money.view.*

class AddMoneyAdapter(val context: Context?,val listener : AddMoneyContract.onEventClickListener) : RecyclerView.Adapter<AddMoneyAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_add_money, parent, false))
    }

    override fun getItemCount(): Int = 10

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView),View.OnClickListener{
        override fun onClick(v: View?) {
            when(v?.id){
                R.id.frameMoney ->{
                    listener.onMoneyAdded("500")
                }
            }
        }

        init {
            itemView.frameMoney.setOnClickListener(this)
        }
    }
}