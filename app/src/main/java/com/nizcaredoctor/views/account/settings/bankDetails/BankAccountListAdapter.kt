package com.nizcaredoctor.views.account.settings.bankDetails

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.nizcaredoctor.R
import com.nizcaredoctor.webservice.model.bankModel.BankAccountItemModel
import kotlinx.android.synthetic.main.item_bank_accounts.view.*

class BankAccountListAdapter(val context: Context?, var bankAccountItemModel: List<BankAccountItemModel>,
                             val listener: BankAccountContract.onEventClickListener) : RecyclerView.Adapter<BankAccountListAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_bank_accounts, parent, false))
    }

    override fun getItemCount(): Int {
        return bankAccountItemModel.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindView(bankAccountItemModel.get(position))
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        override fun onClick(v: View?) {
            when (v?.id) {
                R.id.tvBankRemove ->
                    listener.removeBankAccount(bankAccountItemModel[adapterPosition].id)
                R.id.tvBankEdit, R.id.rootBranch ->
                    listener.editBankAccount(bankAccountItemModel[adapterPosition])
            }
        }

        init {
            itemView.tvBankEdit.setOnClickListener(this)
            itemView.tvBankRemove.setOnClickListener(this)
            itemView.rootBranch.setOnClickListener(this)
        }

        fun bindView(model: BankAccountItemModel) = with(itemView) {
            tvBankName.text = model.bank_name
            if (model.is_default == 1) tvDeault.visibility = View.VISIBLE
            else tvDeault.visibility = View.GONE
            when (model.type) {
                ACCOUNT_TYPES.Savings.accountType ->
                    tvAccountType.text = context.getString(R.string.account_no, ACCOUNT_TYPES.Savings.name, model.account_number)
                ACCOUNT_TYPES.Current.accountType ->
                    tvAccountType.text = context.getString(R.string.account_no, ACCOUNT_TYPES.Current.name, model.account_number)
                ACCOUNT_TYPES.Other.accountType ->
                    tvAccountType.text = context.getString(R.string.account_no, ACCOUNT_TYPES.Other.name, model.account_number)
            }
        }
    }

    enum class ACCOUNT_TYPES(val accountType: Int) {
        Savings(1),
        Current(2),
        Other(3)
    }
}