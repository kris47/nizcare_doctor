package com.nizcaredoctor.views.account.settings.clinics.addClinic.timings

import com.nizcaredoctor.utils.getAccessToken
import com.nizcaredoctor.views.account.settings.clinics.addClinic.timings.timeModel.AllWorkingHoursModel
import com.nizcaredoctor.views.baseViews.BasePresenterImpl
import com.nizcaredoctor.webservice.ApiResponseModel
import com.nizcaredoctor.webservice.RetrofitClient
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class TimingPresenter : BasePresenterImpl<TimingContract.View>(), TimingContract.Presenter {
    override fun getAllBranchTimingsTimings() {
        RetrofitClient.getApi().getAllBranchTimings()
                .enqueue(object : Callback<ApiResponseModel<AllWorkingHoursModel>> {
                    override fun onFailure(call: Call<ApiResponseModel<AllWorkingHoursModel>>?, t: Throwable?) {
                        getView()?.displayFailure(t?.message)
                    }

                    override fun onResponse(call: Call<ApiResponseModel<AllWorkingHoursModel>>?, response: Response<ApiResponseModel<AllWorkingHoursModel>>?) {
                        if (response != null)
                            if (response.isSuccessful) {
                                getView()?.allTimingsSuccess(response.body()?.data?.allWorkingHours)
                            } else {
                                try {
                                    getView()?.displayError(JSONObject(response.errorBody()?.string()).getString("msg"), response.code())
                                } catch (e: Exception) {

                                }
                            }
                    }
                })
    }

    override fun addTimingClinic(map: HashMap<String, String>) {
        getView()?.showProgress()
        RetrofitClient.getApi().addClinicTimings(getAccessToken(), map)
                .enqueue(object : Callback<ApiResponseModel<Any>> {
                    override fun onFailure(call: Call<ApiResponseModel<Any>>?, t: Throwable?) {
                        getView()?.dismissProgress()
                        getView()?.displayFailure(t?.message)
                    }

                    override fun onResponse(call: Call<ApiResponseModel<Any>>?, response: Response<ApiResponseModel<Any>>?) {

                        getView()?.dismissProgress()
                        if (response != null)
                            if (response.isSuccessful) {
                                getView()?.timingSuccess(response.body()?.msg)
                            } else {
                                try {
                                    getView()?.displayError(JSONObject(response.errorBody()?.string()).getString("msg"), response.code())
                                } catch (e: Exception) {

                                }
                            }
                    }
                })
    }
}