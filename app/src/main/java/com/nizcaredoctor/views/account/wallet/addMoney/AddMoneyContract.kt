package com.nizcaredoctor.views.account.wallet.addMoney

interface AddMoneyContract {

    interface onEventClickListener{
        fun onMoneyAdded(value : String)
    }
}