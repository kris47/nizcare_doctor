package com.nizcaredoctor.views.account.referrals

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.nizcaredoctor.R
import com.nizcaredoctor.views.baseViews.BaseViewFragImpl
import kotlinx.android.synthetic.main.fragment_referal.*
import kotlinx.android.synthetic.main.layout_toolbar.*

class ReferalFragment : BaseViewFragImpl() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_referal, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        setListener()
    }

    fun init() {
        tvTitle.text = getString(R.string.referrals)
        val fragmentList = mutableListOf<Fragment>()
        fragmentList.add(ReferredByMeFragment())
        fragmentList.add(ReferredByMeFragment())
        viewPager.adapter = ReferalPagerAdapter(context!!, fragmentList, activity!!.supportFragmentManager)
        tabReferrals.setupWithViewPager(viewPager)
//        viewPager.offscreenPageLimit = 2
    }

    fun setListener() {
        toolbar.setNavigationOnClickListener { activity?.supportFragmentManager?.popBackStack() }
    }
}
