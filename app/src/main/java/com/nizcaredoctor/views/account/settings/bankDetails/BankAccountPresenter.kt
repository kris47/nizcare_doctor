package com.nizcaredoctor.views.account.settings.bankDetails

import com.nizcaredoctor.utils.getAccessToken
import com.nizcaredoctor.views.baseViews.BasePresenterImpl
import com.nizcaredoctor.webservice.ApiResponseModel
import com.nizcaredoctor.webservice.RetrofitClient
import com.nizcaredoctor.webservice.model.bankModel.BankAccountModel
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class BankAccountPresenter : BasePresenterImpl<BankAccountContract.View>(), BankAccountContract.Presenter {

    override fun removeBankAccount(user_bank_detail_id: Int) {
        getView()?.showProgress()
        RetrofitClient.getApi().removeBankAccount(getAccessToken(), user_bank_detail_id.toString())
                .enqueue(object : Callback<ApiResponseModel<Any>> {
                    override fun onFailure(call: Call<ApiResponseModel<Any>>?, t: Throwable?) {
                        getView()?.dismissProgress()
                        getView()?.displayFailure(t?.message)
                    }

                    override fun onResponse(call: Call<ApiResponseModel<Any>>?, response: Response<ApiResponseModel<Any>>) {
                        getView()?.dismissProgress()
                        if (response.isSuccessful) {
                            getView()?.removeBranchSuccess(user_bank_detail_id)
                        } else
                            getView()?.displayError(JSONObject(response.errorBody()?.string()).getString("msg"), response.code())
                    }
                })
    }

    override fun getAllBankAccounts() {
        getView()?.showProgress()
        RetrofitClient.getApi().getBankAccounts(getAccessToken())
                .enqueue(object : Callback<ApiResponseModel<BankAccountModel>> {
                    override fun onFailure(call: Call<ApiResponseModel<BankAccountModel>>?, t: Throwable?) {
                        getView()?.dismissProgress()
                        getView()?.displayFailure(t?.message)
                    }

                    override fun onResponse(call: Call<ApiResponseModel<BankAccountModel>>?, response: Response<ApiResponseModel<BankAccountModel>>) {
                        getView()?.dismissProgress()
                        if (response.isSuccessful) {
                            getView()?.getBankSuccess(response.body()?.data)
                        } else
                            getView()?.displayError(JSONObject(response.errorBody()?.string()).getString("msg"), response.code())
                    }
                })
    }
}