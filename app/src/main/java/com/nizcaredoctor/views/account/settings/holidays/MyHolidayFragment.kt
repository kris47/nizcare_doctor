package com.nizcaredoctor.views.account.settings.holidays

import android.content.Context
import android.os.Bundle
import android.os.Parcelable
import android.support.v4.widget.SwipeRefreshLayout
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import com.google.gson.Gson
import com.nizcaredoctor.R
import com.nizcaredoctor.utils.Constants
import com.nizcaredoctor.utils.addFragment
import com.nizcaredoctor.utils.isOnline
import com.nizcaredoctor.utils.showErrorSnack
import com.nizcaredoctor.views.account.settings.holidays.addHoliday.AddHolidayFragment
import com.nizcaredoctor.views.baseViews.BaseViewFragImpl
import com.nizcaredoctor.webservice.model.branchModel.Branches
import com.nizcaredoctor.webservice.model.branchModel.ClinicDataItem
import com.nizcaredoctor.webservice.model.holidayModel.HolidayDataItem
import com.nizcaredoctor.webservice.model.holidayModel.Holidays
import kotlinx.android.synthetic.main.activity_my_holiday.*
import kotlinx.android.synthetic.main.layout_toolbar.*

class MyHolidayFragment : BaseViewFragImpl(), View.OnClickListener, AdapterView.OnItemSelectedListener,
        MyHolidaysContract.View, MyHolidaysContract.OnEventClickListener, SwipeRefreshLayout.OnRefreshListener {

    private val presenter = MyHolidaysPresenter()
    private var page = 1
    private val branchList = mutableListOf<ClinicDataItem>()
    private var branchId = "0"
    private var holidayList = mutableListOf<HolidayDataItem>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.activity_my_holiday, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        setListener()
        getAllClinics(page)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        presenter.attachView(this)
    }

    override fun onRefresh() {
        getAllHolidays(branchId)
    }

    fun setListener() {
        spinnerClinics.onItemSelectedListener = this
        toolbar.setNavigationOnClickListener { activity?.supportFragmentManager?.popBackStack() }
        ivAddHoliday.setOnClickListener(this)
        swipeRefresh.setOnRefreshListener(this)
    }

    fun init() {
        tvTitle.text = getString(R.string.holidays)
        recycleHolidays.adapter = MyHolidayAdapter(context, holidayList, this)
    }

    private fun getAllClinics(page: Int) {
        if (isOnline(context)) {
            presenter.getMyClinics(page)
        } else
            showDefaultValue(R.raw.no_internet, getString(R.string.no_interner_connection), getString(R.string.interent_substitles))
    }

    fun showDefaultValue(image: Int, data: String, subTitle: String) {
        viewFlipper.displayedChild = 1
        noDataView.setImage(image)
        noDataView.setData(data, subTitle)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.ivAddHoliday -> {
                if (branchList.isEmpty()) {
                    view?.showErrorSnack(getString(R.string.no_clicnic_avail))
                    return
                }
                val fragment = AddHolidayFragment()
                fragment.bindFragment(this)
                val bundle = Bundle()
                bundle.putParcelableArrayList(Constants.BRANCH_LIST, branchList as ArrayList<out Parcelable>)
                fragment.arguments = bundle
                addFragment(activity!!.supportFragmentManager, fragment, R.id.container
                        , false, true, false)
            }
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {

    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val spinner = parent as Spinner
        if (spinner.id == R.id.spinnerClinics) {
            if (spinner.selectedItemPosition == 0)
                branchId = "0"
            else
                branchId = branchList[spinner.selectedItemPosition - 1].id.toString()
            getAllHolidays(branchId)
        }
    }

    override fun setAllClinics(branches: Branches?) {
        if (branches != null) {
            val list = mutableListOf<String>()
            if (branches.data.isNotEmpty()) {
                branchList.clear()
                branchList.addAll(branches.data)
                list.add(0, getString(R.string.all_clinics))
                for (i in 0..branchList.size - 1) {
                    list.add(branchList[i].name!!)
                }
            } else
                list.add(0, getString(R.string.no_clinic_avai))
            setClinicAdapter(list)
            page = branches.current_page ?: 1
        }
    }

    fun setClinicAdapter(list: MutableList<String>) {
        val aa = ArrayAdapter(context, android.R.layout.simple_spinner_item, list)
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerClinics.adapter = aa
        spinnerClinics.setSelection(0, true)
    }

    override fun getAllHolidays(branch_id: String) {
        if (isOnline(context))
            presenter.getMyHolidays(branchId)
        else
            showDefaultValue(R.raw.no_internet, getString(R.string.no_interner_connection), getString(R.string.interent_substitles))
    }

    override fun getHolidaySuccess(data: Holidays?) {
        if (data != null) {
            if (data.data.isNotEmpty()) {
                swipeRefresh.isRefreshing = false
                viewFlipper.displayedChild = 0
                holidayList.clear()
                holidayList.addAll(data.data)
                recycleHolidays.adapter.notifyDataSetChanged()
            } else {
                swipeRefresh.isRefreshing = false
                showDefaultValue(R.raw.no_data, getString(R.string.no_holiday_avail), getString(R.string.click_add_holiday))
            }
        }
    }

    override fun removeHolidaySuccess(holiday_id: Int?) {
        holidayList.remove(holidayList.filter { it.id == holiday_id }.single())
        recycleHolidays.adapter.notifyDataSetChanged()
        if (holidayList.isEmpty())
            showDefaultValue(R.raw.no_data, getString(R.string.no_holiday_avail), getString(R.string.click_add_holiday))
    }

    override fun editHoliday(holidayDataItem: HolidayDataItem) {
        val fragment = AddHolidayFragment()
        fragment.bindFragment(this)
        val bundle = Bundle()
        bundle.putParcelableArrayList(Constants.BRANCH_LIST, branchList as ArrayList<out Parcelable>)
        bundle.putString(Constants.TYPE, Constants.EDIT)
        bundle.putString(Constants.BRANCH_ID, branchId)
        bundle.putString(Constants.HOLIDAY_DATA, Gson().toJson(holidayDataItem))

        fragment.arguments = bundle
        addFragment(activity!!.supportFragmentManager, fragment, R.id.container
                , false, true, true)
    }

    override fun removeHoliday(holiday_Id: Int?) {
        presenter.removeHoliday(holiday_Id)
    }

    override fun onNewHolidayAdded() {
        getAllHolidays(branchId)
    }

    override fun onDetach() {
        super.onDetach()
        presenter.detachView()
    }
}
