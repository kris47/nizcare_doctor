package com.nizcaredoctor.views.account.settings.clinics.addClinic.timings.timeModel

data class TimeModel(
        val name: String,
        var isEnabled: Boolean = true,
        val timings: MutableList<TimeToFromModel>
)