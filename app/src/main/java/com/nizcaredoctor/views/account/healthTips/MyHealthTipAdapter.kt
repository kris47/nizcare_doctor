package com.nizcaredoctor.views.account.healthTips

import android.content.Context
import android.support.v7.widget.PopupMenu
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import com.nizcaredoctor.R
import kotlinx.android.synthetic.main.item_my_answers.view.*

class MyHealthTipAdapter(val context: Context?) : RecyclerView.Adapter<MyHealthTipAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_my_answers, parent, false))
    }

    override fun getItemCount() = 10

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        override fun onClick(v: View?) {
            when (v?.id) {
                R.id.rootQaItem -> {

                }
                R.id.ivMenu ->{
                    val menu = PopupMenu(context!!, v)
                    menu.inflate(R.menu.patient_option_meu)
                    menu.setOnMenuItemClickListener { item: MenuItem? ->
                        when (item?.itemId) {
                            R.id.remove -> {

                            }
                        }
                        true
                    }
                    menu.show()
                }
            }
        }

        init {
            itemView.rootQaItem.setOnClickListener(this)
            itemView.ivMenu.setOnClickListener(this)
        }
    }
}