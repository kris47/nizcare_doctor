package com.nizcaredoctor.views.account.settings.subsciption

import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.nizcaredoctor.R
import com.nizcaredoctor.utils.Constants
import com.nizcaredoctor.utils.PrefsManager
import com.nizcaredoctor.utils.isOnline
import com.nizcaredoctor.utils.showInternetError
import com.nizcaredoctor.views.baseViews.BaseViewActivityImpl
import com.nizcaredoctor.webservice.WebConstants
import com.nizcaredoctor.webservice.model.Profile
import com.nizcaredoctor.webservice.model.subsciptionModel.SubsciptionItemModel
import kotlinx.android.synthetic.main.activity_subscription.*


class SubscriptionActivity : BaseViewActivityImpl(), SubsciptionContract.View {

    private val presenter = SubscriptionPresenter()
    private val subsciptionList = mutableListOf<SubsciptionItemModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_subscription)
        init()
        setListeners()
        if (PrefsManager.get().getObject(Constants.USER_DETAIL, Profile::class.java)?.is_subscribed == 0) {
            tvActive.visibility = View.GONE
            cvActivePlan.visibility = View.GONE
        } else {
            tvActive.visibility = View.VISIBLE
            cvActivePlan.visibility = View.VISIBLE
        }
        if (isOnline(this)) {
            presenter.getSubscriptionPlan()
        } else {
            rootSubs.showInternetError()
        }

    }

    private fun init() {

    }

    private fun setListeners() {
        presenter.attachView(this)
        toolbar.setNavigationOnClickListener { onBackPressed() }
    }

    override fun setSubsciptionPackage(subscription_plans: MutableList<SubsciptionItemModel>?) {
        if (subscription_plans != null) {
            subsciptionList.clear()
            subsciptionList.addAll(subscription_plans)
            pager.clipToPadding = false
            pager.pageMargin = 32
            pager.adapter = SubscriptionAdapter(this, subsciptionList)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detachView()
    }

    fun subscripeForPackage(position: Int) {
        if (isOnline(this)) {
            val map = HashMap<String, Any>()
            map[WebConstants.SUBSCRIPTION_ID] = subsciptionList[position].id
            map[WebConstants.AMOUNT] = subsciptionList[position].amount
            map[WebConstants.PAYMENT_STATUS] = 1
            map[WebConstants.TRANSCATION_ID] = ""
            presenter.subscribeForPackage(map)
        } else {
            rootSubs.showInternetError()
        }
    }

    override fun subscriptionSuccess(msg: String?) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
        finish()
    }
}
