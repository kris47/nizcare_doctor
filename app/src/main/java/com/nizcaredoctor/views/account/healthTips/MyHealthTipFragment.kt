package com.nizcaredoctor.views.account.healthTips

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.nizcaredoctor.R
import com.nizcaredoctor.views.baseViews.BaseViewFragImpl
import kotlinx.android.synthetic.main.fragment_my_answer.*
import kotlinx.android.synthetic.main.fragment_patient_appointment.*
import kotlinx.android.synthetic.main.layout_toolbar.*

class MyHealthTipFragment : BaseViewFragImpl() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_my_health_tip, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        setListener()
    }

    fun init() {
        recycleView.adapter = MyHealthTipAdapter(context)
    }

    fun setListener() {
        toolbar.setNavigationOnClickListener { activity?.supportFragmentManager?.popBackStack() }
    }
}