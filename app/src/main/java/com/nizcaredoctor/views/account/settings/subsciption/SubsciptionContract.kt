package com.nizcaredoctor.views.account.settings.subsciption

import com.nizcaredoctor.views.baseViews.BasePresenter
import com.nizcaredoctor.views.baseViews.BaseView
import com.nizcaredoctor.webservice.model.subsciptionModel.SubsciptionItemModel

interface SubsciptionContract {

    interface View : BaseView {
        fun setSubsciptionPackage(subscription_plans: MutableList<SubsciptionItemModel>?)
        fun subscriptionSuccess(msg: String?)

    }

    interface Presenter : BasePresenter<View> {
        fun getSubscriptionPlan()
        fun subscribeForPackage(map: HashMap<String, Any>)

    }
}