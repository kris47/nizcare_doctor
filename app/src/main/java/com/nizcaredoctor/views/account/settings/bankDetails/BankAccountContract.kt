package com.nizcaredoctor.views.account.settings.bankDetails

import com.nizcaredoctor.views.baseViews.BasePresenter
import com.nizcaredoctor.views.baseViews.BaseView
import com.nizcaredoctor.webservice.model.bankModel.BankAccountItemModel
import com.nizcaredoctor.webservice.model.bankModel.BankAccountModel

interface BankAccountContract {

    interface View : BaseView {
        fun getAllBankAccounts()
        fun getBankSuccess(bankAccountModel: BankAccountModel?)
        fun removeBranchSuccess(user_bank_detail_id: Int)
        fun showDefaultValue(image: Int, data: String, subTitle: String)
    }

    interface Presenter : BasePresenter<View> {
        fun getAllBankAccounts()
        fun removeBankAccount(user_bank_detail_id: Int)
    }

    interface onEventClickListener {
        fun removeBankAccount(user_bank_detail_id: Int)
        fun editBankAccount(bankAccountItemModel: BankAccountItemModel?)
        fun onNewAccountAdded()
    }
}