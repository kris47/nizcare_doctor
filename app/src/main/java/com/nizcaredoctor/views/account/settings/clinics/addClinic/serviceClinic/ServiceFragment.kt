package com.nizcaredoctor.views.account.settings.clinics.addClinic.serviceClinic

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.google.gson.Gson
import com.nizcaredoctor.R
import com.nizcaredoctor.utils.Constants
import com.nizcaredoctor.utils.isOnline
import com.nizcaredoctor.utils.showErrorSnack
import com.nizcaredoctor.utils.showInternetError
import com.nizcaredoctor.views.account.settings.clinics.addClinic.BranchModel
import com.nizcaredoctor.views.baseViews.BaseViewFragImpl
import com.nizcaredoctor.webservice.model.GetItemModel
import kotlinx.android.synthetic.main.fragment_service.*

class ServiceFragment : BaseViewFragImpl(), ServiceContract.View, View.OnClickListener {

    private val presenter = ServicePresenter()
    private val allServicesList = mutableListOf<GetItemModel>()
    private val selectedServiceList = mutableListOf<GetItemModel>()
    private val searchKeyWord = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_service, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        setListeners()

        if (isOnline(activity)) {
            presenter.getServiceTreatments(searchKeyWord)
        } else {
            view.showInternetError()
        }
    }

    private fun init() {
        rvServicesAdded.adapter = ServicesAdapter(activity, selectedServiceList, Constants.SELECTED_SERVICE, this)
        rvServices.adapter = ServicesAdapter(activity, allServicesList, Constants.ALL_SERVICE, this)
    }

    private fun setListeners() {
        presenter.attachView(this)
        tvSaveServices.setOnClickListener(this)
    }

    override fun addServicesSuccess(msg: String?) {
        Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show()
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.tvSaveServices -> {
                if (isOnline(context)) {
                    if (selectedServiceList.isEmpty()) {
                        view?.showErrorSnack("Selected at least one service for clinic")
                    } else {
                        val selectedIds = MutableList(selectedServiceList.size) { it -> selectedServiceList[it].id }
                        presenter.addServices(Gson().toJson(selectedIds), activity?.intent?.getIntExtra("branchId", 0))
                    }
                } else {
                    view?.showInternetError()
                }
            }
        }
    }

    override fun setAllServices(treatSymSurgeries: MutableList<GetItemModel>?) {
        if (treatSymSurgeries != null) {
            allServicesList.clear()
            if (activity?.intent?.getStringExtra("type") == Constants.EDIT) {
                val branchModel = Gson().fromJson(activity?.intent?.getStringExtra(Constants.CLINIC_DATA), BranchModel::class.java)
                val newAllServiceList = mutableListOf<GetItemModel>()
                newAllServiceList.addAll(treatSymSurgeries)

                for (i in 0 until treatSymSurgeries.size)
                    for (j in 0 until branchModel.treatments.size)
                        if (branchModel.treatments[j].treatment_id == treatSymSurgeries[i].id) {
                            selectedServiceList.add(treatSymSurgeries[i])
                        }
                /*for (i in 0 until treatSymSurgeries.size)
                    for (j in 0 until branchModel.treatments.size)
                        if (branchModel.treatments[j].treatment_id == treatSymSurgeries[i].id) {
                            newAllServiceList.removeAt(i)
                        }*/

                allServicesList.addAll(newAllServiceList)
                rvServicesAdded.adapter.notifyDataSetChanged()
                rvServices.adapter.notifyDataSetChanged()

                if (selectedServiceList.isNotEmpty()) {
                    rlAdded.visibility = View.VISIBLE
                }
            } else {
                allServicesList.addAll(treatSymSurgeries)
                rvServices.adapter.notifyDataSetChanged()
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        presenter.detachView()
    }

    fun addService(getItemModel: GetItemModel, adapterPosition: Int) {
        selectedServiceList.add(getItemModel)
        allServicesList.removeAt(adapterPosition)
        rvServicesAdded.adapter.notifyItemInserted(selectedServiceList.size - 1)
        rvServices.adapter.notifyItemRemoved(adapterPosition)
        if (selectedServiceList.isNotEmpty()) {
            rlAdded.visibility = View.VISIBLE
        }
    }

    fun removeService(getItemPos: Int, getItemModel: GetItemModel) {
        if (getItemPos != -1) {
            selectedServiceList.removeAt(getItemPos)
            allServicesList.add(getItemModel)
            rvServicesAdded.adapter.notifyItemRemoved(getItemPos)
            rvServices.adapter.notifyItemInserted(allServicesList.size - 1)
            if (selectedServiceList.isEmpty()) {
                rlAdded.visibility = View.GONE
            }
        }
    }
}