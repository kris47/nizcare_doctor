package com.nizcaredoctor.views.account.settings.holidays

import com.nizcaredoctor.views.baseViews.BasePresenter
import com.nizcaredoctor.views.baseViews.BaseView
import com.nizcaredoctor.webservice.model.branchModel.Branches
import com.nizcaredoctor.webservice.model.holidayModel.HolidayDataItem
import com.nizcaredoctor.webservice.model.holidayModel.Holidays

interface MyHolidaysContract {
    interface View : BaseView {
        fun setAllClinics(branches: Branches?)
        fun getAllHolidays(branch_id: String)
        fun getHolidaySuccess(data: Holidays?)
        fun removeHolidaySuccess(holiday_id: Int?)
    }

    interface presenter : BasePresenter<View> {
        fun getMyClinics(page: Int)
        fun getMyHolidays(branch_id: String)
        fun removeHoliday(holiday_id : Int?)
    }

    interface OnEventClickListener {
        fun editHoliday(holidayDataItem: HolidayDataItem)
        fun removeHoliday(holiday_Id: Int?)
        fun onNewHolidayAdded()
    }
}