package com.nizcaredoctor.views.account.settings.clinics.addClinic.timings

import com.nizcaredoctor.views.account.settings.clinics.addClinic.timings.timeModel.AllWorkingItem
import com.nizcaredoctor.views.baseViews.BasePresenter
import com.nizcaredoctor.views.baseViews.BaseView

interface TimingContract {

    interface View : BaseView {
        fun timingSuccess(msg: String?)
        fun allTimingsSuccess(allWorkingHours: MutableList<AllWorkingItem>?)

    }

    interface Presenter : BasePresenter<View> {
        fun addTimingClinic(map: HashMap<String, String>)
        fun getAllBranchTimingsTimings()

    }
}