package com.nizcaredoctor.views.account.wallet

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.nizcaredoctor.R
import com.nizcaredoctor.utils.addFragment
import com.nizcaredoctor.views.account.wallet.addMoney.AddMoneyFragment
import com.nizcaredoctor.views.baseViews.BaseViewFragImpl
import com.nizcaredoctor.views.patients.addPatient.AddPatientFragment
import kotlinx.android.synthetic.main.activity_wallet.*

class WalletFragment : BaseViewFragImpl(), View.OnClickListener {
    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.tvAddBalance -> {
                addFragment(activity!!.supportFragmentManager, AddMoneyFragment(), R.id.container
                        , false, true, true)
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.activity_wallet, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setListeners()
        init()
    }

    fun setListeners() {
        toolbar.setNavigationOnClickListener { activity?.supportFragmentManager?.popBackStack() }
        tvAddBalance.setOnClickListener(this)
    }

    fun init() {
        val fragmentList = mutableListOf<Fragment>()
        fragmentList.add(AllTransactionFragment())
        fragmentList.add(AllTransactionFragment())
        fragmentList.add(AllTransactionFragment())
        viewPagerWallet.adapter = WalletAdapter(context, fragmentList, activity!!.supportFragmentManager)
        tabWallet.setupWithViewPager(viewPagerWallet)
    }

}
