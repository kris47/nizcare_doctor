package com.nizcaredoctor.views.account.myAnswers

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.nizcaredoctor.R
import com.nizcaredoctor.utils.addFragment
import com.nizcaredoctor.views.baseViews.BaseViewFragImpl
import kotlinx.android.synthetic.main.fragment_my_answer.*
import kotlinx.android.synthetic.main.fragment_patient_appointment.*
import kotlinx.android.synthetic.main.layout_toolbar.*

class MyAnswerFragment : BaseViewFragImpl(), MyAnswerContract.View,
        MyAnswerContract.OnEventClickListener {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_my_answer, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        setListener()
    }

    fun init() {
        tvTitle.text = getString(R.string.my_answers)
        recycleView.adapter = MyAnswerAdapter(context, this)
    }

    fun setListener() {
        toolbar.setNavigationOnClickListener { activity?.supportFragmentManager?.popBackStack() }
    }

    override fun onItemClick() {
        addFragment(activity!!.supportFragmentManager, MyAnswerDetailFragment(), R.id.container
                , false, true, true)
    }
}