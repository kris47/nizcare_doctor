package com.nizcaredoctor.views.account.settings

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.nizcaredoctor.R
import com.nizcaredoctor.utils.PrefsManager
import com.nizcaredoctor.utils.addFragment
import com.nizcaredoctor.views.account.settings.bankDetails.BankAccountListFragment
import com.nizcaredoctor.views.account.settings.clinics.MyClinicFragment
import com.nizcaredoctor.views.account.settings.holidays.MyHolidayFragment
import com.nizcaredoctor.views.account.settings.signature.SignatureFragment
import com.nizcaredoctor.views.account.settings.subsciption.SubscriptionActivity
import com.nizcaredoctor.views.baseViews.BaseViewFragImpl
import com.nizcaredoctor.views.splash.MainActivity
import kotlinx.android.synthetic.main.activity_settings.*

class SettingsFragment : BaseViewFragImpl(), View.OnClickListener {

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.tvLogOut -> {
                PrefsManager.get().removeAll()
                startActivity(Intent(context, MainActivity::class.java))
                activity?.finishAffinity()
            }
            R.id.llSubscription -> {
                startActivity(Intent(context, SubscriptionActivity::class.java))
            }
            R.id.llClinic -> {
                addFragment(activity!!.supportFragmentManager, MyClinicFragment(), R.id.container
                        , false, true, true)
            }
            R.id.llHolidays -> {
                addFragment(activity!!.supportFragmentManager, MyHolidayFragment(), R.id.container
                        , false, true, true)
            }
            R.id.llBankDetails -> {
                addFragment(activity!!.supportFragmentManager, BankAccountListFragment(), R.id.container
                        , false, true, true)
            }
            R.id.llSignatures ->{
                addFragment(activity!!.supportFragmentManager, SignatureFragment(), R.id.container
                        , false, true, true)
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.activity_settings, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setListener()
    }

    fun setListener() {
        tvLogOut.setOnClickListener(this)
        llSubscription.setOnClickListener(this)
        llClinic.setOnClickListener(this)
        llHolidays.setOnClickListener(this)
        llBankDetails.setOnClickListener(this)
        llSignatures.setOnClickListener(this)
        toolbar.setNavigationOnClickListener {
            activity?.onBackPressed()
        }
    }
}
