package com.nizcaredoctor.views.account.settings.clinics.addClinic.serviceClinic

import com.nizcaredoctor.views.baseViews.BasePresenter
import com.nizcaredoctor.views.baseViews.BaseView
import com.nizcaredoctor.webservice.model.GetItemModel

interface ServiceContract {
    interface View : BaseView {
        fun setAllServices(treatSymSurgeries: MutableList<GetItemModel>?)
        fun addServicesSuccess(msg: String?)

    }

    interface Presenter : BasePresenter<View> {
        fun getServiceTreatments(searchKeyWord: String)
        fun addServices(ids: String?, branchId: Int?)

    }
}