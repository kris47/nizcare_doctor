package com.nizcaredoctor.views.account.settings.clinics.addClinic.timings

import android.app.Activity
import android.app.TimePickerDialog
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.nizcaredoctor.R
import com.nizcaredoctor.utils.getDateFromString
import com.nizcaredoctor.views.account.settings.clinics.addClinic.timings.timeModel.TimeToFromModel
import com.nizcaredoctor.webservice.DialogPopup
import kotlinx.android.synthetic.main.item_to_from.view.*
import java.util.*

class TimeToFromAdapter(val activity: Activity?, val timeList: MutableList<TimeToFromModel>) : RecyclerView.Adapter<TimeToFromAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(activity).inflate(R.layout.item_to_from, parent, false))
    }

    override fun getItemCount(): Int {
        return timeList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindTimeView(timeList[position])
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val timeListener = TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
            val min: String = if (minute.toString().length == 1) {
                "0$minute"
            } else {
                minute.toString()
            }
            val timeStart = hourOfDay.toString() + ":" + min

            if (timeList[adapterPosition].time_to != "Select")
                if (getDateFromString(timeStart).after(getDateFromString(timeList[adapterPosition].time_to))) {
                    Toast.makeText(activity, "Time must be before end time", Toast.LENGTH_SHORT).show()
                    return@OnTimeSetListener
                }

            if (timeList.size != 1) {
                for (i in 0 until timeList.size - 1) {
                    if (adapterPosition != i)
                        if (getDateFromString(timeStart).before(getDateFromString(timeList[i].time_to))) {
                            Toast.makeText(activity, "You can't select time from previous slot already selected", Toast.LENGTH_SHORT).show()
                            return@OnTimeSetListener
                        }
                }
            }

            itemView.tvFrom.text = timeStart
            timeList[adapterPosition].time_from = itemView.tvFrom.text.toString()
        }

        private val timeToListener = TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->

            val min: String = if (minute.toString().length == 1) {
                "0$minute"
            } else {
                minute.toString()
            }

            val timeEnd = hourOfDay.toString() + ":" + min
            if (timeList[adapterPosition].time_from != "Select")
                if (getDateFromString(timeList[adapterPosition].time_from).after(getDateFromString(timeEnd))) {
                    Toast.makeText(activity, "Time must be after start time", Toast.LENGTH_SHORT).show()
                    return@OnTimeSetListener
                }

            itemView.tvTo.text = timeEnd
            timeList[adapterPosition].time_to = timeEnd
        }

        init {
            itemView.tvFrom.setOnClickListener {
                var hour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY)
                var minute = Calendar.getInstance().get(Calendar.MINUTE)
                if (timeList[adapterPosition].time_from != "Select") {
                    val cal = Calendar.getInstance()
                    cal.timeInMillis = getDateFromString(timeList[adapterPosition].time_from).time
                    hour = cal.get(Calendar.HOUR_OF_DAY)
                    minute = cal.get(Calendar.MINUTE)
                }

                val timePicker = TimePickerDialog(activity, timeListener,
                        hour, minute, true)
                timePicker.show()
            }

            itemView.tvTo.setOnClickListener {

                var hour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY)
                var minute = Calendar.getInstance().get(Calendar.MINUTE)
                if (timeList[adapterPosition].time_to != "Select") {
                    val cal = Calendar.getInstance()
                    cal.timeInMillis = getDateFromString(timeList[adapterPosition].time_to).time
                    hour = cal.get(Calendar.HOUR_OF_DAY)
                    minute = cal.get(Calendar.MINUTE)
                }
                val timePicker = TimePickerDialog(activity, timeToListener,
                        hour, minute, true)
                timePicker.show()
            }

            itemView.ivRemoveTime.setOnClickListener {
                if (timeList.size == 1) {
                    DialogPopup().alertPopup(activity, "", "You have to add at least one time for selected day", "")
                } else {
                    timeList.removeAt(adapterPosition)
                    notifyItemRemoved(adapterPosition)
                }
            }

        }

        fun bindTimeView(timeToFromModel: TimeToFromModel) = with(itemView) {
            tvFrom.text = timeToFromModel.time_from
            tvTo.text = timeToFromModel.time_to
        }
    }
}
