package com.nizcaredoctor.views.account.settings.bankDetails

import android.content.Context
import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.nizcaredoctor.R
import com.nizcaredoctor.utils.Constants
import com.nizcaredoctor.utils.addFragment
import com.nizcaredoctor.utils.isOnline
import com.nizcaredoctor.utils.showInternetError
import com.nizcaredoctor.views.account.settings.bankDetails.addBankDetail.AddBankDetailsFragment
import com.nizcaredoctor.views.baseViews.BaseViewFragImpl
import com.nizcaredoctor.webservice.model.bankModel.BankAccountItemModel
import com.nizcaredoctor.webservice.model.bankModel.BankAccountModel
import kotlinx.android.synthetic.main.fragment_bank_account_list.*
import kotlinx.android.synthetic.main.layout_toolbar.*

class BankAccountListFragment : BaseViewFragImpl(), View.OnClickListener,
        BankAccountContract.View, BankAccountContract.onEventClickListener, SwipeRefreshLayout.OnRefreshListener {

    private val presenter = BankAccountPresenter()
    private var bankAccountList = mutableListOf<BankAccountItemModel>()

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.ivAddBank -> {
                val fragment = AddBankDetailsFragment()
                fragment.bindListener(this)
                addFragment(activity!!.supportFragmentManager, fragment, R.id.container
                        , false, true, true)
            }
        }
    }

    override fun onRefresh() {
        getAllBankAccounts()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_bank_account_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        setListener()
    }

    fun init() {
        rvBankAccounts.adapter = BankAccountListAdapter(context, bankAccountList, this)
        tvTitle.text = getString(R.string.bank_account)
        getAllBankAccounts()
    }

    fun setListener() {
        toolbar.setNavigationOnClickListener { activity?.supportFragmentManager?.popBackStack() }
        ivAddBank.setOnClickListener(this)
        swipeRefresh.setOnRefreshListener(this)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        presenter.attachView(this)
    }

    override fun onDetach() {
        super.onDetach()
        presenter.detachView()
    }

    override fun getAllBankAccounts() {
        if (isOnline(context))
            presenter.getAllBankAccounts()
        else showDefaultValue(R.raw.no_internet, getString(R.string.no_interner_connection), getString(R.string.interent_substitles))
    }

    override fun getBankSuccess(bankAccountModel: BankAccountModel?) {
        if (bankAccountModel?.bankAccounts != null && bankAccountModel.bankAccounts.isNotEmpty()) {
            swipeRefresh.isRefreshing = false
            bankAccountList.clear()
            bankAccountList.addAll(bankAccountModel.bankAccounts)
            rvBankAccounts.adapter.notifyDataSetChanged()
        } else {
            swipeRefresh.isRefreshing = false
            showDefaultValue(R.raw.no_data, getString(R.string.no_account_available), getString(R.string.click_add_bank))
        }
    }

    override fun removeBankAccount(user_bank_detail_id: Int) {
        if (isOnline(context))
            presenter.removeBankAccount(user_bank_detail_id)
        else view?.showInternetError()
    }

    override fun removeBranchSuccess(user_bank_detail_id: Int) {
        bankAccountList.remove(bankAccountList.filter { it.id == user_bank_detail_id }.single())
        rvBankAccounts.adapter.notifyDataSetChanged()
        if (bankAccountList.isEmpty())
            showDefaultValue(R.raw.no_data, getString(R.string.no_account_available), getString(R.string.click_add_bank))
    }

    override fun showDefaultValue(image: Int, data: String, subTitle: String) {
        viewFlipper.displayedChild = 1
        noDataView.setImage(image)
        noDataView.setData(data, subTitle)
    }

    override fun onNewAccountAdded() {
        getAllBankAccounts()
    }

    override fun editBankAccount(bankAccountItemModel: BankAccountItemModel?) {
        val fragment = AddBankDetailsFragment()
        fragment.bindListener(this)
        val bundle = Bundle()
        bundle.putString(Constants.TYPE, Constants.EDIT)
        bundle.putSerializable(Constants.BANK_DATA, bankAccountItemModel)
        fragment.arguments = bundle
        addFragment(activity!!.supportFragmentManager, fragment, R.id.container
                , false, true, true)
    }
}