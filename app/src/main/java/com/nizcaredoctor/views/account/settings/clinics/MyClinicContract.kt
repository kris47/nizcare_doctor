package com.nizcaredoctor.views.account.settings.clinics

import com.nizcaredoctor.views.account.settings.clinics.addClinic.BranchModel
import com.nizcaredoctor.views.baseViews.BasePresenter
import com.nizcaredoctor.views.baseViews.BaseView
import com.nizcaredoctor.webservice.model.branchModel.Branches

interface MyClinicContract {

    interface View : BaseView {
        fun setAllBranches(branches: Branches?)
        fun getBranchSuccess(branch: BranchModel?)
        fun removeBranch(id: Int?)
        fun removeBranchSuccess(id: Int?)
    }

    interface Presenter : BasePresenter<View> {
        fun getMyClinics(page: Int)
        fun getBranchApi(id: Int?)
        fun removeBranch(branch_id: Int?)
    }

    interface OnEventClickListener {
        fun getBranchAllData(id: Int?)
        fun removeBranch(id: Int?)
    }
}