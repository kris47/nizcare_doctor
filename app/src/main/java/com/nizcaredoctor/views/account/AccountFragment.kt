package com.nizcaredoctor.views.account

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.nizcaredoctor.R
import com.nizcaredoctor.utils.Constants
import com.nizcaredoctor.utils.PrefsManager
import com.nizcaredoctor.utils.addFragment
import com.nizcaredoctor.views.account.bookmarks.BookMarkFragment
import com.nizcaredoctor.views.account.healthTips.MyHealthTipFragment
import com.nizcaredoctor.views.account.myAnswers.MyAnswerFragment
import com.nizcaredoctor.views.account.notification.NotificationFragment
import com.nizcaredoctor.views.account.profile.ProfileActivity
import com.nizcaredoctor.views.account.referralCode.ReferralCodeFragment
import com.nizcaredoctor.views.account.referrals.ReferalFragment
import com.nizcaredoctor.views.account.settings.SettingsActivity
import com.nizcaredoctor.views.account.wallet.WalletFragment
import com.nizcaredoctor.webservice.model.Profile
import kotlinx.android.synthetic.main.fragment_account.*

class AccountFragment : Fragment(), View.OnClickListener {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_account, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        setListeners()
    }

    private fun init() {
        tvProfileName.text = PrefsManager.get().getObject(Constants.USER_DETAIL, Profile::class.java)?.firstname
        activity?.let {
            Glide.with(it)
                    .load(PrefsManager.get().getObject(Constants.USER_DETAIL, Profile::class.java)?.image)
                    .apply(RequestOptions().placeholder(R.drawable.a2))
                    .into(ivProfilePic)
        }
    }

    private fun setListeners() {
        tvViewProfile.setOnClickListener(this)
        rlWallet.setOnClickListener(this)
        tvSettings.setOnClickListener(this)
        rlMyAnswers.setOnClickListener(this)
        tvReferrals.setOnClickListener(this)
        tvMyHealthTips.setOnClickListener(this)
        tvNotifications.setOnClickListener(this)
        tvReferralCode.setOnClickListener(this)
        tvMyBookMarks.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.tvMyBookMarks -> {
                addFragment(activity!!.supportFragmentManager, BookMarkFragment(), R.id.container
                        , false, true, true)
            }
            R.id.tvReferralCode -> {
                addFragment(activity!!.supportFragmentManager, ReferralCodeFragment(), R.id.container
                        , false, true, true)
            }
            R.id.tvNotifications -> {
                addFragment(activity!!.supportFragmentManager, NotificationFragment(), R.id.container
                        , false, true, true)
            }
            R.id.tvMyHealthTips -> {
                addFragment(activity!!.supportFragmentManager, MyHealthTipFragment(), R.id.container
                        , false, true, true)
            }
            R.id.tvReferrals -> {
                addFragment(activity!!.supportFragmentManager, ReferalFragment(), R.id.container
                        , false, true, true)
            }
            R.id.rlMyAnswers -> {
                addFragment(activity!!.supportFragmentManager, MyAnswerFragment(), R.id.container
                        , false, true, true)
            }
            R.id.tvViewProfile -> {
                startActivity(Intent(activity, ProfileActivity::class.java))
            }
            R.id.rlWallet -> {
                addFragment(activity!!.supportFragmentManager, WalletFragment(), R.id.container
                        , false, true, true)
            }
            R.id.tvSettings -> startActivity(Intent(activity, SettingsActivity::class.java))
        }
    }
}