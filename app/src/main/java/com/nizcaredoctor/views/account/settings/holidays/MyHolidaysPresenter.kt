package com.nizcaredoctor.views.account.settings.holidays

import com.nizcaredoctor.utils.getAccessToken
import com.nizcaredoctor.views.baseViews.BasePresenterImpl
import com.nizcaredoctor.webservice.ApiResponseModel
import com.nizcaredoctor.webservice.RetrofitClient
import com.nizcaredoctor.webservice.WebConstants
import com.nizcaredoctor.webservice.model.branchModel.MyClinicModel
import com.nizcaredoctor.webservice.model.holidayModel.HolidayModel
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MyHolidaysPresenter : BasePresenterImpl<MyHolidaysContract.View>(), MyHolidaysContract.presenter {

    override fun removeHoliday(holiday_id: Int?) {
        getView()?.showProgress()
        RetrofitClient.getApi().removeHoliday(getAccessToken(), holiday_id.toString())
                .enqueue(object : Callback<ApiResponseModel<Any>> {
                    override fun onFailure(call: Call<ApiResponseModel<Any>>?, t: Throwable?) {
                        getView()?.displayFailure(t?.message)
                        getView()?.dismissProgress()
                    }

                    override fun onResponse(call: Call<ApiResponseModel<Any>>?, response: Response<ApiResponseModel<Any>>) {
                        getView()?.dismissProgress()
                        if (response.isSuccessful)
                            getView()?.removeHolidaySuccess(holiday_id)
                        else
                            getView()?.displayError(JSONObject(response.errorBody()?.string()).getString("msg"), response.code())
                    }
                })
    }

    override fun getMyHolidays(branch_id: String) {
        getView()?.showProgress()
        val map = HashMap<String, String>()
        if (!branch_id.equals("0"))
            map.put(WebConstants.BRANCH_ID, branch_id)
        RetrofitClient.getApi().getHolidays(getAccessToken(), map)
                .enqueue(object : Callback<ApiResponseModel<HolidayModel>> {
                    override fun onFailure(call: Call<ApiResponseModel<HolidayModel>>?, t: Throwable?) {
                        getView()?.displayFailure(t?.message)
                        getView()?.dismissProgress()
                    }

                    override fun onResponse(call: Call<ApiResponseModel<HolidayModel>>?, response: Response<ApiResponseModel<HolidayModel>>) {
                        getView()?.dismissProgress()
                        if (response.isSuccessful)
                            getView()?.getHolidaySuccess(response.body()?.data?.holidays)
                        else
                            getView()?.displayError(JSONObject(response.errorBody()?.string()).getString("msg"), response.code())
                    }
                })
    }

    override fun getMyClinics(page: Int) {
        getView()?.showProgress()
        RetrofitClient.getApi().getMyClinics(getAccessToken(), page)
                .enqueue(object : Callback<ApiResponseModel<MyClinicModel>> {
                    override fun onFailure(call: Call<ApiResponseModel<MyClinicModel>>?, t: Throwable?) {
                        getView()?.displayFailure(t?.message)
                        getView()?.dismissProgress()
                    }

                    override fun onResponse(call: Call<ApiResponseModel<MyClinicModel>>?, response: Response<ApiResponseModel<MyClinicModel>>) {
                        getView()?.dismissProgress()
                        if (response.isSuccessful)
                            getView()?.setAllClinics(response.body()?.data?.branches)
                        else getView()?.displayError(JSONObject(response.errorBody()?.string()).getString("msg"), response.code())
                    }
                })
    }
}