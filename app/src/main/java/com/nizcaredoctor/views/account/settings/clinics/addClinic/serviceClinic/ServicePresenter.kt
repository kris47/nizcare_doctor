package com.nizcaredoctor.views.account.settings.clinics.addClinic.serviceClinic

import com.nizcaredoctor.utils.getAccessToken
import com.nizcaredoctor.views.baseViews.BasePresenterImpl
import com.nizcaredoctor.webservice.ApiResponseModel
import com.nizcaredoctor.webservice.RetrofitClient
import com.nizcaredoctor.webservice.WebConstants
import com.nizcaredoctor.webservice.model.ServiceTreatmentModel
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ServicePresenter : BasePresenterImpl<ServiceContract.View>(), ServiceContract.Presenter {
    override fun addServices(ids: String?, branchId: Int?) {
        getView()?.showProgress()
        RetrofitClient.getApi().addServicesForClinic(getAccessToken(), ids, branchId)
                .enqueue(object : Callback<ApiResponseModel<Any>> {
                    override fun onFailure(call: Call<ApiResponseModel<Any>>?, t: Throwable?) {
                        getView()?.displayFailure(t?.message)
                        getView()?.dismissProgress()
                    }

                    override fun onResponse(call: Call<ApiResponseModel<Any>>?, response: Response<ApiResponseModel<Any>>) {
                        getView()?.dismissProgress()

                            if (response.isSuccessful) {
                                getView()?.addServicesSuccess(response.body()?.msg)
                            } else
                                getView()?.displayError(JSONObject(response.errorBody()?.string()).getString("msg"), response.code())
                    }
                })
    }

    override fun getServiceTreatments(searchKeyWord: String) {
        RetrofitClient.getApi().getServicesForClinic(getAccessToken(), searchKeyWord, WebConstants.Treatment)
                .enqueue(object : Callback<ApiResponseModel<ServiceTreatmentModel>> {
                    override fun onFailure(call: Call<ApiResponseModel<ServiceTreatmentModel>>?, t: Throwable?) {
                        getView()?.displayFailure(t?.message)
                    }

                    override fun onResponse(call: Call<ApiResponseModel<ServiceTreatmentModel>>?, response: Response<ApiResponseModel<ServiceTreatmentModel>>) {
                            if (response.isSuccessful) {
                                getView()?.setAllServices(response.body()?.data?.treatSymSurgeries)
                            } else
                                getView()?.displayError(JSONObject(response.errorBody()?.string()).getString("msg"), response.code())
                            }

                })
    }
}