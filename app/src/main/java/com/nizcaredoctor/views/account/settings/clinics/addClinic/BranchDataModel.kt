package com.nizcaredoctor.views.account.settings.clinics.addClinic

data class BranchDataModel(
        val branch: BranchModel
)