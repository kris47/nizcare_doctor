package com.nizcaredoctor.views.account.settings.clinics.addClinic.basicInfo

import com.nizcaredoctor.views.baseViews.BasePresenter
import com.nizcaredoctor.views.baseViews.BaseView
import com.nizcaredoctor.webservice.model.GetItemModel

interface BasicInfoContract {
    interface View : BaseView {
        fun setServiceCategory(serviceCategories: MutableList<GetItemModel>?)
        fun clinicSuccess(msg: String?)
    }


    interface Presenter : BasePresenter<View> {
        fun getServiceCategories()
        fun addClinic(map: HashMap<String, String>)
    }
}