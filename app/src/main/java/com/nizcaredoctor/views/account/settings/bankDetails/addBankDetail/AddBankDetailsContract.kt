package com.nizcaredoctor.views.account.settings.bankDetails.addBankDetail

import com.nizcaredoctor.views.baseViews.BasePresenter
import com.nizcaredoctor.views.baseViews.BaseView

interface AddBankDetailsContract {
    interface View : BaseView {
        fun addBankAccountSuccess(msg: String?)
    }

    interface Presenter : BasePresenter<View> {
        fun addBankAccount(map: HashMap<String, String>)
    }
}