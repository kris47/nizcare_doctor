package com.nizcaredoctor.views.account.settings.clinics

import com.nizcaredoctor.utils.getAccessToken
import com.nizcaredoctor.views.account.settings.clinics.addClinic.BranchDataModel
import com.nizcaredoctor.views.baseViews.BasePresenterImpl
import com.nizcaredoctor.webservice.ApiResponseModel
import com.nizcaredoctor.webservice.RetrofitClient
import com.nizcaredoctor.webservice.WebConstants
import com.nizcaredoctor.webservice.model.branchModel.MyClinicModel
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MyClinicPresenter : BasePresenterImpl<MyClinicContract.View>(), MyClinicContract.Presenter {

    override fun removeBranch(id: Int?) {
        val map = HashMap<String, String>()
        map.put(WebConstants.BRANCH_ID, id.toString())
        getView()?.showProgress()
        RetrofitClient.getApi().removeBranch(getAccessToken(), map)
                .enqueue(object : Callback<ApiResponseModel<Any>> {
                    override fun onFailure(call: Call<ApiResponseModel<Any>>?, t: Throwable?) {
                        getView()?.dismissProgress()
                        getView()?.displayFailure(t?.message)
                    }

                    override fun onResponse(call: Call<ApiResponseModel<Any>>?, response: Response<ApiResponseModel<Any>>) {
                        getView()?.dismissProgress()
                        if (response.isSuccessful) {
                            getView()?.removeBranchSuccess(id)
                        } else {
                            getView()?.displayError(JSONObject(response.errorBody()?.string()).getString("msg"), response.code())
                        }
                    }
                })
    }

    override fun getBranchApi(id: Int?) {
        getView()?.showProgress()
        RetrofitClient.getApi().getBranchDetail(getAccessToken(), id)
                .enqueue(object : Callback<ApiResponseModel<BranchDataModel>> {
                    override fun onFailure(call: Call<ApiResponseModel<BranchDataModel>>?, t: Throwable?) {
                        getView()?.dismissProgress()
                        getView()?.displayFailure(t?.message)
                    }

                    override fun onResponse(call: Call<ApiResponseModel<BranchDataModel>>?, response: Response<ApiResponseModel<BranchDataModel>>) {
                        getView()?.dismissProgress()
                        if (response.isSuccessful)
                            getView()?.getBranchSuccess(response.body()?.data?.branch)
                        else
                            getView()?.displayError(JSONObject(response.errorBody()?.string()).getString("msg"), response.code())
                    }
                })
    }

    override fun getMyClinics(page: Int) {
        getView()?.showProgress()
        RetrofitClient.getApi().getMyClinics(getAccessToken(), page)
                .enqueue(object : Callback<ApiResponseModel<MyClinicModel>> {
                    override fun onFailure(call: Call<ApiResponseModel<MyClinicModel>>?, t: Throwable?) {
                        getView()?.displayFailure(t?.message)
                        getView()?.dismissProgress()
                    }

                    override fun onResponse(call: Call<ApiResponseModel<MyClinicModel>>?, response: Response<ApiResponseModel<MyClinicModel>>) {
                        getView()?.dismissProgress()
                        if (response.isSuccessful)
                            getView()?.setAllBranches(response.body()?.data?.branches)
                        else
                            getView()?.displayError(JSONObject(response.errorBody()?.string()).getString("msg"), response.code())
                    }
                })
    }
}