package com.nizcaredoctor.views.account.settings.signature

import com.nizcaredoctor.utils.getAccessToken
import com.nizcaredoctor.views.baseViews.BasePresenterImpl
import com.nizcaredoctor.webservice.ApiResponseModel
import com.nizcaredoctor.webservice.RetrofitClient
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SignaturePresenter : BasePresenterImpl<SignatureContract.View>(), SignatureContract.Presenter {
    override fun addEditSignature(signature: String?) {
        getView()?.showProgress()
        RetrofitClient.getApi().addEditSignature(getAccessToken(), signature)
                .enqueue(object : Callback<ApiResponseModel<Any>> {
                    override fun onFailure(call: Call<ApiResponseModel<Any>>?, t: Throwable?) {
                        getView()?.displayFailure(t?.message)
                        getView()?.dismissProgress()
                    }

                    override fun onResponse(call: Call<ApiResponseModel<Any>>?, response: Response<ApiResponseModel<Any>>) {
                        getView()?.dismissProgress()
                        if (response.isSuccessful)
                        else
                            getView()?.displayError(JSONObject(response.errorBody()?.string()).getString("msg"), response.code())
                    }
                })
    }

}