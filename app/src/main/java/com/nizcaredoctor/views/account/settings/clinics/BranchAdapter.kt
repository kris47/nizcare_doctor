package com.nizcaredoctor.views.account.settings.clinics

import android.app.Activity
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.nizcaredoctor.R
import com.nizcaredoctor.webservice.DialogPopup
import com.nizcaredoctor.webservice.model.branchModel.ClinicDataItem
import kotlinx.android.synthetic.main.item_clinic.view.*

class BranchAdapter(val context: Context?, var branchList: MutableList<ClinicDataItem>,
                    val onEventClickListener: MyClinicContract.OnEventClickListener) : RecyclerView.Adapter<BranchAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_clinic, parent, false))
    }

    override fun getItemCount(): Int {
        return branchList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindView(branchList[position])
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        override fun onClick(v: View?) {
            when (v?.id) {
                R.id.rootBranch -> {
                    if (branchList[adapterPosition].status == 0) {
                        DialogPopup().alertPopup(context as Activity?, context?.resources?.getString(R.string.dialog_alert), "This clinic is under verification. After verification you can enjoy this feature.", "Error")
                    } else {
                        onEventClickListener.getBranchAllData(branchList[adapterPosition].id)
                    }
                }
                R.id.tvClinicDelete -> {
                    onEventClickListener.removeBranch(branchList[adapterPosition].id)
                }
            }
        }

        init {
            itemView.rootBranch.setOnClickListener(this)
            itemView.tvClinicDelete.setOnClickListener(this)

        }

        fun bindView(clinicDataItem: ClinicDataItem) = with(itemView) {
            tvClinicName.text = clinicDataItem.name
            tvClinicFee.text = clinicDataItem.consultation_fees + " " + context.getString(R.string.consultation_fees)
            tvClinicAddress.text = clinicDataItem.address
        }
    }

}
