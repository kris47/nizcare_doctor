package com.nizcaredoctor.views.account.settings.clinics.addClinic

data class ImageModel(
        var branch_id: Int?,
        var image: String?
)