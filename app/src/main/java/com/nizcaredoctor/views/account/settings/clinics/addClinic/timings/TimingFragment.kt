package com.nizcaredoctor.views.account.settings.clinics.addClinic.timings

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.google.gson.Gson
import com.nizcaredoctor.R
import com.nizcaredoctor.utils.*
import com.nizcaredoctor.views.account.settings.clinics.addClinic.BranchModel
import com.nizcaredoctor.views.account.settings.clinics.addClinic.timings.timeModel.AllWorkingItem
import com.nizcaredoctor.views.account.settings.clinics.addClinic.timings.timeModel.TimeModel
import com.nizcaredoctor.views.account.settings.clinics.addClinic.timings.timeModel.TimeToFromModel
import com.nizcaredoctor.views.baseViews.BaseViewFragImpl
import com.nizcaredoctor.webservice.WebConstants
import kotlinx.android.synthetic.main.fragment_timing.*

class TimingFragment : BaseViewFragImpl(), TimingContract.View {

    private val timingList = mutableListOf<TimeModel>()
    private val allWorkingHoursList = mutableListOf<AllWorkingItem>()
    private val presenter = TimingPresenter()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_timing, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        setListeners()
        if (isOnline(context)) {
            presenter.getAllBranchTimingsTimings()
        } else {
            view.showInternetError()
        }
    }

    private fun init() {
        if (timingList.isEmpty()) {
            timingList.add(TimeModel("Same timings for weekdays", true, mutableListOf(TimeToFromModel())))
            timingList.add(TimeModel("Monday", false, mutableListOf(TimeToFromModel())))
            timingList.add(TimeModel("Tuesday", false, mutableListOf(TimeToFromModel())))
            timingList.add(TimeModel("Wednesday", false, mutableListOf(TimeToFromModel())))
            timingList.add(TimeModel("Thursday", false, mutableListOf(TimeToFromModel())))
            timingList.add(TimeModel("Friday", false, mutableListOf(TimeToFromModel())))
            timingList.add(TimeModel("Saturday", false, mutableListOf(TimeToFromModel())))
            timingList.add(TimeModel("Sunday", false, mutableListOf(TimeToFromModel())))
            rvTime.adapter = TimingAdapter(activity, timingList, this)
        }

        if (activity?.intent?.getStringExtra("type") == Constants.EDIT) {
            val branchModel = Gson().fromJson(activity?.intent?.getStringExtra(Constants.CLINIC_DATA), BranchModel::class.java)
            if (branchModel.is_timings_same[0].is_timings_same == 0) {
                timingList[0].isEnabled = false
                for (j in 0 until branchModel.working_hours.size) {
                    if (timingList[branchModel.working_hours[j].day.toInt()].timings[0].time_from == "Select") {
                        timingList[branchModel.working_hours[j].day.toInt()].timings.clear()
                    }
                    timingList[branchModel.working_hours[j].day.toInt()].isEnabled = true
                    timingList[branchModel.working_hours[j].day.toInt()].timings.add(TimeToFromModel(branchModel.working_hours[j].time_from,
                            branchModel.working_hours[j].time_to))
                }

                rvTime.adapter.notifyDataSetChanged()
            } else {
                timingList[0].isEnabled = true
                for(j in 0 until branchModel.working_hours.size){

                }

                for (j in 0 until branchModel.working_hours.size) {
                    if (timingList[branchModel.working_hours[j].day.toInt()].timings[0].time_from == "Select") {
                        timingList[branchModel.working_hours[j].day.toInt()].timings.clear()
                    }
                    timingList[branchModel.working_hours[j].day.toInt()].isEnabled = true
                    timingList[branchModel.working_hours[j].day.toInt()].timings.add(TimeToFromModel(branchModel.working_hours[j].time_from,
                            branchModel.working_hours[j].time_to))
                }
            }
        }
    }

    private fun setListeners() {
        presenter.attachView(this)
        tvSaveTime.setOnClickListener {

            if (isOnline(activity)) {
                if (isTimeValidated() && isTimeInAllWorkingHours()) {
                    val dayArray = arrayOf("mon", "tue", "wed", "thu", "fri", "sat", "sun")
                    val map = HashMap<String, String>()
                    if (timingList[0].isEnabled) {
                        val weekDaysTime = Gson().toJson(timingList[0].timings)
                        map["is_timings_same"] = "1"
                        map["mon"] = weekDaysTime
                        map["tue"] = weekDaysTime
                        map["wed"] = weekDaysTime
                        map["thu"] = weekDaysTime
                        map["fri"] = weekDaysTime
                        if (timingList[6].isEnabled)
                            map["sat"] = Gson().toJson(timingList[6].timings)

                        if (timingList[7].isEnabled)
                            map["sat"] = Gson().toJson(timingList[7].timings)
                    } else {
                        map["is_timings_same"] = "0"
                        for (i in 0 until dayArray.size) {
                            if (timingList[i + 1].isEnabled)
                                map[dayArray[i]] = Gson().toJson(timingList[i + 1].timings)
                        }
                    }
                    val branchModel = Gson().fromJson(activity?.intent?.getStringExtra(Constants.CLINIC_DATA), BranchModel::class.java)
                    map[WebConstants.BRANCH_ID] = branchModel.id.toString()
                    presenter.addTimingClinic(map)
                }
            } else {
                view?.showInternetError()
            }
        }
    }

    private fun isTimeInAllWorkingHours(): Boolean {

        for (i in 0 until timingList.size)
            for (j in 0 until timingList[i].timings.size)
                for (k in 0 until allWorkingHoursList.size) {
                    if (getDateFromString(timingList[i].timings[j].time_from).before(getDateFromString(allWorkingHoursList[k].time_to)) ||
                            getDateFromString(timingList[i].timings[j].time_from).after(getDateFromString(allWorkingHoursList[k].time_from))) {
                        view?.showErrorSnack("Time slot conflict")
                        return false
                    }
                    if (getDateFromString(timingList[i].timings[j].time_to).before(getDateFromString(allWorkingHoursList[k].time_to)) ||
                            getDateFromString(timingList[i].timings[j].time_to).after(getDateFromString(allWorkingHoursList[k].time_from))) {
                        view?.showErrorSnack("Time slot conflict end")
                        return false
                    }
                }




        return true
    }

    private fun isTimeValidated(): Boolean {
        var enabledCount = 0
        for (i in 0 until timingList.size) {
            if (timingList[i].isEnabled) {
                ++enabledCount
                for (j in 0 until timingList[i].timings.size)
                    if (timingList[i].timings[j].time_from == "Select" || timingList[i].timings[j].time_to == "Select") {
                        view?.showErrorSnack("Please select time to save")
                        return false
                    }
            }
        }

        if (enabledCount == 0) {
            view?.showErrorSnack("Please select time to save")
            return false
        }
        return true
    }

    override fun allTimingsSuccess(allWorkingHours: MutableList<AllWorkingItem>?) {
        if (allWorkingHours != null) {
            allWorkingHoursList.clear()
            allWorkingHoursList.addAll(allWorkingHours)
        }
    }

    override fun timingSuccess(msg: String?) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        presenter.detachView()
    }
}