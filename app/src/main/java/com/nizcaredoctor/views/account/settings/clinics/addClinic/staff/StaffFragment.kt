package com.nizcaredoctor.views.account.settings.clinics.addClinic.staff

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.nizcaredoctor.R
import com.nizcaredoctor.views.baseViews.BaseViewFragImpl
import kotlinx.android.synthetic.main.fragment_staff.*

class StaffFragment : BaseViewFragImpl(), StaffInfoContract.View {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_staff, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setListener()
        init()
    }

    private fun init() {
        rvStaff.adapter = StaffAdapter(activity)
    }

    private fun setListener() {

    }
}
