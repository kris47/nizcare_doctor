package com.nizcaredoctor.views.account.settings.clinics.addClinic.timings.timeModel

data class AllWorkingItem(
        val day: String,
        val time_from: String,
        val time_to: String
)
