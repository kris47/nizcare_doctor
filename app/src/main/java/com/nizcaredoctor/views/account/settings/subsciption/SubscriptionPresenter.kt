package com.nizcaredoctor.views.account.settings.subsciption

import com.nizcaredoctor.utils.getAccessToken
import com.nizcaredoctor.views.baseViews.BasePresenterImpl
import com.nizcaredoctor.webservice.ApiResponseModel
import com.nizcaredoctor.webservice.RetrofitClient
import com.nizcaredoctor.webservice.model.subsciptionModel.SubsciptionModel
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SubscriptionPresenter : BasePresenterImpl<SubsciptionContract.View>(), SubsciptionContract.Presenter {
    override fun subscribeForPackage(map: HashMap<String, Any>) {
        getView()?.showProgress()
        RetrofitClient.getApi().subscibreForPlan(getAccessToken(),map).enqueue(object :Callback<ApiResponseModel<Any>>{
            override fun onFailure(call: Call<ApiResponseModel<Any>>?, t: Throwable?) {
                getView()?.displayFailure(t?.message)
                getView()?.dismissProgress()
            }

            override fun onResponse(call: Call<ApiResponseModel<Any>>?, response: Response<ApiResponseModel<Any>>?) {
                getView()?.dismissProgress()
                if (response != null) {
                    if (response.isSuccessful) {
                        getView()?.subscriptionSuccess(response.body()?.msg)
                    } else {
                        try {
                            getView()?.displayError(JSONObject(response.errorBody()?.string()).getString("msg"), response.code())
                        } catch (e: Exception) {

                        }
                    }
                }
            }
        })
    }

    override fun getSubscriptionPlan() {


        RetrofitClient.getApi().getSubsciptionPlan(getAccessToken())
                .enqueue(object : Callback<ApiResponseModel<SubsciptionModel>> {
                    override fun onFailure(call: Call<ApiResponseModel<SubsciptionModel>>?, t: Throwable?) {
                        getView()?.dismissProgress()
                        getView()?.displayFailure(t?.message)
                    }

                    override fun onResponse(call: Call<ApiResponseModel<SubsciptionModel>>?, response: Response<ApiResponseModel<SubsciptionModel>>?) {
                        if (response != null) {
                            if (response.isSuccessful) {
                                getView()?.setSubsciptionPackage(response.body()?.data?.subscription_plans)
                            } else {
                                try {
                                    getView()?.displayError(JSONObject(response.errorBody()?.string()).getString("msg"), response.code())
                                } catch (e: Exception) {

                                }
                            }
                        }
                    }
                })
    }
}