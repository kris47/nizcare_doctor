package com.nizcaredoctor.views.account.settings.clinics.addClinic

import com.nizcaredoctor.views.account.settings.clinics.addClinic.timings.timeModel.AllWorkingItem
import com.nizcaredoctor.webservice.model.User

data class BranchModel(
        var id: Int?,
        var user_id: Int?,
        var service_provider_id: Int?,
        var name: String?,
        var email: String?,
        var phone_no: String?,
        var phone_code: String?,
        var consultation_fees: String?,
        var latitude: String,
        var longitude: String,
        var address: String,
        var city_id: Int,
        var state_id: Int,
        var country_id: Int,
        var show_on_web: Int?,
        var is_timings_same: MutableList<TimingSame>,
        var status: Int?,
        var created_at: String?,
        var images: MutableList<ImageModel>,
        var service_categories: MutableList<ServiceCategoryModel>,
        var treatments: MutableList<Treatment>,
        var working_hours: MutableList<AllWorkingItem>,
        var branch_users: MutableList<BranchUser>
)

data class Treatment(
        var branch_id: Int?,
        var treatment_id: Int?
)

data class TimingSame(
        val is_timings_same: Int
)

data class BranchUser(
        var branch_id: Int?,
        var user_id: Int?,
        var user: User
)

data class ServiceCategoryModel(
        var branch_id: Int?,
        var service_category_id: Int?
)