package com.nizcaredoctor.views.account.settings.holidays

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.nizcaredoctor.R
import com.nizcaredoctor.utils.getDaysDifference
import com.nizcaredoctor.webservice.model.holidayModel.HolidayDataItem
import kotlinx.android.synthetic.main.item_my_holiday.view.*

class MyHolidayAdapter(val context: Context?, var holidayList: List<HolidayDataItem>,
                       var onEventClickListener: MyHolidaysContract.OnEventClickListener) : RecyclerView.Adapter<MyHolidayAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_my_holiday, parent, false))
    }

    override fun getItemCount(): Int {
        return holidayList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindView(holidayList.get(position))
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        override fun onClick(v: View?) {
            when (v?.id) {
                R.id.root, R.id.tvEditHoliday -> onEventClickListener.editHoliday(holidayList[adapterPosition])
                R.id.tvRemoveHoliday -> onEventClickListener.removeHoliday(holidayList[adapterPosition].id)
            }
        }

        init {
            itemView.tvEditHoliday.setOnClickListener(this)
            itemView.tvRemoveHoliday.setOnClickListener(this)
            itemView.root.setOnClickListener(this)
        }

        fun bindView(holidayDataItem: HolidayDataItem) = with(itemView) {
            itemView.tvClinicName.text = holidayDataItem.branch?.name
            itemView.tvDays.text = context.getDaysDifference(holidayDataItem.date_from, holidayDataItem.date_to)
            itemView.tvDate.text = context.getString(R.string.str_dates, holidayDataItem.date_from, holidayDataItem.date_to)
        }
    }
}