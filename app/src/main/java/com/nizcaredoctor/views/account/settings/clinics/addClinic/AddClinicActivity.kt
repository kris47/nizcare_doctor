package com.nizcaredoctor.views.account.settings.clinics.addClinic

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.View
import android.widget.LinearLayout
import com.nizcaredoctor.R
import com.nizcaredoctor.utils.Constants
import com.nizcaredoctor.views.account.settings.clinics.addClinic.basicInfo.BasicInfoFragment
import com.nizcaredoctor.views.account.settings.clinics.addClinic.serviceClinic.ServiceFragment
import com.nizcaredoctor.views.account.settings.clinics.addClinic.staff.StaffFragment
import com.nizcaredoctor.views.account.settings.clinics.addClinic.timings.TimingFragment
import com.nizcaredoctor.views.baseViews.BaseViewActivityImpl
import com.nizcaredoctor.views.write.PagerTitleAdapter
import kotlinx.android.synthetic.main.activity_add_clinic.*

class AddClinicActivity : BaseViewActivityImpl(), View.OnClickListener {
    override fun onClick(v: View?) {
        when (v?.id) {

        }
    }

    val fragmentList = mutableListOf<Fragment>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_clinic)
        init()
        setListener()
    }

    private fun setListener() {
        toolbar.setNavigationOnClickListener { finish() }
    }

    private fun init() {
        fragmentList.add(BasicInfoFragment())
        fragmentList.add(ServiceFragment())
        fragmentList.add(TimingFragment())
        fragmentList.add(StaffFragment())

        val titleArray = arrayOf(getString(R.string.basic_info),
                getString(R.string.services),
                getString(R.string.working_hours),
                getString(R.string.staff))

        viewPagerClinic.adapter = PagerTitleAdapter(fragmentList, titleArray, supportFragmentManager)
        tabClinic.setupWithViewPager(viewPagerClinic)
        viewPagerClinic.offscreenPageLimit = 3
        if (intent.getStringExtra("type") == Constants.ADD) {
            val tabStrip = tabClinic.getChildAt(0) as LinearLayout
            for (i in 0 until tabStrip.childCount) {
                tabStrip.getChildAt(i).isEnabled = false
            }
            viewPagerClinic.setPagingEnabled(false)

        } else {

        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            1 -> (fragmentList[0] as BasicInfoFragment).onActivityResult(requestCode, resultCode, data)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        (fragmentList[0] as BasicInfoFragment).onRequestPermissionsResult(requestCode, permissions, grantResults)
    }
}