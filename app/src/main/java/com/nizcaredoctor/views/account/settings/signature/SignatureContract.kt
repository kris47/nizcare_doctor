package com.nizcaredoctor.views.account.settings.signature

import com.nizcaredoctor.views.baseViews.BasePresenter
import com.nizcaredoctor.views.baseViews.BaseView

interface SignatureContract {
    interface View : BaseView {
        fun addEditSignature(signature: String?)
    }

    interface Presenter : BasePresenter<View> {
        fun addEditSignature(signature: String?)
    }
}