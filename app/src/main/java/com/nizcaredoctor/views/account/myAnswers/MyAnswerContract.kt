package com.nizcaredoctor.views.account.myAnswers

import com.nizcaredoctor.views.baseViews.BasePresenter
import com.nizcaredoctor.views.baseViews.BaseView

interface MyAnswerContract {

    interface View : BaseView {

    }

    interface Presenter : BasePresenter<View> {

    }

    interface OnEventClickListener {
        fun onItemClick()
    }
}