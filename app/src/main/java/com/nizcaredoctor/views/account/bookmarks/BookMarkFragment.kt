package com.nizcaredoctor.views.account.bookmarks

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.nizcaredoctor.R
import com.nizcaredoctor.utils.CommonViewPagerAdapter
import com.nizcaredoctor.views.account.bookmarks.all.AllBookmark
import com.nizcaredoctor.views.baseViews.BaseViewFragImpl
import kotlinx.android.synthetic.main.fragment_book_mark.*
import kotlinx.android.synthetic.main.layout_toolbar.*

class BookMarkFragment : BaseViewFragImpl() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_book_mark, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        setListener()
    }
// adapter design same as qa adapter
    fun init() {
        tvTitle.text = getString(R.string.bookmarks)
        val fragmentList = mutableListOf<Fragment>()
        val titleList = mutableListOf<String>()
        fragmentList.add(AllBookmark())
        fragmentList.add(AllBookmark())
        fragmentList.add(AllBookmark())
        fragmentList.add(AllBookmark())
        titleList.add(getString(R.string.all))
        titleList.add(getString(R.string.posts))
        titleList.add(getString(R.string.answers_caps))
        titleList.add(getString(R.string.products))
        viewPager.adapter = CommonViewPagerAdapter(context!!, fragmentList, titleList, activity!!.supportFragmentManager)
        tabLayout.setupWithViewPager(viewPager)
    }

    fun setListener() {
        toolbar.setNavigationOnClickListener { activity?.supportFragmentManager?.popBackStack() }
    }
}