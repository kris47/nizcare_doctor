package com.nizcaredoctor.views.account.wallet

import android.content.Context
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import com.nizcaredoctor.R

class WalletAdapter(val context: Context?, val fragmentList: MutableList<Fragment>, fragmentManager: FragmentManager) :
        FragmentStatePagerAdapter(fragmentManager) {

    override fun getItem(position: Int): Fragment {
        return fragmentList[position]
    }

    override fun getCount(): Int = fragmentList.size

    override fun getPageTitle(position: Int): CharSequence? {
        return when (position) {
            0 -> context?.getString(R.string.all_expenditure)
            1 -> context?.getString(R.string.received)
            2 -> context?.getString(R.string.expenditure)
            else -> context?.getString(R.string.all_expenditure)
        }
    }
}
