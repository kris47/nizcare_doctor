package com.nizcaredoctor.views.account.settings.holidays.addHoliday

import android.app.DatePickerDialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import com.google.gson.Gson
import com.nizcaredoctor.R
import com.nizcaredoctor.utils.Constants
import com.nizcaredoctor.utils.isOnline
import com.nizcaredoctor.utils.showErrorSnack
import com.nizcaredoctor.views.account.settings.holidays.MyHolidaysContract
import com.nizcaredoctor.views.baseViews.BaseViewFragImpl
import com.nizcaredoctor.webservice.WebConstants
import com.nizcaredoctor.webservice.model.branchModel.ClinicDataItem
import com.nizcaredoctor.webservice.model.holidayModel.HolidayDataItem
import kotlinx.android.synthetic.main.activity_add_holiday.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import java.text.SimpleDateFormat
import java.util.*

class AddHolidayFragment : BaseViewFragImpl(), View.OnClickListener, AdapterView.OnItemSelectedListener,
        AddHolidayContract.View {

    val cal = Calendar.getInstance()
    private var type = "ADD"
    private var branch_id = "0"
    private var holiday_id: Int? = 0
    private val presenter = AddHolidayPresenter()
    private var branchList = mutableListOf<ClinicDataItem>()
    private lateinit var holiday_model: HolidayDataItem
    private lateinit var listener: MyHolidaysContract.OnEventClickListener

    fun bindFragment(listener: MyHolidaysContract.OnEventClickListener) {
        this.listener = listener
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.activity_add_holiday, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        setListener()
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        presenter.attachView(this)
    }

    val fromDateSetListener = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
        cal.set(Calendar.YEAR, year)
        cal.set(Calendar.MONTH, monthOfYear)
        cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)
        tvFromDate.text = SimpleDateFormat(Constants.SIMPLE_DATE_FORMAT).format(cal.time)
    }

    val toDateSetListener = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
        cal.set(Calendar.YEAR, year)
        cal.set(Calendar.MONTH, monthOfYear)
        cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)
        tvToDate.text = SimpleDateFormat(Constants.SIMPLE_DATE_FORMAT).format(cal.time)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.tvFromDate -> {
                if (type == Constants.EDIT) {
                    val sdf = SimpleDateFormat(Constants.SIMPLE_DATE_FORMAT)
                    val date = sdf.parse(holiday_model.date_from)
                    cal.time = date
                    DatePickerDialog(context, fromDateSetListener, cal.get(Calendar.YEAR)
                            , cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH)).show()
                } else
                    DatePickerDialog(context, fromDateSetListener, cal.get(Calendar.YEAR)
                            , cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH)).show()
            }
            R.id.tvToDate -> {
                if (type == Constants.EDIT) {
                    val sdf = SimpleDateFormat(Constants.SIMPLE_DATE_FORMAT)
                    val date = sdf.parse(holiday_model.date_to)
                    cal.time = date
                    DatePickerDialog(context, toDateSetListener, cal.get(Calendar.YEAR)
                            , cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH)).show()
                } else
                    DatePickerDialog(context, toDateSetListener, cal.get(Calendar.YEAR)
                            , cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH)).show()
            }
            R.id.tvAddHoliday -> {
                if (isOnline(context)) {
                    if (isValid()) {
                        val map = HashMap<String, String>()
                        map.put(WebConstants.BRANCH_ID, branch_id)
                        map.put(WebConstants.DATE_FROM, tvFromDate.text.toString())
                        map.put(WebConstants.DATE_TO, tvToDate.text.toString())
                        if (type == Constants.EDIT) {
                            map.put(WebConstants.HOLIDAY_ID, holiday_id.toString())
                        }
                        presenter.addEditHolidays(map)
                    }
                } else {
                    viewFlipper.displayedChild = 1
                    noDataView.setImage(R.raw.no_internet)
                    noDataView.setData(getString(R.string.no_interner_connection), getString(R.string.interent_substitles))
                }
            }
        }
    }

    fun isValid(): Boolean {
        if (tvFromDate.text.isEmpty()) {
            tvFromDate.showErrorSnack(getString(R.string.enter_from_date))
            return false
        }
        if (tvToDate.text.isEmpty()) {
            tvToDate.showErrorSnack(getString(R.string.enter_from_date))
            return false
        }
        return true
    }

    fun init() {
        tvTitle.text = getString(R.string.add_a_holiday)
        val bundle = arguments
        if (bundle != null) {
            branchList = bundle.getParcelableArrayList<ClinicDataItem>(Constants.BRANCH_LIST)
            if (branchList.isNotEmpty()) {
                val list = Array(branchList.size, { i -> branchList[i].name })
                setClinicAdapter(list)
            } else {
                val list = Array(1, { i -> getString(R.string.no_clinic_avai) })
            }
        }
        if (bundle?.getString(Constants.TYPE) == Constants.EDIT) {
            type = Constants.EDIT
            branch_id = bundle.getString(Constants.BRANCH_ID)
            holiday_model = Gson().fromJson(bundle.getString(Constants.HOLIDAY_DATA), HolidayDataItem::class.java)
            tvFromDate.text = holiday_model.date_from
            tvToDate.text = holiday_model.date_to
            holiday_id = holiday_model.id
            for (i in 0..branchList.size - 1) {
                if (branchList[i].id.toString() == branch_id) {
                    spinnerClinics.setSelection(i, true)
                    break
                }
            }
        }
    }

    fun setListener() {
        toolbar.setNavigationOnClickListener { activity?.supportFragmentManager?.popBackStack() }
        tvToDate.setOnClickListener(this)
        tvFromDate.setOnClickListener(this)
        tvAddHoliday.setOnClickListener(this)
        spinnerClinics.onItemSelectedListener = this
    }

    fun setClinicAdapter(list: Array<String?>) {
        val aa = ArrayAdapter(context, android.R.layout.simple_spinner_item, list)
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerClinics.adapter = aa
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {

    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val spinner = parent as Spinner
        if (spinner.id == R.id.spinnerClinics) {
            if (branchList.isNotEmpty())
                branch_id = branchList[spinnerClinics.selectedItemPosition].id.toString()
        }
    }

    override fun holidayAddedSuccess(msg: String?) {
        Toast.makeText(context, msg, Toast.LENGTH_LONG).show()
        if (::listener.isInitialized)
            listener.onNewHolidayAdded()
        activity?.supportFragmentManager?.popBackStack()
    }

    override fun onDetach() {
        super.onDetach()
        presenter.detachView()
    }
}
