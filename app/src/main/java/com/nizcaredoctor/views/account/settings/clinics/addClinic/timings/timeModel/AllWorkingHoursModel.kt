package com.nizcaredoctor.views.account.settings.clinics.addClinic.timings.timeModel

data class AllWorkingHoursModel(
        val allWorkingHours: MutableList<AllWorkingItem>
)