package com.nizcaredoctor.views.account.settings.clinics.addClinic.timings.timeModel

data class TimeToFromModel(
        var time_from: String = "Select",
        var time_to: String = "Select"
)