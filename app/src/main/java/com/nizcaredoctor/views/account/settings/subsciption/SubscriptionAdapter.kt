package com.nizcaredoctor.views.account.settings.subsciption

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.nizcaredoctor.R
import com.nizcaredoctor.utils.ObjectAtPositionPagerAdapter
import com.nizcaredoctor.webservice.model.subsciptionModel.SubsciptionItemModel
import kotlinx.android.synthetic.main.layout_subscription_plan.view.*
import java.util.*

class SubscriptionAdapter(val activity: SubscriptionActivity, val subsciptionList: MutableList<SubsciptionItemModel>) : ObjectAtPositionPagerAdapter() {

    override fun instantiateItemObject(container: ViewGroup?, position: Int): Any {
        val layout = layoutInflater.inflate(R.layout.layout_subscription_plan, container, false)

        layout.tvPlanName.text = subsciptionList[position].name
        layout.tvPlanExpire.text = subsciptionList[position].validity + " " + activity.getString(R.string.months)
        layout.tvPlanPrice.text = subsciptionList[position].amount

        setFeatures(subsciptionList[position].online_consultation, layout.tvOnlineText)
        setFeatures(subsciptionList[position].regular_consultation, layout.tvRegularText)
        setFeatures(subsciptionList[position].instant_consultation, layout.tvInstantText)
        setFeatures(subsciptionList[position].add_upto_no_branches, layout.tvBranchText)
        setFeatures(subsciptionList[position].buy_sell_package, layout.tvBuySellText)
        layout.tvBranchText.text = activity.getString(R.string.add_upto) + " " +
                subsciptionList[position].add_upto_no_branches +
                " " +
                activity.getString(R.string.branches)
        layout.tvUpgrade.text = activity.getString(R.string.upgrade_for) + " " + subsciptionList[position].amount
        layout.rlSubsciptionHeader.setBackgroundColor(getRandomColor())

        layout.tvUpgrade.setOnClickListener {
            activity.subscripeForPackage(position)
        }
        container?.addView(layout)
        return layout

    }


    fun getRandomColor(): Int = Color.argb(255, Random().nextInt(256), Random().nextInt(256), Random().nextInt(256))

    private fun setFeatures(value: Int, tvText: TextView) {
        if (value == 0) {
            tvText.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_do_not_disturb_black_24dp, 0, 0, 0)
        } else {
            tvText.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_tick, 0, 0, 0)
        }
    }

    override fun destroyItemObject(container: ViewGroup?, position: Int, `object`: Any?) {
        container?.removeView(`object` as View)
    }

    private val layoutInflater: LayoutInflater = activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    override fun getCount(): Int {
        return subsciptionList.size
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun getPageWidth(position: Int): Float {
        return 0.87f
    }
}
