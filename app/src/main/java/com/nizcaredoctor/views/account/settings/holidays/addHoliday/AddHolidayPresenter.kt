package com.nizcaredoctor.views.account.settings.holidays.addHoliday

import com.nizcaredoctor.utils.getAccessToken
import com.nizcaredoctor.views.baseViews.BasePresenterImpl
import com.nizcaredoctor.webservice.ApiResponseModel
import com.nizcaredoctor.webservice.RetrofitClient
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AddHolidayPresenter : BasePresenterImpl<AddHolidayContract.View>(), AddHolidayContract.Presenter {
    override fun addEditHolidays(map: HashMap<String, String>) {
        getView()?.showProgress()
        RetrofitClient.getApi().addEditHolidays(getAccessToken(), map)
                .enqueue(object : Callback<ApiResponseModel<Any>> {
                    override fun onFailure(call: Call<ApiResponseModel<Any>>?, t: Throwable?) {
                        getView()?.displayFailure(t?.message)
                        getView()?.dismissProgress()
                    }

                    override fun onResponse(call: Call<ApiResponseModel<Any>>?, response: Response<ApiResponseModel<Any>>) {
                        getView()?.dismissProgress()
                        if (response.isSuccessful)
                            getView()?.holidayAddedSuccess(response.body()?.msg)
                         else
                            getView()?.displayError(JSONObject(response.errorBody()?.string()).getString("msg"), response.code())

                    }
                })
    }
}