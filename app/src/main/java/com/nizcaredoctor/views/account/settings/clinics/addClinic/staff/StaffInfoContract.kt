package com.nizcaredoctor.views.account.settings.clinics.addClinic.staff

import android.view.View
import com.nizcaredoctor.views.baseViews.BasePresenter
import com.nizcaredoctor.views.baseViews.BaseView

interface StaffInfoContract {
    interface View : BaseView

    interface Presenter : BasePresenter<View>
}