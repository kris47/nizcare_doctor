package com.nizcaredoctor.views.account.settings.clinics.addClinic.basicInfo

import com.nizcaredoctor.utils.Constants
import com.nizcaredoctor.utils.PrefsManager
import com.nizcaredoctor.utils.getAccessToken
import com.nizcaredoctor.views.baseViews.BasePresenterImpl
import com.nizcaredoctor.webservice.ApiResponseModel
import com.nizcaredoctor.webservice.RetrofitClient
import com.nizcaredoctor.webservice.model.Profile
import com.nizcaredoctor.webservice.model.ServiceModel
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class BasicInfoPresenter : BasePresenterImpl<BasicInfoContract.View>(), BasicInfoContract.Presenter {
    override fun addClinic(map: HashMap<String, String>) {
        getView()?.showProgress()

        RetrofitClient.getApi().addClinicAPI(getAccessToken(), map)
                .enqueue(object : Callback<ApiResponseModel<Any>> {
                    override fun onFailure(call: Call<ApiResponseModel<Any>>?, t: Throwable?) {
                        getView()?.displayFailure(t?.message)
                        getView()?.dismissProgress()
                    }

                    override fun onResponse(call: Call<ApiResponseModel<Any>>?, response: Response<ApiResponseModel<Any>>?) {
                        getView()?.dismissProgress()
                        if (response != null) {
                            if (response.isSuccessful) {
                                getView()?.clinicSuccess(response.body()?.msg)
                            } else {
                                try {
                                    getView()?.displayError(JSONObject(response.errorBody()?.string()).getString("msg"), response.code())
                                } catch (e: Exception) {

                                }
                            }
                        }
                    }
                })
    }

    override fun getServiceCategories() {
        RetrofitClient.getApi().getDoctorSPServiceCategory(getAccessToken(), PrefsManager.get().getObject(Constants.USER_DETAIL, Profile::class.java)?.user_role_id
                ?: 1).enqueue(object : Callback<ApiResponseModel<ServiceModel>> {
            override fun onFailure(call: Call<ApiResponseModel<ServiceModel>>?, t: Throwable?) {
                getView()?.displayFailure(t?.message)
            }

            override fun onResponse(call: Call<ApiResponseModel<ServiceModel>>?, response: Response<ApiResponseModel<ServiceModel>>?) {
                if (response != null) {
                    if (response.isSuccessful) {
                        getView()?.setServiceCategory(response.body()?.data?.serviceCategories)
                    } else {
                        try {
                            getView()?.displayError(JSONObject(response.errorBody()?.string()).getString("msg"), response.code())
                        } catch (e: Exception) {

                        }
                    }
                }
            }
        })
    }
}