package com.nizcaredoctor.views.signUp

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.nizcaredoctor.R
import com.nizcaredoctor.utils.*
import com.nizcaredoctor.views.baseViews.BaseViewFragImpl
import com.nizcaredoctor.views.login.LoginActivity
import com.nizcaredoctor.views.verification.VerificationFragment
import com.nizcaredoctor.webservice.WebConstants
import kotlinx.android.synthetic.main.fragment_sign_up.*

class SignUPFragment : BaseViewFragImpl(), SignUpContract.View {

    private val presenter = SignUpPresenter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_sign_up, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        setListeners()
    }

    private fun init() {
        dialog = DialogIndeterminate(context)
    }

    private fun setListeners() {
        presenter.attachView(this)
        toolbar.setNavigationOnClickListener {
            view?.hideKeyboard()
            activity?.finish()
            activity?.activityFinishAnimation()
        }

        btnSignup.setOnClickListener {
            if (isValidate()) {
                if (isOnline(context)) {
                    val map = HashMap<String, String>()
                    map[WebConstants.USERTYPE] = Constants.DOCTOR.toString()
                    map[WebConstants.EMAIL] = etEmail.text.trim().toString()
                    map[WebConstants.PHONECODE] = ccp.selectedCountryCode.trim()
                    map[WebConstants.PHONENUMBER] = etPhoneNo.text.trim().toString()
                    map[WebConstants.LOCALE] = PrefsManager.get().getString(Constants.LOCALE, "en")
                    presenter.signUp(map)
                } else {
                    view?.showInternetError()
                }
            }
        }

        tvLogShort.setOnClickListener {
            startActivity(Intent(activity, LoginActivity::class.java))
            activity?.finish()
            activity?.activityFinishAnimation()
        }
    }

    private fun isValidate(): Boolean {
        return when {
            etPhoneNo.text.trim().toString().isEmpty() -> {
                etPhoneNo.error = getString(R.string.enter_mobile_no)
                etPhoneNo.requestFocus()
                false
            }
            etPhoneNo.text.trim().toString().length < 10 -> {
                etPhoneNo.error = getString(R.string.enter_valid_mobile_no)
                etPhoneNo.requestFocus()
                false
            }

            !isValidMobile(etPhoneNo.text.toString()) -> {
                etPhoneNo.error = getString(R.string.enter_valid_mobile_no)
                etPhoneNo.requestFocus()
                false
            }

            etEmail.text.trim().toString().isEmpty() -> {
                tilEmail.error = getString(R.string.email_is_empty)
                tilEmail.requestFocus()
                false
            }

            !isValidEmail(etEmail.text.trim().toString()) -> {
                tilEmail.error = getString(R.string.enter_valid_email)
                tilEmail.requestFocus()

                false
            }

            etPassword.text.toString().trim().isEmpty() -> {
                tilPassword.error = getString(R.string.enter_pasword)
                tilPassword.requestFocus()
                false
            }

            etPassword.text.toString().trim().length < 6 -> {
                tilPassword.error = getString(R.string.password_is_short)
                tilPassword.requestFocus()
                false
            }
            else -> true
        }
    }

    override fun signUpSuccess(msg: String?) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()
        val bundle = Bundle()
        bundle.putString(WebConstants.EMAIL, etEmail.text.trim().toString())
        bundle.putString(WebConstants.PHONECODE, ccp.selectedCountryCode.trim())
        bundle.putString(WebConstants.PHONENUMBER, etPhoneNo.text.trim().toString())
        bundle.putString(WebConstants.PASSWORD, etPassword.text.trim().toString())
        bundle.putString(Constants.VERIFICATIONTYPE, Constants.SIGNUP)
        val fragment = VerificationFragment()
        fragment.arguments = bundle
        activity?.supportFragmentManager?.beginTransaction()?.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
                ?.replace(R.id.container, fragment, "otp")?.addToBackStack("")?.commit()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        presenter.detachView()
    }
}