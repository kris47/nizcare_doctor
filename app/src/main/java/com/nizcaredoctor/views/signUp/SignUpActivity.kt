package com.nizcaredoctor.views.signUp

import android.os.Bundle
import com.nizcaredoctor.R
import com.nizcaredoctor.utils.replaceFragment
import com.nizcaredoctor.views.baseViews.BaseViewActivityImpl

class SignUpActivity : BaseViewActivityImpl() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)
        replaceFragment(supportFragmentManager,
                SignUPFragment(),
                "signUp")
    }
}