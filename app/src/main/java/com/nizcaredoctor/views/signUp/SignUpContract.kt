package com.nizcaredoctor.views.signUp

import com.nizcaredoctor.views.baseViews.BasePresenter
import com.nizcaredoctor.views.baseViews.BaseView

interface SignUpContract {

    interface View: BaseView {
        fun signUpSuccess(msg: String?)

    }

    interface Presenter: BasePresenter<View> {
        fun signUp(map: HashMap<String, String>)
    }
}