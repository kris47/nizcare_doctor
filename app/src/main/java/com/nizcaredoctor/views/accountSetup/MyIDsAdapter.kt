package com.nizcaredoctor.views.accountSetup

import android.app.Activity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.nizcaredoctor.R
import com.nizcaredoctor.webservice.model.IdViewModel
import kotlinx.android.synthetic.main.item_ids.view.*

class MyIDsAdapter(val activity: Activity?, val idsList: MutableList<IdViewModel>) : RecyclerView.Adapter<MyIDsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_ids, parent, false))
    }

    override fun getItemCount(): Int {
        return idsList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindView(idsList[position])
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        init {
            itemView.tvDeleteID.setOnClickListener {
                (activity as AccountSetUpActivity).removeID(adapterPosition)
            }
        }

        fun bindView(item: IdViewModel) = with(itemView) {
            tvDocument.text = item.docName
            if (activity != null) {
                Glide.with(activity)
                        .load(item.docFile)
                        .apply(RequestOptions().placeholder(R.drawable.ic_write_pressed))
                        .into(ivIdDocImage)
            }
        }
    }
}
