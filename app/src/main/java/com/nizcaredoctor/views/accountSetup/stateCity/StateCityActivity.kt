package com.nizcaredoctor.views.accountSetup.stateCity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import com.nizcaredoctor.R
import com.nizcaredoctor.utils.Constants
import com.nizcaredoctor.utils.PrefsManager
import com.nizcaredoctor.utils.hideKeyboard
import com.nizcaredoctor.utils.isOnline
import com.nizcaredoctor.views.accountSetup.RecyclerSingleItemAdapter
import com.nizcaredoctor.views.baseViews.BaseViewActivityImpl
import com.nizcaredoctor.webservice.model.GetItemModel
import com.nizcaredoctor.webservice.model.Profile
import kotlinx.android.synthetic.main.activity_recycler.*


class StateCityActivity : BaseViewActivityImpl(), StateCityContract.View, TextWatcher {

    private lateinit var adapter: RecyclerSingleItemAdapter
    private var filteredList = mutableListOf<GetItemModel>()
    private val list = mutableListOf<GetItemModel>()
    private val presenter = StateCityPresenter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recycler)
        init()
        setListeners()
        adapter = RecyclerSingleItemAdapter(this, list, intent.getStringExtra(Constants.ListType))
        adapter.selectedId = intent.getIntExtra(Constants.SELECTED_POSITION, 1)
        rvRecycler.adapter = adapter

    }

    private fun init() {
        noDataFound.setImage(R.raw.no_data)
        noDataFound.setData("No data found", "")
        noDataFound.visibility = View.GONE

        if (intent.getStringExtra(Constants.ListType) == Constants.States) {
            toolbar.title = getString(R.string.select_state)
            if (isOnline(this)) {
                presenter.getStates((PrefsManager.get().getObject(Constants.USER_DETAIL, Profile::class.java)?.country_id
                        ?: 101).toString())
            } else {
                noDataFound.visibility = View.VISIBLE
                noDataFound.setImage(R.raw.no_internet)
                noDataFound.setData("No internet connection", "It seems that your internet is off. Turn on internet to continue")
            }
        } else if (intent.getStringExtra(Constants.ListType) == Constants.Cities) {
            toolbar.title = getString(R.string.select_city)
            if (isOnline(this)) {
                presenter.getCitiesFromState(intent.getIntExtra(Constants.SELECTED_STATE_ID, 1).toString())
            } else {
                noDataFound.visibility = View.VISIBLE
                noDataFound.setImage(R.raw.no_internet)
                noDataFound.setData("No internet connection", "It seems that your internet is off. Turn on internet to continue")
            }
        }
    }

    private fun setListeners() {
        presenter.attachView(this)
        toolbar.setNavigationOnClickListener { onBackPressed() }
        etCommonSearch.addTextChangedListener(this)
    }

    fun showRecyclerProgress() {
        progress.visibility = View.VISIBLE
    }

    fun dismissRecyclerProgress() {
        progress.visibility = View.GONE
    }

    override fun showProgress() {
        showRecyclerProgress()
    }

    override fun dismissProgress() {
        dismissRecyclerProgress()
    }

    override fun setList(states: MutableList<GetItemModel>?) {
        if (states != null) {
            list.clear()
            list.addAll(states)
            adapter.notifyDataSetChanged()

        }
    }

    fun selectedState(item: GetItemModel) {
        etCommonSearch.hideKeyboard()
        val intent = Intent().putExtra("stateID", item.id).putExtra("stateName", item.name)
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    fun selectedCity(item: GetItemModel) {
        etCommonSearch.hideKeyboard()
        val intent = Intent().putExtra("cityID", item.id).putExtra("cityName", item.name)
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    private fun filter(searchText: String, originalList: MutableList<GetItemModel>): MutableList<GetItemModel> {
        var newText = searchText
        newText = newText.toLowerCase()
        filteredList = mutableListOf<GetItemModel>()
        for (model in originalList) {
            val text = model.name.toLowerCase()
                if (text.contains(newText)) {
                    filteredList.add(model)
                }
        }
        return filteredList
    }

    override fun afterTextChanged(s: Editable?) {

    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        filteredList = filter(s.toString(), list)
        adapter.setFilter(filteredList)

        if (filteredList.isEmpty()) {
            noDataFound.visibility = View.VISIBLE
        } else {
            noDataFound.visibility = View.GONE
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detachView()
    }
}
