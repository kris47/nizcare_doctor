package com.nizcaredoctor.views.accountSetup

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.gson.Gson
import com.nizcaredoctor.R
import com.nizcaredoctor.utils.*
import com.nizcaredoctor.views.accountSetup.stateCity.StateCityActivity
import com.nizcaredoctor.views.baseViews.BaseViewActivityImpl
import com.nizcaredoctor.views.home.HomeActivity
import com.nizcaredoctor.webservice.WebConstants
import com.nizcaredoctor.webservice.model.*
import kotlinx.android.synthetic.main.activity_account_setup.*
import kotlinx.android.synthetic.main.dialog_education.view.*
import pub.devrel.easypermissions.EasyPermissions
import pub.devrel.easypermissions.PermissionRequest
import java.io.File

class AccountSetUpActivity : BaseViewActivityImpl(),
        View.OnClickListener,
        AccountSetupContract.View,
        AdapterView.OnItemSelectedListener,
        ImagePicker.ImagePickerListener,
        EasyPermissions.PermissionCallbacks {

    private val idSelectedList = mutableListOf<IdViewModel>()
    private val documentList = mutableListOf<GetItemModel>()
    private val titleList = mutableListOf<GetItemModel>()
    private val serviceCategory = mutableListOf<GetItemModel>()
    private val languageSpokenList = mutableListOf<String>()
    private val languageSpokenId = mutableListOf<Int>()
    private val selectedSpecialty = mutableListOf<Int>()
    private val selectedLanguagesList = mutableListOf<String>()
    private val educationList = mutableListOf<EducationModel>()
    private lateinit var selectedSpecialities: BooleanArray
    private lateinit var selectedLanguages: BooleanArray
    private var selectedStateId = 0
    private var selectedCityId = 0
    private var selectedTitleId = 0
    private var profilePicURL = ""
    private var eduCertificateURL = ""
    private lateinit var amazonS3: AmazoneS3
    private var regIdImageURL = ""
    private val presenter = AccountSetupPresenter()
    private var isProfileImage = false
    private var isEducationImage = false
    private var isRegistrationAdded = false
    private var isIDProofImageAdded = false
    private var idProofImageURL = ""
    private lateinit var imagePicker: ImagePicker
    private val permissions = arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    private val permissionRequestCode = 56
    private lateinit var educationDialog: Dialog
    private lateinit var dialogView: View
    private var registrationModel: EducationModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_account_setup)
        init()
        setListeners()
        if (isOnline(this)) {
            presenter.getTitles()
            presenter.getServiceCategories()
            presenter.getLanguages()
            presenter.getDocumentTypes((PrefsManager.get().getObject(Constants.USER_DETAIL, Profile::class.java)?.country_id
                    ?: 101).toString())
        } else {
            rootAccount.showInternetError()
        }
        setCollegeDegreeAdapter()
        setIDsAdapter()


        tvAddEducation.setOnClickListener {
            rootAccount.hideKeyboard()
            educationDialog = Dialog(this)
            educationDialog.setCancelable(false)
            educationDialog.window.attributes.windowAnimations = R.style.Theme_Dialog_Translucent
            val inflater = layoutInflater
            dialogView = inflater.inflate(R.layout.dialog_education, null)
            educationDialog.setContentView(dialogView)
            dialogView.tvUploadCertificate.setOnClickListener {
                isEducationImage = true
                openImage()
            }

            dialogView.tvDiscard.setOnClickListener {
                educationDialog.dismiss()
            }

            dialogView.tvSave.setOnClickListener {
                resetEducation()
                if (isEducationValidated()) {
                    educationList.add(EducationModel(dialogView.etDegree.text.toString(),
                            dialogView.etCollege.text.toString(),
                            dialogView.etYearCollege.text.toString(),
                            eduCertificateURL,
                            Constants.EDUCATION))
                    rvEducation.adapter.notifyItemInserted(educationList.size)
                    tvAddText.text = "Add more certificate"
                    eduCertificateURL = ""
                    educationDialog.dismiss()
                }
            }

            educationDialog.show()
        }

        tvEditReg.setOnClickListener {
            registrationModel = null
            regIdImageURL = ""
            rlRegistrationAdded.visibility = View.GONE
            cvAddRegistration.visibility = View.VISIBLE
        }

        tvAddReg.setOnClickListener {
            rootAccount.hideKeyboard()
            educationDialog = Dialog(this)
            educationDialog.window.attributes.windowAnimations = R.style.Theme_Dialog_Translucent
            educationDialog.setCancelable(false)
            val inflater = layoutInflater
            dialogView = inflater.inflate(R.layout.dialog_education, null)
            dialogView.tilDegree.hint = getString(R.string.license_no)
            dialogView.tilCollege.hint = getString(R.string.council_name)
            dialogView.tilYearCollege.hint = getString(R.string.year_of_registration)
            dialogView.tvUploadCertificate.text = getString(R.string.upload_registration)
            educationDialog.setContentView(dialogView)
            dialogView.tvUploadCertificate.setOnClickListener {
                isRegistrationAdded = true
                openImage()
            }

            dialogView.tvDiscard.setOnClickListener {
                educationDialog.dismiss()
            }

            dialogView.tvSave.setOnClickListener {
                resetEducation()
                if (isRegistrationValidated()) {
                    registrationModel = EducationModel(dialogView.etDegree.text.toString(),
                            dialogView.etCollege.text.toString(),
                            dialogView.etYearCollege.text.toString(),
                            regIdImageURL,
                            Constants.REGISTRATION)
                    rlRegistrationAdded.visibility = View.VISIBLE
                    cvAddRegistration.visibility = View.GONE
                    tvLicenseNo.text = registrationModel?.name_number
                    tvCounsilName.text = registrationModel?.org_name
                    tvRegIssueDate.text = registrationModel?.pass_year
                    Glide.with(this)
                            .load(regIdImageURL)
                            .apply(RequestOptions().placeholder(R.drawable.ic_write_pressed))
                            .into(ivReg)
                    educationDialog.dismiss()
                }
            }

            educationDialog.show()
        }
    }

    private fun isEducationValidated(): Boolean {
        return when {
            dialogView.etDegree.text.trim().toString().isEmpty() -> {
                dialogView.tilDegree.isErrorEnabled = true
                dialogView.tilDegree.error = "Please add degree"
                dialogView.tilDegree.requestFocus()

                false
            }

            dialogView.etCollege.text.trim().toString().isEmpty() -> {
                dialogView.tilCollege.isErrorEnabled = true
                dialogView.tilCollege.error = "Please add College/University"
                dialogView.tilCollege.requestFocus()
                false
            }

            dialogView.etYearCollege.text.trim().toString().isEmpty() -> {
                dialogView.tilYearCollege.isErrorEnabled = true
                dialogView.tilYearCollege.error = "Please enter pass out year"
                dialogView.tilYearCollege.requestFocus()

                false
            }

            dialogView.etYearCollege.text.trim().toString().length != 4 -> {
                dialogView.tilYearCollege.isErrorEnabled = true
                dialogView.tilYearCollege.error = "Enter valid pass out year"
                dialogView.tilYearCollege.requestFocus()

                false
            }

            dialogView.etYearCollege.text.trim().toString().toInt() > 2018 -> {
                dialogView.tilYearCollege.isErrorEnabled = true
                dialogView.tilYearCollege.error = "Pass out year can't be more than current year"
                dialogView.tilYearCollege.requestFocus()
                false
            }

            eduCertificateURL == "" -> {
                Toast.makeText(this, "Upload certification image", Toast.LENGTH_SHORT).show()
                //rootAccount.showErrorSnack("Upload certification image")
                false
            }

            else -> true
        }
    }

    private fun isRegistrationValidated(): Boolean {
        return when {
            dialogView.etDegree.text.trim().toString().isEmpty() -> {
                dialogView.tilDegree.requestFocus()
                dialogView.tilDegree.isErrorEnabled = true
                dialogView.tilDegree.error = "Please add license number"
                false
            }

            dialogView.etCollege.text.trim().toString().isEmpty() -> {
                dialogView.tilCollege.requestFocus()
                dialogView.tilCollege.isErrorEnabled = true
                dialogView.tilCollege.error = "Please add council name"
                false
            }

            dialogView.etYearCollege.text.trim().toString().isEmpty() -> {
                dialogView.tilYearCollege.requestFocus()
                dialogView.tilYearCollege.isErrorEnabled = true
                dialogView.tilYearCollege.error = "Please enter year of registration"
                false
            }

            dialogView.etYearCollege.text.trim().toString().length != 4 -> {
                dialogView.tilYearCollege.requestFocus()
                dialogView.tilYearCollege.isErrorEnabled = true
                dialogView.tilYearCollege.error = "Enter valid year of registration"
                false
            }

            dialogView.etYearCollege.text.trim().toString().toInt() > 2018 -> {
                dialogView.tilYearCollege.requestFocus()
                dialogView.tilYearCollege.isErrorEnabled = true
                dialogView.tilYearCollege.error = "Year of registration can't be more than current year"
                false
            }

            regIdImageURL == "" -> {
                Toast.makeText(this, "Upload registration proof image", Toast.LENGTH_SHORT).show()
                // rootAccount.showErrorSnack("Upload registration proof image")
                false
            }

            else -> true
        }
    }


    private fun resetEducation() {
        dialogView.tilCollege.isErrorEnabled = false
        dialogView.tilDegree.isErrorEnabled = false
        dialogView.tilYearCollege.isErrorEnabled = false
    }

    private fun init() {
        imagePicker = ImagePicker(this, false)
        imagePicker.setImagePickerListener(this)
        amazonS3 = AmazoneS3(this)
        dialog = DialogIndeterminate(this)
        tvMale.isSelected = true
        tilEmail.isEnabled = false
        etEmail.setText(PrefsManager.get().getObject(Constants.USER_DETAIL, Profile::class.java)?.email
                ?: "")
    }

    private fun setListeners() {
        presenter.attachView(this)
        ivIdDocImage.setOnClickListener(this)
        tvSubmit.setOnClickListener(this)
        tvAddAnother.setOnClickListener(this)
        tvFemale.setOnClickListener(this)
        tvMale.setOnClickListener(this)
        tvUploadRegistration.setOnClickListener(this)
        etSpeciality.setOnClickListener(this)
        etState.setOnClickListener(this)
        etCity.setOnClickListener(this)
        etLanguage.setOnClickListener(this)
        tvMale.setOnClickListener(this)
        tvFemale.setOnClickListener(this)
        ivProfileImage.setOnClickListener(this)
    }


    private fun setIDsAdapter() {
        rvIDs.adapter = MyIDsAdapter(this, idSelectedList)
    }

    private fun setCollegeDegreeAdapter() {
        rvEducation.adapter = MyEducationAdapter(this, educationList)
    }

    override fun onClick(v: View?) {
        rootAccount.hideKeyboard()
        when (v?.id) {

            R.id.ivProfileImage -> {
                isProfileImage = true
                openImage()

            }

            R.id.ivIdDocImage -> {
                isIDProofImageAdded = true
                openImage()
            }

            R.id.tvAddAnother -> {
                if (idProofImageURL != "") {
                    for (i in 0 until idSelectedList.size) {
                        if (idSelectedList[i].docId == documentList[documentSpinner.selectedItemPosition].id) {
                            rootAccount.showErrorSnack("Document already selected")
                            return
                        }
                    }
                    rvIDs.visibility = View.VISIBLE
                    idSelectedList.add(IdViewModel(documentList[documentSpinner.selectedItemPosition].id,
                            documentList[documentSpinner.selectedItemPosition].name, idProofImageURL))
                    rvIDs.adapter.notifyItemInserted(idSelectedList.size)
                    idProofImageURL = ""
                    ivIdDocImage.setImageResource(R.drawable.ic_camera_c)
                } else {
                    rootAccount.showErrorSnack(getString(R.string.upload_id_image))
                }

            }

            R.id.tvMale -> {
                tvMale.isSelected = true
                tvMale.background = ContextCompat.getDrawable(this, R.drawable.button_green_left)
                tvFemale.background = ContextCompat.getDrawable(this, R.drawable.button_white_right)
                tvMale.setTextColor(ContextCompat.getColor(this, R.color.white))
                tvFemale.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary))
            }

            R.id.tvFemale -> {
                tvMale.isSelected = false
                tvMale.background = ContextCompat.getDrawable(this, R.drawable.button_white_left)
                tvFemale.background = ContextCompat.getDrawable(this, R.drawable.button_green_right)
                tvMale.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary))
                tvFemale.setTextColor(ContextCompat.getColor(this, R.color.white))
            }

            R.id.tvSubmit -> {
                disableErrors()
                root.clearFocus()
                if (isAccountValidated()) {
                    if (isOnline(this)) {
                        setUpAccount()
                    } else {
                        rootAccount.showInternetError()
                    }
                }
            }

            R.id.etSpeciality -> {
                if (serviceCategory.isNotEmpty()) {
                    val services = Array(serviceCategory.size, { i -> serviceCategory[i].name })
                    val builder = AlertDialog.Builder(this)
                    builder.setTitle("Select your specialities")
                            .setCancelable(false)
                            .setMultiChoiceItems(services, selectedSpecialities,
                                    DialogInterface.OnMultiChoiceClickListener { _, which, isChecked ->
                                        selectedSpecialities[which] != isChecked
                                    })
                            .setPositiveButton(R.string.ok, DialogInterface.OnClickListener { dialog, _ ->
                                var text = ""
                                selectedSpecialty.clear()
                                for (i in 0 until serviceCategory.size) {
                                    if (selectedSpecialities[i]) {
                                        selectedSpecialty.add(serviceCategory[i].id)
                                        if (text.isNotEmpty()) {
                                            text += ", "
                                        }
                                        text += serviceCategory[i].name
                                    }
                                }
                                etSpeciality.setText(text)
                                dialog.dismiss()
                            })
                    builder.create().show()
                } else {
                    rootAccount.showErrorSnack("Please wait loading specialities.")
                }
            }

            R.id.etLanguage -> {
                if (languageSpokenList.isNotEmpty()) {
                    val languages = Array(languageSpokenList.size, { i -> languageSpokenList[i] })
                    val builder = AlertDialog.Builder(this)
                    builder.setTitle("Select your preferred language")
                            .setCancelable(false)
                            .setMultiChoiceItems(languages, selectedLanguages,
                                    DialogInterface.OnMultiChoiceClickListener { _, which, isChecked ->
                                        selectedLanguages[which] != isChecked
                                    })
                            .setPositiveButton(R.string.ok, DialogInterface.OnClickListener { dialog, which ->
                                var text = ""
                                selectedLanguagesList.clear()
                                for (i in 0 until languageSpokenList.size) {
                                    if (selectedLanguages[i]) {
                                        selectedLanguagesList.add(languageSpokenList[i])
                                        languageSpokenId.add(languageModelObject.get(i).id)
                                        if (text.isNotEmpty()) {
                                            text += ", "
                                        }
                                        text += languageSpokenList[i]
                                    }
                                }
                                etLanguage.setText(text)
                                dialog.dismiss()
                            })
                    builder.create().show()
                } else {
                    rootAccount.showErrorSnack("Please wait loading languages.")
                }
            }

            R.id.etState -> {
                startActivityForResult(Intent(this@AccountSetUpActivity, StateCityActivity::class.java)
                        .putExtra(Constants.SELECTED_POSITION, selectedStateId)
                        .putExtra(Constants.ListType, Constants.States), 1)
            }

            R.id.etCity -> {
                if (selectedStateId == 0) {
                    rootAccount.showErrorSnack("Please select state first.")
                } else {
                    startActivityForResult(Intent(this@AccountSetUpActivity, StateCityActivity::class.java)
                            .putExtra(Constants.SELECTED_STATE_ID, selectedStateId)
                            .putExtra(Constants.SELECTED_POSITION, selectedCityId)
                            .putExtra(Constants.ListType, Constants.Cities), 2)
                }
            }

        }
    }

    override fun onImageSelectedFromPicker(imageFile: File?) {
        if (isProfileImage) {
            isProfileImage = false
            Glide.with(this)
                    .load(imageFile)
                    .into(ivProfileImage)
            profilePicURL = amazonS3.setFileToUpload(imageFile)
        }

        if (isEducationImage) {
            isEducationImage = false
            dialogView.tvUploadCertificate.text = getString(R.string.image_uploaded)
            eduCertificateURL = amazonS3.setFileToUpload(imageFile)
        }

        if (isRegistrationAdded) {
            isRegistrationAdded = false
            dialogView.tvUploadCertificate.text = getString(R.string.reg_image_added)
            regIdImageURL = amazonS3.setFileToUpload(imageFile)
        }

        if (isIDProofImageAdded) {
            isIDProofImageAdded = false
            idProofImageURL = amazonS3.setFileToUpload(imageFile)
            Glide.with(this)
                    .load(imageFile)
                    .into(ivIdDocImage)
        }
    }

    override fun onPDFSelected(file: File?, path: String?) {

    }

    private fun setUpAccount() {
        val map = HashMap<String, String>()
        map[WebConstants.SETUPADD] = "0"
        map[WebConstants.USER_TITLE] = selectedTitleId.toString()
        map[WebConstants.FIRST_NAME] = etFirstName.text.toString()
        map[WebConstants.LAST_NAME] = etLastName.text.toString()
        map[WebConstants.GENDER] = (if (tvMale.isSelected) 0 else 1).toString()
        map[WebConstants.ADDRESS_HOME] = etAddress.text.toString()
        map[WebConstants.STATE_ID] = selectedStateId.toString()
        map[WebConstants.CITYID] = selectedCityId.toString()
        map[WebConstants.PINCODE] = "452001"
        map[WebConstants.LANGUAGE] = Gson().toJson(languageSpokenId)
        map[WebConstants.EXPERIENCE] = etYearExp.text.toString()
        map[WebConstants.SPECIALITY] = Gson().toJson(selectedSpecialty)
        val finalEduRegList = mutableListOf<EducationModel>()
        finalEduRegList.addAll(educationList)
        registrationModel?.let { finalEduRegList.add(it) }
        map[WebConstants.EDUCATIONREG] = Gson().toJson(finalEduRegList)
        if (profilePicURL != "") {
            map[WebConstants.IMAGE] = profilePicURL
        }

        val idSendList = mutableListOf<IdSendModel>()
        for (i in 0 until idSelectedList.size) {
            idSendList.add(IdSendModel(idSelectedList[i].docId, idSelectedList[i].docFile))
        }
        map[WebConstants.PROOF] = Gson().toJson(idSendList)

        Log.e("data--->", map.toString())

        presenter.signUp(map)
    }

    override fun accountSetupSuccess(profile: Profile?) {
        PrefsManager.get().save(Constants.USER_DETAIL, profile)
        startActivity(Intent(this, HomeActivity::class.java))
        finishAffinity()
    }

    private fun disableErrors() {
        tilFirstName.isErrorEnabled = false
        tilLastName.isErrorEnabled = false
        tilEmail.isErrorEnabled = false
        tilState.isErrorEnabled = false
        tilCity.isErrorEnabled = false
        tilAddress.isErrorEnabled = false
        tilSpeciality.isErrorEnabled = false
        tilLanguage.isErrorEnabled = false
        tilYearExp.isErrorEnabled = false
        tilLicenseNo.isErrorEnabled = false
        tilCounsilName.isErrorEnabled = false
        tilYearIssue.isErrorEnabled = false
    }

    private fun isAccountValidated(): Boolean {
        return when {

            selectedTitleId == 0 -> {
                rootAccount.showErrorSnack("Choose title")
                false
            }

            etFirstName.text.trim().toString().isEmpty() -> {
                tilFirstName.isErrorEnabled = true
                tilFirstName.error = "First name is empty"
                tilFirstName.requestFocus()
                false
            }

            profilePicURL == "" -> {
                rootAccount.showErrorSnack("Upload profile pic to continue")
                false
            }

            !isValidName(etFirstName.text.trim().toString()) -> {
                tilFirstName.isErrorEnabled = true
                tilFirstName.error = "Enter valid first name "
                tilFirstName.requestFocus()
                false
            }

            etLastName.text.trim().toString().isEmpty() -> {
                tilLastName.isErrorEnabled = true
                tilLastName.error = "Last name is empty"
                tilLastName.requestFocus()
                false
            }

            !isValidName(etLastName.text.trim().toString()) -> {
                tilLastName.isErrorEnabled = true
                tilLastName.error = "Enter valid last name"
                tilLastName.requestFocus()
                false
            }

            etEmail.text.trim().toString().isEmpty() -> {
                tilEmail.isErrorEnabled = true
                tilEmail.error = getString(R.string.email_is_empty)
                tilEmail.requestFocus()
                false
            }

            !isValidEmail(etEmail.text.trim().toString()) -> {
                tilEmail.isErrorEnabled = true
                tilEmail.error = getString(R.string.enter_valid_email)
                tilEmail.requestFocus()
                false
            }


            selectedStateId == 0 -> {
                etState.isFocusable = true
                tilState.isErrorEnabled = true
                tilState.error = "Select state"
                rootAccount.showErrorSnack("Select state")

                tilState.requestFocus()
                etState.isFocusable = false
                false
            }


            selectedCityId == 0 -> {
                tilCity.requestFocus()
                tilCity.isErrorEnabled = true
                rootAccount.showErrorSnack("Select city")
                tilCity.error = "Select city"
                false
            }

            etAddress.text.trim().toString().isEmpty() -> {
                tilAddress.requestFocus()
                tilAddress.isErrorEnabled = true
                tilAddress.error = "Address is empty"
                false
            }

            selectedSpecialty.isEmpty() -> {
                tilSpeciality.requestFocus()
                tilSpeciality.isErrorEnabled = true
                rootAccount.showErrorSnack("Select at least one speciality")
                tilSpeciality.error = "Select at least one speciality"
                false
            }

            selectedLanguagesList.isEmpty() -> {
                tilLanguage.requestFocus()
                tilLanguage.isErrorEnabled = true
                rootAccount.showErrorSnack("Select at least one speciality")
                tilLanguage.error = "Select at least one language"
                false
            }

            etYearExp.text.trim().toString().isEmpty() -> {
                tilYearExp.requestFocus()
                tilYearExp.isErrorEnabled = true
                tilYearExp.error = "Enter year of experience"
                false
            }

            educationList.isEmpty() -> {
                rootAccount.showErrorSnack("Add at least one education qualification")
                false
            }

            registrationModel == null -> {
                rootAccount.showErrorSnack("Add registration proof")
                false
            }

            idSelectedList.isEmpty() -> {
                rootAccount.showErrorSnack("Add at least one ID proof")
                false
            }

            else -> true
        }
    }


    override fun setTitles(userTitles: MutableList<GetItemModel>?) {
        if (userTitles != null) {
            titleList.clear()
            titleList.addAll(userTitles)
            val titles = Array(userTitles.size, { i -> userTitles[i].name })
            val arrayAdapter = ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, titles)
            arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            titleSpinner.adapter = arrayAdapter
            titleSpinner.setSelection(0)
            titleSpinner.id = R.id.titleSpinner
            titleSpinner.onItemSelectedListener = this
        }
    }

    override fun setServiceCategory(serviceCategories: MutableList<GetItemModel>?) {
        if (serviceCategories != null) {
            serviceCategory.clear()
            serviceCategory.addAll(serviceCategories)
            selectedSpecialities = BooleanArray(serviceCategories.size, { false })
        }
    }

    private var languageModelObject = mutableListOf<LanguageModelObject>()

    override fun setLanguageSpoken(languages: List<LanguageModelObject>?) {
        this.languageModelObject = languages as MutableList<LanguageModelObject>
        if (languages != null) {
            languageSpokenList.clear()
            for (i in 0..languages.size - 1) {
                languageSpokenList.add(languages.get(i).name)
            }
//            languageSpokenList.addAll(languages.sortedWith(compareBy({it})))
            selectedLanguages = BooleanArray(languageSpokenList.size, { false })
        }
    }

    override fun setDocumentType(document_type: MutableList<GetItemModel>?) {
        if (document_type != null) {
            documentList.addAll(document_type)
            val titles = Array(documentList.size, { i -> documentList[i].name })
            val arrayDocumentAdapter = ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, titles)
            arrayDocumentAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            documentSpinner.adapter = arrayDocumentAdapter
            documentSpinner.setSelection(0)
            documentSpinner.id = R.id.documentSpinner
            documentSpinner.onItemSelectedListener = this
        }
    }

    fun removeID(position: Int) {
        idSelectedList.removeAt(position)
        rvIDs.adapter.notifyItemRemoved(position)
        if (idSelectedList.isEmpty()) {
            rvIDs.visibility = View.GONE
        }
    }

    fun removeEducationItem(adapterPosition: Int) {
        educationList.removeAt(adapterPosition)
        rvEducation.adapter.notifyItemRemoved(adapterPosition)

        if (educationList.isEmpty()) {
            tvAddText.text = "No education certificate"
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {

    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val spinner = parent as Spinner
        if (spinner.id == R.id.titleSpinner) {
            selectedTitleId = titleList[titleSpinner.selectedItemPosition].id
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            1 -> {
                if (resultCode == Activity.RESULT_OK) {
                    selectedCityId = 0
                    etCity.setText("")
                    etState.setText(data?.getStringExtra("stateName"))
                    selectedStateId = data?.getIntExtra("stateID", 1) ?: 0
                }
            }
            2 -> {
                if (resultCode == Activity.RESULT_OK) {
                    etCity.setText(data?.getStringExtra("cityName"))
                    selectedCityId = data?.getIntExtra("cityID", 1) ?: 0
                }
            }
            else -> {
                if (resultCode != Activity.RESULT_OK) {
                    isProfileImage = false
                    isRegistrationAdded = false
                    isEducationImage = false
                    isIDProofImageAdded = false
                }

                imagePicker.onActivityResult(requestCode, resultCode, data)
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }


    override fun onPermissionsDenied(requestCode: Int, perms: MutableList<String>) {
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            openSettingDialog(this)
        }
    }

    override fun onPermissionsGranted(requestCode: Int, perms: MutableList<String>) {
        openImage()
    }

    private fun openImage() {
        //Open image ,camera from here
        if (EasyPermissions.hasPermissions(this, *permissions)) {
            imagePicker.showImagePicker()
        } else {
            EasyPermissions.requestPermissions(
                    PermissionRequest.Builder(this, permissionRequestCode, *permissions)
                            .setRationale(getString(R.string.rational_permission))
                            .setPositiveButtonText(R.string.ok)
                            .setNegativeButtonText(R.string.cancel)
                            .build())
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detachView()
    }
}
