package com.nizcaredoctor.views.accountSetup.stateCity

import com.nizcaredoctor.views.baseViews.BasePresenter
import com.nizcaredoctor.views.baseViews.BaseView
import com.nizcaredoctor.webservice.model.GetItemModel

interface StateCityContract {
    interface View : BaseView {
        fun setList(states: MutableList<GetItemModel>?)
    }

    interface Presenter : BasePresenter<View> {
        fun getStates(countyCode: String)
        fun getCitiesFromState(stateCode: String?)
    }
}