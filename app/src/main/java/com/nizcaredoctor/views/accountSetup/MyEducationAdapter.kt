package com.nizcaredoctor.views.accountSetup

import android.app.Activity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.nizcaredoctor.R
import com.nizcaredoctor.webservice.model.EducationModel
import kotlinx.android.synthetic.main.item_education.view.*

class MyEducationAdapter(val activity: Activity?, val educationList: MutableList<EducationModel>) : RecyclerView.Adapter<MyEducationAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_education, parent, false))
    }

    override fun getItemCount(): Int {
        return educationList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindView(educationList[position])
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        init {
            itemView.tvDelete.setOnClickListener {
                (activity as AccountSetUpActivity).removeEducationItem(adapterPosition)
            }
        }


        fun bindView(item: EducationModel) = with(itemView) {
            tvDegree.text = item.name_number
            tvCollegeName.text = item.org_name
            if (activity != null) {
                Glide.with(activity)
                        .load(item.image)
                        .apply(RequestOptions().placeholder(R.drawable.ic_write_pressed))
                        .into(ivEduction)
            }
        }
    }
}
