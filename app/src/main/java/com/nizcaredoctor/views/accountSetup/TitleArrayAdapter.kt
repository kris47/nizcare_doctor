package com.nizcaredoctor.views.accountSetup

import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.nizcaredoctor.R
import com.nizcaredoctor.webservice.model.GetItemModel

class TitleArrayAdapter(context: Context?, val activity: Activity, resource: Int, val list: MutableList<GetItemModel>) :
        ArrayAdapter<GetItemModel>(context, resource, list) {
    override fun getCount(): Int {
        return list.size
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var v = convertView

        if (v == null) {
            val inflater = activity.layoutInflater
            v = inflater.inflate(R.layout.view_spinner_item, null)
        }
        val lbl = v?.findViewById(R.id.text1) as TextView
        lbl.setTextColor(Color.BLACK)
        lbl.text = list.get(position).name

        v.setOnClickListener {
       //     (activity as AccountSetUpActivity).setSelectedTitle(list[position].id)
        }

        return v
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var v: TextView? = super.getView(position, convertView, parent) as TextView

        if (v == null) {
            v = TextView(context)
        }
        v.setTextColor(Color.BLACK)
        v.setText(list[position].name)
        return v
    }
}