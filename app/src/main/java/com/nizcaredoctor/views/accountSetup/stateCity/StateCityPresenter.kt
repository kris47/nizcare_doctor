package com.nizcaredoctor.views.accountSetup.stateCity

import com.nizcaredoctor.views.baseViews.BasePresenterImpl
import com.nizcaredoctor.webservice.ApiResponseModel
import com.nizcaredoctor.webservice.RetrofitClient
import com.nizcaredoctor.webservice.model.CityModel
import com.nizcaredoctor.webservice.model.StatesModel
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class StateCityPresenter : BasePresenterImpl<StateCityContract.View>(), StateCityContract.Presenter {
    override fun getStates(countyCode: String) {
        getView()?.showProgress()
        RetrofitClient.getApi().getStates(countyCode).enqueue(object : Callback<ApiResponseModel<StatesModel>> {
            override fun onFailure(call: Call<ApiResponseModel<StatesModel>>?, t: Throwable?) {
                getView()?.dismissProgress()
                getView()?.displayFailure(t?.message)
            }

            override fun onResponse(call: Call<ApiResponseModel<StatesModel>>?, response: Response<ApiResponseModel<StatesModel>>?) {
                getView()?.dismissProgress()
                if (response != null) {
                    if (response.isSuccessful) {
                        getView()?.setList(response.body()?.data?.states)
                    } else {
                        try {
                            getView()?.displayError(JSONObject(response.errorBody()?.string()).getString("msg"), response.code())
                        } catch (e: Exception) {

                        }
                    }
                }
            }
        })

    }

    override fun getCitiesFromState(stateCode: String?) {
        getView()?.showProgress()
        RetrofitClient.getApi().getCities(stateCode).enqueue(object : Callback<ApiResponseModel<CityModel>> {
            override fun onFailure(call: Call<ApiResponseModel<CityModel>>?, t: Throwable?) {
                getView()?.dismissProgress()
                getView()?.displayFailure(t?.message)

            }

            override fun onResponse(call: Call<ApiResponseModel<CityModel>>?, response: Response<ApiResponseModel<CityModel>>?) {
                getView()?.dismissProgress()
                if (response != null) {
                    if (response.isSuccessful) {
                        getView()?.setList(response.body()?.data?.cities)
                    } else {
                        try {
                            getView()?.displayError(JSONObject(response.errorBody()?.string()).getString("msg"), response.code())
                        } catch (e: Exception) {

                        }
                    }
                }
            }
        })
    }
}