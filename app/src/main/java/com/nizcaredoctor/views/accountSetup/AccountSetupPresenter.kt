package com.nizcaredoctor.views.accountSetup

import com.nizcaredoctor.utils.Constants
import com.nizcaredoctor.utils.PrefsManager
import com.nizcaredoctor.utils.getAccessToken
import com.nizcaredoctor.views.baseViews.BasePresenterImpl
import com.nizcaredoctor.webservice.ApiResponseModel
import com.nizcaredoctor.webservice.RetrofitClient
import com.nizcaredoctor.webservice.model.*
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AccountSetupPresenter : BasePresenterImpl<AccountSetupContract.View>(), AccountSetupContract.Presenter {
    override fun getTitles() {
        RetrofitClient.getApi().getDoctorSPTitles(getAccessToken()).enqueue(object : Callback<ApiResponseModel<DoctorTitleModel>> {
            override fun onFailure(call: Call<ApiResponseModel<DoctorTitleModel>>?, t: Throwable?) {
                getView()?.displayFailure(t?.message)
            }

            override fun onResponse(call: Call<ApiResponseModel<DoctorTitleModel>>?, response: Response<ApiResponseModel<DoctorTitleModel>>?) {
                if (response != null) {
                    if (response.isSuccessful) {
                        getView()?.setTitles(response.body()?.data?.user_titles)
                    } else {
                        try {
                            getView()?.displayError(JSONObject(response.errorBody()?.string()).getString("msg"), response.code())
                        } catch (e: Exception) {

                        }
                    }
                }
            }
        })
    }

    override fun getServiceCategories() {
        RetrofitClient.getApi().getDoctorSPServiceCategory(getAccessToken(), PrefsManager.get().getObject(Constants.USER_DETAIL, Profile::class.java)?.user_role_id
                ?: 1).enqueue(object : Callback<ApiResponseModel<ServiceModel>> {
            override fun onFailure(call: Call<ApiResponseModel<ServiceModel>>?, t: Throwable?) {
                getView()?.displayFailure(t?.message)
            }

            override fun onResponse(call: Call<ApiResponseModel<ServiceModel>>?, response: Response<ApiResponseModel<ServiceModel>>?) {
                if (response != null) {
                    if (response.isSuccessful) {
                        getView()?.setServiceCategory(response.body()?.data?.serviceCategories)
                    } else {
                        try {
                            getView()?.displayError(JSONObject(response.errorBody()?.string()).getString("msg"), response.code())
                        } catch (e: Exception) {

                        }
                    }
                }
            }
        })
    }

    override fun getLanguages() {
        RetrofitClient.getApi().getLanguages().enqueue(object : Callback<ApiResponseModel<LanguageSpokenModel>> {
            override fun onFailure(call: Call<ApiResponseModel<LanguageSpokenModel>>?, t: Throwable?) {
                getView()?.displayFailure(t?.message)
            }

            override fun onResponse(call: Call<ApiResponseModel<LanguageSpokenModel>>?, response: Response<ApiResponseModel<LanguageSpokenModel>>?) {
                if (response != null) {
                    if (response.isSuccessful) {
                        getView()?.setLanguageSpoken(response.body()?.data?.languages)
                    } else {
                        try {
                            getView()?.displayError(JSONObject(response.errorBody()?.string()).getString("msg"), response.code())
                        } catch (e: Exception) {

                        }
                    }
                }
            }
        })
    }

    override fun getDocumentTypes(countryId: String) {
        RetrofitClient.getApi().getDocumentsType(getAccessToken(), countryId).enqueue(object : Callback<ApiResponseModel<IdModel>> {
            override fun onFailure(call: Call<ApiResponseModel<IdModel>>?, t: Throwable?) {
                getView()?.displayFailure(t?.message)
            }

            override fun onResponse(call: Call<ApiResponseModel<IdModel>>?, response: Response<ApiResponseModel<IdModel>>?) {
                if (response != null) {
                    if (response.isSuccessful) {
                        getView()?.setDocumentType(response.body()?.data?.document_type)
                    } else {
                        try {
                            getView()?.displayError(JSONObject(response.errorBody()?.string()).getString("msg"), response.code())
                        } catch (e: Exception) {

                        }
                    }
                }
            }
        })
    }

    override fun signUp(map: HashMap<String, String>) {
        getView()?.showProgress()
        RetrofitClient.getApi().setUpAccount(getAccessToken(), map).enqueue(object : Callback<ApiResponseModel<OTPModel>> {
            override fun onFailure(call: Call<ApiResponseModel<OTPModel>>?, t: Throwable?) {
                getView()?.dismissProgress()
                getView()?.displayFailure(t?.message)
            }

            override fun onResponse(call: Call<ApiResponseModel<OTPModel>>?, response: Response<ApiResponseModel<OTPModel>>) {
                getView()?.dismissProgress()

                    if (response.isSuccessful)
                        getView()?.accountSetupSuccess(response.body()?.data?.profile)else getView()?.displayError(JSONObject(response.errorBody()?.string()).getString("msg"), response.code())
            }
        })
    }
}