package com.nizcaredoctor.views.accountSetup

import android.app.Activity
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.nizcaredoctor.R
import com.nizcaredoctor.utils.Constants
import com.nizcaredoctor.views.accountSetup.stateCity.StateCityActivity
import com.nizcaredoctor.webservice.model.GetItemModel
import kotlinx.android.synthetic.main.item_single.view.*

class RecyclerSingleItemAdapter(val activity: Activity?, var list: MutableList<GetItemModel>, val listType: String) : RecyclerView.Adapter<RecyclerSingleItemAdapter.ViewHolder>() {

    private var selectedPosition = 0
    var selectedId = 0

    init {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(activity).inflate(R.layout.item_single, parent, false))
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindView(list[position])
    }

    fun setFilter(filteredList: MutableList<GetItemModel>) {
        list = mutableListOf<GetItemModel>()
        list.addAll(filteredList)
        notifyDataSetChanged()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        init {
            itemView.tvSingleText.setOnClickListener {
                if (listType == Constants.States) {
                    (activity as StateCityActivity).selectedState(list[adapterPosition])

                } else {
                    (activity as StateCityActivity).selectedCity(list[adapterPosition])

                }
                selectedPosition = adapterPosition
                selectedId = list[selectedPosition].id
                notifyDataSetChanged()
            }
        }


        fun bindView(getItemModel: GetItemModel) = with(itemView) {
            tvSingleText.text = getItemModel.name
            if (getItemModel.id == selectedId) {
                ivSingleCheck.visibility = View.VISIBLE
                tvSingleText.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary))
            } else {
                ivSingleCheck.visibility = View.GONE
                tvSingleText.setTextColor(ContextCompat.getColor(context, R.color.black85))
            }
        }
    }
}
