package com.nizcaredoctor.views.accountSetup

import com.nizcaredoctor.views.baseViews.BasePresenter
import com.nizcaredoctor.views.baseViews.BaseView
import com.nizcaredoctor.webservice.model.GetItemModel
import com.nizcaredoctor.webservice.model.LanguageModelObject
import com.nizcaredoctor.webservice.model.Profile

interface AccountSetupContract {

    interface View : BaseView {
        fun setTitles(userTitles: MutableList<GetItemModel>?)
        fun setServiceCategory(serviceCategories: MutableList<GetItemModel>?)
        fun setLanguageSpoken(languages: List<LanguageModelObject>?)
        fun setDocumentType(document_type: MutableList<GetItemModel>?)
        fun accountSetupSuccess(profile: Profile?)
    }

    interface Presenter : BasePresenter<View> {
        fun getTitles()
        fun getServiceCategories()
        fun getLanguages()
        fun getDocumentTypes(countryId: String)
        fun signUp(map: HashMap<String, String>)
    }
}