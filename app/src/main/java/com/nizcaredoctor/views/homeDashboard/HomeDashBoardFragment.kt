package com.nizcaredoctor.views.homeDashboard

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.nizcaredoctor.R
import com.nizcaredoctor.utils.CommonViewPagerAdapter
import com.nizcaredoctor.views.baseViews.BaseViewFragImpl
import com.nizcaredoctor.views.homeDashboard.appointment.HomeAppointmentFragment
import com.nizcaredoctor.views.homeDashboard.consultation.HomeConsultationFragment
import kotlinx.android.synthetic.main.home_dashboard.*

class HomeDashBoardFragment : BaseViewFragImpl() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.home_dashboard, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        setListener()
    }

    private fun init() {
        val fragmentList = mutableListOf<Fragment>()
        val titleList = mutableListOf<String>()
        fragmentList.add(HomeConsultationFragment())
        fragmentList.add(HomeAppointmentFragment())
        titleList.add(getString(R.string.consultations))
        titleList.add(getString(R.string.appointments))
        viewPager.adapter = CommonViewPagerAdapter(context!!, fragmentList, titleList, activity!!.supportFragmentManager)
        tabLayout.setupWithViewPager(viewPager)
    }

    private fun setListener() {

    }
}