package com.nizcaredoctor.views.homeDashboard.consultation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.nizcaredoctor.R
import com.nizcaredoctor.views.baseViews.BaseViewFragImpl
import kotlinx.android.synthetic.main.fragment_regular_consultation.*
import kotlinx.android.synthetic.main.layout_doctor_forum.*

class HomeConsultationFragment : BaseViewFragImpl() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_consultation, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        setListener()
    }

    private fun init() {
        rvRegularConsultation.adapter = RegularConsultationAdapter(context)
        rvDoctorForum.adapter = DoctorForumAdapter(context)
    }

    private fun setListener() {

    }
}