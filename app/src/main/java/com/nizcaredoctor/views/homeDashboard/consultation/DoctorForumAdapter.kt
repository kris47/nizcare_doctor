package com.nizcaredoctor.views.homeDashboard.consultation

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.nizcaredoctor.R

class DoctorForumAdapter(val context : Context?) : RecyclerView.Adapter<DoctorForumAdapter.ViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_doctor_forum, parent, false))
    }

    override fun getItemCount() = 10

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

    }

    inner class ViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView){

    }
}