package com.nizcaredoctor.views.verification

import com.nizcaredoctor.utils.Constants
import com.nizcaredoctor.utils.PrefsManager
import com.nizcaredoctor.views.baseViews.BasePresenterImpl
import com.nizcaredoctor.webservice.ApiResponseModel
import com.nizcaredoctor.webservice.RetrofitClient
import com.nizcaredoctor.webservice.model.OTPModel
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class VerificationPresenter : BasePresenterImpl<VerificationContract.View>(), VerificationContract.Presenter {

    override fun verifyOTP(map: HashMap<String, String>) {
        getView()?.showProgress()

        RetrofitClient.getApi().verifyOTP(map).enqueue(object : Callback<ApiResponseModel<OTPModel>> {
            override fun onFailure(call: Call<ApiResponseModel<OTPModel>>?, t: Throwable?) {
                getView()?.dismissProgress()
                getView()?.displayFailure("")
            }

            override fun onResponse(call: Call<ApiResponseModel<OTPModel>>?, response: Response<ApiResponseModel<OTPModel>>?) {
                getView()?.dismissProgress()
                if (response != null) {
                    if (response.isSuccessful) {
                        PrefsManager.get().save(Constants.Access_Token, response.body()?.data?.token
                                ?: "")
                        PrefsManager.get().save(Constants.USER_DETAIL, response.body()?.data?.profile)
                        getView()?.otpSuccess()
                    } else {
                        try {
                            getView()?.displayError(JSONObject(response.errorBody()?.string()).getString("msg"), response.code())
                        } catch (e: Exception) {

                        }
                    }
                }
            }
        })
    }

    override fun verifyPasswordToken(map: HashMap<String, String>) {
        getView()?.showProgress()
        RetrofitClient.getApi().verifyPasswordOTP(map).enqueue(object : Callback<ApiResponseModel<OTPModel>> {
            override fun onFailure(call: Call<ApiResponseModel<OTPModel>>?, t: Throwable?) {
                getView()?.dismissProgress()
                getView()?.displayFailure("")
            }
            override fun onResponse(call: Call<ApiResponseModel<OTPModel>>?, response: Response<ApiResponseModel<OTPModel>>?) {
                getView()?.dismissProgress()
                if (response != null) {
                    if (response.isSuccessful) {
                        getView()?.passwordOtpSuccess(response.body()?.data?.token, response.body()?.msg)
                    } else {
                        try {
                            getView()?.displayError(JSONObject(response.errorBody()?.string()).getString("msg"), response.code())
                        } catch (e: Exception) {

                        }
                    }
                }
            }
        })
    }

    override fun apiResend(hashMap: HashMap<String, String>) {
        getView()?.showProgress()

        RetrofitClient.getApi().signUp(hashMap).enqueue(object : Callback<ApiResponseModel<Any>> {
            override fun onResponse(call: Call<ApiResponseModel<Any>>?, response: Response<ApiResponseModel<Any>>?) {
                getView()?.dismissProgress()
                if (response != null) {
                    if (response.isSuccessful) {
                        getView()?.otpResend(response.body()?.msg)
                    } else {
                        try {
                            getView()?.displayError(JSONObject(response.errorBody()?.string()).getString("msg"), response.code())
                        }catch (e:Exception){

                        }
                    }
                }
            }

            override fun onFailure(call: Call<ApiResponseModel<Any>>?, t: Throwable?) {
                getView()?.dismissProgress()
                getView()?.displayFailure("Server error")
            }
        })
    }

    override fun apiForgotResend(hashMap: HashMap<String, String>) {
        getView()?.showProgress()
        RetrofitClient.getApi().forgotPassword(hashMap).enqueue(object : Callback<ApiResponseModel<OTPModel>> {
            override fun onFailure(call: Call<ApiResponseModel<OTPModel>>?, t: Throwable?) {
                getView()?.displayFailure(t?.message)
                getView()?.dismissProgress()
            }

            override fun onResponse(call: Call<ApiResponseModel<OTPModel>>?, response: Response<ApiResponseModel<OTPModel>>?) {
                getView()?.dismissProgress()
                if (response != null) {
                    if (response.isSuccessful) {
                        getView()?.forgotPasswordOTPResendSuccess(response.body()?.data?.token,response.body()?.msg)
                    } else {
                        try {
                            getView()?.displayError(JSONObject(response.errorBody()?.string()).getString("msg"), response.code())
                        } catch (e: Exception) {

                        }
                    }
                }
            }
        })
    }
}