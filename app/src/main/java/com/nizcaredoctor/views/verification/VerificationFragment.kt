package com.nizcaredoctor.views.verification

import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.text.Editable
import android.text.SpannableStringBuilder
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.Toast
import com.nizcaredoctor.R
import com.nizcaredoctor.utils.*
import com.nizcaredoctor.views.baseViews.BaseViewFragImpl
import com.nizcaredoctor.views.changePassword.ChangePasswordFragment
import com.nizcaredoctor.webservice.WebConstants
import kotlinx.android.synthetic.main.fragment_verification.*

class VerificationFragment : BaseViewFragImpl(), VerificationContract.View {

    private val presenter = VerificationPresenter()
    private var forgotToken = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_verification, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        setListeners()
    }

    private fun init() {
        dialog = DialogIndeterminate(context)
        if (arguments?.getString(Constants.VERIFICATIONTYPE) == Constants.FORGOTPASSWORD) {
            forgotToken = arguments?.getString(WebConstants.TOKEN).toString()
        }
    }

    private fun setListeners() {
        presenter.attachView(this)
        toolbar.setNavigationOnClickListener { activity?.supportFragmentManager?.popBackStack() }
        etPin1.addTextChangedListener(OptWatcher(etPin1))
        etPin2.addTextChangedListener(OptWatcher(etPin2))
        etPin3.addTextChangedListener(OptWatcher(etPin3))
        etPin4.addTextChangedListener(OptWatcher(etPin4))

        btnVerify.setOnClickListener({
            btnVerify.hideKeyboard()
            if (getOTP().length != 4) {
                view?.showErrorSnack(getString(R.string.otp_empty_validation))
                return@setOnClickListener
            }

            if (arguments?.getString(Constants.VERIFICATIONTYPE) == Constants.SIGNUP) {
                if (isOnline(context)) {
                    val map = HashMap<String, String>()
                    map[WebConstants.OTP] = getOTP()
                    map[WebConstants.USERTYPE] = Constants.DOCTOR.toString()
                    map[WebConstants.EMAIL] = arguments?.getString(WebConstants.EMAIL).toString()
                    map[WebConstants.PHONECODE] = arguments?.getString(WebConstants.PHONECODE).toString()
                    map[WebConstants.PHONENUMBER] = arguments?.getString(WebConstants.PHONENUMBER).toString()
                    map[WebConstants.PASSWORD] = arguments?.getString(WebConstants.PASSWORD).toString()
                    map[WebConstants.LOGINTYPE] = Constants.MOBILE.toString()
                    map[WebConstants.APPLANGUAGEID] = PrefsManager.get().getString(Constants.APPLANGUAGEID, "1")
                    map[WebConstants.LOCALE] = PrefsManager.get().getString(Constants.LOCALE, "en")
                    map[WebConstants.LATITUDE] = "0"
                    map[WebConstants.LONGITUDE] = "0"
                    map[WebConstants.FCMTOKEN] = ""

                    presenter.verifyOTP(map)
                } else {
                    view?.showInternetError()
                }
            } else {
                if (isOnline(context)) {
                    val map = HashMap<String, String>()
                    map[WebConstants.OTP] = getOTP()
                    map[WebConstants.TOKEN] = forgotToken
                    presenter.verifyPasswordToken(map)
                } else {
                    view?.showInternetError()
                }
            }

        })

        tvResend.setOnClickListener {
            if (arguments?.getString(Constants.VERIFICATIONTYPE) == Constants.SIGNUP) {
                if (isOnline(context)) {
                    callResendAPI()
                } else {
                    view?.showInternetError()
                }
            } else {
                if (isOnline(context)) {
                    callForgotResentAPI()

                } else {
                    view?.showInternetError()
                }

            }

        }
    }

    private fun callResendAPI() {
        val map = HashMap<String, String>()
        map[WebConstants.USERTYPE] = Constants.DOCTOR.toString()
        map[WebConstants.EMAIL] = arguments?.getString(Constants.EMAIL).toString()
        map[WebConstants.PHONECODE] = arguments?.getString(WebConstants.PHONECODE).toString()
        map[WebConstants.PHONENUMBER] = arguments?.getString(WebConstants.PHONENUMBER).toString()
        map[WebConstants.LOCALE] = PrefsManager.get().getString(Constants.LOCALE, "en")
        presenter.apiResend(map)
    }

    private fun callForgotResentAPI() {
        val hashMap: HashMap<String, String> = HashMap()
        hashMap.put(WebConstants.APPTYPE, WebConstants.DOCTOR.toString())
        hashMap.put(WebConstants.PHONENUMBER, arguments?.getString(WebConstants.PHONENUMBER).toString())
        hashMap.put(WebConstants.LOCALE, PrefsManager.get().getString(Constants.LOCALE, "en"))
        presenter.apiForgotResend(hashMap)
    }

    override fun otpResend(msg: String?) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()
        clearOTP()
    }

    override fun forgotPasswordOTPResendSuccess(token: String?, msg: String?) {
        if (token != null)
            forgotToken = token
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()
        clearOTP()
    }

    override fun passwordOtpSuccess(token: String?, msg: String?) {
        if (token != null) {
            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()
            val bundle = Bundle()
            bundle.putString(WebConstants.TOKEN, token)
            bundle.putString(Constants.VERIFICATIONTYPE, Constants.FORGOTPASSWORD)
            val fragment = ChangePasswordFragment()
            fragment.arguments = bundle
            activity?.supportFragmentManager?.beginTransaction()?.setCustomAnimations(R.anim.slide_in_right,
                    R.anim.slide_out_left,
                    R.anim.slide_in_left,
                    R.anim.slide_out_right)?.replace(R.id.container, fragment, "otp")?.addToBackStack("")?.commit()
        }
    }

    private fun clearOTP() {
        etPin1.text = SpannableStringBuilder("")
        etPin2.text = SpannableStringBuilder("")
        etPin3.text = SpannableStringBuilder("")
        etPin4.text = SpannableStringBuilder("")
        etPin1.requestFocus()
    }

    override fun otpSuccess() {
        activity?.supportFragmentManager?.popBackStack()
        val bundle = Bundle()
        bundle.putString(Constants.EMAIL, arguments?.getString(WebConstants.EMAIL).toString())
        val successFragment = SuccessFragment()
        successFragment.arguments = bundle
        activity?.supportFragmentManager?.beginTransaction()?.setCustomAnimations(R.anim.slide_in_right,
                R.anim.slide_out_left,
                R.anim.slide_in_left,
                R.anim.slide_out_right)?.replace(R.id.container, successFragment)?.commit()
    }


    private fun getOTP(): String {
        val stringBuilder = StringBuilder()
        stringBuilder.append(etPin1.text.toString())
        stringBuilder.append(etPin2.text.toString())
        stringBuilder.append(etPin3.text.toString())
        stringBuilder.append(etPin4.text.toString())
        return stringBuilder.toString()
    }

    inner class OptWatcher(val etOtp: EditText?) : TextWatcher {
        override fun afterTextChanged(p0: Editable?) {
            if (p0?.length == 1) {
                etOtp?.background = context?.let { ContextCompat.getDrawable(it, R.drawable.otp_filled_back) }
                if (etOtp?.nextFocusForwardId != View.NO_ID) {
                    // For first 3 edit text, move to the next one if text length is 1
                    //activity?.findViewById<View>(etOtp?.nextFocusForwardId!!)?.requestFocus()
                    etOtp?.focusSearch(View.FOCUS_RIGHT)?.requestFocus()
                } else {
                    // etOtp.clearFocus()
                    etOtp.hideKeyboard()
                }
            } else {
                etOtp?.background = context?.let { ContextCompat.getDrawable(it, R.drawable.otp_normal_back) }
                etOtp?.focusSearch(View.FOCUS_LEFT)?.requestFocus()
            }


        }

        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        }

        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        presenter.detachView()
    }
}