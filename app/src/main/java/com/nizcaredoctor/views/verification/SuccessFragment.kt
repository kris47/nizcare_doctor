package com.nizcaredoctor.views.verification

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.view.animation.BounceInterpolator
import android.view.animation.Interpolator
import com.nizcaredoctor.R
import com.nizcaredoctor.utils.hideKeyboard
import com.nizcaredoctor.views.accountSetup.AccountSetUpActivity
import kotlinx.android.synthetic.main.fragment_success.*


class SuccessFragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_success, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        setData()
        setListener()
    }

    private fun init() {
        val animRight = AnimationUtils.loadAnimation(activity, R.anim.slide_left_right)
        animRight.duration = 1500
        animRight.fillAfter = true
        animRight.interpolator = BounceInterpolator()
        ivThumb.startAnimation(animRight)
    }

    private fun setData() {

    }

    private fun setListener() {
        tvSetupAccount.setOnClickListener({
            tvSetupAccount.hideKeyboard()
            startActivity(Intent(activity, AccountSetUpActivity::class.java))
            activity?.finish()
        })
    }
}