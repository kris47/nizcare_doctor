package com.nizcaredoctor.views.verification

import com.nizcaredoctor.views.baseViews.BasePresenter
import com.nizcaredoctor.views.baseViews.BaseView

interface VerificationContract {

    interface View : BaseView {
        fun otpSuccess()
        fun passwordOtpSuccess(token: String?, msg: String?)
        fun otpResend(msg: String?)
        fun forgotPasswordOTPResendSuccess(token: String?, msg: String?)
    }

    interface Presenter : BasePresenter<View> {
        fun verifyOTP(map: HashMap<String, String>)
        fun verifyPasswordToken(map: HashMap<String, String>)
        fun apiForgotResend(hashMap: HashMap<String, String>)
        fun apiResend(hashMap: HashMap<String, String>)
    }
}