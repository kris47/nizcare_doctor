package com.nizcaredoctor.views.baseViews

import com.nizcaredoctor.R
import com.nizcaredoctor.utils.DialogIndeterminate
import com.nizcaredoctor.webservice.DialogPopup
import com.nizcaredoctor.webservice.ErrorCodes

open class BaseViewActivityImpl : BaseActivity(), BaseView {

    override fun showProgress() {
        if (dialog == null) {
            dialog = DialogIndeterminate(this)
        }
        dialog?.show()
    }

    override fun dismissProgress() {
        if (dialog != null)
            dialog?.dismiss()
    }

    override fun displayError(errorMessage: String?, code: Int) {
        ErrorCodes.checkCode(this, code, errorMessage)
        dismissProgress()
    }

    override fun displayFailure(failureMessage: String?) {
        DialogPopup().alertPopup(this, resources.getString(R.string.dialog_alert), failureMessage, "").show()
        dismissProgress()
    }

    var dialog: DialogIndeterminate? = null

}