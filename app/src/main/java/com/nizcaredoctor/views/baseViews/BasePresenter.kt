package com.nizcaredoctor.views.baseViews


interface BasePresenter<in V : BaseView> {

    fun attachView(view: V)

    fun detachView()
}