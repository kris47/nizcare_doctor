package com.nizcaredoctor.views.baseViews

import android.support.v4.app.Fragment
import com.nizcaredoctor.R
import com.nizcaredoctor.utils.DialogIndeterminate
import com.nizcaredoctor.webservice.DialogPopup
import com.nizcaredoctor.webservice.ErrorCodes

/**
 * @author ${Harsh Singla CBL-111}
 */


open class BaseViewFragImpl : Fragment(), BaseView {

    override fun showProgress() {
        if (dialog == null) {
            dialog = DialogIndeterminate(activity)
        }
        dialog?.show()
    }

    override fun dismissProgress() {
        if (dialog != null)
            dialog?.dismiss()
    }

    override fun displayError(errorMessage: String?, code: Int) {
        ErrorCodes.checkCode(activity, code, errorMessage)
    }

    override fun displayFailure(failureMessage: String?) {
        DialogPopup().alertPopup(activity, resources.getString(R.string.dialog_alert), failureMessage, "").show()
    }

    var dialog: DialogIndeterminate? = null
}