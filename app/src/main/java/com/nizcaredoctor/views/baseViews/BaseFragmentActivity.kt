package com.nizcaredoctor.views.baseViews

import android.content.Context
import android.os.Bundle
import android.support.v4.app.FragmentActivity
import android.support.v7.app.AppCompatActivity
import com.nizcaredoctor.utils.Constants
import com.nizcaredoctor.utils.LocaleHelper
import com.nizcaredoctor.utils.PrefsManager

open class BaseFragmentActivity : FragmentActivity() {

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(LocaleHelper.onAttach(newBase, PrefsManager.get().getString(Constants.LOCALE, "en")))
    }
}