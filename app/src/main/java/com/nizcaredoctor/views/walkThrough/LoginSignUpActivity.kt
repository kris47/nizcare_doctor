package com.nizcaredoctor.views.walkThrough

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.v4.view.ViewPager
import android.view.animation.AnimationUtils
import android.widget.TextView
import com.nizcaredoctor.views.login.LoginActivity
import com.nizcaredoctor.R
import com.nizcaredoctor.views.baseViews.BaseActivity
import com.nizcaredoctor.views.language.LanguageActivity
import com.nizcaredoctor.views.signUp.SignUpActivity
import kotlinx.android.synthetic.main.activity_login_sign_up.*

class LoginSignUpActivity : BaseActivity(), ViewPager.OnPageChangeListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login_sign_up)
        init()
        setListeners()
        setUpTextSwitcher()
    }

    private fun init() {
        pagerLoginSignUp.adapter = ImagePagerAdapter(this)
        tabsDot.setupWithViewPager(pagerLoginSignUp)
        pagerLoginSignUp.addOnPageChangeListener(this)
    }

    private fun setListeners() {
        tvLogin.setOnClickListener {
            startActivity(Intent(this@LoginSignUpActivity, LoginActivity::class.java))
        }

        tvSignUp.setOnClickListener{
            startActivity(Intent(this@LoginSignUpActivity, SignUpActivity::class.java))
        }

        tvLanguage.setOnClickListener {
            startActivity(Intent(this@LoginSignUpActivity, LanguageActivity::class.java))

        }
    }

    private fun setUpTextSwitcher() {
        tvTextSwitch.setFactory({
            val myText = TextView(tvTextSwitch.context)
            myText.textSize = 36f
            myText.setTextColor(Color.WHITE)
            myText
        })
        tvTextSwitch.inAnimation = AnimationUtils.loadAnimation(tvTextSwitch.context, android.R.anim.slide_in_left)
        tvTextSwitch.outAnimation = AnimationUtils.loadAnimation(tvTextSwitch.context, android.R.anim.slide_out_right)
        tvTextSwitch.setText(getString(R.string.walk_line_1))
    }

    override fun onPageScrollStateChanged(state: Int) {

    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

    }

    override fun onPageSelected(position: Int) {
        when (position) {
            0 -> tvTextSwitch.setText(getString(R.string.walk_line_1))
            1 -> tvTextSwitch.setText(getString(R.string.walk_line_2))
            2 -> tvTextSwitch.setText(getString(R.string.walk_line_3))
        }
    }
}