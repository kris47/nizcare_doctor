package com.nizcaredoctor.views.walkThrough

import android.app.Activity
import android.content.Context
import android.support.v4.view.PagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import com.nizcaredoctor.R
import kotlinx.android.synthetic.main.layout_image_walk_through.view.*

class ImagePagerAdapter(val activity: Activity) : PagerAdapter() {

    private val layoutInflater: LayoutInflater = activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater


    override fun getCount(): Int {
        return 3
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object` as RelativeLayout
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val layout = layoutInflater.inflate(R.layout.layout_image_walk_through, container, false)
        val resId = activity.resources.getIdentifier("a" + (position+1).toString(), "drawable", activity.packageName)
        layout.ivTut.setImageResource(resId)
        container.addView(layout)
        return layout
    }

}
