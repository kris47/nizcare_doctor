package com.nizcaredoctor.views.login

import com.nizcaredoctor.views.baseViews.BasePresenter
import com.nizcaredoctor.views.baseViews.BaseView
import com.nizcaredoctor.webservice.model.OTPModel

interface LoginContract {

    interface View: BaseView {
        fun loginSuccess(data: OTPModel?)

    }

    interface Presenter: BasePresenter<View> {
        fun login(map: HashMap<String, String>)
    }
}