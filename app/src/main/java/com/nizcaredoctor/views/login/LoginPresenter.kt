package com.nizcaredoctor.views.login

import com.nizcaredoctor.views.baseViews.BasePresenterImpl
import com.nizcaredoctor.webservice.ApiResponseModel
import com.nizcaredoctor.webservice.RetrofitClient
import com.nizcaredoctor.webservice.model.OTPModel
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginPresenter : BasePresenterImpl<LoginContract.View>(), LoginContract.Presenter {
    override fun login(map: HashMap<String, String>) {
        getView()?.showProgress()
        RetrofitClient.getApi().login(map).enqueue(object : Callback<ApiResponseModel<OTPModel>> {
            override fun onFailure(call: Call<ApiResponseModel<OTPModel>>?, t: Throwable?) {
                getView()?.dismissProgress()
                getView()?.displayFailure(t?.message)
            }

            override fun onResponse(call: Call<ApiResponseModel<OTPModel>>?, response: Response<ApiResponseModel<OTPModel>>) {
                getView()?.dismissProgress()
                if (response.isSuccessful) {
                    getView()?.loginSuccess(response.body()?.data)
                } else
                    getView()?.displayError(JSONObject(response.errorBody()?.string()).getString("msg"), response.code())

            }
        })
    }
}