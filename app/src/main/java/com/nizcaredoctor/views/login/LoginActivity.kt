package com.nizcaredoctor.views.login

import android.content.Intent
import android.os.Bundle
import android.view.View
import com.nizcaredoctor.R
import com.nizcaredoctor.utils.*
import com.nizcaredoctor.views.baseViews.BaseViewActivityImpl
import com.nizcaredoctor.views.accountSetup.AccountSetUpActivity
import com.nizcaredoctor.views.forgotPassword.ForgotPasswordActivity
import com.nizcaredoctor.views.home.HomeActivity
import com.nizcaredoctor.views.signUp.SignUpActivity
import com.nizcaredoctor.webservice.WebConstants
import com.nizcaredoctor.webservice.model.OTPModel
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : BaseViewActivityImpl(), LoginContract.View, View.OnClickListener {

    private val presenter = LoginPresenter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        init()
        setListeners()
    }

    private fun init() {

    }

    private fun setListeners() {
        presenter.attachView(this)
        tvLoggedIn.setOnClickListener(this)
        tvForgotPassword.setOnClickListener(this)
        toolbar.setNavigationOnClickListener { onBackPressed() }
        tvSignUp.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.tvLoggedIn -> {
                tilPhoneNo.isErrorEnabled = false
                tilPassword.isErrorEnabled = false
                if (isValidated()) {
                    if (isOnline(this)) {
                        val map = HashMap<String, String>()
                        map[WebConstants.PHONENUMBER] = etPhone.text.toString()
                        map[WebConstants.PASSWORD] = etPassword.text.toString()
                        map[WebConstants.LOGINTYPE] = Constants.MOBILE.toString()
                        map[WebConstants.APPTYPE] = WebConstants.DOCTOR.toString()
                        map[WebConstants.APPLANGUAGEID] = PrefsManager.get().getString(Constants.APPLANGUAGEID, "1")
                        map[WebConstants.LOCALE] = PrefsManager.get().getString(Constants.LOCALE, "en")
                        map[WebConstants.LATITUDE] = "0"
                        map[WebConstants.LONGITUDE] = "0"
                        map[WebConstants.FCMTOKEN] = ""
                        presenter.login(map)
                    } else {
                        rlRootLogin.showInternetError()
                    }
                }
            }

            R.id.tvForgotPassword -> {
                startActivity(Intent(this@LoginActivity, ForgotPasswordActivity::class.java))
            }

            R.id.tvSignUp -> {
                startActivity(Intent(this@LoginActivity, SignUpActivity::class.java))
                finish()
                activityFinishAnimation()
            }
        }
    }

    override fun loginSuccess(data: OTPModel?) {
        if (data != null) {
            PrefsManager.get().save(Constants.Access_Token, data.token ?: "")
            PrefsManager.get().save(Constants.USER_DETAIL, data.profile)

            if (data.profile?.signup_step == 0) {
                startActivity(Intent(this, AccountSetUpActivity::class.java))
                finish()
            } else {
                startActivity(Intent(this, HomeActivity::class.java))
                finishAffinity()
            }
        }
    }

    private fun isValidated(): Boolean {
        return when {
            etPhone.text.trim().toString().isEmpty() -> {
                tilPhoneNo.isErrorEnabled = true
                tilPhoneNo.error = getString(R.string.enter_mobile_no)
                tilPhoneNo.requestFocus()
                false
            }

            etPhone.text.trim().toString().length < 10 -> {
                tilPhoneNo.isErrorEnabled = true
                tilPhoneNo.error = getString(R.string.enter_valid_mobile_no)
                tilPhoneNo.requestFocus()
                false
            }

            !isValidMobile(etPhone.text.toString()) -> {
                tilPhoneNo.isErrorEnabled = true
                etPhone.error = getString(R.string.enter_valid_mobile_no)
                etPhone.requestFocus()
                false
            }

            etPassword.text.toString().trim().isEmpty() -> {
                tilPassword.isErrorEnabled = true
                tilPassword.error = getString(R.string.enter_pasword)
                tilPassword.requestFocus()
                false
            }

            etPassword.text.toString().trim().length < 6 -> {
                tilPassword.isErrorEnabled = true
                tilPassword.error = getString(R.string.password_is_short)
                tilPassword.requestFocus()
                false
            }

            else -> true
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        etPassword.hideKeyboard()
        finish()
        activityFinishAnimation()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detachView()
    }
}