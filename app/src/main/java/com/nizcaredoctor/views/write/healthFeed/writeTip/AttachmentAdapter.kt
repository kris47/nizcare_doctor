package com.nizcaredoctor.views.write.healthFeed.writeTip

import android.app.Activity
import android.app.DownloadManager
import android.content.Context.DOWNLOAD_SERVICE
import android.net.Uri
import android.os.Environment
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.nizcaredoctor.R
import com.nizcaredoctor.utils.Constants
import com.nizcaredoctor.views.write.chat.privateChat.PrivateChatActivity
import com.nizcaredoctor.views.write.healthFeed.viewHealthTip.ViewHealthFeedActivity
import com.nizcaredoctor.webservice.model.AttachmentModel
import kotlinx.android.synthetic.main.item_attachements.view.*

class AttachmentAdapter(val activity: Activity, val attachmentList: MutableList<AttachmentModel>) : RecyclerView.Adapter<AttachmentAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_attachements, parent, false))
    }

    override fun getItemCount(): Int {
        return attachmentList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (activity is WriteHealthTipActivity || activity is ViewHealthFeedActivity || activity is PrivateChatActivity) {
            holder.itemView.tvFile.text = attachmentList[position].name
            holder.itemView.tvFile.visibility = View.VISIBLE
        } else {
            holder.itemView.tvFile.visibility = View.GONE
        }

        if (activity is ViewHealthFeedActivity) {
            holder.itemView.ivCross.visibility = View.GONE
        } else {
            holder.itemView.ivCross.visibility = View.VISIBLE
        }

        if (attachmentList[position].fileType == Constants.PDF) {
            holder.itemView.ivTextFile.visibility = View.VISIBLE
            holder.itemView.ivFile.visibility = View.INVISIBLE

        } else {
            holder.itemView.ivTextFile.visibility = View.INVISIBLE
            holder.itemView.ivFile.visibility = View.VISIBLE
            if (attachmentList[position].file == null) {
                Glide.with(activity)
                        .asDrawable()
                        .load(attachmentList[position].name)
                        .into(holder.itemView.ivFile)
            } else {
                Glide.with(activity)
                        .load(attachmentList[position].file)
                        .into(holder.itemView.ivFile)
            }
        }
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        init {

            itemView.ivCross.setOnClickListener {
                attachmentList.removeAt(adapterPosition)
                notifyItemRemoved(adapterPosition)
                if (attachmentList.isEmpty()) {
                    when (activity) {
                        is WriteHealthTipActivity -> activity.hideView()
                        is PrivateChatActivity -> activity.hideView()
                    }
                }
            }

            itemView.setOnClickListener {
                if ((activity is ViewHealthFeedActivity)) {
                    try {
                        val downloadManager = activity.getSystemService(DOWNLOAD_SERVICE) as DownloadManager
                        val Download_Uri = Uri.parse(attachmentList[adapterPosition].name)
                        val request = DownloadManager.Request(Download_Uri)
                        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI or DownloadManager.Request.NETWORK_MOBILE)
                        request.setAllowedOverRoaming(false)
                        request.setTitle("Nizcare Doctor")
                        request.setDescription("Downloading " + attachmentList[adapterPosition].name)
                        request.setVisibleInDownloadsUi(true)
                        request.setNotificationVisibility(View.VISIBLE)
                        if (attachmentList[adapterPosition].fileType == Constants.PDF) {
                            request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "/nizcare" + "/" + attachmentList[adapterPosition].name + ".pdf")
                        } else {
                            request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "/nizcare" + "/" + attachmentList[adapterPosition].name)
                        }
                        val refid = downloadManager.enqueue(request)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }
        }
    }
}