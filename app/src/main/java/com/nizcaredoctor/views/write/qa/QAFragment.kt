package com.nizcaredoctor.views.write.qa

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.nizcaredoctor.R
import com.nizcaredoctor.utils.Constants
import com.nizcaredoctor.utils.isOnline
import com.nizcaredoctor.views.baseViews.BaseViewFragImpl
import com.nizcaredoctor.webservice.model.qaFeedModel.DataItem
import com.nizcaredoctor.webservice.model.qaFeedModel.Questions
import kotlinx.android.synthetic.main.fragment_qa.*

class QAFragment : BaseViewFragImpl(), QAContract.View, SwipeRefreshLayout.OnRefreshListener {

    private val presenter = QAPresenter()
    private lateinit var adapter: QAAdapter
    private val questionsList = mutableListOf<DataItem>()
    private lateinit var layoutManager: LinearLayoutManager
    private var total = 0
    private var page = 1

    private val broadCastQA = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            qaFeeds(1)
        }
    }

    override fun onStart() {
        super.onStart()
        activity?.registerReceiver(broadCastQA, IntentFilter("refreshQA"))
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_qa, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        setListener()
        qaFeeds(page)
    }

    private fun init() {
        layoutManager = LinearLayoutManager(context)
        rvQA.layoutManager = layoutManager
        adapter = QAAdapter(activity, questionsList, this)
        rvQA.adapter = adapter
    }

    private fun setListener() {
        presenter.attachView(this)
        swipeRefresh.setColorSchemeColors(ContextCompat.getColor(context!!, R.color.colorPrimary))
        swipeRefresh.setOnRefreshListener(this)
        rvQA.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (layoutManager.findLastCompletelyVisibleItemPosition() == rvQA.adapter.itemCount - 1) {
                    if (questionsList.size < total) {
                        qaFeeds(++page)
                    }
                }
            }
        })
    }

    private fun qaFeeds(page: Int) {
        if (page == 1) questionsList.clear()
        if (isOnline(context)) {
            presenter.getQAFeed(page)
        } else {
            viewFlipper.displayedChild = 1
            swipeRefresh.isRefreshing = false
            noDataView.setImage(R.raw.no_internet)
            noDataView.setData(getString(R.string.no_interner_connection), getString(R.string.interent_substitles))
        }
    }

    override fun setQAFeedList(questions: Questions?) {
        if (questions != null) {
            if (questions.data.isNotEmpty()) {
                viewFlipper.displayedChild = 2
                questionsList.addAll(questions.data)
                rvQA.adapter.notifyDataSetChanged()
            } else {
                viewFlipper.displayedChild = 1
                noDataView.setImage(R.raw.no_data)
                noDataView.setData("No Q&A feeds available.", "")
            }
            total = questions.total
            page = questions.current_page
        }
    }


    override fun onDestroyView() {
        super.onDestroyView()
        presenter.detachView()
    }

    fun upVote(adapterPosition: Int) {
        if (isOnline(context)) {
            val status = if (questionsList[adapterPosition].answers?.my_vote_count == 0) 1 else 0
            if (status == 1) {
                questionsList[adapterPosition].answers?.my_vote_count = 1
                questionsList[adapterPosition].answers?.upvote_count = questionsList[adapterPosition].answers?.upvote_count?.plus(1)
            } else {
                questionsList[adapterPosition].answers?.my_vote_count = 0
                questionsList[adapterPosition].answers?.upvote_count = questionsList[adapterPosition].answers?.upvote_count?.minus(1)
            }
            adapter.notifyItemChanged(adapterPosition)
            presenter.upVoteAPI(questionsList[adapterPosition].answers?.id,
                    Constants.ANSWER_TYPE,
                    status)
        } else {

        }
    }

    override fun onRefresh() {
        qaFeeds(1)
    }

    override fun showProgress() {
        swipeRefresh.isRefreshing = true
    }

    override fun dismissProgress() {
        swipeRefresh.isRefreshing = false
    }

    fun bookmarkQA(adapterPosition: Int) {
        if (isOnline(context)) {
            val status = if (questionsList[adapterPosition].answers?.bookmark_count == 0) 1 else 0
            questionsList[adapterPosition].answers?.bookmark_count = if (questionsList[adapterPosition].answers?.bookmark_count == 0) 1 else 0
            adapter.notifyItemChanged(adapterPosition)
            presenter.bookmarkQA(questionsList[adapterPosition].answers?.id,
                    Constants.ANSWER_TYPE,
                    status)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        activity?.unregisterReceiver(broadCastQA)
    }
}
