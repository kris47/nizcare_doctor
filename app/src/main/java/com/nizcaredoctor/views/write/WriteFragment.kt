package com.nizcaredoctor.views.write

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.nizcaredoctor.R
import com.nizcaredoctor.views.write.chat.ChatFragment
import com.nizcaredoctor.views.write.enquiries.EnquiriesFragment
import com.nizcaredoctor.views.write.healthFeed.HealthFeedFragment
import com.nizcaredoctor.views.write.qa.QAFragment
import kotlinx.android.synthetic.main.fragment_write.*

class WriteFragment : Fragment(), ViewPager.OnPageChangeListener {

    private val fragmentList = mutableListOf<Fragment>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_write, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        fragmentList.clear()
        fragmentList.add(ChatFragment())
        fragmentList.add(EnquiriesFragment())
        fragmentList.add(HealthFeedFragment())
        fragmentList.add(QAFragment())

        val titleArray = arrayOf(getString(R.string.chat),
                getString(R.string.enquiries),
                getString(R.string.health_feed),
                getString(R.string.qa))

        viewpagerWrite.adapter = PagerTitleAdapter(fragmentList, titleArray, childFragmentManager)
        viewpagerWrite.addOnPageChangeListener(this)
        viewpagerWrite.offscreenPageLimit = 3
        tabWrite.setupWithViewPager(viewpagerWrite)
    }

    override fun onPageScrollStateChanged(state: Int) {

    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

    }

    override fun onPageSelected(position: Int) {

    }
}