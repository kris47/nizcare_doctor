package com.nizcaredoctor.views.write.healthFeed.writeTip

import android.Manifest
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.nizcaredoctor.R
import com.nizcaredoctor.utils.*
import com.nizcaredoctor.views.baseViews.BaseViewActivityImpl
import com.nizcaredoctor.webservice.WebConstants
import com.nizcaredoctor.webservice.model.AttachmentModel
import com.nizcaredoctor.webservice.model.Profile
import kotlinx.android.synthetic.main.activity_write_health_tip.*
import pub.devrel.easypermissions.EasyPermissions
import pub.devrel.easypermissions.PermissionRequest
import java.io.File


class WriteHealthTipActivity : BaseViewActivityImpl(), WriteHealthTipContract.View, EasyPermissions.PermissionCallbacks, ImagePicker.ImagePickerListener {


    private val presenter = WriteHealthTipPresenter()
    private lateinit var attachmentAdapter: AttachmentAdapter
    private val attachmentList = mutableListOf<AttachmentModel>()
    private lateinit var imagePicker: ImagePicker
    private val permissions = arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    private val permissionRequestCode = 56
    private lateinit var amazoneS3: AmazoneS3

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_write_health_tip)
        init()
        setListeners()

    }

    private fun init() {
        attachmentAdapter = AttachmentAdapter(this, attachmentList)
        rvAttachments.adapter = attachmentAdapter
        imagePicker = ImagePicker(this, true)
        imagePicker.setImagePickerListener(this)
    }

    private fun setListeners() {
        presenter.attachView(this)
        tvDone.setOnClickListener {
            rootWrite.hideKeyboard()
            if (isValidated()) {
                var attachURL = ""
                if (attachmentList.isNotEmpty()) {
                    amazoneS3 = AmazoneS3(this)
                    attachURL = if (attachmentList[0].fileType == Constants.PDF) {
                        amazoneS3.setPDFFileToUpload(attachmentList[0].file, attachmentList[0].name)
                    } else {
                        amazoneS3.setFileToUpload(attachmentList[0].file)
                    }
                }


                if (isOnline(this)) {
                    val map = HashMap<String, Any>()
                    map[WebConstants.IS_DRAFT] = Constants.NOT_DRAFT
                    map[WebConstants.USERROLEID] = PrefsManager.get().getObject(Constants.USER_DETAIL, Profile::class.java)?.user_role_id
                            ?: 1
                    map[WebConstants.TITLE] = etTitle.text.trim().toString()
                    map[WebConstants.DESCRIPTION] = etContent.text.trim().toString()
                    map[WebConstants.ATTACHMENT] = attachURL/*Gson().toJson(attachmentPath)*/
                    Log.e("data--->", map.toString())
                    presenter.createHealthFeed(map)
                } else {
                    rootWrite.showInternetError()
                }
            }
        }
        ivAttachment.setOnClickListener {
            openImage()
        }
        toolbar.setNavigationOnClickListener { onBackPressed() }
    }

    private fun isValidated(): Boolean {
        return when {
            etTitle.text.trim().toString().isEmpty() -> {
                rootWrite.showErrorSnack("Enter title for the health feed.")
                false
            }

            etContent.text.trim().toString().isEmpty() -> {
                rootWrite.showErrorSnack("Enter content for the health feed.")
                false
            }
            else -> true
        }
    }

    override fun createFeedSuccess() {
        Toast.makeText(this, getString(R.string.health_feed_successfully_created), Toast.LENGTH_SHORT).show()
        sendBroadcast(Intent("newFeed"))
        onBackPressed()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detachView()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        imagePicker.onActivityResult(requestCode, resultCode, data)
    }

    override fun onImageSelectedFromPicker(imageFile: File?) {
        if (imageFile != null) {
            if (attachmentList.isNotEmpty()) {
                attachmentList.removeAt(0)
            }
            attachmentList.add(AttachmentModel(imageFile.name.toString(), Constants.IMAGE, imageFile))
            attachmentAdapter.notifyDataSetChanged()

            llAttachment.visibility = View.VISIBLE
            Log.d("filePDF", imageFile.name.toString())
        }
    }

    override fun onPDFSelected(file: File?, displayName: String?) {
        if (file != null) {
            if (attachmentList.isNotEmpty()) {
                attachmentList.removeAt(0)
            }
            attachmentList.add(AttachmentModel(displayName.toString(), Constants.PDF, file))
/*
            attachmentAdapter.notifyItemInserted(attachmentList.size)
*/
            attachmentAdapter.notifyDataSetChanged()
            llAttachment.visibility = View.VISIBLE
            Log.d("filePDF", displayName)
        }
    }

    private fun openImage() {
        //Open image ,camera from here
        if (EasyPermissions.hasPermissions(this, *permissions)) {
            imagePicker.showImagePicker()
        } else {
            EasyPermissions.requestPermissions(
                    PermissionRequest.Builder(this, permissionRequestCode, *permissions)
                            .setRationale(getString(R.string.rational_permission))
                            .setPositiveButtonText(R.string.ok)
                            .setNegativeButtonText(R.string.cancel)
                            .build())
        }
    }

    override fun onPermissionsDenied(requestCode: Int, perms: MutableList<String>) {
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            openSettingDialog(this)
        }
    }

    override fun onPermissionsGranted(requestCode: Int, perms: MutableList<String>) {
        openImage()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }

    fun hideView() {
        llAttachment.visibility = View.GONE
    }
}
