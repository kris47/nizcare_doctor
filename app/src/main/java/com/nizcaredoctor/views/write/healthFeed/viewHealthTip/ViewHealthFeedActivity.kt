package com.nizcaredoctor.views.write.healthFeed.viewHealthTip

import android.content.Intent
import android.os.Bundle
import android.view.View
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.gson.Gson
import com.nizcaredoctor.R
import com.nizcaredoctor.utils.*
import com.nizcaredoctor.views.baseViews.BaseViewActivityImpl
import com.nizcaredoctor.views.write.healthFeed.writeTip.AttachmentAdapter
import com.nizcaredoctor.webservice.model.AttachmentModel
import com.nizcaredoctor.webservice.model.healthFeedModel.HealthFeedDataItem
import kotlinx.android.synthetic.main.activity_view_health_feed.*

class ViewHealthFeedActivity : BaseViewActivityImpl(), ViewHealthFeedContract.View {

    private lateinit var healthFeed: HealthFeedDataItem
    private val presenter = ViewHealthFeedPresenter()
    private val attachmentList = mutableListOf<AttachmentModel>()
    val String.extension: String get() = substringAfterLast('.', "")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_health_feed)
        init()
        setListeners()
    }

    private fun init() {
        healthFeed = Gson().fromJson(intent.getStringExtra("healthFeedData"), HealthFeedDataItem::class.java)
        tvHealthFeed.text = healthFeed.title
        tvFullFeed.text = healthFeed.description
        Glide.with(this)
                .load(healthFeed.user?.image)
                .apply(RequestOptions().placeholder(R.drawable.a2))
                .into(ivFeedPerson)

        tvFeedPerson.text = healthFeed.user?.firstname
        tvDaysAgoFeed.text = getDate(healthFeed.created_at.toString())

        if (healthFeed.attachment != null) {
            llAttachment.visibility = View.VISIBLE
            attachmentList.add(AttachmentModel(healthFeed.attachment.toString(), healthFeed.attachment.toString().extension, null))
            rvAttachments.adapter = AttachmentAdapter(this, attachmentList)
        } else {
            llAttachment.visibility = View.GONE
        }

        setLike()
        setBookmark()
    }

    private fun setBookmark() {
        if (healthFeed.bookmark_count == 0) {
            ivBookmarkFeed.setImageResource(R.drawable.ic_bookmark_normal)
        } else {
            ivBookmarkFeed.setImageResource(R.drawable.ic_bookmark_c)
        }
    }

    private fun setLike() {
        tvUpvoteFeed.text = getString(R.string.upvote_with_dot) + " " + healthFeed.upvote_count
        if (healthFeed.my_vote_count == 0) {
            ivUpvote.setImageResource(R.drawable.ic_like_normal)
        } else {
            ivUpvote.setImageResource(R.drawable.ic_like_pressed)
        }
    }

    private fun setListeners() {
        presenter.attachView(this)
        toolbar.setNavigationOnClickListener{ onBackPressed() }

        ivUpvote.setOnClickListener {
            if (isOnline(this)) {
                val status = if (healthFeed.my_vote_count == 0) 1 else 0
                if (status == 1) {
                    healthFeed.my_vote_count = 1
                    healthFeed.upvote_count = healthFeed.upvote_count?.plus(1)
                } else {
                    healthFeed.my_vote_count = 0
                    healthFeed.upvote_count = healthFeed.upvote_count?.minus(1)
                }
                setLike()
                presenter.upVoteAPI(healthFeed.id,
                        Constants.HEALTH_FEED_TYPE,
                        status)
            } else {
                rootHealthFeed.showInternetError()
            }
        }

        ivBookmarkFeed.setOnClickListener {
            if (isOnline(this)) {
                val status = if (healthFeed.bookmark_count == 0) 1 else 0
                healthFeed.bookmark_count = status
                setBookmark()
                presenter.bookmarkQA(healthFeed.id,
                        Constants.HEALTH_FEED_TYPE,
                        status)
            } else {
                rootHealthFeed.showInternetError()
            }
        }

        ivShareFeed.setOnClickListener {
            shareText()
        }
    }

    override fun actionSuccess() {
        sendBroadcast(Intent("newFeed"))
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detachView()
    }
}
