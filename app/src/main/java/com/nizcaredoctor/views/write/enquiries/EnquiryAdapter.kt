package com.nizcaredoctor.views.write.enquiries

import android.app.Activity
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.nizcaredoctor.R
import com.nizcaredoctor.views.write.enquiries.reply.ReplyEnquiryActivity
import kotlinx.android.synthetic.main.item_enquiry.view.*

class EnquiryAdapter(val activity: Activity?) : RecyclerView.Adapter<EnquiryAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(activity).inflate(R.layout.item_enquiry, parent, false))
    }

    override fun getItemCount(): Int {
        return 10
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        init {
            itemView.tvReply.setOnClickListener {
                activity?.startActivity(Intent(activity, ReplyEnquiryActivity::class.java))
            }
        }
    }

}
