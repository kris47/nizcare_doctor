package com.nizcaredoctor.views.write.qa.viewSingleQAAnswer

import com.nizcaredoctor.views.baseViews.BasePresenter
import com.nizcaredoctor.views.baseViews.BasePresenterImpl
import com.nizcaredoctor.views.baseViews.BaseView

interface ViewSingleQAContract {
    interface View : BaseView {
        fun actionSuccess()

    }

    interface Presenter : BasePresenter<View> {
        fun upVoteAPI(id: Int?, answeR_TYPE: String, status: Int)
        fun bookmarkQA(id: Int?, answeR_TYPE: String, status: Int)
    }
}