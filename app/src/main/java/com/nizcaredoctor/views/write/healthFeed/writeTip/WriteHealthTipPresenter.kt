package com.nizcaredoctor.views.write.healthFeed.writeTip

import com.nizcaredoctor.utils.getAccessToken
import com.nizcaredoctor.views.baseViews.BasePresenterImpl
import com.nizcaredoctor.webservice.ApiResponseModel
import com.nizcaredoctor.webservice.RetrofitClient
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class WriteHealthTipPresenter : BasePresenterImpl<WriteHealthTipContract.View>(), WriteHealthTipContract.Presenter {
    override fun createHealthFeed(map: HashMap<String, Any>) {
        getView()?.showProgress()
        RetrofitClient.getApi().createHealthFeed(getAccessToken(), map)
                .enqueue(object : Callback<ApiResponseModel<Any>> {
                    override fun onFailure(call: Call<ApiResponseModel<Any>>?, t: Throwable?) {
                        getView()?.displayFailure(t?.message)
                        getView()?.dismissProgress()
                    }

                    override fun onResponse(call: Call<ApiResponseModel<Any>>?, response: Response<ApiResponseModel<Any>>?) {
                        getView()?.dismissProgress()
                        if (response != null)
                            if (response.isSuccessful) {
                                getView()?.createFeedSuccess()
                            } else {
                                try {
                                    getView()?.displayError(JSONObject(response.errorBody()?.string()).getString("msg"), response.code())
                                } catch (e: Exception) {

                                }
                            }
                    }
                })
    }

}