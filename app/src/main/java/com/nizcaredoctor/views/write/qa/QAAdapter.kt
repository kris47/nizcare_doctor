package com.nizcaredoctor.views.write.qa

import android.app.Activity
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.gson.Gson
import com.nizcaredoctor.R
import com.nizcaredoctor.utils.getDate
import com.nizcaredoctor.utils.shareText
import com.nizcaredoctor.views.write.qa.viewQA.ViewQAActivity
import com.nizcaredoctor.webservice.model.qaFeedModel.DataItem
import kotlinx.android.synthetic.main.item_qa.view.*

class QAAdapter(val activity: Activity?, val questionsList: MutableList<DataItem>, val qaFragment: QAFragment) : RecyclerView.Adapter<QAAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(activity).inflate(R.layout.item_qa, parent, false))
    }

    override fun getItemCount(): Int {
        return questionsList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindView(questionsList[position])

    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        override fun onClick(v: View?) {
            when (v?.id) {
                R.id.tvQAUpvote, R.id.ivUpvote ->
                    qaFragment.upVote(adapterPosition)

                R.id.rootQaItem -> activity?.startActivity(Intent(activity, ViewQAActivity::class.java)
                        .putExtra("qaData", Gson().toJson(questionsList[adapterPosition])))

                R.id.ivBookmarkQA ->
                    qaFragment.bookmarkQA(adapterPosition)

                R.id.ivShareQA ->
                    activity?.shareText()

            }
        }

        fun bindView(dataItem: DataItem) = with(itemView) {
            tvQA.text = dataItem.title
            tvQACategory.text = "in " + dataItem.service_category?.name
            if (activity != null) {
                Glide.with(activity)
                        .load(dataItem.answers?.user?.image)
                        .apply(RequestOptions().placeholder(R.drawable.a2))
                        .into(ivQAPerson)
            }
            tvDaysAgo.text = getDate(dataItem.created_at.toString())
            tvQAPerson.text = dataItem.answers?.user?.firstname
            tvQAText.text = dataItem.answers?.text
            tvQAUpvote.text = activity?.getString(R.string.upvote_with_dot) + " " + dataItem.answers?.upvote_count
            if (dataItem.answers?.my_vote_count == 0) {
                ivUpvote.setImageResource(R.drawable.ic_like_normal)
            } else {
                ivUpvote.setImageResource(R.drawable.ic_like_pressed)
            }

            if (dataItem.answers?.bookmark_count == 0) {
                ivBookmarkQA.setImageResource(R.drawable.ic_bookmark_normal)
            } else {
                ivBookmarkQA.setImageResource(R.drawable.ic_bookmark_c)
            }
        }

        init {
            itemView.tvQAUpvote.setOnClickListener(this)
            itemView.ivUpvote.setOnClickListener(this)
            itemView.rootQaItem.setOnClickListener(this)
            itemView.ivBookmarkQA.setOnClickListener(this)
            itemView.ivShareQA.setOnClickListener(this)
        }
    }
}
