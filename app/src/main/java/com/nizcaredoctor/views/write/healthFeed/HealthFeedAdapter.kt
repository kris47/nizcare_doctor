package com.nizcaredoctor.views.write.healthFeed

import android.app.Activity
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.gson.Gson
import com.nizcaredoctor.R
import com.nizcaredoctor.utils.getDate
import com.nizcaredoctor.utils.shareText
import com.nizcaredoctor.views.write.healthFeed.viewHealthTip.ViewHealthFeedActivity
import com.nizcaredoctor.webservice.model.healthFeedModel.HealthFeedDataItem
import kotlinx.android.synthetic.main.item_health_feed.view.*

class HealthFeedAdapter(val activity: Activity?, val healthFeedList: MutableList<HealthFeedDataItem>, val healthFeedFragment: HealthFeedFragment) : RecyclerView.Adapter<HealthFeedAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(activity).inflate(R.layout.item_health_feed, parent, false))
    }

    override fun getItemCount(): Int {
        return healthFeedList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindView(healthFeedList[position])
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        override fun onClick(v: View?) {
            when (v?.id) {
                R.id.rootItemFeed -> {
                    activity?.startActivityForResult(Intent(activity, ViewHealthFeedActivity::class.java)
                            .putExtra("healthFeedData", Gson().toJson(healthFeedList[adapterPosition])), 78)
                }

                R.id.ivBookmarkFeed -> {
                    healthFeedFragment.bookmarkFeed(adapterPosition)
                }

                R.id.ivShareFeed -> {
                    activity?.shareText()
                }
            }
        }

        fun bindView(healthFeedDataItem: HealthFeedDataItem) = with(itemView) {
            tvHealthFeed.text = healthFeedDataItem.title
            tvFullFeed.text = healthFeedDataItem.description
            if (activity != null) {
                Glide.with(activity)
                        .load(healthFeedDataItem.user?.image)
                        .apply(RequestOptions().placeholder(R.drawable.a2))
                        .into(ivFeedPerson)
            }
            tvFeedPerson.text = healthFeedDataItem.user?.firstname
            tvDaysAgo.text = getDate(healthFeedDataItem.created_at.toString())
            if (healthFeedDataItem.bookmark_count == 0) {
                ivBookmarkFeed.setImageResource(R.drawable.ic_bookmark_normal)
            } else {
                ivBookmarkFeed.setImageResource(R.drawable.ic_bookmark_c)
            }

            tvFeedViews.text = healthFeedDataItem.views_count.toString() + " " + context.getString(R.string.dot_views)
        }

        init {
            itemView.ivBookmarkFeed.setOnClickListener(this)
            itemView.rootItemFeed.setOnClickListener(this)
            itemView.ivShareFeed.setOnClickListener(this)
        }
    }

}
