package com.nizcaredoctor.views.write.qa.viewSingleQAAnswer

import android.content.Intent
import android.os.Bundle
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.gson.Gson
import com.nizcaredoctor.R
import com.nizcaredoctor.utils.*
import com.nizcaredoctor.views.baseViews.BaseViewActivityImpl
import com.nizcaredoctor.webservice.model.qaAllAnswers.AnswerItem
import kotlinx.android.synthetic.main.activity_view_single_qa.*

class ViewSingleQAActivity : BaseViewActivityImpl(), ViewSingleQAContract.View {

    private val presenter = ViewSingleQAPresenter()
    private lateinit var answer: AnswerItem

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_single_qa)
        init()
        setListeners()
    }

    private fun init() {
        answer = Gson().fromJson(intent.getStringExtra("qaAnswerData"), AnswerItem::class.java)
        tvQAPersonTop.text = answer.user?.firstname
        tvDaysAgo.text = getDate(answer.created_at.toString())
        tvFullAnswer.text = answer.text
        tvUpVoteAnswer.text = getString(R.string.upvote_with_dot) + " " + answer.upvote_count
        Glide.with(this)
                .load(answer.user?.image)
                .apply(RequestOptions().placeholder(R.drawable.a2))
                .into(ivQAPersonTop)

        setLike()
        setBookmark()


    }

    private fun setBookmark() {
        if (answer.bookmark_count == 0) {
            ivBookmarkQA.setImageResource(R.drawable.ic_bookmark_normal)
        } else {
            ivBookmarkQA.setImageResource(R.drawable.ic_bookmark_c)
        }
    }

    private fun setLike() {
        tvUpVoteAnswer.text = getString(R.string.upvote_with_dot) + " " + answer.upvote_count
        if (answer.my_vote_count == 0) {
            ivUpvote.setImageResource(R.drawable.ic_like_normal)
        } else {
            ivUpvote.setImageResource(R.drawable.ic_like_pressed)
        }
    }

    private fun setListeners() {
        presenter.attachView(this)
        toolbar.setNavigationOnClickListener { onBackPressed() }
        ivUpvote.setOnClickListener {
            if (isOnline(this)) {
                val status = if (answer.my_vote_count == 0) 1 else 0
                if (status == 1) {
                    answer.my_vote_count = 1
                    answer.upvote_count = answer.upvote_count?.plus(1)
                } else {
                    answer.my_vote_count = 0
                    answer.upvote_count = answer.upvote_count?.minus(1)
                }
                setLike()
                presenter.upVoteAPI(answer.id,
                        Constants.ANSWER_TYPE,
                        status)
            } else {
                rootQA.showInternetError()
            }
        }
        ivBookmarkQA.setOnClickListener {
            if (isOnline(this)) {
                val status = if (answer.bookmark_count == 0) 1 else 0
                answer.bookmark_count = status
                setBookmark()
                presenter.bookmarkQA(answer.id,
                        Constants.ANSWER_TYPE,
                        status)
            } else {
                rootQA.showInternetError()
            }
        }

        ivShareFeed.setOnClickListener { shareText() }

    }

    override fun actionSuccess() {
        sendBroadcast(Intent("refreshQA"))
        sendBroadcast(Intent("refreshQAView"))

    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detachView()
    }
}