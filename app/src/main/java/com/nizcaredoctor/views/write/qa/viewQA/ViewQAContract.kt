package com.nizcaredoctor.views.write.qa.viewQA

import com.nizcaredoctor.views.baseViews.BasePresenter
import com.nizcaredoctor.views.baseViews.BaseView
import com.nizcaredoctor.webservice.model.qaAllAnswers.Answers

interface ViewQAContract {

    interface View : BaseView {
        fun setAllAnswers(answers: Answers?)
        fun actionSuccess()
    }

    interface Presenter : BasePresenter<View> {
        fun getAllAnswers(id: Int, page: Int)
        fun upVoteAPI(id: Int?, answeR_TYPE: String, my_vote_count: Int?)
        fun bookmarkQA(id: Int?, answeR_TYPE: String, status: Int)
    }
}