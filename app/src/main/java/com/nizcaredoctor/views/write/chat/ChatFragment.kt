package com.nizcaredoctor.views.write.chat

import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.nizcaredoctor.R
import com.nizcaredoctor.utils.Constants
import com.nizcaredoctor.utils.isOnline
import com.nizcaredoctor.views.baseViews.BaseViewFragImpl
import com.nizcaredoctor.webservice.model.GetItemModel
import com.nizcaredoctor.webservice.model.privateQuestionModel.PrivateDataItem
import com.nizcaredoctor.webservice.model.privateQuestionModel.Questions
import kotlinx.android.synthetic.main.fragment_chat.*

class ChatFragment : BaseViewFragImpl(), View.OnClickListener, ChatContract.View, AdapterView.OnItemSelectedListener {


    private val questionList = mutableListOf<PrivateDataItem>()
    private val presenter = ChatPresenter()
    private lateinit var layoutManager: LinearLayoutManager
    private var page = 1
    private var total = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_chat, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        setListeners()

        if (isOnline(context)) {
            presenter.getServiceCategories()
        }
        getPrivatequestions(page)

    }

    private fun getPrivatequestions(page: Int) {
        if (page == 1) questionList.clear()
        if (isOnline(context)) {
            presenter.getServiceCategories()
            presenter.getPrivateQuestions(page)
        } else {
            dismissProgress()
            viewFlipper.displayedChild = 1
            noDataView.setImage(R.raw.no_internet)
            noDataView.setData(getString(R.string.no_interner_connection), getString(R.string.interent_substitles))
        }
    }

    private fun init() {
        swipeRefresh.setColorSchemeColors(ContextCompat.getColor(context!!, R.color.colorPrimary))
        layoutManager = LinearLayoutManager(context)
        rvPublicChat.layoutManager = layoutManager
        rvPublicChat.adapter = PublicChatListAdapter(activity, Constants.PUBLIC_CHAT, questionList)
    }

    private fun setListeners() {
        presenter.attachView(this)
        tvPublic.setOnClickListener(this)
        tvPrivate.setOnClickListener(this)

        swipeRefresh.setOnRefreshListener {
            if (isOnline(context)) {
                presenter.getServiceCategories()
            }
            getPrivatequestions(1)
        }

        rvPublicChat.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (layoutManager.findLastCompletelyVisibleItemPosition() == rvPublicChat.adapter.itemCount - 1) {
                    if (questionList.size < total) {
                        getPrivatequestions(++page)
                    }
                }
            }
        })
    }

    override fun showProgress() {
        swipeRefresh.isRefreshing = true
    }

    override fun dismissProgress() {
        swipeRefresh.isRefreshing = false
    }

    override fun setServiceCategory(serviceCategories: MutableList<GetItemModel>?) {
        if (serviceCategories != null) {
            val titles = Array(serviceCategories.size, { i -> serviceCategories[i].name })
            val arrayDocumentAdapter = ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, titles)
            arrayDocumentAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinnerSpeciality.adapter = arrayDocumentAdapter
            spinnerSpeciality.setSelection(0)
            spinnerSpeciality.id = R.id.spinnerSpeciality
            spinnerSpeciality.onItemSelectedListener = this

        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.tvPublic -> {
                tvPublic.background = context?.let { ContextCompat.getDrawable(it, R.drawable.button_green_left) }
                tvPrivate.background = context?.let { ContextCompat.getDrawable(it, R.drawable.button_white_right) }
                context?.let { ContextCompat.getColor(it, R.color.white) }?.let { tvPublic.setTextColor(it) }
                context?.let { ContextCompat.getColor(it, R.color.colorPrimary) }?.let { tvPrivate.setTextColor(it) }
                rvPublicChat.adapter = PublicChatListAdapter(activity, Constants.PUBLIC_CHAT, questionList)
            }

            R.id.tvPrivate -> {
                tvPublic.background = context?.let { ContextCompat.getDrawable(it, R.drawable.button_white_left) }
                tvPrivate.background = context?.let { ContextCompat.getDrawable(it, R.drawable.button_green_right) }
                context?.let { ContextCompat.getColor(it, R.color.colorPrimary) }?.let { tvPublic.setTextColor(it) }
                context?.let { ContextCompat.getColor(it, R.color.white) }?.let { tvPrivate.setTextColor(it) }
                rvPublicChat.adapter = PublicChatListAdapter(activity, Constants.PRIVATE_CHAT, questionList)
            }
        }
    }

    override fun setPrivateChatQuestions(questions: Questions?) {
        if (questions != null) {
            if (questions.data.isNotEmpty()) {
                viewFlipper.displayedChild = 2
                questionList.addAll(questions.data)
                rvPublicChat.adapter.notifyDataSetChanged()
            } else {
                viewFlipper.displayedChild = 1
                noDataView.setImage(R.raw.no_data)
                noDataView.setData("No Private questions available.", "")
            }

            page = questions.current_page
            total = questions.total
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {

    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

    }

    override fun onDestroyView() {
        super.onDestroyView()
        presenter.detachView()
    }
}
