package com.nizcaredoctor.views.write.healthFeed

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.nizcaredoctor.R
import com.nizcaredoctor.utils.Constants
import com.nizcaredoctor.utils.isOnline
import com.nizcaredoctor.views.baseViews.BaseViewFragImpl
import com.nizcaredoctor.views.write.healthFeed.writeTip.WriteHealthTipActivity
import com.nizcaredoctor.webservice.model.healthFeedModel.HealthFeedDataItem
import com.nizcaredoctor.webservice.model.healthFeedModel.HealthFeeds
import kotlinx.android.synthetic.main.fragment_health_feed.*

class HealthFeedFragment : BaseViewFragImpl(), HealthFeedContract.View {

    private val healthFeedList = mutableListOf<HealthFeedDataItem>()
    private lateinit var adapter: HealthFeedAdapter
    private val presenter = HealthFeedPresenter()
    private var page = 1
    private var total = 0
    private lateinit var layoutManager: LinearLayoutManager
    private val broadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            getHealthFeeds(1)
        }
    }

    override fun onStart() {
        super.onStart()
        activity?.registerReceiver(broadcastReceiver, IntentFilter("newFeed"))
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_health_feed, container, false)
    }

    private var hits = 0

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        setListeners()
        getHealthFeeds(1)


    }

    private fun init() {
        layoutManager = LinearLayoutManager(context)
        rvHealthFeed.layoutManager = layoutManager
        adapter = HealthFeedAdapter(activity, healthFeedList, this)
        rvHealthFeed.adapter = adapter
        swipeRefresh.setColorSchemeColors(ContextCompat.getColor(context!!, R.color.colorPrimary))


    }

    private fun setListeners() {
        presenter.attachView(this)
        cvAddHealthTip.setOnClickListener {
            startActivity(Intent(activity, WriteHealthTipActivity::class.java))
        }
        swipeRefresh.setOnRefreshListener {
            getHealthFeeds(1)
        }

        rvHealthFeed.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (layoutManager.findLastCompletelyVisibleItemPosition() == adapter.itemCount - 1) {
                    if (healthFeedList.size < total) {
                        Log.d("hits", hits++.toString() + "    " + page)
                        getHealthFeeds(++page)
                    }
                }
            }
        })
    }

    override fun setHealthFeeds(healthFeeds: HealthFeeds?) {
        if (healthFeeds != null) {
            if (healthFeeds.data.isNotEmpty()) {
                viewFlipper.displayedChild = 2
                healthFeedList.addAll(healthFeeds.data)
                adapter.notifyDataSetChanged()
            } else {
                viewFlipper.displayedChild = 1
                noDataView.setImage(R.raw.no_data)
                noDataView.setData("No Health feeds available.", "")
            }
            total = healthFeeds.total
            page = healthFeeds.current_page ?: 0
        }
    }

    private fun getHealthFeeds(currPage: Int) {
        if (currPage == 1) healthFeedList.clear()
        if (isOnline(context)) {
            presenter.getAllHealthFeeds(currPage)
        } else {
            viewFlipper.displayedChild = 1
            swipeRefresh.isRefreshing = false
            noDataView.setImage(R.raw.no_internet)
            noDataView.setData(getString(R.string.no_interner_connection), getString(R.string.interent_substitles))
        }
    }

    override fun showProgress() {
        swipeRefresh.isRefreshing = true
    }

    override fun dismissProgress() {
        swipeRefresh.isRefreshing = false
    }

    override fun onDestroyView() {
        super.onDestroyView()
        presenter.detachView()
    }

    override fun onDestroy() {
        super.onDestroy()
        activity?.unregisterReceiver(broadcastReceiver)
    }

    fun bookmarkFeed(adapterPosition: Int) {
        if (isOnline(context)) {
            val status = if (healthFeedList[adapterPosition].bookmark_count == 0) 1 else 0
            healthFeedList[adapterPosition].bookmark_count = status
            adapter.notifyItemChanged(adapterPosition)
            presenter.bookmarkQA(healthFeedList[adapterPosition].id,
                    Constants.HEALTH_FEED_TYPE,
                    status)
        }
    }

}
