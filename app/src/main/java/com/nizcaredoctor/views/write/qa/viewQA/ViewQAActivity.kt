package com.nizcaredoctor.views.write.qa.viewQA

import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.view.View
import com.google.gson.Gson
import com.nizcaredoctor.R
import com.nizcaredoctor.utils.Constants
import com.nizcaredoctor.utils.getDate
import com.nizcaredoctor.utils.isOnline
import com.nizcaredoctor.views.baseViews.BaseViewActivityImpl
import com.nizcaredoctor.webservice.model.qaAllAnswers.AnswerItem
import com.nizcaredoctor.webservice.model.qaAllAnswers.Answers
import com.nizcaredoctor.webservice.model.qaFeedModel.DataItem
import kotlinx.android.synthetic.main.activity_view_qa.*

class ViewQAActivity : BaseViewActivityImpl(), ViewQAContract.View {

    private val presenter = ViewQAPresenter()
    private lateinit var qaQuestion: DataItem
    private val answersList = mutableListOf<AnswerItem>()
    private lateinit var adapter: QAAnswersAdapter
    private val page = 0
    private val broadCastViewQA = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            if (isOnline(this@ViewQAActivity)) {
                presenter.getAllAnswers(qaQuestion.id ?: 0, page)
            } else {
                viewFlipper.displayedChild = 1
                dismissProgress()
                noDataView.setImage(R.raw.no_internet)
                noDataView.setData(getString(R.string.no_interner_connection), getString(R.string.interent_substitles))
            }
        }
    }

    override fun onStart() {
        super.onStart()
        registerReceiver(broadCastViewQA, IntentFilter("refreshQAView"))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_qa)
        init()
        setListeners()
        if (isOnline(this)) {
            presenter.getAllAnswers(qaQuestion.id ?: 0, page)
        } else {
            viewFlipper.displayedChild = 1
            dismissProgress()
            noDataView.setImage(R.raw.no_internet)
            noDataView.setData(getString(R.string.no_interner_connection), getString(R.string.interent_substitles))
        }
    }

    private fun init() {
        qaQuestion = Gson().fromJson(intent.getStringExtra("qaData"), DataItem::class.java)
        tvQAFullQues.text = qaQuestion.title
        tvQAFullText.text = qaQuestion.description
        val gender = if (qaQuestion.user?.gender ?: "1" == "1") getString(R.string.male) else getString(R.string.male)
        tvQAFullInfo.text = qaQuestion.user?.firstname + ", $gender · " + getDate(qaQuestion.created_at.toString())
        adapter = QAAnswersAdapter(this, answersList)
        rvQAAnswers.adapter = adapter
    }

    private fun setListeners() {
        presenter.attachView(this)
        toolbar.setNavigationOnClickListener { onBackPressed() }
    }

    override fun showProgress() {
        progress.visibility = View.VISIBLE
    }

    override fun dismissProgress() {
        progress.visibility = View.GONE
    }

    @SuppressLint("SetTextI18n")
    override fun setAllAnswers(answers: Answers?) {
        if (answers?.data != null) {
            if (answers.data.isNotEmpty()) {
                viewFlipper.displayedChild = 2
                tvQATotalAnswers.text = answers.data.size.toString() + " " + getString(R.string.answers)
                answersList.clear()
                answersList.addAll(answers.data)
                rvQAAnswers.adapter.notifyDataSetChanged()
            } else {
                viewFlipper.displayedChild = 1
                noDataView.setImage(R.raw.no_data)
                noDataView.setData("No answers found", "")
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(broadCastViewQA)
        presenter.detachView()
    }

    fun bookmarkQAAnswer(adapterPosition: Int) {
        if (isOnline(this)) {
            val status = if (answersList[adapterPosition].bookmark_count == 0) 1 else 0
            answersList[adapterPosition].bookmark_count = if (answersList[adapterPosition].bookmark_count == 0) 1 else 0
            adapter.notifyItemChanged(adapterPosition)
            presenter.bookmarkQA(answersList[adapterPosition].id,
                    Constants.ANSWER_TYPE,
                    status)
        }
    }

    fun upvoteAnswer(adapterPosition: Int) {
        if (isOnline(this)) {
            val status = if (answersList[adapterPosition].my_vote_count == 0) 1 else 0
            if (status == 1) {
                answersList[adapterPosition].my_vote_count = 1
                answersList[adapterPosition].upvote_count = answersList[adapterPosition].upvote_count?.plus(1)
            } else {
                answersList[adapterPosition].my_vote_count = 0
                answersList[adapterPosition].upvote_count = answersList[adapterPosition].upvote_count?.minus(1)
            }
            adapter.notifyItemChanged(adapterPosition)
            presenter.upVoteAPI(answersList[adapterPosition].id,
                    Constants.ANSWER_TYPE,
                    status)

        }
    }

    override fun actionSuccess() {
        sendBroadcast(Intent("refreshQA"))
    }
}
