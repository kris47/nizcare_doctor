package com.nizcaredoctor.views.write.chat

import com.nizcaredoctor.utils.Constants
import com.nizcaredoctor.utils.PrefsManager
import com.nizcaredoctor.utils.getAccessToken
import com.nizcaredoctor.views.baseViews.BasePresenterImpl
import com.nizcaredoctor.webservice.ApiResponseModel
import com.nizcaredoctor.webservice.RetrofitClient
import com.nizcaredoctor.webservice.model.Profile
import com.nizcaredoctor.webservice.model.ServiceModel
import com.nizcaredoctor.webservice.model.privateQuestionModel.PrivateChatResponse
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ChatPresenter : BasePresenterImpl<ChatContract.View>(), ChatContract.Presenter {
    override fun getPrivateQuestions(page: Int) {
        getView()?.showProgress()
        RetrofitClient.getApi().getPrivateQuestions(getAccessToken(),
                Constants.ANSWER_TYPE, page)
                .enqueue(object : Callback<ApiResponseModel<PrivateChatResponse>> {
                    override fun onFailure(call: Call<ApiResponseModel<PrivateChatResponse>>?, t: Throwable?) {
                        getView()?.dismissProgress()
                    }

                    override fun onResponse(call: Call<ApiResponseModel<PrivateChatResponse>>?,
                                            response: Response<ApiResponseModel<PrivateChatResponse>>?) {
                        getView()?.dismissProgress()
                        if (response != null) {
                            if (response.isSuccessful) {
                                getView()?.setPrivateChatQuestions(response.body()?.data?.questions)
                            } else {
                                try {
                                    getView()?.displayError(JSONObject(response.errorBody()?.string()).getString("msg"), response.code())
                                } catch (e: Exception) {

                                }
                            }
                        }
                    }
                })
    }

    override fun getServiceCategories() {
        RetrofitClient.getApi().getDoctorSPServiceCategory(getAccessToken(), PrefsManager.get().getObject(Constants.USER_DETAIL, Profile::class.java)?.user_role_id
                ?: 1).enqueue(object : Callback<ApiResponseModel<ServiceModel>> {
            override fun onFailure(call: Call<ApiResponseModel<ServiceModel>>?, t: Throwable?) {
                getView()?.displayFailure(t?.message)
            }

            override fun onResponse(call: Call<ApiResponseModel<ServiceModel>>?, response: Response<ApiResponseModel<ServiceModel>>?) {
                if (response != null) {
                    if (response.isSuccessful) {
                        getView()?.setServiceCategory(response.body()?.data?.serviceCategories)
                    } else {
                        try {
                            getView()?.displayError(JSONObject(response.errorBody()?.string()).getString("msg"), response.code())
                        } catch (e: Exception) {

                        }
                    }
                }
            }
        })
    }
}