package com.nizcaredoctor.views.write.chat.privateChat

import com.nizcaredoctor.views.baseViews.BasePresenter
import com.nizcaredoctor.views.baseViews.BaseView

interface PrivateChatContract {

    interface View : BaseView {
        fun answerSuccess(msg: String?)

    }

    interface Presenter : BasePresenter<View> {
        fun submitPrivateAnswer(text: String, question_id: Int?, toJson: String)

    }
}