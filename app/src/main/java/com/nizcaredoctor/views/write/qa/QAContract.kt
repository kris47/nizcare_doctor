package com.nizcaredoctor.views.write.qa

import com.nizcaredoctor.views.baseViews.BasePresenter
import com.nizcaredoctor.views.baseViews.BaseView
import com.nizcaredoctor.webservice.model.qaFeedModel.Questions

interface QAContract {

    interface View : BaseView {
        fun setQAFeedList(questions: Questions?)

    }

    interface Presenter : BasePresenter<View> {
        fun getQAFeed(page:Int)
        fun upVoteAPI(id: Int?, answeR_TYPE: String, my_vote_count: Int?)
        fun bookmarkQA(id: Int?, answeR_TYPE: String, status: Int)

    }
}