package com.nizcaredoctor.views.write.chat

import android.app.Activity
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.gson.Gson
import com.nizcaredoctor.R
import com.nizcaredoctor.utils.Constants
import com.nizcaredoctor.utils.getDate
import com.nizcaredoctor.views.write.chat.privateChat.PrivateChatActivity
import com.nizcaredoctor.webservice.model.privateQuestionModel.PrivateDataItem
import kotlinx.android.synthetic.main.item_public_chat.view.*

class PublicChatListAdapter(val activity: Activity?, val chatType: String, val questionList: MutableList<PrivateDataItem>) : RecyclerView.Adapter<PublicChatListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        return ViewHolder(LayoutInflater.from(activity).inflate(R.layout.item_public_chat, parent, false))
    }

    override fun getItemCount(): Int {
        return questionList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (chatType == Constants.PUBLIC_CHAT) {
            holder.bindPublicChat()
        } else {
            holder.bindPrivateChat(questionList[position])
        }
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        init {
            itemView.setOnClickListener {
                if (chatType == Constants.PUBLIC_CHAT) {
                    //    activity?.startActivity(Intent(activity, PrivateChatActivity::class.java))

                } else {
                    activity?.startActivity(Intent(activity, PrivateChatActivity::class.java)
                            .putExtra("privateChatData", Gson().toJson(questionList[adapterPosition])))

                }
            }
        }


        fun bindPublicChat() = with(itemView) {
            tvPublicChatGroup.text = "We care group"
            tvGroupInfo.text = "235 members"
        }

        fun bindPrivateChat(privateDataItem: PrivateDataItem) = with(itemView) {
            tvPublicChatGroup.text = privateDataItem.question?.title
            val gender = if (privateDataItem.question?.user?.gender ?: "1" == "1") activity?.getString(R.string.male) else activity?.getString(R.string.male)
            tvGroupInfo.text = privateDataItem.question?.user?.firstname + ", $gender · " + getDate(privateDataItem.created_at.toString())
        }
    }

}
