package com.nizcaredoctor.views.write.qa.viewQA

import com.nizcaredoctor.utils.getAccessToken
import com.nizcaredoctor.views.baseViews.BasePresenterImpl
import com.nizcaredoctor.webservice.ApiResponseModel
import com.nizcaredoctor.webservice.RetrofitClient
import com.nizcaredoctor.webservice.model.qaAllAnswers.AllQAAnswersModel
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ViewQAPresenter : BasePresenterImpl<ViewQAContract.View>(), ViewQAContract.Presenter {

    override fun getAllAnswers(id: Int, page: Int) {
        getView()?.showProgress()
        RetrofitClient.getApi().getAllQAAnswers(getAccessToken(), id, page).enqueue(object : Callback<ApiResponseModel<AllQAAnswersModel>> {
            override fun onFailure(call: Call<ApiResponseModel<AllQAAnswersModel>>?, t: Throwable?) {
                getView()?.displayFailure(t?.message)
                getView()?.dismissProgress()
            }

            override fun onResponse(call: Call<ApiResponseModel<AllQAAnswersModel>>?, response: Response<ApiResponseModel<AllQAAnswersModel>>?) {
                getView()?.dismissProgress()
                if (response != null)
                    if (response.isSuccessful) {
                        getView()?.setAllAnswers(response.body()?.data?.answers)
                    } else {
                        try {
                            getView()?.displayError(JSONObject(response.errorBody()?.string()).getString("msg"), response.code())
                        } catch (e: Exception) {

                        }
                    }
            }
        })
    }

    override fun bookmarkQA(id: Int?, answeR_TYPE: String, status: Int) {
        RetrofitClient.getApi().bookmarkQAAPI(getAccessToken(), answeR_TYPE, id, status)
                .enqueue(object : Callback<ApiResponseModel<Any>> {
                    override fun onFailure(call: Call<ApiResponseModel<Any>>?, t: Throwable?) {
                        getView()?.displayFailure(t?.localizedMessage)
                    }

                    override fun onResponse(call: Call<ApiResponseModel<Any>>?, response: Response<ApiResponseModel<Any>>) {
                        if (response != null)
                            if (response.isSuccessful) {
                                getView()?.actionSuccess()
                            } else {
                                try {
                                    getView()?.displayError(JSONObject(response.errorBody()?.string()).getString("msg"), response.code())
                                } catch (e: Exception) {

                                }
                            }
                    }
                })
    }

    override fun upVoteAPI(id: Int?, answeR_TYPE: String, my_vote_count: Int?) {
        RetrofitClient.getApi().upVoteQA(getAccessToken(), answeR_TYPE, id, my_vote_count)
                .enqueue(object : Callback<ApiResponseModel<Any>> {
                    override fun onFailure(call: Call<ApiResponseModel<Any>>?, t: Throwable?) {
                        getView()?.displayFailure(t?.message)
                    }

                    override fun onResponse(call: Call<ApiResponseModel<Any>>?, response: Response<ApiResponseModel<Any>>?) {
                        if (response != null)
                            if (response.isSuccessful) {
                                getView()?.actionSuccess()
                            } else {
                                try {
                                    getView()?.displayError(JSONObject(response.errorBody()?.string()).getString("msg"), response.code())
                                } catch (e: Exception) {

                                }
                            }
                    }
                })
    }


}