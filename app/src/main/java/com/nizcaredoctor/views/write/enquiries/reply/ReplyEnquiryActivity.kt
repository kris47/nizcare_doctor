package com.nizcaredoctor.views.write.enquiries.reply

import android.os.Bundle
import com.nizcaredoctor.R
import com.nizcaredoctor.utils.activityFinishAnimation
import com.nizcaredoctor.views.baseViews.BaseActivity
import kotlinx.android.synthetic.main.activity_reply_enquiry.*

class ReplyEnquiryActivity : BaseActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reply_enquiry)
        tvSend.setOnClickListener { finish() }
        toolbar.setNavigationOnClickListener { onBackPressed() }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
        activityFinishAnimation()
    }
}
