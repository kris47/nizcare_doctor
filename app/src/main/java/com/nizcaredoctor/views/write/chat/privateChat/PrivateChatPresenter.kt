package com.nizcaredoctor.views.write.chat.privateChat

import com.nizcaredoctor.utils.getAccessToken
import com.nizcaredoctor.views.baseViews.BasePresenterImpl
import com.nizcaredoctor.webservice.ApiResponseModel
import com.nizcaredoctor.webservice.RetrofitClient
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PrivateChatPresenter : BasePresenterImpl<PrivateChatContract.View>(), PrivateChatContract.Presenter {
    override fun submitPrivateAnswer(text: String, question_id: Int?, attachments: String) {
        getView()?.showProgress()
        RetrofitClient.getApi().postPrivateAnswer(getAccessToken(), text, question_id,attachments).enqueue(object : Callback<ApiResponseModel<Any>> {
            override fun onResponse(call: Call<ApiResponseModel<Any>>?, response: Response<ApiResponseModel<Any>>?) {
                getView()?.dismissProgress()
                if (response != null)
                    if (response.isSuccessful) {
                        getView()?.answerSuccess(response.body()?.msg)
                    } else {
                        try {
                            getView()?.displayError(JSONObject(response.errorBody()?.string()).getString("msg"), response.code())
                        } catch (e: Exception) {

                        }
                    }
            }

            override fun onFailure(call: Call<ApiResponseModel<Any>>?, t: Throwable?) {

            }
        })
    }

}