package com.nizcaredoctor.views.write.chat

import com.nizcaredoctor.views.baseViews.BasePresenter
import com.nizcaredoctor.views.baseViews.BaseView
import com.nizcaredoctor.webservice.model.GetItemModel
import com.nizcaredoctor.webservice.model.privateQuestionModel.Questions

interface ChatContract {

    interface View : BaseView {
        fun setPrivateChatQuestions(questions: Questions?)
        fun setServiceCategory(serviceCategories: MutableList<GetItemModel>?)

    }

    interface Presenter : BasePresenter<View> {
        fun getPrivateQuestions(page: Int)
        fun getServiceCategories()
    }
}