package com.nizcaredoctor.views.write.healthFeed.writeTip

import com.nizcaredoctor.views.baseViews.BasePresenter
import com.nizcaredoctor.views.baseViews.BaseView

interface WriteHealthTipContract {
    interface View : BaseView {
        fun createFeedSuccess()

    }

    interface Presenter : BasePresenter<View> {
        fun createHealthFeed(map: HashMap<String, Any>)

    }
}