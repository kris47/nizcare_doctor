package com.nizcaredoctor.views.write.healthFeed

import com.nizcaredoctor.views.baseViews.BasePresenter
import com.nizcaredoctor.views.baseViews.BaseView
import com.nizcaredoctor.webservice.model.healthFeedModel.HealthFeeds

interface HealthFeedContract {
    interface View : BaseView {
        fun setHealthFeeds(healthFeeds: HealthFeeds?)
    }

    interface Presenter : BasePresenter<View> {
        fun getAllHealthFeeds(page: Int)
        fun bookmarkQA(id: Int?, type: String, status: Int)

    }
}