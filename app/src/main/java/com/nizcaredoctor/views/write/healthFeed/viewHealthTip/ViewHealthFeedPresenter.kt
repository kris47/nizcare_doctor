package com.nizcaredoctor.views.write.healthFeed.viewHealthTip

import com.nizcaredoctor.utils.getAccessToken
import com.nizcaredoctor.views.baseViews.BasePresenterImpl
import com.nizcaredoctor.webservice.ApiResponseModel
import com.nizcaredoctor.webservice.RetrofitClient
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ViewHealthFeedPresenter : BasePresenterImpl<ViewHealthFeedContract.View>(), ViewHealthFeedContract.Presenter {

    override fun bookmarkQA(id: Int?, answeR_TYPE: String, status: Int) {
        RetrofitClient.getApi().bookmarkQAAPI(getAccessToken(), answeR_TYPE, id, status)
                .enqueue(object : Callback<ApiResponseModel<Any>> {
                    override fun onFailure(call: Call<ApiResponseModel<Any>>?, t: Throwable?) {
                        getView()?.displayFailure(t?.message)
                    }

                    override fun onResponse(call: Call<ApiResponseModel<Any>>?, response: Response<ApiResponseModel<Any>>?) {
                        if (response != null)
                            if (response.isSuccessful) {
                                getView()?.actionSuccess()
                            } else {
                                try {
                                    getView()?.displayError(JSONObject(response.errorBody()?.string()).getString("msg"), response.code())
                                } catch (e: Exception) {

                                }
                            }
                    }
                })
    }

    override fun upVoteAPI(id: Int?, answeR_TYPE: String, status: Int) {
        RetrofitClient.getApi().upVoteQA(getAccessToken(), answeR_TYPE, id, status)
                .enqueue(object : Callback<ApiResponseModel<Any>> {
                    override fun onFailure(call: Call<ApiResponseModel<Any>>?, t: Throwable?) {
                        getView()?.displayFailure(t?.message)
                    }

                    override fun onResponse(call: Call<ApiResponseModel<Any>>?, response: Response<ApiResponseModel<Any>>?) {
                        if (response != null)
                            if (response.isSuccessful) {
                            } else {
                                try {
                                    getView()?.displayError(JSONObject(response.errorBody()?.string()).getString("msg"), response.code())
                                } catch (e: Exception) {

                                }
                            }
                    }
                })
    }
}