package com.nizcaredoctor.views.write.qa

import com.nizcaredoctor.utils.Constants
import com.nizcaredoctor.utils.PrefsManager
import com.nizcaredoctor.utils.getAccessToken
import com.nizcaredoctor.views.baseViews.BasePresenterImpl
import com.nizcaredoctor.webservice.ApiResponseModel
import com.nizcaredoctor.webservice.RetrofitClient
import com.nizcaredoctor.webservice.model.Profile
import com.nizcaredoctor.webservice.model.qaFeedModel.QAFeedModel
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class QAPresenter : BasePresenterImpl<QAContract.View>(), QAContract.Presenter {
    override fun bookmarkQA(id: Int?, answeR_TYPE: String, status: Int) {
        RetrofitClient.getApi().bookmarkQAAPI(getAccessToken(), answeR_TYPE, id, status)
                .enqueue(object : Callback<ApiResponseModel<Any>> {
                    override fun onFailure(call: Call<ApiResponseModel<Any>>?, t: Throwable?) {
                        getView()?.displayFailure(t?.message)
                    }

                    override fun onResponse(call: Call<ApiResponseModel<Any>>?, response: Response<ApiResponseModel<Any>>?) {
                        if (response != null)
                            if (response.isSuccessful) {

                            } else {
                                try {
                                    getView()?.displayError(JSONObject(response.errorBody()?.string()).getString("msg"), response.code())
                                } catch (e: Exception) {

                                }
                            }
                    }
                })
    }

    override fun upVoteAPI(id: Int?, answeR_TYPE: String, my_vote_count: Int?) {
        RetrofitClient.getApi().upVoteQA(getAccessToken(), answeR_TYPE, id, my_vote_count)
                .enqueue(object : Callback<ApiResponseModel<Any>> {
                    override fun onFailure(call: Call<ApiResponseModel<Any>>?, t: Throwable?) {
                        getView()?.displayFailure(t?.message)
                    }

                    override fun onResponse(call: Call<ApiResponseModel<Any>>?, response: Response<ApiResponseModel<Any>>?) {
                        if (response != null)
                            if (response.isSuccessful) {

                            } else {
                                try {
                                    getView()?.displayError(JSONObject(response.errorBody()?.string()).getString("msg"), response.code())
                                } catch (e: Exception) {

                                }
                            }
                    }
                })
    }

    override fun getQAFeed(page:Int) {
        getView()?.showProgress()
        RetrofitClient.getApi().getQAFeed(getAccessToken(), PrefsManager.get().getObject(Constants.USER_DETAIL, Profile::class.java)?.user_role_id
                ?: 1, "2", page)
                .enqueue(object : Callback<ApiResponseModel<QAFeedModel>> {
                    override fun onFailure(call: Call<ApiResponseModel<QAFeedModel>>?, t: Throwable?) {
                        getView()?.dismissProgress()
                        getView()?.displayFailure(t?.message)
                    }

                    override fun onResponse(call: Call<ApiResponseModel<QAFeedModel>>?, response: Response<ApiResponseModel<QAFeedModel>>?) {
                        getView()?.dismissProgress()
                        if (response != null) {
                            if (response.isSuccessful) {
                                getView()?.setQAFeedList(response.body()?.data?.questions)
                            } else {
                                try {
                                    getView()?.displayError(JSONObject(response.errorBody()?.string()).getString("msg"), response.code())
                                } catch (e: Exception) {

                                }
                            }
                        }

                    }
                })
    }
}