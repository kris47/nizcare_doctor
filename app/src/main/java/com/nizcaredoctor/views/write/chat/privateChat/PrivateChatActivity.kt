package com.nizcaredoctor.views.write.chat.privateChat

import android.Manifest
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.gson.Gson
import com.nizcaredoctor.R
import com.nizcaredoctor.utils.*
import com.nizcaredoctor.views.baseViews.BaseViewActivityImpl
import com.nizcaredoctor.views.write.healthFeed.writeTip.AttachmentAdapter
import com.nizcaredoctor.webservice.model.AttachmentModel
import com.nizcaredoctor.webservice.model.Profile
import com.nizcaredoctor.webservice.model.privateQuestionModel.PrivateDataItem
import kotlinx.android.synthetic.main.activity_private_chat.*
import pub.devrel.easypermissions.EasyPermissions
import pub.devrel.easypermissions.PermissionRequest
import java.io.File

class PrivateChatActivity : BaseViewActivityImpl(), PrivateChatContract.View, EasyPermissions.PermissionCallbacks, ImagePicker.ImagePickerListener {


    private val presenter = PrivateChatPresenter()
    private lateinit var question: PrivateDataItem
    private lateinit var attachmentAdapter: AttachmentAdapter
    private val attachmentList = mutableListOf<AttachmentModel>()
    private lateinit var imagePicker: ImagePicker
    private val permissions = arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    private val permissionRequestCode = 56
    private lateinit var amazoneS3: AmazoneS3

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_private_chat)
        init()
        setListener()
    }

    private fun init() {
        question = Gson().fromJson(intent.getStringExtra("privateChatData"), PrivateDataItem::class.java)
        tvPrivateChatTitle.text = question.question?.title
        tvPrivateChatText.text = question.question?.description
        val gender = if (question.question?.user?.gender ?: "1" == "1") getString(R.string.male) else getString(R.string.male)
        tvPrivateChatInfo.text = question.question?.user?.firstname + ", $gender · " + getDate(question.created_at.toString())
        tvPrivateDoctorName.text = PrefsManager.get().getObject(Constants.USER_DETAIL, Profile::class.java)?.firstname
        Glide.with(this)
                .load(PrefsManager.get().getObject(Constants.USER_DETAIL, Profile::class.java)?.image)
                .apply(RequestOptions().placeholder(R.drawable.a2))
                .into(ivPrivateDoctor)

        attachmentAdapter = AttachmentAdapter(this, attachmentList)
        rvAttachments.adapter = attachmentAdapter
        imagePicker = ImagePicker(this, true)
    }

    private fun setListener() {
        presenter.attachView(this)
        imagePicker.setImagePickerListener(this)
        toolbar.setNavigationOnClickListener { onBackPressed() }
        tvSubmitChat.setOnClickListener {
            rootPrivate.hideKeyboard()
            if (isOnline(this)) {
                if (etPrivateChatReply.text.trim().toString().isEmpty()) {
                    rootPrivate.showErrorSnack("Please write answer to submit")
                } else {
                    var attachURL = ""
                    if (attachmentList.isNotEmpty()) {
                        amazoneS3 = AmazoneS3(this)
                        attachURL = if (attachmentList[0].fileType == Constants.PDF) {
                            amazoneS3.setPDFFileToUpload(attachmentList[0].file, attachmentList[0].name)
                        } else {
                            amazoneS3.setFileToUpload(attachmentList[0].file)
                        }
                    }
                    Log.e("data Private chat---->", attachURL)
                    presenter.submitPrivateAnswer(etPrivateChatReply.text.toString(),
                            question.question_id,
                            /*Gson().toJson(attachmentPath)*/attachURL)
                }
            } else {
                rootPrivate.showInternetError()
            }
        }
        ivAttachment.setOnClickListener {
            openImage()
        }
    }

    override fun answerSuccess(msg: String?) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
        finish()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detachView()
    }


    private fun openImage() {
        //Open image ,camera from here
        if (EasyPermissions.hasPermissions(this, *permissions)) {
            imagePicker.showImagePicker()
        } else {
            EasyPermissions.requestPermissions(
                    PermissionRequest.Builder(this, permissionRequestCode, *permissions)
                            .setRationale(getString(R.string.rational_permission))
                            .setPositiveButtonText(R.string.ok)
                            .setNegativeButtonText(R.string.cancel)
                            .build())
        }
    }

    override fun onPermissionsDenied(requestCode: Int, perms: MutableList<String>) {
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            openSettingDialog(this)
        }
    }

    override fun onPermissionsGranted(requestCode: Int, perms: MutableList<String>) {
        openImage()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }

    override fun onImageSelectedFromPicker(imageFile: File?) {
        if (imageFile != null) {
            if (attachmentList.isNotEmpty()) {
                attachmentList.removeAt(0)
            }
            attachmentList.add(AttachmentModel(imageFile.name.toString(), Constants.IMAGE, imageFile))
            attachmentAdapter.notifyDataSetChanged()

            llAttachment.visibility = View.VISIBLE
            Log.d("fileImage", imageFile.name.toString())
        }
    }

    override fun onPDFSelected(file: File?, displayName: String?) {
        if (file != null) {
            if (attachmentList.isNotEmpty()) {
                attachmentList.removeAt(0)
            }
            attachmentList.add(AttachmentModel(displayName.toString(), Constants.PDF, file))
/*
            attachmentAdapter.notifyItemInserted(attachmentList.size)
*/
            attachmentAdapter.notifyDataSetChanged()

            llAttachment.visibility = View.VISIBLE
            Log.d("filePDF", displayName)
        }
    }

    fun hideView() {
        llAttachment.visibility = View.GONE
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        imagePicker.onActivityResult(requestCode, resultCode, data)
    }
}
