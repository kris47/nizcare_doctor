package com.nizcaredoctor.views.write.healthFeed

import com.nizcaredoctor.utils.Constants
import com.nizcaredoctor.utils.PrefsManager
import com.nizcaredoctor.utils.getAccessToken
import com.nizcaredoctor.views.baseViews.BasePresenterImpl
import com.nizcaredoctor.webservice.ApiResponseModel
import com.nizcaredoctor.webservice.RetrofitClient
import com.nizcaredoctor.webservice.model.Profile
import com.nizcaredoctor.webservice.model.healthFeedModel.HealthFeedModel
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class HealthFeedPresenter : BasePresenterImpl<HealthFeedContract.View>(), HealthFeedContract.Presenter {
    override fun bookmarkQA(id: Int?, type: String, status: Int) {
        RetrofitClient.getApi().bookmarkQAAPI(getAccessToken(), type, id, status)
                .enqueue(object : Callback<ApiResponseModel<Any>> {
                    override fun onFailure(call: Call<ApiResponseModel<Any>>?, t: Throwable?) {
                        getView()?.displayFailure(t?.message)
                    }

                    override fun onResponse(call: Call<ApiResponseModel<Any>>?, response: Response<ApiResponseModel<Any>>?) {
                        if (response != null)
                            if (response.isSuccessful) {

                            } else {
                                try {
                                    getView()?.displayError(JSONObject(response.errorBody()?.string()).getString("msg"), response.code())
                                } catch (e: Exception) {

                                }
                            }
                    }
                })
    }

    override fun getAllHealthFeeds(page: Int) {
        getView()?.showProgress()
        RetrofitClient.getApi().getAllHealthFeeds(getAccessToken(), PrefsManager.get().getObject(Constants.USER_DETAIL, Profile::class.java)?.user_role_id
                ?: 1, Constants.ALL_HEALTH_FEEDS, page)
                .enqueue(object : Callback<ApiResponseModel<HealthFeedModel>> {
                    override fun onFailure(call: Call<ApiResponseModel<HealthFeedModel>>?, t: Throwable?) {
                        getView()?.dismissProgress()
                        getView()?.displayFailure(t?.message)
                    }

                    override fun onResponse(call: Call<ApiResponseModel<HealthFeedModel>>?, response: Response<ApiResponseModel<HealthFeedModel>>?) {
                        getView()?.dismissProgress()
                        if (response != null)
                            if (response.isSuccessful) {
                                getView()?.setHealthFeeds(response.body()?.data?.healthFeeds)
                            } else {
                                try {
                                    getView()?.displayError(JSONObject(response.errorBody()?.string()).getString("msg"), response.code())
                                } catch (e: Exception) {

                                }
                            }
                    }
                })
    }

}