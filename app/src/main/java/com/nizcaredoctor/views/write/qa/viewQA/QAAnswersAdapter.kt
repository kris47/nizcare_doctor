package com.nizcaredoctor.views.write.qa.viewQA

import android.app.Activity
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.gson.Gson
import com.nizcaredoctor.R
import com.nizcaredoctor.utils.getDate
import com.nizcaredoctor.utils.shareText
import com.nizcaredoctor.views.write.qa.viewSingleQAAnswer.ViewSingleQAActivity
import com.nizcaredoctor.webservice.model.qaAllAnswers.AnswerItem
import kotlinx.android.synthetic.main.item_qa_answers.view.*

class QAAnswersAdapter(val activity: Activity, val answersList: MutableList<AnswerItem>) : RecyclerView.Adapter<QAAnswersAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(activity).inflate(R.layout.item_qa_answers, parent, false))
    }

    override fun getItemCount(): Int {
        return answersList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindView(answersList[position])
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

        init {
            itemView.setOnClickListener {
                activity.startActivity(Intent(activity, ViewSingleQAActivity::class.java)
                        .putExtra("qaAnswerData", Gson().toJson(answersList[adapterPosition])))
            }
            itemView.ivUpvote.setOnClickListener(this)
            itemView.ivBookmarkQA.setOnClickListener(this)
            itemView.ivShareQA.setOnClickListener(this)
        }

        fun bindView(answerItem: AnswerItem) = with(itemView) {

            Glide.with(activity)
                    .load(answerItem.user?.image)
                    .apply(RequestOptions().placeholder(R.drawable.a2))
                    .into(ivQAPerson)

            tvDaysAgo.text = getDate(answerItem.created_at.toString())
            tvQAPerson.text = answerItem.user?.firstname
            tvQAText.text = answerItem.text
            tvQAUpvote.text = activity.getString(R.string.upvote_with_dot) + " " + answerItem.upvote_count
            if (answerItem.my_vote_count == 0) {
                ivUpvote.setImageResource(R.drawable.ic_like_normal)
            } else {
                ivUpvote.setImageResource(R.drawable.ic_like_pressed)
            }

            if (answerItem.bookmark_count == 0) {
                ivBookmarkQA.setImageResource(R.drawable.ic_bookmark_normal)
            } else {
                ivBookmarkQA.setImageResource(R.drawable.ic_bookmark_c)
            }
        }

        override fun onClick(v: View?) {
            when (v?.id) {
                R.id.ivUpvote -> {
                    (activity as ViewQAActivity).upvoteAnswer(adapterPosition)
                }

                R.id.ivBookmarkQA -> {
                    (activity as ViewQAActivity).bookmarkQAAnswer(adapterPosition)
                }
                R.id.ivShareQA -> activity.shareText()
            }
        }
    }

}
