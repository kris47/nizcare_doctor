package com.nizcaredoctor.views.write.healthFeed.viewHealthTip

import com.nizcaredoctor.views.baseViews.BasePresenter
import com.nizcaredoctor.views.baseViews.BaseView

interface ViewHealthFeedContract {
    interface View : BaseView {
        fun actionSuccess()

    }

    interface Presenter : BasePresenter<View> {
        fun upVoteAPI(id: Int?, answeR_TYPE: String, status: Int)
        fun bookmarkQA(id: Int?, answeR_TYPE: String, status: Int)
    }
}