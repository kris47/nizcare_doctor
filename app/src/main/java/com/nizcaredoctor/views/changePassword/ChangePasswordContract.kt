package com.nizcaredoctor.views.changePassword

import com.nizcaredoctor.views.baseViews.BasePresenter
import com.nizcaredoctor.views.baseViews.BaseView

interface ChangePasswordContract {
    interface View : BaseView {
        fun changePasswordSuccess()

    }

    interface Presenter : BasePresenter<View> {
        fun changePassword(token: String, password: String)

    }
}