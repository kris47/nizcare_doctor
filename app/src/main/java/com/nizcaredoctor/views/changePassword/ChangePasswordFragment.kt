package com.nizcaredoctor.views.changePassword

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.nizcaredoctor.R
import com.nizcaredoctor.utils.isOnline
import com.nizcaredoctor.utils.showErrorSnack
import com.nizcaredoctor.utils.showInternetError
import com.nizcaredoctor.views.baseViews.BaseViewFragImpl
import com.nizcaredoctor.webservice.WebConstants
import kotlinx.android.synthetic.main.fragment_change_password.*

class ChangePasswordFragment : BaseViewFragImpl(), ChangePasswordContract.View {

    private val presenter = ChangePasswordPresenter()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_change_password, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.attachView(this)
        toolbar.setNavigationOnClickListener { activity?.onBackPressed() }
        tvChangePass.setOnClickListener {
            if (isValidated()) {
                if (isOnline(activity)) {
                    presenter.changePassword(arguments?.getString(WebConstants.TOKEN).toString(), etNewPassword.text.trim().toString())
                } else {
                    view.showInternetError()
                }
            }
        }
    }

    private fun isValidated(): Boolean {
        return when {

            etNewPassword.text.toString().trim().isEmpty() -> {
                tilNewPassword.isErrorEnabled = true
                tilNewPassword.error = getString(R.string.enter_pasword)
                tilNewPassword.requestFocus()
                false
            }

            etNewPassword.text.toString().trim().length < 6 -> {
                tilNewPassword.isErrorEnabled = true
                tilNewPassword.error = getString(R.string.password_is_short)
                tilNewPassword.requestFocus()
                false
            }

            etConfirmPassword.text.toString().trim().isEmpty() -> {
                tilConfirmPassword.isErrorEnabled = true
                tilConfirmPassword.error = getString(R.string.enter_pasword)
                tilConfirmPassword.requestFocus()
                false
            }

            etConfirmPassword.text.toString().trim().length < 6 -> {
                tilConfirmPassword.isErrorEnabled = true
                tilConfirmPassword.error = getString(R.string.password_is_short)
                tilConfirmPassword.requestFocus()
                false
            }
            etNewPassword.text.trim().toString() != etConfirmPassword.text.trim().toString() -> {
                view?.showErrorSnack(getString(R.string.password_not_matched))
                false
            }

            else -> true
        }
    }

    override fun changePasswordSuccess() {
        Toast.makeText(context, "Your password has been changed successfully. Login with new password again to continue", Toast.LENGTH_LONG).show()
        activity?.finish()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        presenter.detachView()
    }
}