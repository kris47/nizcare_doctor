package com.nizcaredoctor.views.changePassword

import com.nizcaredoctor.views.baseViews.BasePresenterImpl
import com.nizcaredoctor.webservice.ApiResponseModel
import com.nizcaredoctor.webservice.RetrofitClient
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ChangePasswordPresenter : BasePresenterImpl<ChangePasswordContract.View>(), ChangePasswordContract.Presenter {
    override fun changePassword(token: String, password: String) {
        getView()?.showProgress()

        RetrofitClient.getApi().changePassword(token, password).enqueue(object : Callback<ApiResponseModel<Any>> {
            override fun onResponse(call: Call<ApiResponseModel<Any>>?, response: Response<ApiResponseModel<Any>>?) {
                getView()?.dismissProgress()
                if (response != null) {
                    if (response.isSuccessful) {
                        getView()?.changePasswordSuccess()
                    } else {
                        try {
                            getView()?.displayError(JSONObject(response.errorBody()?.string()).getString("msg"), response.code())
                        } catch (e: Exception) {

                        }
                    }
                }
            }

            override fun onFailure(call: Call<ApiResponseModel<Any>>?, t: Throwable?) {
                getView()?.dismissProgress()
                getView()?.displayFailure(t?.message)
            }
        })
    }

}