package com.nizcaredoctor.views.patients

import com.nizcaredoctor.views.baseViews.BasePresenter
import com.nizcaredoctor.views.baseViews.BaseView
import com.nizcaredoctor.webservice.model.branchModel.Branches
import com.nizcaredoctor.webservice.model.patientModel.PatientDataModel

interface PatientsContract {
    interface View : BaseView {
        fun setAllBranches(branches: Branches?)
        fun getAllCustomers(branch_id: String, search: String)
        fun getAllCustomersSuccess(patientDataModel: PatientDataModel?)
        fun removePatientSuccess(msg : String?)
    }

    interface Presenter : BasePresenter<View> {
        fun getMyClinics(page: Int)
        fun getAllCustomers(branch_id: String, search: String)
        fun onPatientRemove(branch_id: String, user_id: String)
    }

    interface onEventClickListener {
        fun onNewPatientAdded()
        fun onPatientRemove(branch_id: String, user_id: String)
    }
}