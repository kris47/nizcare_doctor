package com.nizcaredoctor.views.patients.patientInfo

import android.os.Bundle
import android.support.design.widget.BottomSheetBehavior
import android.support.design.widget.BottomSheetDialog
import android.support.v4.app.Fragment
import android.view.View
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.nizcaredoctor.R
import com.nizcaredoctor.utils.Constants
import com.nizcaredoctor.views.baseViews.BaseFragmentActivity
import com.nizcaredoctor.views.patients.patientInfo.appointment.AppointmentFragment
import com.nizcaredoctor.views.patients.patientInfo.consultation.ConsultationFragment
import com.nizcaredoctor.views.patients.patientInfo.labTest.LabTestFragment
import com.nizcaredoctor.views.patients.patientInfo.medicalRecord.MedicalRecordFragment
import com.nizcaredoctor.views.patients.patientInfo.prescription.PrescriptionFragment
import com.nizcaredoctor.views.patients.patientInfo.visit.VisitFragment
import com.nizcaredoctor.webservice.model.patientModel.PatientDataItemModel
import kotlinx.android.synthetic.main.activity_patient_info.*
import kotlinx.android.synthetic.main.bottom_sheet_patients.*
import kotlinx.android.synthetic.main.bottom_sheet_patients.view.*
import android.widget.FrameLayout

class PatientInfoActivity : BaseFragmentActivity(), View.OnClickListener {
    private lateinit var patientData: PatientDataItemModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_patient_info)
        init()
        setListeners()
        setData()
    }

    fun setData() {
        val bundle = intent.extras
        if (bundle != null) {
            patientData = bundle.getParcelable(Constants.PATIENT_INFO)
            tvPatientFullName.text = getString(R.string.patient_name, patientData.user?.firstname, patientData.user?.lastname)
            Glide.with(this).load(patientData.user?.image).apply(RequestOptions().placeholder(R.drawable.a3))
                    .into(ivPatientPic)
        }
    }

    private fun init() {
        val fragmentList = mutableListOf<Fragment>()
        fragmentList.add(AppointmentFragment())
        fragmentList.add(ConsultationFragment())
        fragmentList.add(VisitFragment())
        fragmentList.add(PrescriptionFragment())
        fragmentList.add(LabTestFragment())
        fragmentList.add(MedicalRecordFragment())

        viewPagerPatient.adapter = PatientPagerAdapter(this, fragmentList, supportFragmentManager)
        tabPatient.setupWithViewPager(viewPagerPatient)
        viewPagerPatient.offscreenPageLimit = 6
    }

    private fun setListeners() {
        toolbar.setNavigationOnClickListener { onBackPressed() }
        ivPatientMenu.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.ivPatientMenu -> {
                val mBottomSheetDialog = BottomSheetDialog(this@PatientInfoActivity)
                val sheetView = layoutInflater.inflate(R.layout.bottom_sheet_patients, null)
                mBottomSheetDialog.setContentView(sheetView)
                mBottomSheetDialog.show()
                sheetView.tvCancel.setOnClickListener { mBottomSheetDialog.dismiss() }
            }
        }
    }
}