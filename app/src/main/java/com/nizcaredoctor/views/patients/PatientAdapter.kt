package com.nizcaredoctor.views.patients

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.PopupMenu
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.nizcaredoctor.R
import com.nizcaredoctor.utils.Constants
import com.nizcaredoctor.views.patients.patientInfo.PatientInfoActivity
import com.nizcaredoctor.webservice.model.patientModel.PatientDataItemModel
import kotlinx.android.synthetic.main.item_patients.view.*

class PatientAdapter(val context: Context?, val patientList: List<PatientDataItemModel>,
                     val listener: PatientsContract.onEventClickListener) : RecyclerView.Adapter<PatientAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_patients, parent, false))
    }

    override fun getItemCount(): Int = patientList.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind(patientList.get(position))
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        init {
            itemView.rlRoot.setOnClickListener(this)
            itemView.ivMore.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            when (v?.id) {
                R.id.rlRoot -> {
                    val bundle = Bundle()
                    bundle.putParcelable(Constants.PATIENT_INFO, patientList[adapterPosition])
                    val intent = Intent(context, PatientInfoActivity::class.java)
                    intent.putExtras(bundle)
                    context?.startActivity(intent)

                }
                R.id.ivMore -> {
                    val menu = PopupMenu(context!!, v)
                    menu.inflate(R.menu.patient_option_meu)
                    menu.setOnMenuItemClickListener { item: MenuItem? ->
                        when (item?.itemId) {
                            R.id.remove -> {
                                listener.onPatientRemove(patientList[adapterPosition].branch_id.toString(),
                                        patientList[adapterPosition].user_id.toString())
                            }
                        }
                        true
                    }
                    menu.show()
                }
            }
        }

        fun onBind(model: PatientDataItemModel) {
            itemView.tvPatientName.text = context?.getString(R.string.patient_name, model.user?.firstname, model.user?.lastname)
            itemView.tvPatientInfo.text = context?.getString(R.string.patient_info,
                    getGender(model.user?.gender), model.user?.user_detail?.dob)
            Glide.with(context!!).load(model.user?.image).
                    apply(RequestOptions().placeholder(R.drawable.a3)).into(itemView.ivPatient)
        }

        fun getGender(gender: Int?): String {
            if (gender == 0) return context?.getString(R.string.male).toString() else return context?.getString(R.string.female).toString()
        }
    }
}