package com.nizcaredoctor.views.patients

import com.nizcaredoctor.utils.getAccessToken
import com.nizcaredoctor.views.baseViews.BasePresenterImpl
import com.nizcaredoctor.webservice.ApiResponseModel
import com.nizcaredoctor.webservice.RetrofitClient
import com.nizcaredoctor.webservice.WebConstants
import com.nizcaredoctor.webservice.model.branchModel.MyClinicModel
import com.nizcaredoctor.webservice.model.patientModel.PatientModel
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PatientsPresenter : BasePresenterImpl<PatientsContract.View>(), PatientsContract.Presenter {
    override fun getMyClinics(page: Int) {
        getView()?.showProgress()
        RetrofitClient.getApi().getMyClinics(getAccessToken(), page)
                .enqueue(object : Callback<ApiResponseModel<MyClinicModel>> {
                    override fun onFailure(call: Call<ApiResponseModel<MyClinicModel>>?, t: Throwable?) {
                        getView()?.displayFailure(t?.message)
                        getView()?.dismissProgress()
                    }

                    override fun onResponse(call: Call<ApiResponseModel<MyClinicModel>>?, response: Response<ApiResponseModel<MyClinicModel>>) {
                        getView()?.dismissProgress()
                        if (response.isSuccessful)
                            getView()?.setAllBranches(response.body()?.data?.branches)
                        else
                            getView()?.displayError(JSONObject(response.errorBody()?.string()).getString("msg"), response.code())
                    }
                })
    }

    override fun getAllCustomers(branch_id: String, search: String) {
//        getView()?.showProgress()
        val map = HashMap<String, String>()
        if (!branch_id.equals("0"))
            map.put(WebConstants.BRANCH_ID, branch_id)
        if (search.isNotEmpty())
            map.put(WebConstants.SEARCH, search)

        RetrofitClient.getApi().getALlPatients(getAccessToken(), map)
                .enqueue(object : Callback<ApiResponseModel<PatientModel>> {
                    override fun onFailure(call: Call<ApiResponseModel<PatientModel>>?, t: Throwable?) {
                        getView()?.displayFailure(t?.message)
//                        getView()?.dismissProgress()
                    }

                    override fun onResponse(call: Call<ApiResponseModel<PatientModel>>?, response: Response<ApiResponseModel<PatientModel>>) {
                        getView()?.dismissProgress()
                        if (response.isSuccessful)
                            getView()?.getAllCustomersSuccess(response.body()?.data?.customers)
                        else
                            getView()?.displayError(JSONObject(response.errorBody()?.string()).getString("msg"), response.code())
                    }
                })
    }

    override fun onPatientRemove(branch_id: String, user_id: String) {
        getView()?.showProgress()
        RetrofitClient.getApi().removeCustomer(getAccessToken(), branch_id, user_id)
                .enqueue(object : Callback<ApiResponseModel<Any>> {
                    override fun onFailure(call: Call<ApiResponseModel<Any>>?, t: Throwable?) {
                        getView()?.displayFailure(t?.message)
                        getView()?.dismissProgress()
                    }

                    override fun onResponse(call: Call<ApiResponseModel<Any>>?, response: Response<ApiResponseModel<Any>>) {
                        getView()?.dismissProgress()
                        if (response.isSuccessful)
                            getView()?.removePatientSuccess(response.body()?.msg)
                        else
                            getView()?.displayError(JSONObject(response.errorBody()?.string()).getString("msg"), response.code())
                    }
                })
    }
}