package com.nizcaredoctor.views.patients.addPatient

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.app.DatePickerDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.TextInputEditText
import android.support.design.widget.TextInputLayout
import android.support.v4.content.ContextCompat
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.nizcaredoctor.R
import com.nizcaredoctor.utils.*
import com.nizcaredoctor.views.accountSetup.stateCity.StateCityActivity
import com.nizcaredoctor.views.baseViews.BaseViewFragImpl
import com.nizcaredoctor.views.patients.PatientsContract
import com.nizcaredoctor.webservice.WebConstants
import com.nizcaredoctor.webservice.model.LanguageModelObject
import com.nizcaredoctor.webservice.model.branchModel.ClinicDataItem
import kotlinx.android.synthetic.main.fragment_add_patient.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import pub.devrel.easypermissions.EasyPermissions
import pub.devrel.easypermissions.PermissionRequest
import java.io.File
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

class AddPatientFragment : BaseViewFragImpl(), View.OnClickListener, AddPatientsContract.View,
        AdapterView.OnItemSelectedListener, ImagePicker.ImagePickerListener, EasyPermissions.PermissionCallbacks {

    private lateinit var imagePicker: ImagePicker
    private val presenter = AddPatientsPresenter()
    private var branchList = mutableListOf<ClinicDataItem>()
    private val permissions = arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    private val permissionRequestCode = 56
    private var profilePicURL = ""
    private lateinit var amazonS3: AmazoneS3
    private var selectedStateId = 0
    private var selectedCityId = 0
    private var REQUEST_CODE_STATE = 100
    private var REQUEST_CODE_CITY = 101
    private var languageModelObject = mutableListOf<LanguageModelObject>()
    private val languageSpokenList = mutableListOf<String>()
    private lateinit var selectedLanguages: BooleanArray
    private var languageSpokenId = mutableListOf<Int>()
    private var branch_id = "0"
    private lateinit var listener: PatientsContract.onEventClickListener

    val cal = Calendar.getInstance()
    val dateSetListener = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
        cal.set(Calendar.YEAR, year)
        cal.set(Calendar.MONTH, monthOfYear)
        cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)
        etDob.setText(SimpleDateFormat(Constants.SIMPLE_DATE_FORMAT).format(cal.time))
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.ivProfileImage -> {
                openImage()
            }
            R.id.tvFemale -> {
                tvMale.isSelected = false
                tvMale.background = ContextCompat.getDrawable(context!!, R.drawable.button_white_left)
                tvFemale.background = ContextCompat.getDrawable(context!!, R.drawable.button_green_right)
                tvMale.setTextColor(ContextCompat.getColor(context!!, R.color.colorPrimary))
                tvFemale.setTextColor(ContextCompat.getColor(context!!, R.color.white))
            }
            R.id.tvMale -> {
                tvMale.isSelected = true
                tvMale.background = ContextCompat.getDrawable(context!!, R.drawable.button_green_left)
                tvFemale.background = ContextCompat.getDrawable(context!!, R.drawable.button_white_right)
                tvMale.setTextColor(ContextCompat.getColor(context!!, R.color.white))
                tvFemale.setTextColor(ContextCompat.getColor(context!!, R.color.colorPrimary))
            }
            R.id.etDob -> {
                DatePickerDialog(context, dateSetListener, 2000
                        , 0, 1).show()
            }
            R.id.etState -> {
                startActivityForResult(Intent(context, StateCityActivity::class.java)
                        .putExtra(Constants.SELECTED_POSITION, selectedStateId)
                        .putExtra(Constants.ListType, Constants.States), REQUEST_CODE_STATE)
            }
            R.id.etCity -> {
                if (selectedStateId == 0) {
                    rootAccount.showErrorSnack(getString(R.string.select_state_first))
                } else {
                    startActivityForResult(Intent(context, StateCityActivity::class.java)
                            .putExtra(Constants.SELECTED_STATE_ID, selectedStateId)
                            .putExtra(Constants.SELECTED_POSITION, selectedCityId)
                            .putExtra(Constants.ListType, Constants.Cities), REQUEST_CODE_CITY)
                }
            }
            R.id.etBloodGrp -> {
                val stringArray = resources.getStringArray(R.array.blood_groups)
                val builder = AlertDialog.Builder(context)
                builder.setTitle("Choose blood group")
                builder.setItems(stringArray, { dialog: DialogInterface?, which: Int ->
                    etBloodGrp.setText(stringArray[which].toString())
                    dialog?.dismiss()
                })
                builder.show()
            }
            R.id.etLanguage -> {
                languageSpokenId = SelectLanguage().selectLanguage(context, languageSpokenList, selectedLanguages, languageModelObject, etLanguage)
            }
            R.id.tvAddPatient -> {
                if (isValid()) {
                    val map = HashMap<String, String>()
                    map.put(WebConstants.BRANCH_ID, branch_id)
                    map.put(WebConstants.FIRST_NAME, etFirstName.text.toString().trim())
                    map.put(WebConstants.LAST_NAME, etLastName.text.toString().trim())
                    map.put(WebConstants.PHONENUMBER, etMobile.text.toString().trim())
                    if (profilePicURL.isNotEmpty())
                        map.put(WebConstants.IMAGE, profilePicURL)
                    map.put(WebConstants.GENDER, (if (tvMale.isSelected) 0 else 1).toString())
                    map.put(WebConstants.DOB, etDob.text.toString().trim())
                    map.put(WebConstants.LANGUAGE, Gson().toJson(languageSpokenId))
                    map.put(WebConstants.EMAIL, etEmail.text.toString().trim())
                    map.put(WebConstants.HEIGHT, etHeight.text.toString().trim())
                    map.put(WebConstants.WEIGHT, etWeight.text.toString().trim())
                    map.put(WebConstants.BLOOD_GRP, etBloodGrp.text.toString().trim())
                    map.put(WebConstants.ADDRESS, etAddress.text.toString().trim())
                    map.put(WebConstants.STATE_ID, selectedStateId.toString())
                    map.put(WebConstants.CITYID, selectedCityId.toString())
                    map.put(WebConstants.TYPE_OF_VISIT, etTypeOfVisit.text.toString().trim())
                    presenter.addPatients(map)
                }
            }
        }
    }

    override fun addPatientSuccess(msg: String?) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()
        if (::listener.isInitialized)
            listener.onNewPatientAdded()
        activity?.supportFragmentManager?.popBackStack()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_add_patient, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        setListener()
        if (isOnline(context)) {
            presenter.getLanguages()
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        presenter.attachView(this)
    }

    override fun onDetach() {
        super.onDetach()
        presenter.detachView()
    }

    fun init() {
        tvTitle.text = getString(R.string.add_a_patient)
        imagePicker = ImagePicker(this, false)
        imagePicker.setImagePickerListener(this)
        amazonS3 = AmazoneS3(context)
        tvMale.isSelected = true

        val bundle = arguments
        if (bundle != null) {
            branchList = bundle.getParcelableArrayList<ClinicDataItem>(Constants.BRANCH_LIST)
            val list = Array(branchList.size, { i -> branchList[i].name })
            setClinicAdapter(list)
        }
        else{

        }
    }

    fun setListener() {
        toolbar.setNavigationOnClickListener { activity?.supportFragmentManager?.popBackStack() }
        spinnerClinics.onItemSelectedListener = this
        checkForEmptyData(etFirstName, tilFirstName, R.string.error_empty_first_name)
        checkForEmptyData(etLastName, tilLastName, R.string.error_empty_last_name)
        checkForEmptyData(etDob, tilDob, R.string.error_empty_dob)
        checkForEmptyData(etLanguage, tilLanguage, R.string.error_empty_lang)
        checkForEmptyData(etMobile, tilMobile, R.string.error_empty_mobile)
        checkForEmptyData(etEmail, tilEmail, R.string.error_empty_email)
        checkForEmptyData(etHeight, tilHeight, R.string.error_empty_height)
        checkForEmptyData(etWeight, tilWeight, R.string.error_empty_weight)
        checkForEmptyData(etAddress, tilAddress, R.string.error_empty_address)
        tvAddPatient.setOnClickListener(this)
        ivProfileImage.setOnClickListener(this)
        tvMale.setOnClickListener(this)
        tvFemale.setOnClickListener(this)
        etLanguage.setOnClickListener(this)
        etDob.setOnClickListener(this)
        etState.setOnClickListener(this)
        etCity.setOnClickListener(this)
        etBloodGrp.setOnClickListener(this)
    }

    fun bindListener(listener: PatientsContract.onEventClickListener) {
        this.listener = listener
    }

    private fun checkForEmptyData(editText: TextInputEditText, tilEditText: TextInputLayout, errorMessage: Int) {
        editText.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable) {
                if (editText.text.isEmpty()) {
                    tilEditText.error = getString(errorMessage)
                } else {
                    tilEditText.isErrorEnabled = false
                }
            }
        })
    }

    override fun setLanguageSpoken(languages: List<LanguageModelObject>?) {
        this.languageModelObject = languages as MutableList<LanguageModelObject>
        languageSpokenList.clear()
        for (i in 0..languages.size - 1) {
            languageSpokenList.add(languages.get(i).name)
        }
        selectedLanguages = BooleanArray(languageSpokenList.size, { false })
    }

    fun setClinicAdapter(list: Array<String?>) {
        val aa = ArrayAdapter(context, android.R.layout.simple_spinner_item, list)
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerClinics.adapter = aa
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val spinner = parent as Spinner
        if (spinner.id == R.id.spinnerClinics) {
            if (branchList.isNotEmpty())
                branch_id = branchList[spinnerClinics.selectedItemPosition].id.toString()
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {

    }

    fun showErrorMsg(editText: TextInputEditText, tilEditText: TextInputLayout, errorMsg: String): Boolean {
        tilEditText.isErrorEnabled = true
        tilEditText.error = errorMsg
        editText.requestFocus()
        return false
    }

    fun isValid(): Boolean {
        if (etFirstName.text.toString().trim().isEmpty())
            return showErrorMsg(etFirstName, tilFirstName, getString(R.string.error_empty_first_name))
        if (etLastName.text.toString().trim().isEmpty())
            return showErrorMsg(etLastName, tilLastName, getString(R.string.error_empty_last_name))
        if (etDob.text.toString().trim().isEmpty())
            return showErrorMsg(etDob, tilDob, getString(R.string.error_empty_dob))
        if (etLanguage.text.toString().trim().isEmpty())
            return showErrorMsg(etLanguage, tilLanguage, getString(R.string.error_empty_lang))
        if (etMobile.text.toString().trim().isEmpty())
            return showErrorMsg(etMobile, tilMobile, getString(R.string.error_empty_mobile))
        if (etEmail.text.toString().trim().isEmpty())
            return showErrorMsg(etEmail, tilEmail, getString(R.string.error_empty_email))
        if (!isValidEmail(etEmail.text.trim().toString()))
            return showErrorMsg(etEmail, tilEmail, getString(R.string.enter_valid_email))
        if (etHeight.text.toString().trim().isEmpty())
            return showErrorMsg(etHeight, tilHeight, getString(R.string.error_empty_height))
        if (etWeight.text.toString().trim().isEmpty())
            return showErrorMsg(etWeight, tilWeight, getString(R.string.error_empty_weight))
        if (etBloodGrp.text.toString().trim().isEmpty())
            return showErrorMsg(etBloodGrp, tilBloodGrp, getString(R.string.error_empty_blood_grp))
        if (etState.text.toString().trim().isEmpty())
            return showErrorMsg(etState, tilState, getString(R.string.error_empty_state))
        if (etCity.text.toString().trim().isEmpty())
            return showErrorMsg(etCity, tilCity, getString(R.string.error_empty_city))
        if (etAddress.text.toString().trim().isEmpty())
            return showErrorMsg(etAddress, tilAddress, getString(R.string.error_empty_address))
        return true
    }

    private fun openImage() {
        //Open image ,camera from here
        if (EasyPermissions.hasPermissions(context!!, *permissions)) {
            imagePicker.showImagePicker()
        } else {
            EasyPermissions.requestPermissions(
                    PermissionRequest.Builder(this, permissionRequestCode, *permissions)
                            .setRationale(getString(R.string.rational_permission))
                            .setPositiveButtonText(R.string.ok)
                            .setNegativeButtonText(R.string.cancel)
                            .build())
        }
    }

    override fun onImageSelectedFromPicker(imageFile: File?) {
        Glide.with(this)
                .load(imageFile)
                .into(ivProfileImage)
        profilePicURL = amazonS3.setFileToUpload(imageFile)
    }

    override fun onPDFSelected(file: File?, displayName: String?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onPermissionsDenied(requestCode: Int, perms: MutableList<String>) {
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            openSettingDialog(activity)
        }
    }

    override fun onPermissionsGranted(requestCode: Int, perms: MutableList<String>) {
        openImage()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            REQUEST_CODE_STATE -> {
                if (resultCode == Activity.RESULT_OK) {
                    selectedCityId = 0
                    etCity.setText("")
                    etState.setText(data?.getStringExtra("stateName"))
                    selectedStateId = data?.getIntExtra("stateID", 1) ?: 0
                }
            }
            REQUEST_CODE_CITY -> {
                if (resultCode == Activity.RESULT_OK) {
                    etCity.setText(data?.getStringExtra("cityName"))
                    selectedCityId = data?.getIntExtra("cityID", 1) ?: 0
                }
            }
            else -> {
                imagePicker.onActivityResult(requestCode, resultCode, data)
            }
        }
    }
}