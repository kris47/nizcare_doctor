package com.nizcaredoctor.views.patients.addPatient

import com.nizcaredoctor.views.baseViews.BasePresenter
import com.nizcaredoctor.views.baseViews.BaseView
import com.nizcaredoctor.webservice.model.LanguageModelObject

interface AddPatientsContract {
    interface View : BaseView {
        fun setLanguageSpoken(languages: List<LanguageModelObject>?)
        fun addPatientSuccess(msg : String?)
    }

    interface Presenter : BasePresenter<View> {
        fun getLanguages()
        fun addPatients(map: HashMap<String, String>)
    }
}