package com.nizcaredoctor.views.patients.patientInfo

import android.content.Context
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import com.nizcaredoctor.R

class PatientPagerAdapter(val context: Context, val fragmentList: MutableList<Fragment>, fragmentManager: FragmentManager) :
        FragmentStatePagerAdapter(fragmentManager) {

    override fun getItem(position: Int): Fragment {
        return fragmentList[position]
    }

    override fun getCount(): Int {
        return fragmentList.size
    }

    override fun getPageTitle(position: Int): CharSequence? {
        when (position) {
            0 -> return context.getString(R.string.appointments)
            1 -> return context.getString(R.string.consultations)
            2 -> return context.getString(R.string.visits)
            3 -> return context.getString(R.string.prescription)
            4 -> return context.getString(R.string.lab_tests)
            5 -> return context.getString(R.string.medical_records)
        }
        return ""
    }
}