package com.nizcaredoctor.views.patients

import android.content.Context
import android.os.Bundle
import android.os.Parcelable
import android.support.v4.widget.SwipeRefreshLayout
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import com.nizcaredoctor.R
import com.nizcaredoctor.utils.Constants
import com.nizcaredoctor.utils.addFragment
import com.nizcaredoctor.utils.isOnline
import com.nizcaredoctor.utils.showError
import com.nizcaredoctor.views.baseViews.BaseViewFragImpl
import com.nizcaredoctor.views.patients.addPatient.AddPatientFragment
import com.nizcaredoctor.webservice.model.branchModel.Branches
import com.nizcaredoctor.webservice.model.branchModel.ClinicDataItem
import com.nizcaredoctor.webservice.model.patientModel.PatientDataItemModel
import com.nizcaredoctor.webservice.model.patientModel.PatientDataModel
import kotlinx.android.synthetic.main.fragment_patients.*
import java.util.*

class PatientsFragment : BaseViewFragImpl(), View.OnClickListener, PatientsContract.View, AdapterView.OnItemSelectedListener,
        PatientsContract.onEventClickListener, SwipeRefreshLayout.OnRefreshListener {

    private val presenter = PatientsPresenter()
    private val branchList = mutableListOf<ClinicDataItem>()
    private val patientList = mutableListOf<PatientDataItemModel>()
    private var branchId = "0"
    private var page = 1
    private lateinit var timer: Timer
    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.tvAddNew -> {
                if (branchList.isNotEmpty()) {
                    val fragment = AddPatientFragment()
                    fragment.bindListener(this)
                    val bundle = Bundle()
                    bundle.putParcelableArrayList(Constants.BRANCH_LIST, branchList as ArrayList<out Parcelable>)
                    fragment.arguments = bundle
                    addFragment(activity!!.supportFragmentManager, fragment, R.id.container
                            , false, true, true)
                } else
                    showError(context, getString(R.string.no_clinic_avai))
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_patients, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        setListener()
    }

    fun init() {
        rvPatients.adapter = PatientAdapter(activity, patientList,this)
        getAllClinics(page)
    }

    fun setListener() {
        tvAddNew.setOnClickListener(this)
        spinnerClinics.onItemSelectedListener = this
        swipeRefresh.setOnRefreshListener(this)
        etSearch.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                timer = Timer()
                timer.schedule(object : TimerTask() {
                    override fun run() {
                        getAllCustomers(branchId, etSearch.text.toString().trim())
                    }
                }, 1000)
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }
        })
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        presenter.attachView(this)
    }

    override fun onDetach() {
        super.onDetach()
        presenter.detachView()
    }

    private fun getAllClinics(page: Int) {
        if (isOnline(context)) {
            presenter.getMyClinics(page)
        } else
            showDefaultValue(R.raw.no_internet, getString(R.string.no_interner_connection), getString(R.string.interent_substitles))
    }

    fun showDefaultValue(image: Int, data: String, subTitle: String) {
        viewFlipper.displayedChild = 2
        noDataView.setImage(image)
        noDataView.setData(data, subTitle)
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val spinner = parent as Spinner
        if (spinner.id == R.id.spinnerClinics) {
            if (spinner.selectedItemPosition == 0)
                branchId = "0"
            else
                branchId = branchList[spinner.selectedItemPosition - 1].id.toString()
            getAllCustomers(branchId, etSearch.text.toString().trim())
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {

    }

    override fun setAllBranches(branches: Branches?) {
        if (branches != null) {
            val list = mutableListOf<String>()
            if (branches.data.isNotEmpty()) {
                branchList.clear()
                branchList.addAll(branches.data)
                list.add(0, getString(R.string.all_clinics))
                for (i in 0..branchList.size - 1) {
                    list.add(branchList[i].name!!)
                }
            } else
                list.add(0, getString(R.string.no_clinic_avai))
            setClinicAdapter(list)
        }
    }

    fun setClinicAdapter(list: MutableList<String>) {
        val aa = ArrayAdapter(context, android.R.layout.simple_spinner_item, list)
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerClinics.adapter = aa
        spinnerClinics.setSelection(0, true)
    }

    override fun getAllCustomers(branch_id: String, search: String) {
        if (isOnline(context))
            presenter.getAllCustomers(branch_id, search)
        else
            showDefaultValue(R.raw.no_internet, getString(R.string.no_interner_connection), getString(R.string.interent_substitles))
    }

    override fun getAllCustomersSuccess(patientDataModel: PatientDataModel?) {
        if (swipeRefresh.isRefreshing)
            swipeRefresh.isRefreshing = false
        if (patientDataModel != null) {
            if (patientDataModel.data.isNotEmpty()) {
                viewFlipper.displayedChild = 0
                patientList.clear()
                patientList.addAll(patientDataModel.data)
                rvPatients.adapter.notifyDataSetChanged()
            } else {
                showDefaultValue(R.raw.no_data, getString(R.string.no_patient_available), getString(R.string.click_add_patient))
            }
        }
    }

    override fun onNewPatientAdded() {
        getAllCustomers(branchId, etSearch.text.toString().trim())
    }

    override fun onRefresh() {
        viewFlipper.displayedChild = 1
        getAllCustomers(branchId, etSearch.text.toString().trim())
    }

    override fun onPatientRemove(branch_id: String, user_id: String) {
        presenter.onPatientRemove(branch_id, user_id)
    }

    override fun removePatientSuccess(msg: String?) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()
        getAllCustomers(branchId, etSearch.text.toString().trim())
    }
}