package com.nizcaredoctor.views.patients.addPatient

import com.nizcaredoctor.utils.getAccessToken
import com.nizcaredoctor.views.baseViews.BasePresenterImpl
import com.nizcaredoctor.webservice.ApiResponseModel
import com.nizcaredoctor.webservice.RetrofitClient
import com.nizcaredoctor.webservice.model.LanguageSpokenModel
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AddPatientsPresenter : BasePresenterImpl<AddPatientsContract.View>(), AddPatientsContract.Presenter {

    override fun getLanguages() {
        RetrofitClient.getApi().getLanguages().enqueue(object : Callback<ApiResponseModel<LanguageSpokenModel>> {
            override fun onFailure(call: Call<ApiResponseModel<LanguageSpokenModel>>?, t: Throwable?) {
                getView()?.displayFailure(t?.message)
            }

            override fun onResponse(call: Call<ApiResponseModel<LanguageSpokenModel>>?, response: Response<ApiResponseModel<LanguageSpokenModel>>?) {
                if (response != null) {
                    if (response.isSuccessful) {
                        getView()?.setLanguageSpoken(response.body()?.data?.languages)
                    } else {
                        try {
                            getView()?.displayError(JSONObject(response.errorBody()?.string()).getString("msg"), response.code())
                        } catch (e: Exception) {

                        }
                    }
                }
            }

        })
    }

    override fun addPatients(map: HashMap<String, String>) {
        getView()?.showProgress()
        RetrofitClient.getApi().addCustomer(getAccessToken(), map).enqueue(object : Callback<ApiResponseModel<Any>> {
            override fun onFailure(call: Call<ApiResponseModel<Any>>?, t: Throwable?) {
                getView()?.displayFailure(t?.message)
            }

            override fun onResponse(call: Call<ApiResponseModel<Any>>?, response: Response<ApiResponseModel<Any>>?) {
                getView()?.dismissProgress()
                if (response != null) {
                    if (response.isSuccessful) {
                        getView()?.addPatientSuccess(response.body()?.msg)
                    } else {
                        try {
                            getView()?.displayError(JSONObject(response.errorBody()?.string()).getString("msg"), response.code())
                        } catch (e: Exception) {

                        }
                    }
                }
            }

        })
    }
}