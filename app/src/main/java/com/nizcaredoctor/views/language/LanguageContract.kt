package com.nizcaredoctor.views.language

import com.nizcaredoctor.views.baseViews.BasePresenter
import com.nizcaredoctor.views.baseViews.BaseView
import com.nizcaredoctor.webservice.model.AppLanguagesItem

interface LanguageContract {

    interface View : BaseView {
        fun setLanguageData(appLanguages: MutableList<AppLanguagesItem>?)

    }

    interface Presenter : BasePresenter<View> {
        fun getLanguages()
    }
}