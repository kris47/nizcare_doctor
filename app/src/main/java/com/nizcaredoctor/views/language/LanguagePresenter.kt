package com.nizcaredoctor.views.language

import com.nizcaredoctor.views.baseViews.BasePresenterImpl
import com.nizcaredoctor.webservice.ApiResponseModel
import com.nizcaredoctor.webservice.RetrofitClient
import com.nizcaredoctor.webservice.model.LanguageModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LanguagePresenter: BasePresenterImpl<LanguageContract.View>(),LanguageContract.Presenter {
    override fun getLanguages() {
        getView()?.showProgress()

        RetrofitClient.getApi().getAppLanguages().enqueue(object : Callback<ApiResponseModel<LanguageModel>>{
            override fun onFailure(call: Call<ApiResponseModel<LanguageModel>>?, t: Throwable?) {
                getView()?.dismissProgress()
                getView()?.displayFailure("")
            }

            override fun onResponse(call: Call<ApiResponseModel<LanguageModel>>?, response: Response<ApiResponseModel<LanguageModel>>?) {
                getView()?.dismissProgress()
                if (response != null) {
                    if(response.isSuccessful)
                    {
                        getView()?.setLanguageData(response.body()?.data?.app_languages)
                    }else{
                        getView()?.displayError(response.errorBody()?.string(),response.code())
                    }
                }
            }
        })
    }
}