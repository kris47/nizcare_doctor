package com.nizcaredoctor.views.language

import android.app.Activity
import android.content.Intent
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.nizcaredoctor.R
import com.nizcaredoctor.utils.Constants
import com.nizcaredoctor.utils.PrefsManager
import com.nizcaredoctor.views.walkThrough.LoginSignUpActivity
import com.nizcaredoctor.webservice.model.AppLanguagesItem
import kotlinx.android.synthetic.main.item_rv_language.view.*

class LanguageAdapter(val items: MutableList<AppLanguagesItem>, val context: Activity?) : RecyclerView.Adapter<LanguageAdapter.ViewHolder>() {


    private var selectedPosition = 0
    var selectedId = 0

    override fun getItemCount(): Int {
        return items.size
    }

    // Inflates the item views
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_rv_language, parent, false))
    }

    // Binds each language in the ArrayList to a view
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (items[position].id == selectedId) {
            holder.itemView.ivCheck.visibility = View.VISIBLE
            context?.let { ContextCompat.getColor(it, R.color.colorPrimary) }?.let { holder.itemView.tvLanguage.setTextColor(it) }
        } else {
            holder.itemView.ivCheck.visibility = View.GONE
            context?.let { ContextCompat.getColor(it, R.color.black85) }?.let { holder.itemView.tvLanguage.setTextColor(it) }
        }
        holder.itemView.tvLanguage?.text = items[position].name
    }


    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        init {
            view.tvLanguage.setOnClickListener {
                PrefsManager.get().save(Constants.LOCALE, items[adapterPosition].locale)
                PrefsManager.get().save(Constants.APPLANGUAGEID, items[adapterPosition].id.toString())
                selectedPosition = adapterPosition
                selectedId = items[adapterPosition].id
                notifyDataSetChanged()
                (context as LanguageActivity).finishAffinity()
                context.startActivity(Intent(context, LoginSignUpActivity::class.java))
            }
        }
    }
}