package com.nizcaredoctor.views.language

import android.os.Bundle
import android.view.View
import com.nizcaredoctor.R
import com.nizcaredoctor.utils.Constants
import com.nizcaredoctor.utils.PrefsManager
import com.nizcaredoctor.utils.isOnline
import com.nizcaredoctor.views.baseViews.BaseViewActivityImpl
import com.nizcaredoctor.webservice.model.AppLanguagesItem
import kotlinx.android.synthetic.main.activity_language.*

class LanguageActivity : BaseViewActivityImpl(), LanguageContract.View {


    private val presenter = LanguagePresenter()
    private val languages = mutableListOf<AppLanguagesItem>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_language)
        init()
        setListeners()
        setData()
        if (isOnline(this)) {
            noData.visibility = View.GONE
            presenter.getLanguages()
        } else {
            progress.visibility = View.GONE
            noData.visibility = View.VISIBLE
            noData.setImage(R.raw.no_internet)
            noData.setData(getString(R.string.no_interner_connection), getString(R.string.interent_substitles))
        }
    }

    private fun init() {

    }

    private fun setData() {
        val adapter = LanguageAdapter(languages, this)
        rvLanguage.adapter = adapter
        adapter.selectedId = PrefsManager.get().getString(Constants.APPLANGUAGEID, "0").toInt()
    }

    private fun setListeners() {
        toolbar.setNavigationOnClickListener { onBackPressed() }
        presenter.attachView(this)
    }

    override fun setLanguageData(appLanguages: MutableList<AppLanguagesItem>?) {
        if (appLanguages != null) {
            if (appLanguages.isEmpty()) {
                noData.visibility = View.VISIBLE
                noData.setData("No internet connection", "It seems that your internet is off. Turn on internet to continue")
            } else {
                noData.visibility = View.GONE
                languages.clear()
                PrefsManager.get().save(Constants.LOCALE, appLanguages[0].locale)
                PrefsManager.get().save(Constants.APPLANGUAGEID, appLanguages[0].id.toString())
                languages.addAll(appLanguages)
                rvLanguage.adapter.notifyDataSetChanged()
            }
        }
    }

    override fun showProgress() {
        progress.visibility = View.VISIBLE
    }

    override fun dismissProgress() {
        progress.visibility = View.GONE
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detachView()
    }

}