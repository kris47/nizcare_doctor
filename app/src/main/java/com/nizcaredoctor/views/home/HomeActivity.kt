package com.nizcaredoctor.views.home

import android.os.Bundle
import android.support.design.widget.BottomSheetDialog
import android.support.v4.app.Fragment
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.nizcaredoctor.R
import com.nizcaredoctor.utils.addFragment
import com.nizcaredoctor.views.account.AccountFragment
import com.nizcaredoctor.views.appointment.BookAppointmentFragment
import com.nizcaredoctor.views.homeDashboard.HomeDashBoardFragment
import com.nizcaredoctor.views.patients.PatientsFragment
import com.nizcaredoctor.views.write.WriteFragment
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.bottom_sheet_home.view.*
import kotlinx.android.synthetic.main.fragment_patients.*

class HomeActivity : AppCompatActivity(), ViewPager.OnPageChangeListener, View.OnClickListener {

    private val fragmentList = mutableListOf<Fragment>()
    private lateinit var mBottomSheetDialog: BottomSheetDialog

    companion object {
        const val HOME = 0
        const val PATIENTS = 1
        const val WRITE = 2
        const val ACCOUNT = 3
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        init()
        setListeners()
    }

    private fun init() {
        fragmentList.add(HomeDashBoardFragment())
        fragmentList.add(PatientsFragment())
        fragmentList.add(WriteFragment())
        fragmentList.add(AccountFragment())
        viewpager.adapter = CommonPagerAdapter(fragmentList, supportFragmentManager)
        viewpager.offscreenPageLimit = 4
        viewpager.addOnPageChangeListener(this)
        changeTab(HOME)
    }

    private fun setListeners() {
        rlAdd.setOnClickListener(this)
        rlHome.setOnClickListener(this)
        rlWrite.setOnClickListener(this)
        rlAccount.setOnClickListener(this)
        rlPatients.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.rlHome -> changeTab(HOME)
            R.id.rlWrite -> changeTab(WRITE)
            R.id.rlAccount -> changeTab(ACCOUNT)
            R.id.rlPatients -> changeTab(PATIENTS)

            R.id.tvAddPatintVisit -> {
                mBottomSheetDialog.dismiss()
                addFragment(supportFragmentManager, BookAppointmentFragment(), R.id.container
                        , false, true, true)
            }
            R.id.tvAppointment -> {
                mBottomSheetDialog.dismiss()
                addFragment(supportFragmentManager, BookAppointmentFragment(), R.id.container
                        , false, true, true)
            }
            R.id.tvCancel -> {
                mBottomSheetDialog.dismiss()
            }
            R.id.tvAddPrescription -> {
                mBottomSheetDialog.dismiss()
                addFragment(supportFragmentManager, BookAppointmentFragment(), R.id.container
                        , false, true, true)
            }
            R.id.tvAddPatient -> {
                mBottomSheetDialog.dismiss()
                val patientsFragment = fragmentList.get(1)
                (patientsFragment as PatientsFragment).tvAddNew.performClick()
            }
            R.id.tvAddTipPlan -> {
                mBottomSheetDialog.dismiss()
                addFragment(supportFragmentManager, BookAppointmentFragment(), R.id.container
                        , false, true, true)
            }
            R.id.rlAdd -> {
                mBottomSheetDialog = BottomSheetDialog(this@HomeActivity)
                val sheetView = layoutInflater.inflate(R.layout.bottom_sheet_home, null)
                mBottomSheetDialog.setContentView(sheetView)
                mBottomSheetDialog.show()
                sheetView.tvAddPatintVisit.setOnClickListener(this)
                sheetView.tvAppointment.setOnClickListener(this)
                sheetView.tvCancel.setOnClickListener(this)
                sheetView.tvAddPrescription.setOnClickListener(this)
                sheetView.tvAddPatient.setOnClickListener(this)
                sheetView.tvAddTipPlan.setOnClickListener(this)
            }
        }
    }

    private fun changeTab(tab: Int) {
        viewpager.setCurrentItem(tab, false)
        ivHome.isSelected = tab == HOME
        tvHome.isSelected = tab == HOME
        ivPatients.isSelected = tab == PATIENTS
        tvPatients.isSelected = tab == PATIENTS
        ivWrite.isSelected = tab == WRITE
        tvWrite.isSelected = tab == WRITE
        ivAccount.isSelected = tab == ACCOUNT
        tvAccount.isSelected = tab == ACCOUNT
    }

    override fun onPageScrollStateChanged(state: Int) {

    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

    }

    override fun onPageSelected(position: Int) {

    }
}