package com.nizcaredoctor.views.splash

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import com.nizcaredoctor.R
import com.nizcaredoctor.utils.Constants
import com.nizcaredoctor.utils.PrefsManager
import com.nizcaredoctor.utils.getDate
import com.nizcaredoctor.views.home.HomeActivity
import com.nizcaredoctor.views.walkThrough.LoginSignUpActivity
import com.nizcaredoctor.webservice.model.Profile
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val styledString = SpannableString(getString(R.string.nizcare))
        styledString.setSpan(ForegroundColorSpan(Color.WHITE), 0, 2, 0)
        styledString.setSpan(ForegroundColorSpan(ContextCompat.getColor(this, R.color.yellow)), 3, getString(R.string.nizcare).length, 0)
        tvSplash.text = styledString

        Handler().postDelayed({
            if (PrefsManager.get().getString(Constants.Access_Token, "").isEmpty()) {
                startActivity(Intent(this@MainActivity, LoginSignUpActivity::class.java))
                finish()
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
            } else if (PrefsManager.get().getObject(Constants.USER_DETAIL, Profile::class.java)?.signup_step ?: 0 == 0) {
                startActivity(Intent(this@MainActivity, LoginSignUpActivity::class.java))
                finish()
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
            } else {
                startActivity(Intent(this@MainActivity, HomeActivity::class.java))
                finish()
            }
        }, 2000)
    }
}