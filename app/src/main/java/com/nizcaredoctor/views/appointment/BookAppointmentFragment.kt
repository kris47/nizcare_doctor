package com.nizcaredoctor.views.appointment

import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.nizcaredoctor.R
import com.nizcaredoctor.views.baseViews.BaseViewFragImpl
import kotlinx.android.synthetic.main.activity_book_appointment.*
import kotlinx.android.synthetic.main.layout_new_appointment.*
import kotlinx.android.synthetic.main.layout_toolbar.*

class BookAppointmentFragment : BaseViewFragImpl(), View.OnClickListener {
    private var isMorningVisible = true
    private var isAfternoonVisible = false
    private var isEveningVisible = false

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.tvFollowUp -> {
                tvNewAppointment.isSelected = false
                tvFollowUp.background = ContextCompat.getDrawable(context!!, R.drawable.button_green_right)
                tvNewAppointment.background = ContextCompat.getDrawable(context!!, R.drawable.button_white_left)
                tvFollowUp.setTextColor(ContextCompat.getColor(context!!, R.color.white))
                tvNewAppointment.setTextColor(ContextCompat.getColor(context!!, R.color.colorAccent))
                frameFollowUp.visibility = View.VISIBLE
                frameNewAppointment.visibility = View.GONE
            }
            R.id.tvNewAppointment -> {
                tvNewAppointment.isSelected = true
                tvFollowUp.setTextColor(ContextCompat.getColor(context!!, R.color.colorAccent))
                tvNewAppointment.setTextColor(ContextCompat.getColor(context!!, R.color.white))
                tvNewAppointment.background = ContextCompat.getDrawable(context!!, R.drawable.button_green_left)
                tvFollowUp.background = ContextCompat.getDrawable(context!!, R.drawable.button_white_right)
                frameFollowUp.visibility = View.GONE
                frameNewAppointment.visibility = View.VISIBLE
            }
            R.id.relMorning -> {
                if (isMorningVisible) {
                    isMorningVisible = false
                    rvMorningTimming.visibility = View.GONE
                } else {
                    isMorningVisible = true
                    rvMorningTimming.visibility = View.VISIBLE
                }
            }
            R.id.relAfternoon -> {
                if (isAfternoonVisible) {
                    isAfternoonVisible = false
                    rvAfternoonTimming.visibility = View.GONE
                } else {
                    isAfternoonVisible = true
                    rvAfternoonTimming.visibility = View.VISIBLE
                }
            }
            R.id.relEvening -> {
                if (isEveningVisible) {
                    isEveningVisible = false
                    rvEveningTimming.visibility = View.GONE
                } else {
                    isEveningVisible = true
                    rvEveningTimming.visibility = View.VISIBLE
                }
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.activity_book_appointment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        setListener()
    }

    fun init() {
        tvNewAppointment.isSelected = true
        tvTitle.text = getString(R.string.create_appointment)
        rvMorningTimming.adapter = SelectTimingAdapter(context!!, getTimeList(0))
        rvAfternoonTimming.adapter = SelectTimingAdapter(context!!, getTimeList(1))
        rvEveningTimming.adapter = SelectTimingAdapter(context!!, getTimeList(2))
    }

    fun setListener() {
        toolbar.setNavigationOnClickListener { activity?.supportFragmentManager?.popBackStack() }
        relMorning.setOnClickListener(this)
        relAfternoon.setOnClickListener(this)
        relEvening.setOnClickListener(this)
        tvNewAppointment.setOnClickListener(this)
        tvFollowUp.setOnClickListener(this)
    }

    fun getTimeList(type: Int): List<String> {
        val list = mutableListOf<String>()

        when (type) {
            0 -> {
                list.add("09:00 am")
                list.add("09:30 am")
                list.add("10:00 am")
                list.add("10:30 am")
                list.add("11:00 am")
                list.add("11:30 am")
            }
            1 -> {
                list.add("12:00 pm")
                list.add("12:30 pm")
                list.add("01:00 pm")
                list.add("01:30 pm")
                list.add("02:00 pm")
                list.add("02:30 pm")
                list.add("03:00 pm")
            }
            2 -> {
                list.add("03:30 pm")
                list.add("04:00 pm")
                list.add("04:30 pm")
                list.add("05:00 pm")
                list.add("05:30 pm")
                list.add("06:00 pm")
            }
        }

        return list
    }
}