package com.nizcaredoctor.views.appointment

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.nizcaredoctor.R
import kotlinx.android.synthetic.main.item_select_timming.view.*

class SelectTimingAdapter(val context: Context, val list: List<String>) : RecyclerView.Adapter<SelectTimingAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_select_timming, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind(list.get(position))
    }

    override fun getItemCount() = list.size

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        init {

        }

        override fun onClick(v: View?) {
            when (v?.id) {

            }
        }

        fun onBind(time: String) {
            itemView.tvTime.text = time
        }
    }
}