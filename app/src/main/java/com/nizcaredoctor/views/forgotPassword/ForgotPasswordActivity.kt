package com.nizcaredoctor.views.forgotPassword

import android.os.Bundle
import com.nizcaredoctor.R
import com.nizcaredoctor.utils.activityFinishAnimation
import com.nizcaredoctor.utils.replaceFragment
import com.nizcaredoctor.views.baseViews.BaseViewActivityImpl

class ForgotPasswordActivity : BaseViewActivityImpl() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)
        replaceFragment(supportFragmentManager,
                ForgotPasswordFragment(),
                "signUp")
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
        activityFinishAnimation()
    }
}