package com.nizcaredoctor.views.forgotPassword

import com.nizcaredoctor.views.baseViews.BasePresenter
import com.nizcaredoctor.views.baseViews.BaseView

interface ForgotPasswordContract {

    interface View : BaseView {
        fun forgotPasswordSuccess(token: String?, msg: String?)

    }

    interface Presenter : BasePresenter<View> {
        fun forgotPassword(map: HashMap<String, String>)

    }
}