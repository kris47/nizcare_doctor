package com.nizcaredoctor.views.forgotPassword

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.nizcaredoctor.R
import com.nizcaredoctor.utils.*
import com.nizcaredoctor.views.baseViews.BaseViewFragImpl
import com.nizcaredoctor.views.verification.VerificationFragment
import com.nizcaredoctor.webservice.WebConstants
import kotlinx.android.synthetic.main.fragment_forgot_password.*

class ForgotPasswordFragment : BaseViewFragImpl(), ForgotPasswordContract.View {

    private val presenter = ForgotPasswordPresenter()
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_forgot_password, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.attachView(this)
        back.setOnClickListener { activity?.onBackPressed() }
        tvSend.setOnClickListener {
            tilPhoneNumber.isErrorEnabled = false
            if (validatePhoneNumber()) {
                if (isOnline(activity)) {
                    val map = HashMap<String, String>()
                    map[WebConstants.LOCALE] = PrefsManager.get().getString(Constants.LOCALE, "en")
                    map[WebConstants.PHONENUMBER] = etPhoneNumber.text.trim().toString()
                    map[WebConstants.APPTYPE] = WebConstants.DOCTOR.toString()
                    presenter.forgotPassword(map)
                } else {
                    view.showInternetError()
                }
            }
        }
    }

    private fun validatePhoneNumber(): Boolean {

        return when {
            etPhoneNumber.text.trim().toString().isEmpty() -> {
                tilPhoneNumber.isErrorEnabled = true
                tilPhoneNumber.requestFocus()
                tilPhoneNumber.error = activity?.getString(R.string.enter_mobile_no)
                false
            }

            !isValidMobile(etPhoneNumber.text.trim().toString()) -> {
                tilPhoneNumber.isErrorEnabled = true
                tilPhoneNumber.requestFocus()
                tilPhoneNumber.error = activity?.getString(R.string.enter_valid_mobile_no)
                false
            }

            else -> true
        }
    }

    override fun forgotPasswordSuccess(token: String?, msg: String?) {
        if (token != null) {
            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()
            val bundle = Bundle()
            bundle.putString(WebConstants.TOKEN, token)
            bundle.putString(WebConstants.PHONENUMBER,etPhoneNumber.text.trim().toString())
            bundle.putString(Constants.VERIFICATIONTYPE, Constants.FORGOTPASSWORD)
            val fragment = VerificationFragment()
            fragment.arguments = bundle
            activity?.supportFragmentManager?.beginTransaction()?.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
                    ?.replace(R.id.container, fragment, "otp")?.addToBackStack("")?.commit()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        presenter.detachView()
    }
}