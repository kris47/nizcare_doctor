package com.nizcaredoctor.views.forgotPassword

import com.nizcaredoctor.views.baseViews.BasePresenterImpl
import com.nizcaredoctor.webservice.ApiResponseModel
import com.nizcaredoctor.webservice.RetrofitClient
import com.nizcaredoctor.webservice.model.OTPModel
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ForgotPasswordPresenter : BasePresenterImpl<ForgotPasswordContract.View>(), ForgotPasswordContract.Presenter {
    override fun forgotPassword(map: HashMap<String, String>) {

        getView()?.showProgress()
        RetrofitClient.getApi().forgotPassword(map).enqueue(object : Callback<ApiResponseModel<OTPModel>> {
            override fun onFailure(call: Call<ApiResponseModel<OTPModel>>?, t: Throwable?) {
                getView()?.displayFailure(t?.message)
                getView()?.dismissProgress()
            }

            override fun onResponse(call: Call<ApiResponseModel<OTPModel>>?, response: Response<ApiResponseModel<OTPModel>>?) {
                getView()?.dismissProgress()
                if (response != null) {
                    if (response.isSuccessful) {
                        getView()?.forgotPasswordSuccess(response.body()?.data?.token,response.body()?.msg)
                    } else {
                        try {
                            getView()?.displayError(JSONObject(response.errorBody()?.string()).getString("msg"), response.code())
                        } catch (e: Exception) {

                        }
                    }
                }
            }
        })
    }
}